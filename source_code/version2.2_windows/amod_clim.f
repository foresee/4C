!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                    climate data modules                       *!
!*                                                               *!
!*    containes:                                                 *!
!*    DATA_CLIMATE                                               *!
!*    DATA_EVAPO                                                 *!
!*    DATA_INTER                                                 *!
!*    DATA_DEPO                                                  *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under                   *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

module data_climate

! flag defines structure of climate data file 
  integer      :: flag_climtyp = 0
  integer                                 :: i_exit    ! day number of first missing data record
  integer,allocatable,save,dimension(:)   :: recs,yy
  integer,allocatable,save,dimension(:,:) :: dd,mm
  real,allocatable,save,dimension(:,:)    :: tp,hm,prc,prs,rd,wd, tx, tn,vp, sdu,bw, sde
  real,allocatable,save,dimension(:)      :: tpmean
  real :: airtemp     = -99.,   &  ! air temperature (�C)
          airtemp_1   = -99.,   &  ! air temperature of previous day (�C)
          airtemp_2   = -99.,   &  ! air temperature of two days before (�C)
          airtemp_max = -99.,   &  ! maximum air temperature (�C)
          airtemp_min = -99.,   &  ! minimum air temperature (�C)
          hum         = -99.,   &  ! humidity (%)
          prec        = -99.,   &  ! precipitation (mm)
          press       = -99.,   &  ! pressure  (hPa)
          rad         = -99.,   &  ! solar radiation(J/cm2)
          rad_max     = -99.,   &  ! maximal solar radiation(J/cm2)
          wind        = -99.,   &  ! wind velocity (m/s)
          par_day     = -99.,   &  ! photosynth. activ radiation (mol/m2)
          par_av      = -99.,   &  ! average of PAR for PS/NPP model (mol/m2)
          rnet_tot    = -99.,   &  ! total net radiation(J/cm2)
          avg_incl          ,   &  ! average sun inclination [degrees]
          beta              ,   &  ! average sun inclination [radians]
          dlength     = -99.,   &  ! day length == Photoperiode (h)
          dptemp      = -99.,   &  ! dew point temperature
          co2         = -99.       ! atmospheric CO2 content (mol/mol)
integer :: flag_vegper = 0         ! indicates vegetation period described by temeprature > 10�C
integer :: flag_tveg = 0  
integer :: iday_vegper = 0  
  
 ! cumulative and mean values per year
  real :: med_air           ! yearly mean air temperature
  real :: med_air_ms        ! average temperature May - September
  real :: med_air_mj        ! avarage temperature May - July
  real :: sum_prec          ! yearly precipitation sum
  real :: sum_prec_ms       ! precipitation sum May - September
  real :: sum_prec_mj       ! precipitation sum may - July
  real :: med_air_wm        ! average temperature of the warmest month
  real :: med_air_cm        ! average temperature of the coldest month
  real :: med_rad
  real :: med_rad1          ! annual mean of daily  solar radiation of the first year of simulation
  real :: med_wind
  real :: gdday             ! annual growing degree day

 ! values per month
  real, dimension(12) :: temp_mon   ! mean monthly average air temperature (�C)
  real, dimension(12) :: prec_mon   ! mean monthly precipitation sum (mm)
  real, dimension(12) :: rad_mon    ! mean monthly average of daily radiation (J/cm2)
  real, dimension(12) :: hum_mon    ! mean monthly average daily relative humidity (%)
  real :: aet_dec           ! sum of AET of last december (mm)
  real :: temp_dec          ! mean average air temperature of last december (�C)
  real :: prec_dec          ! precipitation sum of last december (mm)
  real :: rad_dec           ! mean average of daily radiation of last december (J/cm2)
  real :: hum_dec           ! mean average of daily relative humidity of last december (%)

 ! values per week
  real, dimension(53) :: temp_week  ! mean monthly average air temperature (�C)
  real, dimension(53) :: prec_week  ! mean monthly precipitation sum (mm)

! for calculation of long-term monthly means
  real, dimension(12)   :: tempmean_mo    ! long-term monthly means
  real, dimension(12)   :: tempmean_mo_a  ! annual monthly means 

  integer :: days_summer = -99,   &  ! number of summer days (Tmax > 25�C)
             days_hot    = -99,   &  ! number of hot days (Tmax > 30�C)
             days_ice    = -99,   &  ! number of ice days (Tmax < 0�C)
             days_dry    = -99,   &  ! number of days without precipitation
             days_hrain  = -99,   &  ! number of days with heavy rain (> 10mm)
             days_snow   = -99,   &  ! number of days with snow (4C simulation)
             days_rain   = -99,   &  ! number of days with rain > 0.1 mm
             days_rain_mj   = -99,   &  ! number of days with rain > 0.1 mm May - July
			 days_wof    = -99       ! number of days without frost Tmin > 0�C

  ! total mean values       
  real :: med_air_all       ! overall yearly mean air temperature
  real :: sum_prec_all      ! overall mean yearly precipitation sum
  real :: med_rad_all       ! overall mean annual radiation
  real :: gdday_all         ! overlall mean annual growing degree day

  ! monthly climate indices
  real :: ind_cout_mo       ! monthly index Coutange
  real :: ind_wiss_mo       ! monthly index v. Wissmann
  real :: ind_arid_mo       ! monthly Index UNEP
  real :: cwb_mo            ! monthly climate water balance 
  
  ! annual climate indices
  real :: ind_arid_an       ! annual aridity index UNEP
  real :: cwb_an            ! annual climate water balance
  real :: ind_lang_an       ! annual climate index acc. Linsser/Lang
  real :: ind_cout_an       ! annual index Coutange
  real :: ind_wiss_an       ! annual index v. Wissmann
  real :: ind_mart_an       ! annual index Martonne
  real :: ind_mart_vp       ! annual index martonne vegetation period (May- Sept.)
  real :: ind_emb           ! annual index Emberger
  real :: ind_weck          ! annual index Weck
  real :: ind_reich         ! annual index Reichel
  real :: con_gor           ! annual continentality index Gorczynski
  real :: con_cur           ! annual continentality index Currey
  real :: con_con           ! annual continentality index Conrad
  real :: ind_bud           ! annual dryness index Budyko
  real :: ind_shc           ! annual index Seljaninov  
  real :: cwb_an_m          ! mean annual climate water balance of simulation period

  ! meann annual climate inidces of the simulation period
  real :: ind_arid_an_m       ! annual aridity index UNEP
  real :: ind_lang_an_m       ! annual climate index acc. Linsser/Lang
  real :: ind_cout_an_m       ! annual index Coutange
  real :: ind_wiss_an_m       ! annual index v. Wissmann
  real :: ind_mart_an_m       ! annual index Martonne
  real :: ind_mart_vp_m       ! annual index martonne vegetation perio (May- Sept.)
  real :: ind_emb_m           ! annual index Emberger
  real :: ind_weck_m          ! annual index Weck
  real :: ind_reich_m         ! annual index Reichel
  real :: con_gor_m           ! annual continentality index Gorczynski
  real :: con_cur_m           ! annual continentality index Currey
  real :: con_con_m           ! annual continentality index Conrad
  real :: ind_bud_m           ! annual dryness index Budyko
  real :: ind_shc_m           ! annual index Seljaninov    
 
  ! values for evaluation of npp module
  real,allocatable,save,dimension(:) :: tempfield
  real,allocatable,save,dimension(:) :: globfield
  real,allocatable,save,dimension(:) :: dayfield
  real, dimension(5) :: clim_waterb = 0. ! climatic water balance (fire_risk)

  ! Mauna Loa CO2 time series, annual means
  REAL :: year_CO2 = 2016
  REAL :: Mauna_Loa_CO2(1959:2016)  ! time series of annual mean CO2 measured at Mauna Loa, Hawaii
  Real :: RCP_2p6(1765:2300)
  Real :: RCP_6p0(1765:2150)

  DATA Mauna_Loa_CO2 /0.00031598, 0.00031691, 0.00031765, 0.00031845, 0.00031899, &
                      0.00031952, 0.00032003, 0.00032137, 0.00032218, 0.00032305, &
                      0.00032462, 0.00032568, 0.00032632, 0.00032746, 0.00032968, &
                      0.00033025, 0.00033115, 0.00033215, 0.0003339,  0.0003355,  &
                      0.00033685, 0.00033869, 0.00033993, 0.00034113, 0.00034278, &
                      0.00034442, 0.0003459,  0.00034715, 0.00034893, 0.00035148, &
                      0.00035291, 0.00035419, 0.00035559, 0.00035637, 0.00035704, &
                      0.00035888, 0.00036088, 0.00036264, 0.00036376, 0.00036663, &
                      0.00036831, 0.00036948, 0.00037102, 0.0003731,  0.00037564, &
                      0.00037738, 0.00037975, 0.00038185, 0.00038372, 0.00038557, &
                      0.00038738, 0.00038985, 0.00039163, 0.00039382, 0.00039648, &
                      0.00039861, 0.00040083, 0.00040421/
  DATA RCP_2p6/278.05,278.11,278.22,278.34,278.47,278.60,278.73,278.87,279.01,279.15,279.30,279.46,279.62,279.78,279.94,280.10,280.24,280.38,280.52,280.66,&
   280.80,280.96,281.12,281.28,281.44,281.60,281.75,281.89,282.03,282.17,282.30,282.43,282.55,282.67,282.79,282.90,283.01,283.11,283.21,283.31,283.40,283.49,&
   283.58,283.66,283.74,283.80,283.85,283.89,283.93,283.96,284.00,284.04,284.09,284.13,284.17,284.20,284.22,284.24,284.26,284.28,284.30,284.32,284.34,284.36,&
   284.38,284.40,284.39,284.28,284.13,283.98,283.83,283.68,283.53,283.43,283.40,283.40,283.43,283.50,283.60,283.73,283.90,284.08,284.23,284.40,284.58,284.73,&
   284.88,285.00,285.13,285.28,285.43,285.58,285.73,285.90,286.08,286.23,286.38,286.50,286.63,286.78,286.90,287.00,287.10,287.23,287.38,287.53,287.70,287.90,&
   288.13,288.40,288.70,289.03,289.40,289.80,290.23,290.70,291.20,291.68,292.13,292.58,292.98,293.30,293.58,293.80,294.00,294.18,294.33,294.48,294.60,294.70,&
   294.80,294.90,295.03,295.23,295.50,295.80,296.13,296.48,296.83,297.20,297.63,298.08,298.50,298.90,299.30,299.70,300.08,300.43,300.78,301.10,301.40,301.73,&
   302.08,302.40,302.70,303.03,303.40,303.78,304.13,304.53,304.98,305.40,305.83,306.30,306.78,307.23,307.70,308.18,308.60,309.00,309.40,309.75,310.00,310.18,&
   310.30,310.38,310.38,310.30,310.20,310.13,310.10,310.13,310.20,310.33,310.50,310.75,311.10,311.50,311.93,312.43,313.00,313.60,314.23,314.85,315.50,316.27,&
   317.08,317.80,318.40,318.93,319.65,320.65,321.61,322.64,323.90,324.99,325.86,327.14,328.68,329.74,330.59,331.75,333.27,334.85,336.53,338.36,339.73,340.79,&
   342.20,343.78,345.28,346.80,348.65,350.74,352.49,353.86,355.02,355.89,356.78,358.13,359.84,361.46,363.16,365.32,367.35,368.87,370.47,372.52,374.76,376.81,&
   378.81,380.83,382.78,384.80,387.00,389.29,391.56,393.84,396.12,398.40,400.68,402.97,405.25,407.53,409.80,412.07,414.33,416.52,418.60,420.60,422.52,424.35,&
   426.10,427.75,429.31,430.78,432.16,433.44,434.59,435.65,436.63,437.52,438.33,439.06,439.69,440.22,440.66,441.02,441.35,441.62,441.86,442.08,442.28,442.46,&
   442.60,442.70,442.75,442.76,442.73,442.66,442.55,442.41,442.25,442.08,441.89,441.67,441.42,441.13,440.80,440.43,440.01,439.54,439.05,438.54,438.02,437.48,&
   436.92,436.34,435.76,435.18,434.60,434.00,433.38,432.78,432.19,431.62,431.06,430.51,429.96,429.41,428.86,428.30,427.73,427.14,426.57,426.00,425.46,424.94,&
   424.43,423.93,423.43,422.93,422.43,421.92,421.40,420.90,420.41,419.95,419.50,419.06,418.62,418.17,417.71,417.25,416.77,416.28,415.80,415.32,414.87,414.42,&
   413.99,413.55,413.10,412.64,412.17,411.69,411.19,410.70,410.22,409.76,409.31,408.86,408.42,407.97,407.52,407.07,406.61,406.15,405.69,405.26,404.84,404.45,&
   404.07,403.68,403.29,402.90,402.51,402.10,401.69,401.28,400.89,400.53,400.17,399.83,399.49,399.14,398.79,398.43,398.06,397.68,397.31,396.96,396.62,396.30,&
   395.99,395.68,395.36,395.03,394.70,394.36,394.00,393.66,393.33,393.02,392.72,392.44,392.14,391.85,391.54,391.23,390.91,390.58,390.26,389.95,389.65,389.38,&
   389.11,388.83,388.55,388.27,387.97,387.67,387.35,387.05,386.75,386.48,386.21,385.96,385.70,385.44,385.16,384.88,384.59,384.29,384.00,383.72,383.45,383.20,&
   382.96,382.72,382.46,382.20,381.93,381.66,381.37,381.08,380.81,380.56,380.32,380.09,379.86,379.61,379.36,379.11,378.84,378.56,378.29,378.03,377.78,377.56, &
   377.33,377.11,376.87,376.63,376.38,376.12,375.85,375.59,375.34,375.11,374.89,374.67,374.45,374.23,373.99,373.75,373.50,373.24,372.98,372.74,372.52,372.30,&
   372.10,371.89,371.67,371.44,371.21,370.96,370.71,370.46,370.23,370.01,369.80,369.60,369.40,369.19,368.97,368.74,368.50,368.26,368.02,367.79,367.58,367.38,&
   367.18,366.98,366.78,366.57,366.35,366.11,365.87,365.64,365.42,365.21,365.02,364.83,364.64,364.44,364.23,364.02,363.79,363.55,363.32,363.11,362.91,362.72,&
   362.54,362.35,362.16,361.95,361.75,361.52,361.29,361.07,360.86,360.67/
  
 DATA RCP_6p0/ 278.05,278.11,278.22,278.34,278.47,278.60,278.73,278.87,279.01,279.15,279.30,279.46,279.62,279.78,279.94,280.10,280.24,280.38,280.52,280.66,&
  280.80,280.96,281.12,281.28,281.44,281.60,281.75,281.89,282.03,282.17,282.30,282.43,282.55,282.67,282.79,282.90,283.01,283.11,283.21,283.31,283.40,283.49,&
  283.58,283.66,283.74,283.80,283.85,283.89,283.93,283.96,284.00,284.04,284.09,284.13,284.17,284.20,284.22,284.24,284.26,284.28,284.30,284.32,284.34,284.36,&
  284.38,284.40,284.39,284.28,284.13,283.98,283.83,283.68,283.53,283.43,283.40,283.40,283.43,283.50,283.60,283.73,283.90,284.08,284.23,284.40,284.58,284.73,&
  284.88,285.00,285.13,285.28,285.43,285.58,285.73,285.90,286.08,286.23,286.38,286.50,286.63,286.78,286.90,287.00,287.10,287.23,287.38,287.53,287.70,287.90,&
  288.13,288.40,288.70,289.03,289.40,289.80,290.23,290.70,291.20,291.68,292.13,292.58,292.98,293.30,293.58,293.80,294.00,294.18,294.33,294.48,294.60,294.70,&
  294.80,294.90,295.03,295.23,295.50,295.80,296.13,296.48,296.83,297.20,297.63,298.08,298.50,298.90,299.30,299.70,300.08,300.43,300.78,301.10,301.40,301.73,&
  302.08,302.40,302.70,303.03,303.40,303.78,304.13,304.53,304.98,305.40,305.83,306.30,306.78,307.23,307.70,308.18,308.60,309.00,309.40,309.75,310.00,310.18,&
  310.30,310.38,310.38,310.30,310.20,310.13,310.10,310.13,310.20,310.33,310.50,310.75,311.10,311.50,311.93,312.43,313.00,313.60,314.23,314.85,315.50,316.27,&
  317.08,317.80,318.40,318.93,319.65,320.65,321.61,322.64,323.90,324.99,325.86,327.14,328.68,329.74,330.59,331.75,333.27,334.85,336.53,338.36,339.73,340.79,&
  342.20,343.78,345.28,346.80,348.65,350.74,352.49,353.86,355.02,355.89,356.78,358.13,359.84,361.46,363.16,365.32,367.35,368.87,370.47,372.52,374.76,376.81,&
  378.81,380.83,382.78,384.80,386.93,389.07,391.17,393.24,395.30,397.35,399.39,401.42,403.43,405.43,407.40,409.36,411.30,413.22,415.14,417.08,419.04,421.00,&
  422.98,424.95,426.92,428.88,430.83,432.81,434.83,436.92,439.07,441.29,443.57,445.90,448.28,450.70,453.15,455.65,458.18,460.76,463.41,466.12,468.91,471.77,&
  474.69,477.67,480.70,483.78,486.92,490.10,493.34,496.64,500.02,503.48,507.02,510.63,514.31,518.03,521.80,525.62,529.49,533.40,537.38,541.44,545.59,549.82,&
  554.13,558.49,562.87,567.27,571.70,576.15,580.61,585.10,589.65,594.26,598.92,603.54,608.02,612.36,616.57,620.65,624.58,628.38,632.06,635.65,639.14,642.60,&
  646.06,649.52,652.95,656.36,659.75,663.11,666.42,669.72,673.02,676.29,679.50,682.65,685.71,688.69,691.59,694.40,697.11,699.73,702.28,704.76,707.20,709.60,&
  711.93,714.21,716.40,718.52,720.56,722.51,724.37,726.16,727.90,729.59,731.24,732.85,734.39,735.86,737.26,738.59,739.83,740.99,742.08,743.12,744.13,745.10,&
  746.02,746.88,747.68,748.40,749.05,749.62,750.09,750.51,750.87,751.20,751.49,751.74,751.92,752.00 /

end module data_climate
    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
module data_evapo
  ! evapotranspiration data
  real :: aet       = 0.	! daily total actual evapotranspiration / mm
  real :: aet_cum   = 0.	! yearly total actual evapotranspiration / mm
  real :: aet_m     = 0.	! mean yearly total actual evapotranspiration / mm
  real :: pet       = 0.	! daily total potential evapotranspiration / mm	
  real :: pet_cum   = 0.    ! yearly total potential evapotranspiration / mm
  real :: pet_m     = 0.    ! mean yearly total potential evapotranspiration / mm
  real :: pev_s     = 0.    ! potential evaporation of soil / mm
  real :: pev_sn    = 0.    ! potential evaporation of snow / mm
  real :: dew_rime  = 0.    ! dew or rime resp. / mm 
  real :: dew_cum   = 0.    ! yearly total dew or rime resp. / mm 
  real :: dew_m     = 0.    ! mean yearly total dew or rime resp. / mm 
  real :: trans_dem = 0.    ! potential transpiration / mm
  real :: trans_tree= 0.    ! actual transpiration of trees / mm
  real :: trans_sveg= 0.    ! actual transpiration of ground vegetation / mm
  real :: tra_tr_cum= 0.    ! yearly transpiration of trees / mm
  real :: tra_sv_cum= 0.    ! yearly transpiration of ground vegetation / mm
  real :: aev_s     = 0.    ! actual evaporation of soil / mm
  real :: aev_i     = 0. 	! actual evaporation of intercepted water / mm
  real :: demand_mistletoe_cohort = 0.   ! helping variable: transfer of mistletoe demand from evapo.f to soil.f

  REAL, dimension(12) :: aet_mon    ! monthly actual evapotranspiration sum / mm
  REAL, dimension(53) :: aet_week   ! weekly actual evapotranspiration sum / mm
  REAL, dimension(12) :: pet_mon    ! monthly potential evapotranspiration sum / mm
  REAL, dimension(53) :: pet_week   ! weekly potential evapotranspiration sum / mm
  
  real :: Rnet_cum  = 0.    ! net radiation J/cm�
  integer:: unit_eva

end module data_evapo
    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
module data_inter
  ! interception data
  real   :: interc_can   = 0.      ! total daily canopy interception / mm
  real   :: int_st_can   = 0.      ! canopy interception storage / mm
  real   :: int_cum_can  = 0.      ! cumulative canopy interception / mm
  real   :: interc_m_can = 0.      ! mean yearly canopy interception / mm
  real   :: prec_stand   = 0.      ! stand precipitation / mm	
  real   :: prec_stand_red= 0.     ! reduction of stand precipitation by percentage (drought experiments) / %	
  real   :: interc_sveg  = 0.      ! total daily interception of  ground vegetation / mm
  real   :: int_st_sveg  = 0.      ! interception storage of  ground vegetation / mm
  real   :: int_cum_sveg = 0.      ! cumulative interception of  ground vegetation / mm
  real   :: interc_m_sveg= 0.      ! mean yearly interception of  ground vegetation / mm
  real   :: stem_flow    = 0.	   ! stem flow / mm
  logical:: lint_snow    = .false. ! interception of snow = .true.

end module data_inter
    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
module data_depo
  ! deposition data
  real,allocatable,save,dimension(:,:)    :: NHd, NOd  ! input fields / mg N/m2
  real :: NH_dep  = 0.      ! deposition of NHx-N / g N/m2
  real :: NO_dep  = 0.      ! deposition of NOx-N / g N/m2
  real :: Ndep_cum = 0.     ! yearly cumulative deposition / g N/m2
  real :: Ndep_cum_all = 0. ! overall mean yearly deposition / g N/m2
end module data_depo



