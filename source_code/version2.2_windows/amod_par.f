!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                                                               *!
!*data module for a variety of parameters (non-species dependent)*!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!
module  data_par

! from npp.f:
  real :: pi = 3.1415926536  ! PI
  real :: zero = 1.E-6       ! numerical zero
  REAL :: lambda = 0.7 ,     & ! optimum ratio of ci to ca [-]
          Cmass = 12.0 ,     & ! molar mass of carbon [g/mol]
          gmin = 0.0 ,       & ! minimum conductance [mol/(m2*d)]
          ps = 0.7 ,         & ! shape of PS response curve
          pn = 0.025 ,       & ! slope of N function (eqn 27) at 20 �C [g(N) (mymol s-1)-1]
          nc0 = 0.00715 ,    & ! minimum N content [g/g] (eqn 27)
          qco2 = 0.08 ,      & ! C3 quantum efficiency (eqn 16)
          qco2a = 1.0 ,      & ! scaling parameter (eqn A7)
          o2 = 20.9 ,        & ! partial pressure of oxygen (kPa)
		  co2_st= 0.00035,   & ! atmospheric CO2 content (mol/mol)
          pfref = 0.2 ,      & ! albedo of the canopy
          cpart = 0.5 ,      & ! part of C in biomass [-]
          rmolw = 0.622 ,    & ! ratio of molecular weights of water and air
          R_gas = 8.314 ,    & ! universal gas constant [J/mol/K] = [Pa/m3/K]
          c_karman = 0.41 ,  & ! von Karman's constant [-]
          c_air    = 1.005 , & ! specific heat of air at const. pressure [J/g/K]
          psycro =0.000662 , & ! psychrometer constant [hPa/K]
          h_breast =137 ,    & ! breast height for inventory measurements [cm]
          h_sapini = 200 ,   & ! height below which tree is initialised with sapling allometry
          h_bo_br_diff = 50, & ! minimal difference between height of crown base and breast height
          Q10_T = 2.           ! used for calculation of dayfract from air temperature          

 DOUBLE PRECISION ::        p0_co2 ,           & ! parameter variable for calculation of CO2 scenarios
          p1_co2 ,           & ! parameter variable for calculation of CO2 scenarios
          p2_co2 ,           & ! parameter variable for calculation of CO2 scenarios
          p3_co2 ,           & ! parameter variable for calculation of CO2 scenarios
          p4_co2 ,           & ! parameter variable for calculation of CO2 scenarios
          p1_co2h ,          & ! parameter variable for calculation of historical CO2 scenarios
          p2_co2h ,          & ! parameter variable for calculation of historical CO2 scenarios
          p3_co2h ,          & ! parameter variable for calculation of historical CO2 scenarios
          p4_co2h ,          & ! parameter variable for calculation of historical CO2 scenarios
          p5_co2               ! parameter variable for calculation of CO2 scenarios

! Transformation coefficients
   REAL :: gm2_in_kgha = 10.  ! transf. coeff. from g/m2 in kg/ha
   REAL :: kgha_in_gm2 = 0.1  ! transf. coeff. from kg/ha in g/m2
   REAL :: gm2_in_tha = 0.01  ! transf. coeff. from g/m2 in t/ha
   REAL :: tha_in_gm2 = 100.  ! transf. coeff. from t/ha in g/m2
   REAL :: kg_in_g = 1000.    ! transf. coeff. from kg in g
   REAL :: GR_in_PAR = 0.5*4.6/100. ! from global rad. in J/cm2 to PAR in mol/m2
                                    ! explanation of conversion factor:
                                    ! 0.5: PAR is 50% of incident radiation
                                    ! 4.6: 1 J = 4.6e-6 mol (Larcher 1995);
                                    ! 100: conversion J/cm2 -> MJ/m2

! soil parameter
   real :: dens_om = 1.4  ! specific density of organic matter g/cm3

! parameter for snow
   real :: temp_snow = 0.2    ! threshold of air temperature for snow accumulation

! parameter for calculation of potential evapotranspiration rate
real :: alpha_PT = 1.26 ! Priestley-Taylor coefficient

! parameter for calculation of transpiration demand
real :: alfm  = 1.4    
real :: gpmax = 14000. ! mol/(m2*d)  

! parameter for growing degree day calculation
   real :: thr_gdd = 5.

! van Genuchten parameter for flag_wred=9
   real :: l_gnu = 0.5
! fol biomass per mistletoe [kg DW/tree], 1 Viscum (10years) see Pfiz 2010
real ::  mistletoe_x_fol = 0.0158


! set of characters
character(len=*), parameter :: charset = &
   "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-_"

end module data_par
