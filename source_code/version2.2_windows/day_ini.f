!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                                                               *!
!*                    Subroutine DAY_INI for:                    *!
!*                                                               *!
!*          allocation of daily weather variables                *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

SUBROUTINE day_ini

USE data_biodiv
USE data_climate
USE data_depo
USE data_evapo
USE data_simul
USE data_site
USE data_stand
USE data_par

implicit none
type(Coh_Obj), pointer :: p  ! pointer to cohort list
real, external :: photoper
real, external :: daylength
integer i, j

j = time
i = iday

airtemp     = tp(i,j)+deltaT
airtemp_1   = tp(i-1,j)+deltaT
airtemp_2   = tp(i-2,j)+deltaT
airtemp_max = tx(i,j)
airtemp_min = tn(i,j)
prec      = prc(i,j)*deltaPrec
hum         = hm(i,j)
if (hum .le. 0.) then
   hum = 1.
else if (hum .gt. 100.) then
   hum = 100.
endif
if (prs(i,j) .gt. 0.) then
    press = prs(i,j)
else
    press = 1013.
endif

rad       = rd(i,j)
wind      = wd(i,j)
if (wind .lt. 0.) wind = 0.5

dlength = photoper(i+0.,xlat)
med_air  = med_air + airtemp
sum_prec = sum_prec + prec

if(recs(time).eq.365) then
  if(i.gt.120 .and. i.lt.274) then
     med_air_ms = med_air_ms + airtemp
	 sum_prec_ms = sum_prec_ms + prec
  end if
  if(i.gt.120 .and. i .lt. 213)  then
         med_air_mj = med_air_mj + airtemp
		 sum_prec_mj = sum_prec_mj + prec
  end if
else
   if(i.gt.121 .and. i.lt.275) then
     med_air_ms = med_air_ms + airtemp
	 sum_prec_ms = sum_prec_ms + prec
     if(i.gt.121 .and. i .lt.214)  then
	      med_air_mj = med_air_mj + airtemp
		  sum_prec_mj = sum_prec_mj + prec
	 end if
  end if
end if
 
med_rad  = med_rad + rad
med_wind = med_wind + wind
if (airtemp.gt. thr_gdd) then
    gdday = gdday +  airtemp
    gdday_all = gdday_all + airtemp
end if
if (airtemp_max .ge. 25.) then
    days_summer = days_summer + 1
    if (airtemp_max .ge. 30.) then
        days_hot = days_hot + 1
    endif
endif
if( airtemp_min .gt. 0) days_wof = days_wof +1
if ((airtemp_max .lt. 0.) .and. (airtemp_max .gt. -90.)) then
    days_ice = days_ice + 1
endif
if (prec .lt. 1.E-06) then
    days_dry = days_dry + 1
else if (prec .gt. 10.) then
    days_hrain = days_hrain + 1
else if (prec .gt. 0.1) then
     days_rain = days_rain +1
	 if(recs(time).eq.365) then
           if(i.gt.120 .and. i .lt. 213) days_rain_mj = days_rain_mj +1
	 else
	        if(i.gt.121 .and. i .lt.214) days_rain_mj = days_rain_mj +1
	 end if  
endif

drIndd = 0.

lightsum = lightsum + rad/100                  ! sum global radiation in mJ/m2
abslightsum = abslightsum + rad/100*totFPARsum ! sum absorbed global radiation in mJ/m2

! set standardised deposition data for areal application of deposition:
NO_dep = NOd(i,j)*0.001       ! mg N/m2 ==> g N/m2
NH_dep = NHd(i,j)*0.001       ! mg N/m2 ==> g N/m2

pev_sn   = 0.
dew_rime = 0.

fire_indw = -99
fire_inde = -99

! water and N uptake
p => pt%first
do while (associated(p))
  p%coh%supply = 0.
  p%coh%Nuptc_d = 0.
  p => p%next
enddo  ! p (cohorts)
END SUBROUTINE day_ini
