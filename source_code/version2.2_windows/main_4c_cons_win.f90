!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                                                               *!
!*                    Subroutines for:                           *!
!*                   - windows shell -                           *!
!*                                                               *!
!*  contains:                                                    *!
!*     main program for 4C                                       *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/XXXXXXXXXXXXXXXXXXXXX     *!
!*                                                               *!
!*****************************************************************!
    
    PROGRAM foresee

    USE data_simul
	
	real time1, time2

	call CPU_time (time1)
    call Act_Dir(actDir)

	CALL topmenu_win
    call CPU_time (time2)
    
    print *, ' 4C total run time ', time2-time1, ' sec'

	END PROGRAM foresee

