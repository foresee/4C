!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                                                               *!
!*                    Subroutines for:                           *!
!*                                                               *!
!* Contains the following subroutines:                           *!
!*                                                               *!
!* PREPARE_GLOBAL: general preparation of simulation             *!
!*                 contains internal subroutines:                *!
!*                 TOPMENU: main menu                            *!
!*                 EDITSIM: edit simulation file names           *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

SUBROUTINE prepare_global

use data_simul
use data_out
use data_species
use data_stand
use data_site
use data_tsort
use data_climate

implicit none

character  anf
logical ex

! main menu

DO
  call topmenu

 if (anf == '2') then

    call editsim
    call testfile(simfile,ex)
    if(ex .eqv. .false.) cycle
    call readsim
     if (flag_mult910) print *,'  Check the data from file:'

  ELSE if (anf == '1' .or. anf == ' ') then

    call testfile(simfile,ex)
    if(ex .eqv. .false.) cycle
    call readsim
     if (flag_mult910) print *,'  Check the data from file:'

  end if

  if (ex .eqv. .true.) exit

end DO

call outtest

contains

!---------------------------------------------------------------

SUBROUTINE topmenu

print *,'  ****************************************************'
print *,'  ****************  Welcome to 4C  *******************'
print *,'  ****************************************************'
print *,' '
print *,'  1 <Enter>.. Start with default simulation control: ',trim(simfile)
print *,' '
print *,'  2...........Edit simulation control file name'
print *,' '
print *,'  ****************************************************'
write(*,'(A)',advance='no') '  Make your choice: '
read(*,'(A)') anf

END subroutine topmenu

!-----------------------------------------------------------------------------

SUBROUTINE editsim

     open(1000,file='user')

    write(1000,'(A,A,A)',advance='no') ' Simulation control file (default= ',trim(simfile),'): '
    read (*,'(A)') simfile
    if (simfile == ' ') then
        simfile = 'test0.sim'
    end if

end subroutine editsim

!-----------------------------------------------------------------------------

END subroutine prepare_global

!**************************************************************


