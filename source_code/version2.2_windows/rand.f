!*****************************************************************!
!*                                                               *!
!*                4C (FORSEE) Simulation Model                   *!
!*                                                               *!
!*                                                               *!
!*              Function:                                        *!
!* Algorithm as described in APPL. STATIST. 31:2 (1982)          *!
!*      The function returns a pseudo-random number uniformly    *!
!*      distributed between 0 and 1.                             *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!
REAL FUNCTION RAND()

      INTEGER IE,IM,IG

! IE, IM and IG should be set to integer values between
! 1 and 30000 before the first entry.

      COMMON /RANDOM/ IE,IM,IG
      IE=171*MOD(IE,177)-2* (IE/177)
      IM=172*MOD(IM,176)-35*(IM/176)
      IG=170*MOD(IG,178)-63*(IG/178)

      IF (IE.LT.0)IE=IE+30269
      IF (IM.LT.0)IM=IM+30307
      IF (IG.LT.0)IG=IG+30323

      RAND = AMOD(FLOAT(IE) /30269.0+FLOAT(IM)/30307.0+ FLOAT(IG) /30323.0,1.0)
               
      RETURN

END function rand
