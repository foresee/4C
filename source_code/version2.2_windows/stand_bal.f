!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                    Subroutines for:                           *!
!*    - STAND_BALANCE: Recalculation of stand variables          *!
!*      contains:                                                *!
!*               UPDATE_AGE                                      *!
!*    - STAND_BAL_SPEC                                           *!
!*    - CLASS                                                    *!
!*    - CLASST                                                   *!
!*    - CLASS_MAN                                                *!
!*    - CALC_HEIDOM                                              *!
!*    - MAX_HEIGHT(nrmax,anz,cohl)                               *!
!*    - STANDUP: Update of cover and ceppot                      *!
!*    - LITTER: Summation variables of litter fractions          *!
!*    - calc_ind_rep: calculation of representation index        *!
!*    - overstorey                                               *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!
subroutine stand_balance
use data_species
use data_stand
use data_climate
use data_simul
use data_manag
use data_out
use data_par

implicit none

integer i, ntr, nd, hanz
integer, dimension(nspecies) :: helpin, helpout
real conv     ! conversion factor

if (flag_trace) write (unit_trace, '(I4,I10,A)') iday, time, ' stand_balance'

if(time>0. .and. flag_standup.ne.2) call update_age

! calculation of total dead biomass per cohort and total biomass of allcohorts
! calc. of ceppot
anz_sveg     = 0
anz_tree     = 0.
anz_tree_in  = 0.
anz_tree_out = 0.
anz_spec_in  = 0.
anz_spec_out = 0.
anz_coh_in   = 0.
anz_coh_out  = 0.
anz_coh_act  = 0.
lai_in       = 0.
lai_out      = 0.
totfol_in    = 0.
totfol_out   = 0.
med_diam_in  = 0.
med_diam_out = 0.
hmean_in     = 0.
hmean_out    = 0.
mean_height  = 0.
sumbio       = 0.
sumbio_in    = 0.
sumbio_out   = 0.
sumNPP       = 0.
drIndAl      = 0.
Ndem         = 0.
helpin       = 0
helpout      = 0
basal_area  = 0. 
totstem_m3 = 0.
totsteminc_m3 = 0.
totsteminc = 0
autresp    = 0.
totfol     = 0.
totsap     = 0.
totfrt     = 0.
totfrt_p   = 0.
totcrt     = 0.
tottb      = 0.
tothrt     = 0.
sumbio_sv  = 0.

zeig=>pt%first
do
    if(.not.associated(zeig)) exit
    
    ns  = zeig%coh%species
    ntr = zeig%coh%ntreeA
	svar(ns)%daybb = zeig%coh%day_bb
	if(ns.le.nspec_tree) then
        if(zeig%coh%ident .le. coh_ident_max) then
           anz_coh_act = anz_coh_act + 1
           anz_tree    = anz_tree + ntr
           zeig%coh%totBio = zeig%coh%x_fol + zeig%coh%x_sap + zeig%coh%x_hrt + zeig%coh%x_tb + zeig%coh%x_frt +zeig%coh%x_crt 
           zeig%coh%Dbio  = zeig%coh%nTreeD * zeig%coh%totBio
            sumbio  = sumbio  + ntr * zeig%coh%totBio
            sumNPP  = sumNPP  + ntr * zeig%coh%NPP
            Ndem    = Ndem + ntr * zeig%coh%Ndemc_c
            select case (flag_dis)
	         case (0,1)
             autresp = autresp + ntr * zeig%coh%maintres
             case (2)
             autresp = autresp + ntr * (zeig%coh%maintres+zeig%coh%biocost_all)
            end select
            totfol  = totfol  + ntr * zeig%coh%x_fol
            totsap  = totsap  + ntr * zeig%coh%x_sap
            totfrt  = totfrt  + ntr * zeig%coh%x_frt
            totcrt  = totcrt  + ntr * zeig%coh%x_crt
            tottb   = tottb   + ntr * zeig%coh%x_tb
            tothrt  = tothrt  + ntr * zeig%coh%x_hrt
            if (zeig%coh%height.le.thr_height) then
                seedlfrt = seedlfrt + zeig%coh%x_frt * ntr
            endif
            totstem_m3 = totstem_m3 + (ntr*zeig%coh%x_sap + ntr*zeig%coh%x_hrt)   &
                       /(spar(ns)%prhos*1000000)             ! conversion kg/patch ---m³/ha

            nd = zeig%coh%nDaysGr
            if (nd .gt. 0) drIndAl = drIndAl + ntr * zeig%coh%drIndAl * zeig%coh%NPP / nd

        endif

        if(zeig%coh%ident > coh_ident_max) then
            anz_tree_in = anz_tree_in + ntr    
            sumbio_in   = sumbio_in + ntr * zeig%coh%totBio
            anz_coh_in  = anz_coh_in + 1
            helpin(ns)  = ns
            lai_in      = lai_in + ntr * zeig%coh%t_leaf/kpatchsize
            totfol_in   = totfol_in + ntr * zeig%coh%x_fol
            med_diam_in = med_diam_in + ntr * (zeig%coh%diam**2)
            hmean_in    = hmean_in + ntr * zeig%coh%height
            totfrt      = totfrt  + ntr * zeig%coh%x_frt    
       endif

        if((zeig%coh%nTreeD > 0.1) .or. (zeig%coh%nTreeM > 0.1) .or. (zeig%coh%nTreet > 0.1)) then
            hanz         = zeig%coh%nTreeD + zeig%coh%nTreeM + zeig%coh%nTreet
            anz_tree_out = anz_tree_out + hanz    
            sumbio_out   = sumbio_out + hanz * zeig%coh%totBio
            sumNPP       = sumNPP  + hanz * zeig%coh%NPP    ! eliminated (died or harvested) trees produce during the year as well;
            autresp      = autresp + hanz * zeig%coh%maintres
            anz_coh_out  = anz_coh_out + 1
            helpout(ns)  = ns
            lai_out      = lai_out + hanz * zeig%coh%t_leaf/kpatchsize
            totfol_out   = totfol_out + hanz * zeig%coh%x_fol
            med_diam_out = med_diam_out + hanz * (zeig%coh%diam**2)
            hmean_out    = hmean_out + hanz * zeig%coh%height
        endif

	else
       ntr = zeig%coh%ntreeA
	   anz_sveg = anz_sveg +1
	   zeig%coh%totBio = zeig%coh%x_fol + (1.+spar(ns)%alphac)*(zeig%coh%x_sap + zeig%coh%x_hrt) + zeig%coh%x_frt
       sumbio_sv = sumbio_sv + ntr * zeig%coh%totBio
       totfrt_p  = totfrt_p  + ntr * zeig%coh%x_frt
   end if   !only trees
    zeig=>zeig%next
end do

   if (flag_cumNPP .eq. 1) then

   cum_sumNPP  = cum_sumNPP + sumNPP

   flag_cumNPP = 0

   endif


   if (sumNPP .gt. 1E-06) drIndAl    = drIndAl / sumNPP

!  conversion kg/patch ---> kg/ha; N/patch ---> N/ha
   conv = 10000./kpatchsize

   totfrt_p = totfrt_p + totfrt    ! Rootmass f. patch (trees and soil veg.) save before conversion; Wurzelmenge vor Umrechnung sichern
   if (totfrt_p .gt. zero) then     
       totfrt_1 = 1./totfrt_p      ! reciprocal for later calculationshKehrwert f. spaetere Berechnungen 
   else
       totfrt_1 = 0.
   endif
   totfrt       = totfrt * conv
   totfol       = totfol * conv
   totfol_in    = totfol_in * conv
   totfol_out   = totfol_out * conv
   tottb        = tottb * conv
   totsap       = totsap * conv
   tothrt       = tothrt * conv
   totcrt       = totcrt * conv
   sumbio       = sumbio * conv
   sumbio_in    = sumbio_in * conv
   sumbio_out   = sumbio_out * conv
   sumbio_sv    = sumbio_sv * conv
   Ndem         = Ndem / kpatchsize       ! g per tree --> g/m2
   totstem_m3   = totstem_m3* conv      ! m3/ha
   anz_tree_ha  = anz_tree * conv
   anz_tree_in  = anz_tree_in * conv
   anz_tree_out = anz_tree_out * conv

   do i=1, nspec_tree+1       ! for all but mistletoe
    if (helpin(i) > 0) anz_spec_in = anz_spec_in + 1
    if (helpout(i) > 0) anz_spec_out = anz_spec_out + 1
   enddo

    if(anz_tree_in > 0.) then
        med_diam_in  = sqrt(med_diam_in/anz_tree_in)
        hmean_in     = hmean_in / anz_tree_in
    endif
    if(anz_tree_out > 0.) then
        med_diam_out = sqrt(med_diam_out/anz_tree_out)
        hmean_out    = hmean_out / anz_tree_out
    endif

! call species values
call classt

call stand_bal_spec

call calc_ind_rep
!call classification of stand diameter and height

call class

! moving of understorey tree cohorts to overstorey tree cohorts


if(flag_mg.ne.33) call overstorey
contains

!**************************************************************

subroutine update_age

if(flag_standup.ne. 2) then 
zeig=>pt%first
do
if(.not.associated(zeig)) exit
zeig%coh%x_age=zeig%coh%x_age + 1
zeig=>zeig%next
end do
end if
end subroutine update_age

end subroutine stand_balance

!**************************************************************

subroutine stand_bal_spec

use data_climate
use data_out
use data_simul
use data_site
use data_stand
use data_species
use data_par
use data_manag

implicit none

integer :: i, j, k, ntr, nd, lowtree, hntr, spec_new
real,dimension(nspec_tree):: vgldom1, vgldom2, vgldom_spec1, vgldom_spec2
integer,dimension(nspec_tree):: anzdom1, anzdom2, anzdom_spec1, anzdom_spec2, &
                                helpdiam     
integer,dimension(nspecies):: helpanz
integer helpntr
integer help_nr_inf_trees
logical lhelp
INTEGER leapyear
real atemp, hh, help_height_top
real triangle
real, external :: daylength

if (flag_trace) write (unit_trace, '(I4,I10,A)') iday, time, ' stand_bal_spec'

! Initialisation
vgldom1     = 0.
vgldom2     = 0.
anzdom1     = 0
anzdom2     = 0
med_diam    = 0.
mean_diam   = 0.
mean_height = 0.
hdom        = 0.      ! dominante height (highest or two highest cohorst); Hoehe (hoehste oder die beiden hoechsten Kohorten)

anz_spec     = 0      ! currently existing species
lowtree      = 0      ! amount of trees with DBH=0 for the whole population; Anzahl Baeume mit DBH = 0 fuer gesamten Bestand
hntr         = 0
helpanz      = 0      ! auxiliary variable to count species; Hilfsvariable um Spezies zu zaehlen
helpdiam     = 0      ! amount of trees with DBH=0 per species; Anzahl Baeume mit DBH = 0 pro Spezies
vgldom_spec1 = 0.
vgldom_spec2 = 0.
anzdom_spec1 = 0
anzdom_spec2 = 0

svar%med_diam   = 0.
svar%mean_diam  = 0.
svar%mean_jrb = 0.
svar%mean_height= 0.
svar%basal_area = 0.
svar%sumNPP     = 0.
svar%Ndem       = 0.
svar%Nupt       = 0.
svar%sum_ntreea = 0.
svar%sum_ntreed = 0.
svar%sum_bio    = 0.
svar%sum_lai    = 0.
svar%anz_coh    = 0.
svar%drIndAl    = 0.
svar%totsteminc = 0.
svar%totsteminc_m3 = 0.
svar%fol        = 0.
svar%sap        = 0.
svar%hrt        = 0.
svar%frt        = 0.


    !update height of mistletoe to height of mistletoe-infected cohort
    if (flag_mistle.ne.0) then
        zeig => pt%first
        DO WHILE (ASSOCIATED(zeig))
            if (zeig%coh%mistletoe.eq.1) then
                help_height_top=zeig%coh%height
            end if
            zeig => zeig%next
        ENDDO
        zeig => pt%first
        DO WHILE (ASSOCIATED(zeig))
          if (zeig%coh%species.eq.nspec_tree+2) then
              zeig%coh%height = help_height_top               !upper crown
              zeig%coh%x_hbole = zeig%coh%height-50.          !lower crown
          end if
          zeig => zeig%next
        ENDDO
    end if    ! end update of height of Mistletoe

    ! update of #of mistletoe upon dist_manag 
     if (flag_mistle.ne.0) then
        zeig => pt%first
        DO WHILE (ASSOCIATED(zeig))
            if (zeig%coh%mistletoe.eq.1) then
                help_nr_inf_trees=zeig%coh%nTreeA
            end if
            zeig => zeig%next
        ENDDO

        zeig => pt%first
        DO WHILE (ASSOCIATED(zeig))
            if (zeig%coh%species.eq.nspec_tree+2) then
               zeig%coh%nTreeA= help_nr_inf_trees*AMAX1(1.,dis_rel(time))
               zeig%coh%nta=zeig%coh%nTreeA
            end if
            zeig => zeig%next
        ENDDO
     end if    ! end  update #of mistletoe
    
     zeig => pt%first
    DO WHILE (ASSOCIATED(zeig))
      
      ns = zeig%coh%species
      helpanz(ns) = helpanz(ns) + 1   ! all species incl. ground vegetation;

      IF(zeig%coh%ident .le. coh_ident_max) THEN
		ntr = zeig%coh%ntreea
        
        IF(ns .le. nspec_tree) THEN
!            
            IF((ns .le. nspec_tree) .and. (ntr > 0.) .and. (zeig%coh%diam > 0.)) THEN
               svar(ns)%med_diam    = svar(ns)%med_diam + ntr * (zeig%coh%diam**2)
               med_diam             = med_diam + ntr * (zeig%coh%diam**2)
               mean_diam            = mean_diam + ntr*zeig%coh%diam
			   svar(ns)%mean_diam   = svar(ns)%mean_diam + ntr*zeig%coh%diam
			   svar(ns)%mean_height = svar(ns)%mean_height + ntr*zeig%coh%height
			   svar(ns)%mean_jrb    = svar(ns)%mean_jrb  + ntr*zeig%coh%jrb
			   mean_height          = mean_height + ntr*zeig%coh%height
			   hntr = hntr + ntr

            ELSE
              ! Trees with DBH=0 for population and per species; Baeume mit DBH =0 fuer Bestand und pro Spezies
               lowtree      = lowtree + ntr
               helpdiam(ns) = helpdiam(ns) + ntr
            ENDIF  ! ns
            IF(zeig%coh%height > vgldom1(ns)) THEN
                vgldom2(ns) = vgldom1(ns)
                anzdom2(ns) = anzdom1(ns)
                vgldom1(ns) = zeig%coh%height
                anzdom1(ns) = ntr
            ELSE
                if(zeig%coh%height > vgldom2(ns))then
                   vgldom2(ns) = zeig%coh%height
                   anzdom2(ns) = ntr
                endif
            ENDIF   ! vgldom1
            IF(zeig%coh%height > vgldom_spec1(ns)) THEN
                vgldom_spec2(ns) = vgldom_spec1(ns)
                anzdom_spec2(ns) = anzdom_spec1(ns)
                vgldom_spec1(ns) = zeig%coh%height
                anzdom_spec1(ns) = ntr
            ELSE
                if(zeig%coh%height > vgldom_spec2(ns))then
                vgldom_spec2(ns) = zeig%coh%height
                anzdom_spec2(ns) = ntr
                endif
            ENDIF ! vgldom_spec2
        ELSE
            svar(ns)%dom_height = zeig%coh%height
        ENDIF ! end loop across trees

            svar(ns)%sumNPP     = svar(ns)%sumNPP  + ntr * zeig%coh%NPP
            svar(ns)%sum_ntreea = svar(ns)%sum_ntreea + ntr
            svar(ns)%sum_ntreed = svar(ns)%sum_ntreed + zeig%coh%nTreeD + zeig%coh%nTreeM  ! died or harvested trees of current year; ausgeschiedene Bäume des akt. Jahres
            svar(ns)%Ndem       = svar(ns)%Ndem + ntr * zeig%coh%Ndemc_c
            svar(ns)%Nupt       = svar(ns)%Nupt + ntr * zeig%coh%Nuptc_c
            svar(ns)%sum_bio    = svar(ns)%sum_bio + ntr * zeig%coh%totBio
            svar(ns)%sum_lai    = svar(ns)%sum_lai + ntr * zeig%coh%t_leaf/kpatchsize
            svar(ns)%anz_coh    = svar(ns)%anz_coh + 1
            svar(ns)%totsteminc = svar(ns)%totsteminc + ntr * zeig%coh%stem_inc
            if (zeig%coh%species.ne.nspec_tree+2)    then                                       !no stem increment for mistletoe
             svar(ns)%totsteminc_m3 = svar(ns)%totsteminc_m3 + ntr * zeig%coh%stem_inc /(spar(ns)%prhos*1000000)
            endif
            svar(ns)%fol = svar(ns)%fol +  ntr * zeig%coh%x_fol
            svar(ns)%sap = svar(ns)%sap +  ntr * zeig%coh%x_sap
            svar(ns)%hrt = svar(ns)%hrt +  ntr * zeig%coh%x_hrt
            svar(ns)%frt = svar(ns)%frt +  ntr * zeig%coh%x_frt
            nd = zeig%coh%nDaysGr
            if (nd .gt. 0) svar(ns)%drIndAl    = svar(ns)%drIndAl + ntr * zeig%coh%drIndAl * zeig%coh%NPP / nd

      ENDIF  ! coh%ident

        zeig%coh%ntreed = 0.
        zeig%coh%ntreem = 0.
        zeig => zeig%next
    ENDDO   ! cohort loop

    ! neue Spezies feststellen und belegen
 if (time .gt. 1) then
     do i=1,nspecies
          if (helpanz(i) > 0) then
            spec_new = 0
            lhelp = .True.
            do j=1,anrspec
                if (nrspec(j) .eq. i) lhelp = .False. 
            enddo
            if (lhelp) then
                spec_new = i
	            if(spec_new.le.nspec_tree) then 
                    IF(spar(spec_new)%Phmodel==1) THEN
                        svar(spec_new)%Pro = 0.
                        svar(spec_new)%Inh = 1.
                    ELSE
                        svar(spec_new)%Pro   = 0.
                        svar(spec_new)%Inh   = 0.
                        svar(spec_new)%Tcrit = 0.
                    END IF

                    ! initialize pheno state variables with climate from the actual year
                    do j = spar(ns)%end_bb+1, 365

                      atemp = tp(j, time)
                      hh = DAYLENGTH(j,lat)
                      SELECT CASE(ns)
                      CASE(1,8)
                      !Fagus
                        ! Promotor-Inhibitor model 11
                                 svar(ns)%Pro = svar(ns)%Pro + spar(ns)%PPa*  &
                                 triangle(spar(ns)%PPtmin,spar(ns)%PPtopt,spar(ns)%PPtmax,atemp)*  & 
                                          (1-svar(ns)%Inh)*hh/24 - &
                                          spar(ns)%PPb*svar(ns)%Pro*(24-hh)/24

                                  svar(ns)%Inh = svar(ns)%Inh - spar(ns)%PIa*&
                                  triangle(spar(ns)%PItmin,spar(ns)%PItopt,spar(ns)%PItmax,atemp)*  &
                                  svar(ns)%Inh*hh/24

                      CASE(4)
                      ! Quercus
                        ! Promotor-Inhibitor model 12
                                 svar(ns)%Pro = svar(ns)%Pro + spar(ns)%PPa*  &
                                 triangle(spar(ns)%PPtmin,spar(ns)%PPtopt,spar(ns)%PPtmax,atemp)*  &
                                          (1-svar(ns)%Inh)*hh/24

                                  svar(ns)%Inh = svar(ns)%Inh - spar(ns)%PIa*  &
                                  triangle(spar(ns)%PItmin,spar(ns)%PItopt,spar(ns)%PItmax,atemp)*  &
                                  svar(ns)%Inh*hh/24 + spar(ns)%PPb*(24-hh)/24
               
                      CASE(5, 11)
                      ! Betula, Robinia
                              IF(spar(ns)%Phmodel==1) THEN
                              ! Promotor-Inhibitor model 2

                                 svar(ns)%Pro = svar(ns)%Pro + spar(ns)%PPa*  &
                                 triangle(spar(ns)%PPtmin,spar(ns)%PPtopt,spar(ns)%PPtmax,atemp)*  &
                                          (1-svar(ns)%Inh) - spar(ns)%PPb*svar(ns)%Pro*(24-hh)/24

                                  svar(ns)%Inh = svar(ns)%Inh - spar(ns)%PIa*  &
                                  triangle(spar(ns)%PItmin,spar(ns)%PItopt,spar(ns)%PItmax,atemp)*svar(ns)%Inh

                              END IF

                      END SELECT 
                    Enddo 

                    IF(spar(spec_new)%phmodel==4) THEN
                        svar(spec_new)%daybb = svar(spec_new)%ext_daybb
                    ELSE
                        svar(spec_new)%daybb = 181 + leapyear(time_cur)
                    ENDIF

                endif
            endif
          endif
     enddo
 endif   ! time
 
 k = 0
do i=1,nspecies

  if (helpanz(i) > 0) then
    k = k + 1
    anrspec = k
    nrspec(k) = i 
  endif
  
  ntr = svar(i)%sum_ntreea
  
  if (svar(i)%sumNPP .gt. 1E-06) svar(i)%drIndAl = svar(i)%drIndAl / svar(i)%sumNPP
  
  if (i .le. nspec_tree) then
    IF(helpanz(i) > 0) THEN
       anz_spec = anz_spec + 1
       IF(helpdiam(i) < ntr) THEN
           svar(i)%med_diam = SQRT(svar(i)%med_diam / (ntr - helpdiam(i)))
		   
       ENDIF

       svar(i)%Ndem       = svar(i)%Ndem / kpatchsize       ! g per tree --> g/m2
       svar(i)%Nupt       = svar(i)%Nupt / kpatchsize       ! g per tree --> g/m2

       if (ntr .ne. 0) then
          svar(i)%mean_height = svar(i)%mean_height / ntr
          svar(i)%mean_diam   = svar(i)%mean_diam / ntr
          svar(i)%mean_jrb    = svar(i)%mean_jrb / ntr
          
          svar(i)%basal_area = pi*(ntr-helpdiam(i))*(svar(i)%med_diam*svar(i)%med_diam/40000)*10000/kpatchsize
       else
          svar(i)%sum_ntreea = 0.
       endif

       call calc_heidom_spec(i)
    ENDIF
   
   end if   ! nspec_tree

!  conversion kg/patch ---> kg/ha; N/patch ---> N/ha
     helpntr = svar(i)%sum_nTreeA* 10000./kpatchsize
     if(helpntr.eq.0 .and. svar(i)%sum_nTreeA.eq.1) then
	          svar(i)%sum_nTreeA = 1
     else 
	          svar(i)%sum_nTreeA = helpntr

	 end if
    svar(i)%sum_bio    = svar(i)%sum_bio * 10000./kpatchsize
    svar(i)%fol = svar(i)%fol * 10000./kpatchsize
    svar(i)%sap = svar(i)%sap* 10000./kpatchsize
    svar(i)%hrt= svar(i)%hrt* 10000./kpatchsize
    svar(i)%frt= svar(i)%frt* 10000./kpatchsize
    svar(i)%totstem_m3= ( svar(i)%sap +  svar(i)%hrt)/ (spar(i)%prhos*1000000)  ! m3/ha
    svar(i)%totsteminc = svar(i)%totsteminc * 10000./kpatchsize      ! kg/ha  
    svar(i)%totsteminc_m3 =   svar(i)%totsteminc_m3 * 10000./kpatchsize      ! kg/ha  
    totsteminc_m3 = totsteminc_m3 + svar(i)%totsteminc_m3
    totsteminc = totsteminc + svar(i)%totsteminc

 end do

! new calculation of dominant height
call calc_heidom


if(anz_tree>0)then
  if(lowtree<anz_tree) then
     med_diam    = sqrt(med_diam /(anz_tree-lowtree))
     mean_diam   = mean_diam /(anz_tree-lowtree)
     mean_height = mean_height /(anz_tree-lowtree)
     basal_area = pi*(anz_tree-lowtree)*(med_diam*med_diam/40000)*10000/kpatchsize
  endif
else
   if (hntr .ne. 0) then
      med_diam    = sqrt(med_diam /hntr)
      mean_diam   = mean_diam / hntr
      mean_height = mean_height / hntr
   else
      med_diam    = 0.
      mean_diam   = 0.
      mean_height = 0.
   endif
endif

end subroutine stand_bal_spec

!**************************************************************

subroutine class
use data_stand
use data_simul
use data_species
use data_par
implicit none
integer i,k


diam_class=0;height_class=0
diam_class_age=0.
diam_class_h = 0.
zeig=>pt%first

do
 if(.not.associated(zeig)) exit
 k = zeig%coh%species
 if (k.ne.nspec_tree+2) then   !exclusion of mistletoe
  if(zeig%coh%diam<=dclass_w) then
        diam_class(1,k)=diam_class(1,k)+zeig%coh%ntreea
        diam_class_h(1,k) =  diam_class_h(1,k) + zeig%coh%ntreea*zeig%coh%height
        diam_class_age(1,k) = diam_class_age(1,k)+zeig%coh%x_age*zeig%coh%ntreea
  end if
 do i=2,num_class
  if(zeig%coh%diam.le.(dclass_w + dclass_w*(i-1)) .and. zeig%coh%diam>(dclass_w + dclass_w*(i-2))) then
          diam_class(i,k)=diam_class(i,k) + zeig%coh%ntreea
          diam_class_h(i,k) =  diam_class_h(i,k) + zeig%coh%ntreea*zeig%coh%height
          diam_class_age(i,k) = diam_class_age(i,k)+zeig%coh%x_age*zeig%coh%ntreea
  
  else if(zeig%coh%diam.gt. (dclass_w + dclass_w*(num_class-2)))  then
          diam_class(num_class,k)=diam_class(num_class,k) + zeig%coh%ntreea
          diam_class_h(num_class,k) =  diam_class_h(num_class,k) + zeig%coh%ntreea*zeig%coh%height
          diam_class_age(num_class,k) =  diam_class_age(num_class,k) + zeig%coh%x_age+zeig%coh%ntreea
		  exit
  end if
 enddo

 if(zeig%coh%height<=100) height_class(1) = height_class(1)+zeig%coh%ntreea
 if(zeig%coh%height>100.and.zeig%coh%height<500) height_class(2) = height_class(2)+zeig%coh%ntreea
 do i=3,num_class-2
 if(zeig%coh%height>(i+2)*100.and.zeig%coh%height<=(i+3)*100) height_class(i) = height_class(i)+zeig%coh%ntreea
 enddo
 if(zeig%coh%height>5000.and.zeig%coh%height<5500) height_class(num_class-1) = height_class(num_class-1)+zeig%coh%ntreea
 if(zeig%coh%height>5500) height_class(num_class) = height_class(num_class)+zeig%coh%ntreea

 endif!exclusion of mistletoe
 zeig=>zeig%next

enddo

do i=1,num_class
   do k=1,nspec_tree
    if(diam_class(i,k).ne.0) diam_class_h(i,k) =  (diam_class_h(i,k)/diam_class(i,k))*10000./kpatchsize
    if(diam_class_age(i,k).ne.0.and.diam_class(i,k).ne.0 ) diam_class_age(i,k) =diam_class_age(i,k)/diam_class(i,k)
    diam_class(i,k) = diam_class(i,k)*10000./kpatchsize
  end do
end do
end subroutine class

!**************************************************************

subroutine classt
use data_stand
use data_simul
use data_species
implicit none
integer i,k

diam_class_t=0;height_class=0
diam_class_h = 0.
zeig=>pt%first
do
 if(.not.associated(zeig)) exit
 k = zeig%coh%species
 if (k.ne.nspec_tree+2) then   ! exclusion mistletoe
  if(zeig%coh%diam<=dclass_w) then
       diam_class_t(1,k)=diam_class_t(1,k)+zeig%coh%ntreed
 end if
 do i=2,num_class
  if(zeig%coh%diam.le.(dclass_w + dclass_w*(i-1)) .and. zeig%coh%diam>(dclass_w + dclass_w*(i-2))) then
          diam_class_t(i,k)=diam_class_t(i,k) + zeig%coh%ntreed
  
  else if(zeig%coh%diam.gt. (dclass_w + dclass_w*(num_class-2)))  then
          diam_class_t(num_class,k)=diam_class_t(num_class,k) + zeig%coh%ntreed
		  exit
  end if
 enddo

 endif !exclusion of mistletoe
 zeig=>zeig%next

enddo

do i=1,num_class
  do k=1,nspec_tree
       diam_class_t(i,k)=diam_class_t(i,k)*10000./kpatchsize
  end do
end do  
end subroutine classt

!**************************************************************


subroutine class_man
use data_stand
use data_simul
use data_species
use data_manag
implicit none
integer i , k 
real anz
diam_classm=0
diam_classm_h=0.
diam_class_mvol = 0.

   zeig=>pt%first
   do
    if(.not.associated(zeig)) exit
    if(zeig%coh%ntreem.ne.0.or.(zeig%coh%ntreed.gt.0 .and. zeig%coh%diam.gt.tardiam_dstem)) then
      if(zeig%coh%diam.le.tardiam_dstem) then
           anz = zeig%coh%ntreem
      else
          anz = zeig%coh%ntreem + zeig%coh%ntreed
      end if
      k = zeig%coh%species

      if(zeig%coh%diam<=dclass_w) then
        diam_classm(1,k)=diam_classm(1,k)+anz
         diam_classm_h(1,k) =  diam_classm_h(1,k) + anz*zeig%coh%height
         diam_class_mvol(1,k) = diam_class_mvol(1,k) +anz*(zeig%coh%x_sap + zeig%coh%x_hrt)
      end if

      if(zeig%coh%diam<=dclass_w*2.and.zeig%coh%diam.gt.dclass_w) then
         diam_classm(2,k)=diam_classm(2,k)+anz
         diam_classm_h(2,k) =  diam_classm_h(2,k) + anz*zeig%coh%height
         diam_class_mvol(2,k) = diam_class_mvol(2,k) + anz*(zeig%coh%x_sap + zeig%coh%x_hrt)
      end if

      do i=3,num_class
         if(zeig%coh%diam.le.(dclass_w*2 + dclass_w*(i-2)) .and. zeig%coh%diam>(dclass_w*2 + dclass_w*(i-3))) then
              diam_classm(i,k) = diam_classm(i,k) + anz
              diam_classm_h(i,k) =  diam_classm_h(i,k) + anz*zeig%coh%height
              diam_class_mvol(i,k) = diam_class_mvol(i,k) + anz*(zeig%coh%x_sap + zeig%coh%x_hrt)


         else if(zeig%coh%diam.gt. (dclass_w*2 + dclass_w*(num_class-3)))  then
          diam_classm(num_class,k)=diam_classm(num_class,k) + anz
          diam_classm_h(num_class,k) =  diam_classm_h(num_class,k) + anz*zeig%coh%height
          diam_class_mvol(num_class,k) = diam_class_mvol(num_class,k) + anz*(zeig%coh%x_sap + zeig%coh%x_hrt)

         end if
      enddo
    end if
    zeig=>zeig%next
   enddo

do i=1,num_class
  do k=1,nspecies
    
    if(diam_classm(i,k).ne.0) diam_classm_h(i,k) =  diam_classm_h(i,k)/diam_classm(i,k)
    if(diam_class_mvol(i,k).ne.0.) then
           diam_class_mvol(i,k) = diam_class_mvol(i,k)/(spar(k)%prhos*1000000)*10000/kpatchsize
    end if
    diam_classm(i,k) = diam_classm(i,k)*10000./kpatchsize
  end do
end do
end subroutine class_man

!**************************************************************

subroutine calc_heidom
 
 use data_out
 use data_simul
 use data_stand
 
 implicit none
 
 real    :: mh

 integer   :: nhelp,      &
              nh1,nh2,    &
              testflag=0, &
              j

 allocate (height_rank(anz_coh))

 nh1=0
 nh2=0
 mh = 0
 testflag = 0
 nhelp = nint(kpatchsize/100)
 if(anz_tree.le.nhelp) nhelp = anz_tree

! sorting by height of cohorts
 call dimsort(anz_coh, 'height',height_rank)

 if(anz_tree>1) then
     do j=anz_coh, 1,-1
       call dimsort(anz_coh, 'height',height_rank)

       zeig=>pt%first
       do
          if(.not.associated(zeig)) exit
          if(zeig%coh%ident.eq.height_rank(j)) then
              nh2 = nh1
              nh1 = nh1 + zeig%coh%ntreea
              if(nh1.le. nhelp) then
                    mh = mh + zeig%coh%ntreea*zeig%coh%height
              else
                    mh = mh + zeig%coh%height*( nhelp - nh2)
                    testflag=1
                    exit
              end if
          endif
       zeig=>zeig%next
       if(testflag.eq.1) exit
       end do
       if(testflag.eq.1) exit
       if(nh1.eq.nhelp) exit
     end do
  if (nhelp.lt. nh1) then
     hdom = mh/nhelp
  else
      hdom = mh/nh1
  end if
  end if
 deallocate(height_rank)
end subroutine calc_heidom

!**************************************************************

 subroutine calc_heidom_spec(ispec)

!*****************************************************
! species specific dominant height calculation
!*****************************************************

 use data_out
 use data_simul
 use data_stand
 
 implicit none
 
 real    :: mh

 integer   :: nhelp,      &
              nh1,nh2,    &
              testflag=0, &
              j,          &
              ispec

 allocate (height_rank(anz_coh))
 hdom = 0
 nh1=0
 nh2=0
 mh = 0
 testflag = 0
! calculation of number of trees used for H100 ( 100/ ha = nhelp/ kpachtsize)
 nhelp = nint(kpatchsize/100)
 if(anz_tree.le.nhelp) nhelp = anz_tree

! sorting by height of cohorts
 call dimsort(anz_coh, 'height',height_rank)

 if(anz_tree>1) then
     do j=anz_coh, 1,-1
       call dimsort(anz_coh, 'height',height_rank)

       zeig=>pt%first
       do
          if(.not.associated(zeig)) exit
          if(zeig%coh%ident.eq.height_rank(j).and. zeig%coh%species.eq.ispec) then
              nh2 = nh1
              nh1 = nh1 + zeig%coh%ntreea
                if(nh1.le. nhelp) then
                    mh = mh + zeig%coh%ntreea*zeig%coh%height
              else
                    mh = mh + zeig%coh%height*( nhelp - nh2)
                    testflag=1
                    exit
              end if
          endif
       zeig=>zeig%next
       if(testflag.eq.1) exit
       end do
       if(testflag.eq.1) exit
       if(nh1.eq.nhelp) exit
     end do
  if (nhelp.lt. nh1.and. nhelp.ne.0) then
     hdom = mh/nhelp
  else if(nh1.ne.0) then
      hdom = mh/nh1
  end if
 else if(anz_tree.eq.1) then
      zeig=>pt%first
      do
        if(.not.associated(zeig)) exit
        if(zeig%coh%species.eq.ispec) hdom=zeig%coh%height
        zeig=>zeig%next
      end do
 end if
 deallocate(height_rank)
 svar(ispec)%dom_height = hdom

end subroutine calc_heidom_spec

!**************************************************************

subroutine max_height(nrmax,anz,cohl)
 
 use data_out
 use data_simul
 use data_stand
 
 implicit none
 
 integer  :: nrmax
 integer  :: nrmax_h
 integer  :: anz, testflag,i
 real     :: help_h1, help_h2
 integer,dimension(0:anz_coh) :: cohl

 testflag=0
 nrmax = -1
 nrmax_h = -1
 help_h2=0.
 help_h1=0.
 zeig=>pt%first
 do
    if(.not.associated(zeig)) exit
    do  i=0,anz-1
      if(cohl(i).eq.zeig%coh%ident) then
          testflag=1
      endif
    end do
        if (testflag.eq.0) then
          help_h2= zeig%coh%height
          nrmax_h = zeig%coh%ident
          if(help_h2.gt. help_h1) then
            help_h1 = help_h2
            nrmax = nrmax_h
          end if

       end if

    zeig=>zeig%next
    testflag = 0
 end do
    anz = anz +1
    cohl(anz-1) = nrmax

end subroutine max_height

!**************************************************************

SUBROUTINE standup

! update of stand variables (LAI, cover, waldtyp)

USE data_par
USE data_stand
USE data_soil
USE data_species
use data_out
use data_simul

implicit none

integer i
REAL :: sumfol_can = 0.
REAL :: sumfol_sveg= 0.
REAL :: ntr, cover3

! estimating degree of covering

 if (flag_trace) write (unit_trace, '(I4,I10,A)') iday, time, ' standup'

cover3      = 0.
sumfol_can = 0.
sumfol_sveg= 0.
crown_area = 0.

do i = 1, anrspec
   svar(nrspec(i))%crown_area = 0.
enddo

zeig=>pt%first
do
   IF(.not.associated(zeig)) exit
 if (zeig%coh%crown_area .ge. 0) then
   ntr   = zeig%coh%nTreeA
   ns    = zeig%coh%species
   cover3 = cover3 + ntr * zeig%coh%crown_area
   if (ns .le. nspec_tree) then
      sumfol_can          = sumfol_can + ntr * zeig%coh%x_fol
      crown_area          = crown_area + ntr * zeig%coh%crown_area
   else
      sumfol_sveg = sumfol_sveg + ntr * zeig%coh%x_fol
   endif
   svar(ns)%crown_area =  svar(ns)%crown_area + ntr * zeig%coh%crown_area
 endif
   zeig=>zeig%next
end do

cover3 = cover3 / kpatchsize

anz_tree = 0
zeig=>pt%first
do
    IF(.not.associated(zeig)) exit
    ns=zeig%coh%species
    if (ns .le. nspec_tree) then
      zeig%coh%rel_fol = zeig%coh%ntreea * zeig%coh%x_fol/sumfol_can
      ceppot_can       = ceppot_can + zeig%coh%rel_fol * spar(ns)%ceppot_spec
      anz_tree         = anz_tree + zeig%coh%ntreea
    else if (ns.eq.nspec_tree+1) then
       zeig%coh%rel_fol = zeig%coh%ntreea * zeig%coh%x_fol/sumfol_sveg
       ceppot_sveg      = ceppot_sveg + zeig%coh%rel_fol * spar(ns)%ceppot_spec
    endif
    zeig=>zeig%next
end do

!Berechnung LAI und ceppot
ceppot_can  = 0.
ceppot_sveg = 0.
LAI_can     = 0.
LAI_sveg    = 0.

   DO i=1,anrspec
     ns = nrspec(i)
     IF (ns .le. nspec_tree) THEN
        LAI_can  = LAI_can + svar(ns)%act_sum_lai
     ELSE
        LAI_sveg = LAI_sveg + svar(ns)%act_sum_lai
     ENDIF
   ENDDO

   DO i=1,anrspec
     ns = nrspec(i)
      IF (ns .le. nspec_tree) THEN
        IF(LAI_can .gt. 0.) THEN
          ceppot_can = ceppot_can + svar(ns)%act_sum_lai/LAI_can * spar(ns)%ceppot_spec
        ELSE
          ceppot_can = 0.
        ENDIF
      ELSE
        IF(LAI_sveg .gt. 0.) THEN
          ceppot_sveg = ceppot_sveg + svar(ns)%act_sum_lai/LAI_sveg * spar(ns)%ceppot_spec
        ELSE
          ceppot_sveg= 0.
        ENDIF
      END IF
   ENDDO

if (LAI .gt. 1.) then
    cover = cover3
else if (LAI .le. zero) then
    cover = 0.1 * cover3
else
    cover = LAI * cover3    ! to combine with leave surface; an Blattflaeche koppeln
endif
call wclas(waldtyp)    ! forest type

END SUBROUTINE standup

!******************************************************************************

SUBROUTINE senescence

! update of senescence rates

USE data_stand
USE data_species
USE data_simul
IMPLICIT NONE

  ! senescence rates
  zeig=>pt%first
  DO
    IF(.not.associated(zeig)) exit
     if (zeig%coh%species.ne.nspec_tree+2) then                          ! exclude mistletoe from senescence
         select case (flag_dis)
	     ! case (1,2)
	     !  zeig%coh%sfol = spar(zeig%coh%species)%psf * zeig%coh%x_fol + zeig%coh%x_fol_loss
         !  zeig%coh%sfrt = spar(zeig%coh%species)%psr * zeig%coh%x_frt + zeig%coh%x_frt_loss
	      case (0,1,2)
	       zeig%coh%sfol = spar(zeig%coh%species)%psf * zeig%coh%x_fol
           zeig%coh%sfrt = spar(zeig%coh%species)%psr * zeig%coh%x_frt
	     end select
      IF (zeig%coh%height.ge.thr_height .and.zeig%coh%species.LE. nspec_tree) THEN
         zeig%coh%ssap = spar(zeig%coh%species)%pss * zeig%coh%x_sap
      ELSE
         zeig%coh%ssap = 0
	     if(zeig%coh%species.GT.nspec_tree) zeig%coh%ssap = spar(zeig%coh%species)%pss*zeig%coh%x_sap
      ENDIF
     end if    !exclusion of mistletoe
  zeig=>zeig%next
  END DO

END SUBROUTINE senescence

!**************************************************************

SUBROUTINE litter

! Calculation of summation variables of litter fractions

use data_par
use data_out
use data_simul
use data_soil
use data_soil_cn
use data_species
use data_stand

implicit none

real hconvd
integer taxnr, i

if (flag_trace) write (unit_trace, '(I4,I10,A)') iday, time_cur, ' litter'

zeig => pt%first
do while (associated(zeig))
  taxnr = zeig%coh%species
  if(taxnr.le.nspec_tree) then
     totfol_lit_tree  = totfol_lit_tree  + zeig%coh%litC_fol
     totfrt_lit_tree  = totfrt_lit_tree  + zeig%coh%litC_frt
  end if
     totfol_lit  = totfol_lit  + zeig%coh%litC_fol
     totfrt_lit  = totfrt_lit  + zeig%coh%litC_frt
     tottb_lit   = tottb_lit   + zeig%coh%litC_tb
     totcrt_lit  = totcrt_lit  + zeig%coh%litC_crt
     totstem_lit = totstem_lit + zeig%coh%litC_stem

  zeig => zeig%next
enddo  ! zeig (cohorts)

!  litter biomass: x kg C/tree to kg/ha  (n*x*1000g/(kPatchSize m2)/cpart==> kg/ha)
   hconvd = (1000*gm2_in_kgha) / (kpatchsize * cpart)  !
   totfrt_lit      = totfrt_lit  * hconvd
   totfol_lit      = totfol_lit  * hconvd
   tottb_lit       = tottb_lit   * hconvd
   totcrt_lit      = totcrt_lit  * hconvd
   totstem_lit     = totstem_lit * hconvd
   totfol_lit_tree = totfol_lit_tree * hconvd
   totfrt_lit_tree = totfrt_lit_tree * hconvd

do i = 1,nspec_tree
   tottb_lit       = tottb_lit + dead_wood(i)%C_tb(1)*gm2_in_kgha
   totstem_lit     = totstem_lit + dead_wood(i)%C_stem(1)*gm2_in_kgha
enddo

END subroutine litter

!**************************************************************

SUBROUTINE calc_ind_rep

USE data_stand
USE data_species
USE data_simul
implicit none

 integer ::  i
 real    ::   hi
 real, dimension(nspecies)  :: rindex_spec
  rindex1 = 0.
  rindex2 = 0.

 if(anz_spec.ne.0) then
    hi = 1/real(anz_spec)
    rindex_spec = 0.
    do i = 1, nspecies

        if(sumbio.ne.0) then
            rindex_spec(i) =  svar(i)%sum_bio/sumbio
        end if

    end do
    rindex1 = 0.
    rindex2 = 1.
    do i = 1, nspecies
        if(rindex_spec(i).ne.0) then
             rindex1 = rindex1 + abs(hi -rindex_spec(i))
             rindex2 = rindex2 * abs(hi -rindex_spec(i))
        end if
    end  do


    if(hi.ne.1) then
          rindex1 = 1. - rindex1/(2*(1.-hi))
    else
          rindex1 = 0.
    end if

    rindex2 = rindex2**anz_spec
 end if
 
 END subroutine calc_ind_rep

!**************************************************************

SUBROUTINE overstorey
 
 use data_out
 USE data_stand
 USE data_species
 USE data_simul
 implicit none

 real,dimension(nspec_tree)    :: mindbh, maxdbh, dminage, dmaxage
 integer                     :: i, nrmin, taxnr, agedmin, agedmax
 real                        :: dbhmin, dbhmax
 integer                     :: anzoverst, nrmax
 
 anzoverst = 0
 mindbh=0.
 do i =1,nspec_tree

     call min_dbh(nrmin,dbhmin,agedmin, i)

     mindbh(i) = dbhmin
	 dminage(i) = agedmin

	 	call max_dbh(nrmax, dbhmax, agedmax, i)

      maxdbh(i) = dbhmax
	  dmaxage(i) = agedmax
 end do

if (time.eq.0) then
   zeig=>pt%first
   do
    IF(.not.associated(zeig)) exit
    taxnr = zeig%coh%species

	if(taxnr .le.nspec_tree) then
	    if(zeig%coh%x_age.lt. (dminage(taxnr) +20) .and. dminage(taxnr).lt. dmaxage(taxnr)) then
	        zeig%coh%underst =2
	    end if
	end if

	zeig=>zeig%next

  end do

else
   zeig=>pt%first
   do
    IF(.not.associated(zeig)) exit
    taxnr = zeig%coh%species

    if(zeig%coh%height.gt. 130..and. zeig%coh%underst.eq.4) then
	     zeig%coh%underst = 2
    end if
	zeig=>zeig%next
   end do

end if ! time
END SUBROUTINE overstorey
