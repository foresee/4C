!*****************************************************************!
!*                                                               *!
!*                  4C (FORESEE) Simulation Model                *!
!*                                                               *!
!*                                                               *!
!*     SUBROUTINE                                                *!
!*     timsort - for sorting of harvested timber to              *!
!*               different timber qualities                      *!
!*    definition:                                                *!    
!*     ste     - stems                                           *!
!*     sg1/sg2 - stem segments                                   *!
!*     in1/in2 - industrial wood                                 *!
!*     fue     - fuelwood                                        *!
!*    Subroutine:                                                *!
!*     out_tim - generating field sort                           *!
!*     out_timlist                                               *!
!*     fuction rabf                                              *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

SUBROUTINE timsort

use data_stand
use data_species
use data_tsort
use data_simul
use data_par
use data_manag

!***************** wpm ************************
use data_wpm
use data_stand
use data_species
!***************** wpm ************************

implicit none

integer i
real      h,dbh,db, dcrb, hbo,  llazmin, lx,lxz,lldmin ,llasdmin,liszmin,help,lisdmin,llzmin
real      h1,h2,dcrb_org,db_org,suml, h_org, sumbio_help,sumvol, h3
real (KIND = dg)  :: calcvol
character(4)  K
character(2)  standt
integer   count,count_old,taxid
real, external :: rabf
real           :: diam_base=0.   ! diameter at basis
llazmin=0.;lx=0.;lldmin=0.;llasdmin=0.; liszmin=0.;lisdmin=0.;llzmin=0.
count = 1
sumbio_help = 0
DO i=1,nspec_tree

    zeig => pt%first
    DO
        IF (.NOT. ASSOCIATED(zeig)) EXIT
!       Douglasie --- > Kiefer        
		taxid = zeig%coh%species
		IF(taxid .EQ. 10) taxid = 3
		IF(taxid .EQ. i) THEN

                calcvol = 0.
                sumvol = 0.
                h = zeig%coh%height -stoh(i)    ! stump correction
                hbo =  zeig%coh%x_hbole
                dbh = zeig%coh%diam
                dcrb = zeig%coh%dcrb
                dcrb_org = dcrb
                suml = stoh(i)                  ! hight of stump; stockh�he
                h_org =  zeig%coh%height

! calculation of stump biomass for harvesting


! selection of small trees with out dbh
!               IF (dbh.eq.0.) THEN
!litter


                diam_base= sqrt((zeig%coh%x_ahb+zeig%coh%asapw)*4/pi)


                if(hbo.ne.0) then
                    db = dcrb + (hbo-stoh(i))*(diam_base-dcrb)/hbo
                else if (hbo.eq.0)then
                    db = diam_base*h/(h + stoh(i))
                end if



               db_org = db

                 if( hbo.eq.0) then
 				     llzmin=0
					 lx=0
                 end if
! stems
                 
               help =  rabf(i,lzmin(i))
               if(db.ge.(lzmin(i) + rabf(i,lzmin(i)))) then

! calculation of lenght at diameter = lzmin
!  llzmin > h can occur
                  if ( dcrb .gt. lzmin(i)+rabf(i,lzmin(i))) then
                      llzmin = h-(h-hbo)*(lzmin(i)+rabf(i,lzmin(i)))/dcrb
                  else if (dcrb.le.lzmin(i)+rabf(i,lzmin(i))) then
                      llzmin = hbo -hbo*(lzmin(i)+rabf(i,lzmin(i))-dcrb)/(db-dcrb)
                  end if
		  
! calculation of diameter at llzmin/2
                  if (llzmin/2.lt. hbo) then

                    lx = dcrb + (db-dcrb)*(hbo-llzmin/2)/hbo
                  else
                    lx = dcrb*(h- llzmin/2)/(h-hbo)
                  end if
                  
! begin of sorting stem lumbre
if( flag_sort.eq.0) then
                  if( llzmin.ge. lmin(i).and. lx.ge. ldmin(i)+rabf(i,ldmin(i))) then
                     k = 'ste'
                     if(zeig%coh%ntreem.ne.0.) then
                       standt='ab'
                       call out_tim(count,i,k,llzmin + zug ,lx,lzmin(i), zeig%coh%ntreem,&
                            standt,h,hbo,db,dcrb,calcvol)
                     end if
                     if(zeig%coh%ntreea.ne.0.) then
                       standt='vb'
                       call out_tim(count,i,k,llzmin + zug ,lx,lzmin(i), zeig%coh%ntreea,&
                               standt,h,hbo,db,dcrb,calcvol)
                     end if
                     if(zeig%coh%ntreed.ne.0..and.flag_mg.ne.0.and.zeig%coh%diam.gt.tardiam_dstem) then
                     if(thin_flag1(1).ge.0) then
                       standt='tb'
                       flag_deadsort = 1
                       call out_tim(count,i,k,llzmin + zug ,lx,lzmin(i), zeig%coh%ntreed,&
                               standt,h,hbo,db,dcrb,calcvol)
                     end if
                     end if
                     suml = suml +llzmin+zug
                     sumvol = sumvol + calcvol
                     calcvol = 0.
                     count =  count + 1
                     h = h-llzmin-zug
                     if(h.lt.0.) h = 0.
                     db =  lzmin(i)
                  else
! H�he Hx berechnen, wo lx = ldmin(i)+rabf(i,ldmin(i)) , dann testen, ob 2*Hx >= lmin ist,
!wenn ja, abspeichern in sto
                     if (dcrb .gt.(ldmin(i)+rabf(i,ldmin(i)))) then
                         lldmin= h-(h-hbo)*(ldmin(i)+rabf(i,ldmin(i)))/dcrb
                     else if (dcrb.le.(ldmin(i)+rabf(i,ldmin(i)))) then
                         lldmin = hbo - hbo*(ldmin(i)+rabf(i,ldmin(i))-dcrb)/(db_org-dcrb)
                     end if
! calculation of diameter at 2*lldmin  and test against lzmin
!(Durchmesser an der Spitze des Stammst�ckes
                     if (2*lldmin.lt. hbo) then

                         lx = dcrb + (db_org-dcrb)*(hbo-lldmin*2)/hbo
                     else
                         lx = dcrb*(h- lldmin*2)/(h-hbo)
                     end if

                     if (2*lldmin .ge. lmin(i) .and. lx.ge.lzmin(i)+rabf(i,lx)) then
!second test  Stammholz!
                             k = 'ste'
                             if(zeig%coh%ntreem.ne.0.) then
                               standt='ab'
                               call out_tim(count,i,k,2*lldmin + zug,ldmin(i),lx, zeig%coh%ntreem,&
                                            standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreea.ne.0.) then
                               standt='vb'
                                call out_tim(count,i,k,2*lldmin + zug,ldmin(i),lx, zeig%coh%ntreea,&
                                            standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreed.ne.0..and.flag_mg.ne.0.and.zeig%coh%diam.gt.tardiam_dstem) then
                              if(thin_flag1(1).ge.0) then
                               standt='tb'
                                flag_deadsort = 1
                                call out_tim(count,i,k,2*lldmin + zug,ldmin(i),lx, zeig%coh%ntreed,&
                                            standt,h,hbo,db,dcrb_org,calcvol)
                             end  if
                             end if

                             suml = suml + 2*lldmin + zug
                             sumvol=sumvol + calcvol
                             calcvol = 0.
                             count =  count + 1
                             h = h - 2*lldmin-zug
                             if(h.lt.0.) h = 0.
                             db = lx
                     end if
                   end if
                end if  ! db.gt. lzmin(i)
               end if   ! flag_sortst
! ende test Stammholz
               
! begin test auf Stammst�cke
               
! calculation of length at diamter lazmin(i)
               Do while((h.ge.lasfixl1(i).or.h.ge.lasfixl2(i)).and. hbo.ne.0.)
                count_old = count
               IF(db .ge.laszmin(i)+rabf(i,laszmin(i)).and. db.gt.lasdmin(i)+ rabf(i,lasdmin(i))) THEN
                    if (dcrb .eq.0.) then
                       llazmin = (h_org-suml) -(h_org-stoh(i))*(laszmin(i)+rabf(i,laszmin(i)))/db
                    else if ( dcrb .gt. laszmin(i)+rabf(i,laszmin(i))) then
                       llazmin =(h_org-suml)- (h_org-hbo)*(laszmin(i)+rabf(i,laszmin(i)))/dcrb
                    else if (dcrb.le.laszmin(i)+rabf(i,laszmin(i))) then
                       llazmin = (hbo-suml) -hbo*(laszmin(i)+rabf(i,laszmin(i))-dcrb)/(db_org-dcrb)
                    end if

                    if(llazmin.ge.lasfixl1(i)) then
                         if(flag_sort.eq.2) then
                            llazmin = lasfixl2(i)
                         else
                            llazmin = lasfixl1(i)
                         end if
                    else if(llazmin.ge.lasfixl2(i)) then
                        llazmin = lasfixl2(i)
                    end if

! calculation of diameter lx at llazmin/2 

                    if (dcrb .eq.0.) then
                       lx = db*(h_org-(suml+llazmin/2))/(h_org-stoh(i))
                    else if ((suml+llazmin/2).lt. hbo) then

                        lx = dcrb + (db_org-dcrb)*(hbo-(suml+llazmin/2))/hbo
                    else
                        lx = dcrb*(h_org-(suml+ llazmin/2))/(h_org-hbo)
                    end if
! calculation of diameter at llazmin
                             if (dcrb .eq.0.) then
                                lxz = db*(h_org-(suml+llazmin))/(h_org-stoh(i))
                             else if ((suml+llazmin).lt. hbo) then

                                lxz = dcrb + (db_org-dcrb)*(hbo-(suml+llazmin))/hbo
                             else
                                lxz = dcrb*(h_org-(suml+ llazmin))/(h_org-hbo)
                             end if
! test 
                    help = lasdmin(i)+rabf(i,lasdmin(i))

! if flag_sort = 2 only lasfixl2 is used
                     h3 = lasdmin(i)+rabf(i,lasdmin(i))

                    if (llazmin.ge. lasfixl1(i).and. lx.ge. lasdmin(i)+rabf(i,lasdmin(i)).and. flag_sort.ne.2) then
                             k = 'sg1'
                             if(zeig%coh%ntreem.ne.0.) then
                               standt = 'ab'
                               call out_tim(count,i,k,llazmin+zug,lx,lxz, zeig%coh%ntreem,&
                                            standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreea.ne.0.) then
                               standt = 'vb'
                               call out_tim(count,i,k,llazmin+zug,lx,lxz, zeig%coh%ntreea,&
                                            standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreed.ne.0..and.flag_mg.ne.0.and.zeig%coh%diam.gt.tardiam_dstem) then
                              if(thin_flag1(1).ge.0) then
                                flag_deadsort = 1
                               standt = 'tb'
                               call out_tim(count,i,k,llazmin+zug,lx,lxz, zeig%coh%ntreed,&
                                            standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             end if

                             suml = suml + llazmin+zug
                             sumvol = sumvol +calcvol
                             calcvol =0.
                             count =  count + 1
                             h = h - llazmin-zug
                             if(h.lt.0.) h = 0.
                             db = lxz

! test 
! if flag_sort = 3 only lasfixl1 is unsed
                     else if (llazmin.ge.lasfixl2(i).and.llazmin.lt.lasfixl1(i).and. lx.ge. lasdmin(i)+rabf(i,lasdmin(i)).and. flag_sort.ne.3) then
                             k = 'sg2'
                             if(zeig%coh%ntreem.ne.0.) then
                               standt = 'ab'
                               call out_tim(count,i,k,llazmin + zug,lx, lxz,zeig%coh%ntreem,standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreea.ne.0.) then
                               standt = 'vb'
                               call out_tim(count,i,k,llazmin + zug,lx, lxz,zeig%coh%ntreea,standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreed.ne.0..and.flag_mg.ne.0.and.zeig%coh%diam.gt.tardiam_dstem) then
                              if(thin_flag1(1).ge.0) then
                                  flag_deadsort = 1
                               standt = 'tb'
                               call out_tim(count,i,k,llazmin + zug,lx, lxz,zeig%coh%ntreed,standt,h,hbo,db,dcrb_org,calcvol)
                              end if
                              end if

                             suml = suml + llazmin+zug
                             sumvol = sumvol + calcvol
                             calcvol = 0.
                             count =  count + 1
                             h = h - llazmin-zug
                             if(h.lt.0.) h = 0.
                             db = lxz
                             

                     else
                         if (dcrb.eq.0) then
                           llasdmin = (h_org-suml)- (h_org-stoh(i))*(lasdmin(i)+rabf(i,lasdmin(i)))/db
                         else if (dcrb .gt. lasdmin(i)+rabf(i,lasdmin(i))) then
                           llasdmin = (h_org-suml)-(h_org-hbo)*(lasdmin(i)+rabf(i,lasdmin(i)))/dcrb
                         else if (dcrb.le.lasdmin(i)+rabf(i,lasdmin(i))) then
                           llasdmin = (hbo-suml)-hbo*(lasdmin(i)+rabf(i,lasdmin(i))-dcrb)/(db_org-dcrb)
                         end if
                         if(2*llasdmin.ge.lasfixl1(i)) then
                             llasdmin = lasfixl1(i)/2.
                         else if(2*llasdmin.ge.lasfixl2(i)) then
                             llasdmin = lasfixl2(i)/2.
                         end if

!calculation lx diameter at 2*llasdmin
                        if (dcrb .eq.0.) then
                            lx = db*(h_org-suml-llasdmin*2)/(h_org-stoh(i))
                        else if ((suml+2*llasdmin).lt. hbo) then

                             lx = dcrb + (db_org-dcrb)*(hbo-(suml+2*llasdmin))/hbo
                        else
                             lx = dcrb*(h_org- suml-2*llasdmin)/(h_org-hbo)
                        end if

! if flag_sort = 2 only lasfixl2 is used
                        if(2*llasdmin.ge.lasfixl1(i).and.lx.ge.laszmin(i)+rabf(i,laszmin(i)).and. flag_sort.ne.2) then
                             k = 'sg1'
                             if(zeig%coh%ntreem.ne.0.) then
                               standt = 'ab'
                               call out_tim(count,i,k,2*llasdmin + zug,lasdmin(i),lx, zeig%coh%ntreem, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreea.ne.0.) then
                               standt = 'vb'
                               call out_tim(count,i,k,2*llasdmin + zug,lasdmin(i),lx, zeig%coh%ntreea, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                              if(zeig%coh%ntreed.ne.0..and.flag_mg.ne.0.and.zeig%coh%diam.gt.tardiam_dstem) then
                              if(thin_flag1(1).ge.0) then
                               flag_deadsort = 1
                                standt = 'tb'
                               call out_tim(count,i,k,2*llasdmin + zug,lasdmin(i),lx, zeig%coh%ntreed, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             end if

                             count =  count + 1
                             suml = suml + 2*llasdmin + zug
                             sumvol = sumvol + calcvol
                             calcvol = 0.
                             h = h - 2*llasdmin-zug
                             if(h.lt.0.) h = 0.
                             db = lx
                             

! if flag_sort = 3 only lasfixl1 is unsed
                        else if (2*llasdmin.ge.lasfixl2(i).and.2*llasdmin.lt.lasfixl2(i) .and.lx.ge.laszmin(i)+rabf(i,laszmin(i)).and.flag_sort.ne.3) then
                             k = 'sg2'
                             if(zeig%coh%ntreem.ne.0.) then
                               standt = 'ab'
                               call out_tim(count,i,k,2*llasdmin + zug,lasdmin(i),lx, zeig%coh%ntreem, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreea.ne.0.) then
                               standt = 'vb'
                               call out_tim(count,i,k,2*llasdmin + zug,lasdmin(i),lx, zeig%coh%ntreea, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreed.ne.0..and.flag_mg.ne.0.and.zeig%coh%diam.gt.tardiam_dstem) then
                               if(thin_flag1(1).ge.0) then
                                flag_deadsort = 1
                               standt = 'tb'
                               call out_tim(count,i,k,2*llasdmin + zug,lasdmin(i),lx, zeig%coh%ntreed, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             end if

                             count =  count + 1
                             suml = suml +  2*llasdmin + zug
                             sumvol = sumvol + calcvol
                             calcvol = 0.
                             h = h - 2*llasdmin-zug
                             if(h.lt.0.) h = 0.
                             db = lx
                        end if
                     end if

               END IF     ! db.gt. laszmin(i)
             if(count.eq.count_old) exit
             END DO      
! end test 

! assortment LAS1a for pine
Do while((h.ge.lasfixl1(i).or.h.ge.lasfixl2(i)).and.i.eq.3)
                count_old = count
               IF(db .ge.las1zmin(i)+rabf(i,las1zmin(i)).and. db.gt.las1dmin(i)+ rabf(i,las1dmin(i))) THEN
                    if (dcrb .eq.0.) then
                       llazmin = (h_org-suml) -(h_org-stoh(i))*(las1zmin(i)+rabf(i,las1zmin(i)))/db
                    else if ( dcrb .gt. las1zmin(i)+rabf(i,las1zmin(i))) then
                       llazmin =(h_org-suml)- (h_org-hbo)*(las1zmin(i)+rabf(i,las1zmin(i)))/dcrb
                    else if (dcrb.le.las1zmin(i)+rabf(i,las1zmin(i))) then
                       llazmin = (hbo-suml) -hbo*(las1zmin(i)+rabf(i,las1zmin(i))-dcrb)/(db_org-dcrb)
                    end if
                    
                    if(llazmin.ge.lasfixl1(i)) then
                         if(flag_sort.eq.2) then
                            llazmin = lasfixl2(i)
                         else
                            llazmin = lasfixl1(i)
                         end if
                    else if(llazmin.ge.lasfixl2(i)) then
                        llazmin = lasfixl2(i)
                    end if
                    
! calculation of diameter lx  at llazmin/2
                    if (dcrb .eq.0.) then
                       lx = db*(h_org-(suml+llazmin/2))/(h_org-stoh(i))
                    else if ((suml+llazmin/2).lt. hbo) then

                        lx = dcrb + (db_org-dcrb)*(hbo-(suml+llazmin/2))/hbo
                    else
                        lx = dcrb*(h_org-(suml+ llazmin/2))/(h_org-hbo)
                    end if
                    
! calculation of diameter at llazmin
                             if (dcrb .eq.0.) then
                                lxz = db*(h_org-(suml+llazmin))/(h_org-stoh(i))
                             else if ((suml+llazmin).lt. hbo) then

                                lxz = dcrb + (db_org-dcrb)*(hbo-(suml+llazmin))/hbo
                             else
                                lxz = dcrb*(h_org-(suml+ llazmin))/(h_org-hbo)
                             end if
                             

! if flag_sort = 2 only lasfixl2 is used
                    help = las1dmin(i)+rabf(i,las1dmin(i))
                    if (llazmin.ge. lasfixl1(i).and. lx.ge. las1dmin(i)+rabf(i,las1dmin(i)).and.flag_sort.ne.2) then
                              k = 'sg1'

                             if(zeig%coh%ntreem.ne.0.) then
                               standt = 'ab'
                               call out_tim(count,i,k,llazmin+zug,lx,lxz, zeig%coh%ntreem,&
                                            standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreea.ne.0.) then
                               standt = 'vb'
                               call out_tim(count,i,k,llazmin+zug,lx,lxz, zeig%coh%ntreea,&
                                            standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreed.ne.0..and.flag_mg.ne.0.and.zeig%coh%diam.gt.tardiam_dstem) then
                               if(thin_flag1(1).ge.0) then
                                flag_deadsort = 1
                               standt = 'tb'
                               call out_tim(count,i,k,llazmin+zug,lx,lxz,zeig%coh%ntreed, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             end if

                             suml = suml + llazmin+zug
                             sumvol = sumvol +calcvol
                             calcvol =0.
                             count =  count + 1
                             h = h - llazmin-zug
                             if(h.lt.0.) h = 0.
                             db = lxz
                             

! if flag_sort = 3 only lasfixl1 is used
                     else if (llazmin.ge.lasfixl2(i).and.llazmin.lt.lasfixl1(i).and. lx.ge. las1dmin(i)+rabf(i,las1dmin(i)).and.flag_sort.ne.3) then
                             k = 'sg2'

                             if(zeig%coh%ntreem.ne.0.) then
                               standt = 'ab'
                               call out_tim(count,i,k,llazmin + zug,lx, lxz,zeig%coh%ntreem,standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreea.ne.0.) then
                               standt = 'vb'
                               call out_tim(count,i,k,llazmin + zug,lx, lxz,zeig%coh%ntreea,standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreed.ne.0..and.flag_mg.ne.0.and.zeig%coh%diam.gt.tardiam_dstem) then
                               if(thin_flag1(1).ge.0) then
                                flag_deadsort = 1
                               standt = 'tb'
                               call out_tim(count,i,k,llazmin+zug,lx,lxz,zeig%coh%ntreed, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             end if

                             suml = suml + llazmin+zug
                             sumvol = sumvol + calcvol
                             calcvol = 0.
                             count =  count + 1
                             h = h - llazmin-zug
                             if(h.lt.0.) h = 0.
                             db = lxz
                             

                     else
                         if (dcrb.eq.0) then
                           llasdmin = (h_org-suml)- (h_org-stoh(i))*(las1dmin(i)+rabf(i,las1dmin(i)))/db
                         else if (dcrb .gt. las1dmin(i)+rabf(i,las1dmin(i))) then
                           llasdmin = (h_org-suml)-(h_org-hbo)*(las1dmin(i)+rabf(i,las1dmin(i)))/dcrb
                         else if (dcrb.le.las1dmin(i)+rabf(i,las1dmin(i))) then
                           llasdmin = (hbo-suml)-hbo*(las1dmin(i)+rabf(i,las1dmin(i))-dcrb)/(db_org-dcrb)
                         end if
                         if(2*llasdmin.ge.lasfixl1(i)) then
                             llasdmin = lasfixl1(i)/2.
                         else if(2*llasdmin.ge.lasfixl2(i)) then
                             llasdmin = lasfixl2(i)/2.
                         end if

!calculation lx diameter at 2*llasdmin
                        if (dcrb .eq.0.) then
                            lx = db*(h_org-suml-llasdmin*2)/(h_org-stoh(i))
                        else if ((suml+2*llasdmin).lt. hbo) then

                             lx = dcrb + (db_org-dcrb)*(hbo-(suml+2*llasdmin))/hbo
                        else
                             lx = dcrb*(h_org- suml-2*llasdmin)/(h_org-hbo)
                        end if
                        

 ! if flag_sort = 2 only lasfixl2 is used
                       if(2*llasdmin.ge.lasfixl1(i).and.lx.ge.las1zmin(i)+rabf(i,las1zmin(i)).and.flag_sort.ne.2) then
                             k = 'sg1'

                             if(zeig%coh%ntreem.ne.0.) then
                               standt = 'ab'
                               call out_tim(count,i,k,2*llasdmin + zug,las1dmin(i),lx, zeig%coh%ntreem, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreea.ne.0.) then
                               standt = 'vb'
                               call out_tim(count,i,k,2*llasdmin + zug,las1dmin(i),lx, zeig%coh%ntreea, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreed.ne.0..and.flag_mg.ne.0.and.zeig%coh%diam.gt.tardiam_dstem) then
                               if(thin_flag1(1).ge.0) then
                                flag_deadsort = 1
                               standt = 'tb'
                               call out_tim(count,i,k,2*llasdmin + zug,las1dmin(i),lx,zeig%coh%ntreed, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             end if

                             count =  count + 1
                             suml = suml + 2*llasdmin + zug
                             sumvol = sumvol + calcvol
                             calcvol = 0.
                             h = h - 2*llasdmin-zug
                             if(h.lt.0.) h = 0.
                             db = lx
                             

! if flag_sort = 3 only lasfixl1 is used
                        else if (2*llasdmin.ge.lasfixl2(i).and.2*llasdmin.lt.lasfixl2(i) .and.lx.ge.las1zmin(i)+rabf(i,las1zmin(i)).and.flag_sort.ne.3) then
                             k = 'sg2'

                             if(zeig%coh%ntreem.ne.0.) then
                               standt = 'ab'
                               call out_tim(count,i,k,2*llasdmin + zug,las1dmin(i),lx, zeig%coh%ntreem, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreea.ne.0.) then
                               standt = 'vb'
                               call out_tim(count,i,k,2*llasdmin + zug,las1dmin(i),lx, zeig%coh%ntreea, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                              if(zeig%coh%ntreed.ne.0..and.flag_mg.ne.0.and.zeig%coh%diam.gt.tardiam_dstem) then
                               if(thin_flag1(1).ge.0) then
                                flag_deadsort = 1
                               standt = 'tb'
                               call out_tim(count,i,k,2*llasdmin + zug,las1dmin(i),lx,zeig%coh%ntreed, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             end if
                             count =  count + 1
                             suml = suml +  2*llasdmin + zug
                             sumvol = sumvol + calcvol
                             calcvol = 0.
                             h = h - 2*llasdmin-zug
                             if(h.lt.0.) h = 0.
                             db = lx
                        end if
                     end if
               END IF     ! db.gt. laszmin(i)
             if(count.eq.count_old) exit
             END DO     
! end test LAS1a for pine

! begin test industrial wood
			   Do while((h.ge.isfixl1(i).or.h.ge.isfixl2(i)).and.hbo.ne.0)
               count_old = count
               IF(db.gt.iszmin(i)+rabf(i,iszmin(i)).and.db.gt. isdmin(i)+rabf(i,isdmin(i))) THEN
                   help = iszmin(i)+rabf(i,iszmin(i))
                   
! calculation of length at diameter iszmin(i)
                    if (dcrb .eq.0.) then
                       liszmin = h_org -suml -(h_org-stoh(i))*(iszmin(i)+rabf(i,iszmin(i)))/db
                    else if ( dcrb .gt. iszmin(i)+rabf(i,iszmin(i))) then
                       liszmin = (h_org-suml)- (h_org-hbo)*(iszmin(i)+rabf(i,iszmin(i)))/dcrb
                    else if (dcrb.le.(i)+rabf(i,iszmin(i))) then
                       liszmin = (hbo-suml) -hbo*(iszmin(i)+rabf(i,iszmin(i))-dcrb)/(db_org-dcrb)
                    end if
                    if(liszmin.ge.isfixl1(i)) then
                        liszmin = isfixl1(i)
                    else if (liszmin.ge.isfixl2(i)) then
                       liszmin = isfixl2(i)
                    end if

 ! calculation of diameter lx  at liszmin/2
                    if (dcrb .eq.0.) then
                       lx = db*(h_org-suml-liszmin/2)/(h_org-stoh(i))
                    else if ((suml+liszmin/2).lt. hbo) then

                        lx = dcrb + (db_org-dcrb)*(hbo-(suml+liszmin/2))/hbo
                    else
                        lx = dcrb*(h_org-suml- liszmin/2)/(h_org-hbo)
                    end if
                    
! calculation of diameter at liszmin
                    if (dcrb .eq.0.) then
                       lxz = db*(h_org-suml-liszmin)/(h_org-stoh(i))
                    else if ((suml+liszmin).lt. hbo) then

                        lxz = dcrb + (db_org-dcrb)*(hbo-(suml+liszmin))/hbo
                    else
                        lxz = dcrb*(h_org-(suml+ liszmin))/(h_org-hbo)
                    end if
                    

! test industrial wood Fix length 1
                    if (liszmin.ge. isfixl1(i).and. lx.ge. isdmin(i)+rabf(i,isdmin(i))) then
                             k = 'in1'
                             if(zeig%coh%ntreem.ne.0.) then
                               standt = 'ab'
                               call out_tim(count,i,k,liszmin + zug,lx, lxz,zeig%coh%ntreem, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreea.ne.0.) then
                               standt = 'vb'
                               call out_tim(count,i,k,liszmin + zug,lx, lxz,zeig%coh%ntreea, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreed.ne.0..and.flag_mg.ne.0.and.zeig%coh%diam.gt.tardiam_dstem) then
                                 if(thin_flag1(1).ge.0) then
                                flag_deadsort = 1
                                standt = 'tb'
                               call out_tim(count,i,k,liszmin + zug,lx, lxz,zeig%coh%ntreed, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             end if

                             suml = suml + liszmin + zug
                             sumvol = sumvol + calcvol
                             calcvol = 0.
                             count =  count + 1
                             h = h - liszmin-zug
                             if(h.lt.0.) h = 0.
                             db = lxz
                             
 ! test industrial wood fix length 2 
                     else if (liszmin.ge.isfixl2(i).and.liszmin.lt.isfixl1(i).and. lx.ge. isdmin(i)+rabf(i,isdmin(i))) then
                             k = 'in2'
                             if(zeig%coh%ntreem.ne.0.) then
                               standt = 'ab'
                               call out_tim(count,i,k,liszmin + zug,lx, lxz,zeig%coh%ntreem, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreea.ne.0.) then
                               standt = 'vb'
                               call out_tim(count,i,k,liszmin + zug,lx, lxz,zeig%coh%ntreea, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                            if(zeig%coh%ntreed.ne.0..and.flag_mg.ne.0.and.zeig%coh%diam.gt.tardiam_dstem) then
                                 if(thin_flag1(1).ge.0) then
                                flag_deadsort = 1
                                standt = 'tb'
                               call out_tim(count,i,k,liszmin + zug,lx, lxz,zeig%coh%ntreed, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             end if

                             suml = suml + liszmin + zug
                             sumvol = sumvol + calcvol
                             calcvol =0.
                             count =  count + 1
                             h = h - liszmin-zug
                             if(h.lt.0.) h = 0.
                             db = lxz
                             

                     else

                        if (dcrb.eq.0) then
                           h1 = isdmin(i)
                           h2 = rabf(i,isdmin(i))
                           llasdmin = h_org - suml-(h_org-stoh(i))*(isdmin(i)+rabf(i,isdmin(i)))/db
                         else if (dcrb .gt. isdmin(i)+rabf(i,isdmin(i))) then
                           llasdmin = (h_org-suml)-(h_org-hbo)*(isdmin(i)+rabf(i,isdmin(i)))/dcrb
                         else if (dcrb.le.isdmin(i)+rabf(i,isdmin(i))) then
                           llasdmin = hbo-suml -hbo*(isdmin(i)+rabf(i,isdmin(i))-dcrb)/(db_org-dcrb)
                         end if

                         if(2*llasdmin.ge.isfixl1(i)) then
                            llasdmin = isfixl1(i)/2.
                         else if (2*llasdmin.ge.isfixl2(i)) then
                             llasdmin = isfixl2(i)/2.
                         end if

!calculation lx diameter at 2*lisdmin
                         if (dcrb .eq.0.) then
                            lx = db*(h_org -suml -llasdmin*2)/(h_org -stoh(i))
                         else if (2*llasdmin.lt. hbo) then
                             lx = dcrb + (db_org-dcrb)*(hbo-suml-llasdmin)/hbo
                         else
                             lx = dcrb*(h_org- llasdmin)/(h_org-hbo)
                         end if
                         
! test isfixl1 
                        if(2*lisdmin.ge.isfixl1(i).and.lx.ge.iszmin(i)+rabf(i,iszmin(i))) then
                             k = 'in1'
                             if(zeig%coh%ntreem.ne.0.) then
                               standt = 'ab'
                               call out_tim(count,i,k,2*lisdmin+zug,lx, isdmin(i), zeig%coh%ntreem, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreea.ne.0.) then
                               standt = 'vb'
                               call out_tim(count,i,k,2*lisdmin+zug,lx, isdmin(i), zeig%coh%ntreea, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreed.ne.0..and.flag_mg.ne.0.and.zeig%coh%diam.gt.tardiam_dstem) then
                                if(thin_flag1(1).ge.0) then
                                 flag_deadsort = 1
                                standt = 'tb'
                               call out_tim(count,i,k,2*lisdmin+zug,lx, isdmin(i), zeig%coh%ntreed, standt,h,hbo,db,dcrb_org,calcvol)
                              end if
                              end if

                             suml = suml + 2*lisdmin+zug
                             sumvol = sumvol + calcvol
                             calcvol = 0.
                             count =  count + 1
                             h = h - 2*lisdmin-zug
                             if(h.lt.0.) h = 0.
                             db = lx
                             
! test isfixl2 
                        else if (2*lisdmin.ge.isfixl2(i).and.2*lisdmin.lt.isfixl2(i) .and.lx.ge.iszmin(i)+rabf(i,iszmin(i))) then
                             k = 'in2'
                             if(zeig%coh%ntreem.ne.0.) then
                               standt = 'ab'
                               call out_tim(count,i,k,2*lisdmin+zug,lx, isdmin(i),zeig%coh%ntreem, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreea.ne.0.) then
                               standt = 'vb'
                               call out_tim(count,i,k,2*lisdmin+zug,lx, isdmin(i),zeig%coh%ntreea, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                              if(zeig%coh%ntreed.ne.0..and.flag_mg.ne.0.and.zeig%coh%diam.gt.tardiam_dstem) then
                                if(thin_flag1(1).ge.0) then
                                 flag_deadsort = 1
                              standt = 'tb'
                               call out_tim(count,i,k,2*lisdmin+zug,lx, isdmin(i),zeig%coh%ntreed, standt,h,hbo,db,dcrb_org,calcvol)
                             end if
                             end if

                             suml = suml + 2*lisdmin+zug
                             sumvol = sumvol + calcvol
                             calcvol = 0.
                             count =  count + 1
                             h = h - 2*lisdmin-zug
                             if(h.lt.0.) h = 0.
                             db = lx
                        end if
                     end if
              END IF      ! db .ge. iszmin
              if(count.eq.count_old) exit
               END DO      
               
! ende test industrial wood
               
! begin fuelwood
                   if (h.ne.0.and. db .ne.0) then
                             k = 'fue'
                             lx=0.
                             if(zeig%coh%ntreem.ne.0.) then
                               standt = 'ab'
                               if (suml.eq.stoh(i)) then
                                   
                         ! calculation of fuel wood in the case of total use of stem for fuel wood
                                    calcvol = (zeig%coh%x_sap + zeig%coh%x_hrt)/spar(i)%prhos/1000000.     !  kg DW/tree ---> m�/tree
                               else
!                        ! calculation of fuelwood volume from all stem segments and total volume of stem, error because stump is not considered
                                 calcvol = (zeig%coh%x_sap + zeig%coh%x_hrt)/spar(i)%prhos/1000000. - sumvol  ! m�/tree
                               end if
                               call out_tim(count,i,k,h,db, lx, zeig%coh%ntreem, standt,h_org,hbo,db,dcrb_org,calcvol)
                             end if
                             if(zeig%coh%ntreea.ne.0.) then
                               if(suml.eq.stoh(i)) then
                         ! calculation of fuel wood in the case of total use of stem for fuel wood
                                 calcvol = (zeig%coh%x_sap + zeig%coh%x_hrt)/spar(i)%prhos/1000000.     !  kg DW/tree ---> m�/tree
                               help = zeig%coh%x_sap + zeig%coh%x_hrt
                               else
                         ! calculation of fuelwood volume from all stem segments and total volume of stem, error because stump is not considered
                                 calcvol = (zeig%coh%x_sap + zeig%coh%x_hrt)/spar(i)%prhos/1000000. - sumvol  ! m�/tree
                               end if
                               standt = 'vb'
                               call out_tim(count,i,k,h,db, lx, zeig%coh%ntreea, standt,h_org,hbo,db,dcrb_org,calcvol)
                             end if
                            if(zeig%coh%ntreed.ne.0..and.flag_mg.ne.0.and.zeig%coh%diam.gt.tardiam_dstem) then
                                 if(thin_flag1(1).ge.0) then
                                 if(suml.eq.stoh(i)) then
                         ! calculation of fuel wood in the case of total use of stem for fuel wood
                                 calcvol = (zeig%coh%x_sap + zeig%coh%x_hrt)/spar(i)%prhos/1000000.     !  kg DW/tree ---> m�/tree
                               help = zeig%coh%x_sap + zeig%coh%x_hrt 
                               else
                         ! calculation of fuelwood volume from all stem segments and total volume of stem, error because stump is not considered
                                 calcvol = (zeig%coh%x_sap + zeig%coh%x_hrt)/spar(i)%prhos/1000000. - sumvol  ! m�/tree
                               end if
                                flag_deadsort = 1
                                standt = 'tb'
                               call out_tim(count,i,k,h,db, lx, zeig%coh%ntreed, standt,h_org,hbo,db,dcrb_org,calcvol)
                               end if
                               end if
                             count =  count + 1
                    end if
             end if
         zeig => zeig%next
     end do
 end do



end subroutine timsort

subroutine out_tim(cou,nr,k, len, d,zapf, anz,standt,h,hbo,db,dcrb,calcvol)
use data_tsort
use data_simul
use data_par

!***************** wpm ************************
use data_wpm
use data_stand
use data_species
use data_manag
!***************** wpm ************************
type(mansort_type) :: mansort_ini

integer nr,cou

real    len, d, anz,zapf, volume, v1,v2,r,r1,rc,vhelp
real (KIND = dg)  :: calcvol
character(4)  k
character(2) standt
type(timber)    ::tim_ini

 tim_ini%year = time
 tim_ini%count= cou
 tim_ini%ttype = k
 tim_ini%specnr = nr
 tim_ini%length = len
 tim_ini%dia = d
 tim_ini%diaor = d -rabf(nr,d)
if(tim_ini%diaor.lt.0) tim_ini%diaor=0
!calculaiton of volume for  stem segment, depending on the charcteristics (cone, 2 cones, or frustum of a cone)
! cone: vol=1./3.(pi*h*r�)
! frustum: vol = pi*h(r1�+r1*r2+r2�)/3
 r = db*0.5
 r1 = zapf*0.5
 rc = dcrb*0.5
 if(k.eq. 'ste') then
     if((len + 10.).lt.hbo) then
       volume =( pi*len*(r*r + r*r1 + r1*r1)/3.)/1000000.    ! frustum
     else
       v1 = pi*(hbo-stoh(nr))*(r*r +r*rc + rc*rc)/3.
       v2 = pi*(len-stoh(nr)-hbo)*(rc*rc+ rc*r1 + r1*r1)/3.
       volume = (v1+v2)/1000000.
     end if
 else if (k.eq.'fue'.and.hbo.ne.0)then
     if( db.gt.dcrb) then
        if(len.lt.(h-hbo)) then
          volume = ( pi * len* r*r/3 )/1000000.
        else
         v1 = pi* (len-h+hbo)*(r*r + r*rc + rc*rc)/3.  ! frustum
         vhelp = pi*hbo*(r*r + r*rc + rc*rc)/3.
         v2 = pi* (h-hbo)* rc*rc/3.                              ! cone

         volume = (v1+v2)/1000000.
        end if
     else
         volume = (pi*len*r*r/3.)/1000000.
     end if
  else if (k.eq.'fue'.and.hbo.eq.0)then

         volume = (pi*len*r*r/3.)/1000000.
 else
! stem timber  or industrial timber 

    if(hbo .eq.0.) then
       volume = (pi*len*r*r/3.)/1000000.
    else
       volume = ( pi*len*(r*r +r*r1 + r1*r1)/3.)/1000000.
    end if
 end if
if( volume.lt.0) then
  volume = volume
end if

 if(calcvol.eq.0.) then
         tim_ini%vol = volume
         calcvol = volume
  else
          tim_ini%vol = volume
  end if
 tim_ini%zapfd = zapf
 tim_ini%zapfdor = zapf - rabf(nr,zapf)
 if ( tim_ini%zapfdor.lt.0) tim_ini%zapfdor=0.
 tim_ini%tnum = anz
 tim_ini%stype = standt
 tim_ini%hei_tree = h
 tim_ini%hbo_tree = hbo
 tim_ini%dcrb = dcrb

 IF (.not. associated(st%first)) THEN
          ALLOCATE (st%first)
          st%first%tim = tim_ini
          NULLIFY(st%first%next)
          anz_list = 1
 ELSE
          ALLOCATE(ztim)
          ztim%tim = tim_ini
          ztim%next => st%first
          st%first => ztim
          anz_list = anz_list +1
 END IF

!***************** wpm ************************
 ! information needed for wpm
if ( flag_wpm > 0 .and. (tim_ini%stype .eq. 'ab'.or.tim_ini%stype .eq. 'tb')) then
   if (flag_manreal.eq.1.and.maninf.ne.'tending'.and.maninf.ne.'brushing') then
	mansort_ini%year		= tim_ini%year
	mansort_ini%count		= tim_ini%count
	mansort_ini%spec		= tim_ini%specnr
	mansort_ini%typus		= tim_ini%ttype
	mansort_ini%diam		= tim_ini%dia
	mansort_ini%diam_wob	= tim_ini%diaor
	mansort_ini%volume		= (tim_ini%vol/kpatchsize)*10000.   ! m�/patchsize ---> m3/ha
    mansort_ini%dw =  (tim_ini%vol/kpatchsize)*10000*spar(tim_ini%specnr)%prhos*1000000.*cpart   ! m�/patchsize ---> kg C/ha
	mansort_ini%number		= tim_ini%tnum

	if (.not. associated(first_mansort)) then
	  allocate (first_mansort)
	  first_mansort%mansort = mansort_ini
	  nullify(first_mansort%next)
	  else
	  ! build new mansort object	
	  allocate(act_mansort)
	  act_mansort%mansort = mansort_ini
	  ! chain into the list 
	  act_mansort%next => first_mansort
	  ! set the first pointer to the new object
	  first_mansort => act_mansort
	 end if
   end if
end if

 ! information needed for sea or wpm+sea
if ( (flag_wpm == 2 .or. flag_wpm == 3) .and. tim_ini%stype .eq. 'vb') then
	mansort_ini%year		= tim_ini%year
	mansort_ini%count		= tim_ini%count
	mansort_ini%spec		= tim_ini%specnr
	mansort_ini%typus		= tim_ini%ttype
	mansort_ini%diam		= tim_ini%dia
	mansort_ini%diam_wob	= tim_ini%diaor
	mansort_ini%volume		= (tim_ini%vol/kpatchsize)*10000.   ! m�/patchsize ---> m3/ha
    mansort_ini%dw =  (tim_ini%vol/kpatchsize)*10000*spar(tim_ini%specnr)%prhos*1000000.*cpart   ! m�/patchsize ---> kg C/ha
	mansort_ini%number		= tim_ini%tnum

	if (.not. associated(first_standsort)) then
	  allocate (first_standsort)
	  first_standsort%mansort = mansort_ini
	  nullify(first_standsort%next)
	 else
	  ! build new mansort object	
	  allocate(act_standsort)
	  act_standsort%mansort = mansort_ini
	  ! chain into the list
	  act_standsort%next => first_standsort
	  ! set the first pointer to the new object
	  first_standsort => act_standsort
	end if
end if

!***************** wpm ************************


end subroutine out_tim

subroutine out_timlist
use data_tsort
use data_simul
integer timunit

timunit = getunit()

open (timunit,file = 'timlist.dat', status='unknown')

write( timunit,*)  ' year  ','count ',' spec','type ',' length','  diameter', 'diam wo bark', 'top diam. ',' top d. wo bark','Volume(m�)','  number '
  ztim=>st%first
  do
    IF (.not.ASSOCIATED(ztim)) exit
     write(timunit,'(3I6,1x,A5,1x,F8.3,1x,f7.3,1x,f7.3,1x,f7.3,1x,f7.3,1x,f7.3,1x,f10.2)') ztim%tim%year, ztim%tim%count,  &
              ztim%tim%specnr,ztim%tim%ttype,ztim%tim%length,ztim%tim%dia,ztim%tim%diaor,ztim%tim%zapfd,ztim%tim%zapfdor, ztim%tim%vol,ztim%tim%tnum
    ztim=>ztim%next
  end do
close(timunit)

end subroutine out_timlist

real function rabf(spec, db)
! calculation of rabz i.A.
use data_tsort
use data_species
integer iz, spec

do iz = 1,nspec_tree

   if(iz.eq.spec) then
     if(db.lt.rabth(spec,1)) then
          rabf = rabz(spec,1)
     else if (db.ge.rabth(spec,1).and. db.lt.rabth(spec,2)) then
          rabf = rabz(spec,2)
     else
          rabf = rabz(spec,3)
     end if

   end if

end do

end  function rabf