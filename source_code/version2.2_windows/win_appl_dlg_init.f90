!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                    Subroutines for:                           *!
!*                   - windows shell -                           *!
!*                                                               *!
!*  contains:                                                    *!
!*   InitMain                                                    *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/XXXXXXXXXXXXXXXXXXXXX     *!
!*                                                               *!
!*****************************************************************!

SUBROUTINE InitMain(dlgParent)

use dflogm
use data_simul

type (dialog) dlg
type (dialog) dlgParent
type (dialog) local_dlg

external Start, Cancel

include '4C_dialogs.fd'

logical retlog
character(150) dir
integer nflag
character(90), dimension(5) :: charflag
character(20) text, helpsim

	local_dlg = dlgParent

	if ( .not. DlgInit( idd_4C_main, dlg ) ) then
		write (*,*) "error: resource not found"
	else

	 	! set functionality of the OK and CANCEL button
		retlog = DlgSetSub( dlg, IDSTOP, Cancel )
		retlog = DlgSetSub( dlg, ID_START_4C, Start )
	endif 	
	
	retint = DlgModal(dlg)

END SUBROUTINE InitMain
