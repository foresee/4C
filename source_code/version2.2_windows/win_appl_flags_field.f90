!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                    Subroutines for:                           *!
!*                                                               *!
!*    - data_general_dlg: module to store the general dlg info	 *!
!*    - flag_field :      module to store the flags				 *!
!*    - flags_comments:   module to store the flags commens		 *!
!*                                                               *!                                 
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/XXXXXXXXXXXXXXXXXXXXX     *!
!*                                                               *!      
!*****************************************************************!

module data_general_dlg
	
	! which way was chosen
	integer :: id_ctr	

end module

!***********************************************************************

! data module containig the flags changes for the win_appl
module flag_field

	integer,allocatable,save,dimension(:,:)	::	flagsave
	character(50), dimension(22) :: flagnames	
	integer act_run

    end ! module flags_field

!**********************
    
!contains all flag comments for the combo-boxes
module flags_comments
	character(60), dimension(30) :: flag_mort_com
	character(60), dimension(30) :: flag_reg_com
	character(60), dimension(30) :: flag_forska_com
	character(60), dimension(30) :: flag_stand_com
	character(60), dimension(30) :: flag_sveg_com
	character(60), dimension(30) :: flag_mg_com
	character(60), dimension(30) :: flag_dis_com
	character(60), dimension(30) :: flag_light_com
	character(60), dimension(30) :: flag_folhei_com
	character(60), dimension(30) :: flag_volfunc_com
	character(60), dimension(30) :: flag_resp_com
	character(60), dimension(30) :: flag_limi_com
	character(60), dimension(30) :: flag_decomp_com
	character(60), dimension(30) :: flag_sign_com
	character(60), dimension(30) :: flag_wred_com
	character(60), dimension(30) :: flag_wurz_com
	character(60), dimension(30) :: flag_cond_com
	character(60), dimension(30) :: flag_int_com
	character(60), dimension(30) :: flag_eva_com
	character(100), dimension(30) :: flag_CO2_com
	character(60), dimension(30) :: flag_sort_com
	character(60), dimension(30) :: flag_wpm_com
	character(60), dimension(30) :: flag_stat_com
end

