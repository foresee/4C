!*****************************************************************!
!*                                                               *!
!*                4C (FORESEE) Simulation Model                  *!
!*                                                               *!
!*                                                               *!
!*              Post Processing for 4C (FORESEE)                 *!
!*						Subroutines:		                     *!
!*	- wpm:   control subroutine for wpm							 *!
!*	- calculate_output: wood production model					 *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

subroutine wpm()

use data_simul
use data_wpm

implicit none
	

character(150) mansortFile, manrecFile, spinupFile, file

	
	! begin program
	call setFlags

	! input
    if(flag_wpm.eq.5.or. flag_wpm.eq.4 .or.flag_wpm.eq.6) then
	    wob = .FALSE.
	    mansortfile = 'input/wpm_mansort.ini'
		manrecfile = 'input/wpm_manrec.ini'
     	call read_mansort(mansortFile, manrecFile)
	end if
    if(flag_wpm.eq.4) output_spinup = .TRUE.
	if(flag_wpm.eq.6) then
	
	    spinup_on = .TRUE.
		spinupFile = 'input/spinup.wpm'
		 

     end if
	
	call allocate_in_output
	call ini_input

	! simulation
	if ( associated(first_mansort) ) then
		! wood processing
		call calculate_product_lines
		if (debug) then
			file = trim(dirout) // 'calculate_prod_lines.wpm'
			call write_product_lines(file)
		end if
	end if

	call calculate_wood_processing
	
	if (debug) then
		file = trim(dirout) // 'calculate_wood_proc.wpm'
		call write_product_lines(file)
	end if

	! use categories
	call calculate_use_categories
	if (debug) then
		file = trim(dirout) // 'calculate_use_cat.wpm'
		call write_wpm_output()
	end if

	! ouput for every year
	if ( spinup_on ) call read_spinup(spinupFile)
	call calculate_output

!	call write_wpm_output()
	
	if (output_spinup) then
		file = trim(dirout) // 'spinup.wpm'
		call write_spinup(file)
	end if

    end subroutine wpm



!***********************************************************
! simulation: lifespan - recycling, burning, atmosphere, landfill

subroutine calculate_output

use data_wpm

implicit none

integer i, j, k, l
real rec_value, burn_value, land_value, rest, sum_rest, sum_out
real func, func1, func2
real, dimension(nr_use_cat) :: a, b, c, d
real, dimension(nr_use_cat) :: val
integer age
! stores the pieces of wood, dimensions: 2, max_age of every use category	
! %pieces(1,:) stores values for the actual year
! %pieces(2,:) stores new calculated values after calculating the recycling part for the actual year
type store_wood
	real, pointer, dimension(:,:) :: pieces
	real :: rec_value
end type store_wood
! list of store_wood arrays of dimension of number of use category
type(store_wood), allocatable, dimension(:) :: wood_pieces
	
	allocate(wood_pieces(nr_use_cat))

	! simulation: recycling, burning, atmosphere, landfill
	! allocate wood_pieces for use categories, get the parameters
	do j=1,nr_use_cat

		allocate(wood_pieces(j)%pieces(2,max_age(j)))

		wood_pieces(j)%pieces(:,:) = 0.

		a(j)	= use_categories(j)%lifespan_function%a
		b(j)	= use_categories(j)%lifespan_function%b
		c(j)	= use_categories(j)%lifespan_function%c
		d(j)	= use_categories(j)%lifespan_function%d

	end do

      sub_material = 0.

	do i=1,size(years)
		
		! set used values to 0
		burn_value	= 0.
		land_value	= 0.
		val = 0.

		do j = 1, nr_use_cat
			wood_pieces(j)%rec_value = 0.
		end do
		
		! for each use category
		do j=1, nr_use_cat
			! put the calculated values from the last year
			! fill the wood_pieces with values from the categories
			if ( spinup_on .and. i == 1 ) then
				wood_pieces(j)%pieces(2,:) = use_categories(j)%spinup(:)	
			end if
	
			wood_pieces(j)%pieces(1,:) = 0.
			wood_pieces(j)%pieces(1,:) = wood_pieces(j)%pieces(2,:)
			
			! spinup output
			if (output_spinup .and. i == size(years)) then
				use_categories(j)%spinup(:) = wood_pieces(j)%pieces(1,:)
			end if

		
			
			wood_pieces(j)%pieces(1,1) = wood_pieces(j)%pieces(1,1) + use_categories(j)%value(i)
			use_categories(j)%value(i) = 0.
			
			! set used values to 0
			wood_pieces(j)%pieces(2,:) = 0.
			rec_value	= 0.
			func		= 0.
			rest		= 0.
			sum_rest	= 0.


			! for all wood pieces: sum up recycling, burning, output
			do k=1,max_age(j)-1
				! lifespan function: percentual quotient of the REMAINING wood
				age = k-1
				if ( age > 1) then
					func1 = d(j) - a(j) / ( 1 + ( b(j) * EXP( -c(j) * age ) ) )	
					func2 = d(j) - a(j) / ( 1 + ( b(j) * EXP( -c(j) * (age-1) ) ) )	
					func  = func1 / func2
				else
					func = d(j) - a(j) / ( 1 + ( b(j) * EXP( -c(j) * age ) ) )
					func = func / 100	
				endif
				rest = wood_pieces(j)%pieces(1,k) * (func)
				
				! sum up the remaining pieces
				sum_rest	= sum_rest + rest
					
				! calculated wood pieces into wood_pieces(j)%pieces(1,k) or (if too old) into the recycling
				wood_pieces(j)%pieces(2,k+1) = rest
						
			end do

			! all the wood per year:		
			! sum_rest + (rec_value + land_value + burn_value)
			sum_out = sum(wood_pieces(j)%pieces(1,:)) - sum_rest 	
			rec_value	= sum_out * use_categories(j)%rec_par(1)
			land_value	= land_value + sum_out * use_categories(j)%rec_par(2)
			burn_value	= burn_value + sum_out * use_categories(j)%rec_par(3)
			
			! use recycling parameters to calculate this year use_categories values from rec_value
			if ( i <= size(years) ) then
				do l=1,nr_use_cat
					wood_pieces(l)%rec_value	= wood_pieces(l)%rec_value + rec_value * use_categories(j)%rec_use_par(l)
					val(l) = val(l) + rec_value * use_categories(j)%rec_use_par(l)	
				end do
			end if
			
            ! calculate material substitution for use categorie 1 and 2 from recycling
            ! sub_material(i) = sub_material(i) + val(1) + val(2)
		      if (j.eq.1 ) then
                         sub_material(i) = sub_material(i) + (wood_pieces(j)%pieces(1,1) + rec_value) * sub_par(3)
                    elseif (j.eq.2) then
                         sub_material(i) = sub_material(i) + ((wood_pieces(j)%pieces(1,1) + rec_value) * sub_par(3))
                    endif

			! store the output for the year and use category
			val(j) = val(j) + sum_rest
			

		end do
		
		! fill the final values to the use_categories
		do j=1, nr_use_cat
			use_categories(j)%value(i)	= val(j)
			wood_pieces(j)%pieces(2,2)	= wood_pieces(j)%pieces(2,2) + wood_pieces(j)%rec_value
			
			! store the last wood_pieces as spinup value
			if (output_spinup .and. i == size(years)) then
				use_categories(j)%spinup(1) = use_categories(j)%spinup(1) + wood_pieces(j)%rec_value
			end if
			
		end do
		
		! sum up the use categories
		sum_use_cat(i)	= sum(val)
		
		burning(i)	= burning(i) + burn_value

              ! calculate energy substitution
		sub_energy(i)	= burning(i) * sub_par(2)

              ! calculate sum of harvest emission, energy and material substitution
	        sub_sum(i) = emission_har(i) + sub_energy(i) + sub_material(i)

		
		! fill the landfill spinup value into the first year
		if ( spinup_on .and. i == 1) landfill(i) = landfill(i) + landfill_spinup
		
		! landfill values
		if ( i > 1 ) then
			landfill(i)	= landfill(i-1)*0.995 + land_value
		endif

	end do
		
	! store landfill spinup value
	landfill_spinup = landfill( size(years) ) 		 	
	
	! write atmosphere: summation of burning values per year
	do i=1, size(years)
		if (i == 1) then
			atmo_cum(i)		= atmo_cum(i) + burning(i)
			atmo_year(i)	= atmo_year(i) + burning(i)
		else
			atmo_cum(i)		= atmo_cum(i) + atmo_cum(i-1) + landfill(i-1)*0.005 + burning(i)
			atmo_year(i)	= atmo_year(i) + landfill(i-1)*0.005 + burning(i)
		end if
	end do
	
	
	! deallocate wood_pieces
	do j=1,nr_use_cat
		deallocate(wood_pieces(j)%pieces)
	end do
	deallocate(wood_pieces)

end subroutine calculate_output
