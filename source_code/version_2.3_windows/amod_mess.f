!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                                                               *!
!* data module for statistics with observed and simulated values *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!
module data_mess

character(150):: dirmess = 'mess/'    ! directory of files with measurements

integer:: anz_mesf = 1   ! amount of measurement files 
character(150),allocatable,dimension(:) :: mesfile ! name of files with measurements

logical:: flag_mess        ! TRUE: measurements within the simulation period / FALSE: no measurements

integer:: unit_cons        ! console unit
integer:: unit_stat        ! output unit for statistical analysis
integer:: unit_mess = -99  ! unit of file with measurements
integer:: unit_mout = -99  ! output unit of file with measurements and residuals
integer unitday, unitsum, unitlit, unittemp, unitwater, unitsoil, unitsoilini, unitcbal, &
        unitveg, unitveg_pi, unitveg_sp, unitveg_bi
integer,allocatable,save,dimension(:)   :: unit_mon      ! array of output unit numbers for monthly values
integer,allocatable,save,dimension(:)   :: unit_mon_stat ! array of output unit numbers for statistics of monthly values

integer imkind,    & ! amount of read maesurement value typs
        tkind,     & ! chronological resolution of measurement values( 1 - Tage
                     !                                      2 - Jahre)
        imess,     & ! amount of read measuerment values
        anz_val,   & ! amount of filled in measurement values
        imk_nme,   & ! amount of measurment numbers for mean value calculation NME
        imk_nmae,  & ! amount of measure numbers for mean value calculation NMAE
        imk_nrmse, & ! amount of measure numbers for mean value calculation NRMSE
        imk_rsq      ! amount of measure numbers for mean value calculations RSQ

real,allocatable,dimension(:,:):: mess1, mess2, sim1, help2 
integer,allocatable,dimension(:,:):: mtz     ! arry for dates of measurements: day of the year, year
integer,allocatable,dimension(:,:):: help1, stz
integer,allocatable,dimension(:):: app

real:: &
       nme_av,      & ! Average normalised mean error 
       nmae_av,     & ! Average normalised mean absolut error
       nrmse_av,    & ! Average normalised root mean square error
       pme_av,      & ! Average mean precental error
       prmse_av,    & ! Average mean squared percental error
       tic_av,      & ! Average Theil's inequality coefficient
       meff_av,     & ! Average modell efficiency
       rsq_av         ! Average coefficient of determination


type res_struct 
  character(15) :: mkind     ! measurement value type
  integer :: imes            ! amount of measurement value 
  integer :: tkind           ! chronological resolution of measurement values
  integer,pointer,dimension(:):: day, year
  real,pointer,dimension(:) :: resid
  real,pointer,dimension(:) :: sim
  real,pointer,dimension(:) :: mess
end type res_struct 

type (res_struct),allocatable,dimension(:),target :: val

integer:: ikind = 50  ! amount of allowed measurement value types
integer:: fkind = 0   ! amount of not defined measurement value types

character(10), dimension(50):: sim_kind  

integer, dimension(80):: mpos1, mpos2   ! position of measurement value in input file
integer, dimension(80):: spos1, spos2   ! position of variables in simulations output 
integer, dimension(80):: opos1, opos2   ! position of variables in simulation output file

end module data_mess

!**************************************************************

