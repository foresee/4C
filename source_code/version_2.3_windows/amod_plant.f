!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                                                               *!
!*      data module for planting of seedlings/saplings           *!
!*    ! arrays have to be adapted to the species number !        *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

module data_plant

 integer                    :: quspec= 2                                ! number of planted species
 integer, dimension(13)      :: infspec=(/1,0,1,0,0,0,0,0,0,0,0,0,0/)                 ! sign of planted species = 0/1
 integer, dimension(13)      :: npl_mix =(/3000,0,6000,4500,0,0,0,0,0,0, 0, 0, 0/)          ! number of plants in mixed stands
 integer, dimension(13)      :: numplant=(/178,6000,8000,9000,10000,0,8000,1666, 1140, 2000, 5000, 0, 0/)     ! number of plants per ha
 integer, dimension(13)      :: specpl=(/1,2,3,4,5,6,7,8, 9, 10, 11, 12, 13/)    ! number of species
 real, dimension(13)         :: plant_height=(/130.,37.5,17.5,40.,8.7,0.,17.5,40.,17.5, 30.0, 30.0, 0., 0./)  ! mean height of plants
 real, dimension(13)         :: plant_hmin=(/70.,25.,10.,30.,3.,0.,10.,30., 10., 20., 10., 0.,0./)    ! minimum height of plants
 real, dimension(13)         :: hsdev=(/3.33,4.1,7.5,3.33,5.9,0.0,7.5,3.33, 7.5,4.1, 7.5, 0.,0. /)      ! standard deviation od height
 real, dimension(13)         :: pl_age=(/10.,4.,2.,2.,1.,0.,2.,1.,2.,2., 2., 0.,0./)     ! age of plants
 real                       :: kappa = 1.2 !1.2 
 real                       :: ksi = 2.99 !1.5
 integer, dimension(11,10)  :: m_numplant
 integer, dimension(11,10)  :: m_specpl
 real, dimension(11,10)     :: m_plant_height
 real, dimension(11,10)     :: m_plant_hmin
 real, dimension(11,10)     :: m_pl_age
 real, dimension(11,10)     :: m_hsdev
 integer                    :: m_numclass

end module data_plant

