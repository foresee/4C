
!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                                                               *!
!*              data module for site data                        *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!                                               *!
MODULE data_site

    INTEGER               :: patch_id      ! Patch identifier
    character(50)         :: stand_id      ! Stand identifier
    REAL                  :: xlat          ! latitude in radians
    REAL                  :: lat = 52.24   ! Default Potsdam coordinates			
    REAL                  :: long  = 13.04
    REAL, DIMENSION(:), ALLOCATABLE    :: latitude     ! array of latitudes for multi run 8
    REAL, ALLOCATABLE, DIMENSION(:)    :: NHdep  ! yearly deposition
    REAL, ALLOCATABLE, DIMENSION(:)    :: NOdep  ! yearly deposition
    INTEGER, ALLOCATABLE, DIMENSION(:) :: gwtable  ! groundwater level class
                                                  ! 1: 0   - 0.5 m
                                                  ! 2: 0.5 - 1.0 m
                                                  ! 3: 1.0 - 1.5 m
                                                  ! 4: 1.5 - 2.0 m
                                                  ! 5: > 2.0 m

    character(50),ALLOCATABLE, DIMENSION(:) ::  sitenum
! KLara
    character(50),ALLOCATABLE, DIMENSION(:) ::  clim_id
! �WK
    CHARACTER(13),ALLOCATABLE, DIMENSION(:) ::  soilid

END module data_site

