!*****************************************************************!
!*                                                               *!
!*              Post Processing for 4C (FORESEE)                 *!
!*                                                               *!
!*                                                               *!
!*                  Modules and Subroutines:                     *!
!*                                                               *!
!*      data_mansort:  module to store the mansort, manrec input *!
!*      wood_processing:  module to store wood processing infos	 *!
!*	    wpm_output:  module to store simulation output			 *!
!*	    lifespan_par: module to store lifespan parameters		 *!
!*      ini_input: initialize the values of the modules		     *!
!*      allocate_in_output: allocates module values              *!
!*      deallocate_in_output: deallocates module values          *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!
! module contains informations from mansort file: "removals"
! module contains information for "roundwood" and "after processing" steps
! module contains lifespan function parameters
! module contains wmp output
module data_wpm
!***************************************************************
! module contains informations from mansort file: "removals"
!                           cm		cm      cm			cm      cm			m�/ha    kg C/ha
!# year   count  spec type  len     diam    diam wob	top_d   t_d wob		Volume   DW			number

	type mansort_type
	 integer		:: year, count, spec, number
	 character(4)	:: typus
	 real			:: diam, volume, dw, diam_wob
	end type mansort_type

	type manrec_type
	 integer		:: year, measure
	 character(28)	:: management
	end type manrec_type

	type  mansort_obj
		type(mansort_type)		    :: mansort
		type(mansort_obj), pointer :: next
	end type mansort_obj

	type  manrec_obj
		type(manrec_type)		    :: manrec
		type(manrec_obj), pointer   :: next
	end type manrec_obj

	! pointer to the the mansort, manrec lists, sea list
	type(mansort_obj), pointer	:: first_mansort
	type(manrec_obj) , pointer	:: first_manrec
	type(mansort_obj), pointer	:: first_standsort
    ! pointer variable for manipulating mansort, manrec lists, sea list
	type(mansort_obj), pointer	:: act_mansort
	type(manrec_obj) , pointer	:: act_manrec
	type(mansort_obj), pointer	:: act_standsort
	! years from the manrec file with needed management
	integer, allocatable, save, dimension(:) :: management_years
	integer :: nr_pr_ln	
	
	! number of simulation years, management (manrec) years and wpm relevant management years
	integer		:: nr_years
	integer		:: nr_management_years
	integer		:: wpm_manag_years
	
	! sea: number of timber grades, number of tree species
	integer		:: nr_timb_grades
	integer		:: nr_spec
	
!***************************************************************
! module contains information for "roundwood" and "after processing" steps
!***************************************************************
    
	! value: carbon per simulation year
	! proc_par: parameters for the processing
	! use_par:  parameters for the "use categories" distribution
	type wood_type
		real, pointer, dimension(:) :: value	
		real, dimension(3,7) :: proc_par
		real, dimension(7) :: use_par
	end type wood_type

	! for each simulation year
	! product lines: sawntimber_sw, sawntimber_hw (softwood, hardwood),
	! plywood, particle_board, chem_pulpwood, mech_pulpwood, fuelwood
	type(wood_type), allocatable, save, dimension(:) :: product_lines

	! save the results by the procentual sorting of product lines
	! three sortings, product lines, years
	real, allocatable, save, dimension(:,:,:) :: pl
	
	! with or without wob
	logical :: wob

!***************************************************************
! module contains lifespan function parameters
!***************************************************************
	type lifespan_type
		real hl
		real a, b, c, d
	end type lifespan_type

	type(lifespan_type) :: short_lifespan
	type(lifespan_type) :: medium_short_lifespan
	type(lifespan_type) :: medium_long_lifespan
	type(lifespan_type) :: long_lifespan

!***************************************************************
! module contains wmp output
!*************************************************************** 
    
	integer, allocatable, save, dimension(:) :: years
	integer :: nr_use_cat
	integer, allocatable, dimension(:) :: max_age
	! use_categories: building_materials, other_building_materials, structural_support,
	! 					furnishing, packing_materials, long_life_paper, short_life_paper
	! per simulation year
	! rec_par: recycling, landfill, burning parameter of use
	type use_categories_type
		type(lifespan_type) :: lifespan_function
		real, pointer, dimension(:) :: value
		real, dimension(3) :: rec_par
		real, dimension(7) :: rec_use_par
		! spin up values
		real, pointer, dimension(:) :: spinup	
	end type use_categories_type
	
	! spin up value
	real :: landfill_spinup

	! spinup_on
	logical :: spinup_on
	! debug and spinup output
	logical :: debug
	logical :: output_spinup

	! list of use_cateories, sum of use categories per year, use_cat at the beginning
	type(use_categories_type), allocatable, save, dimension(:)	:: use_categories
	real, allocatable, save, dimension(:)	:: sum_use_cat
	real, allocatable, save, dimension(:)	:: sum_input
	real, allocatable, save, dimension(:,:) :: use_cat
	
	real, allocatable, save, dimension(:)	:: burning
	real, allocatable, save, dimension(:)	:: landfill
	! atmosphere per year, atmosphere cummulative
	real, allocatable, save, dimension(:)	:: atmo_year
	real, allocatable, save, dimension(:)	:: atmo_cum

!******************** substitution ********************	
	real, allocatable, save, dimension(:)	:: emission_har, sub_energy, sub_material, sub_sum
	real, dimension (3) :: sub_par

!********************** sea ****************************
	! sea timber grades:
	! _tg(tree species, timber grade, year)
	real, allocatable, save, dimension(:,:,:) :: mansort_tg
	real, allocatable, save, dimension(:,:,:) :: standsort_tg

	! prices (spec, timber grades)
	real, allocatable, save, dimension(:,:)	:: chainsaw_prices
	real, allocatable, save, dimension(:,:)	:: harvester_prices
	real, allocatable, save, dimension(:)	:: planting_prices
	real, allocatable, save, dimension(:,:)	:: planting_sub
	real, allocatable, save, dimension(:,:)   :: fence
	real, dimension(2)	:: fix
	real, dimension(2)	:: brushing
	real, dimension(2)	:: tending_prices
	real, dimension(2,2) :: ext_for
	real, dimension(4)	:: int_rate
	real, allocatable, save, dimension(:,:)	:: sum_costs
	real, allocatable, save, dimension(:,:)	:: subsidy
	real, allocatable, save, dimension(:,:)	:: npv
	real, allocatable, save, dimension(:,:) :: net_prices

	! percentual of chainsaw to harvester methods
	real, dimension(2) :: hsystem = (/0.8, 0.2/)
	! percentual of decidious wood in a forest
	real :: dec_per

	! planting year
	integer :: plant_year = 0
	integer :: flag_plant = 0

	! costs, assets (spec, year)
	real, allocatable, save, dimension(:,:) :: ms_costs
	real, allocatable, save, dimension(:,:) :: ms_assets
	real, allocatable, save, dimension(:,:) :: st_costs
	real, allocatable, save, dimension(:,:) :: st_assets

end ! module data_wpm

!***************************************************************
!************* functions ***************************************
!***************************************************************
! initializes the inputfiles and fills the parameters
subroutine ini_input
	
use data_simul
use data_wpm

implicit none

integer i

	!******************* ini wpm and sea **************************
	do i =1,nr_years
	   years(i) = i
	enddo
	management_years(:)			= 0
	!******************* ini wpm **************************
	! parameters for the round wood => product lines
	do i=1,nr_pr_ln
		product_lines(i)%value(:)	= 0.
	end do
	
	! distribution of round wood to product lines 
	! redistribution of timber grades:
	! wiener model
!	product_lines(1)%proc_par(1,:)	= (/0.6,	 0.,		0.,		0.,		0.4,		0.,		0./)
!	product_lines(2)%proc_par(1,:)	= (/0.,		 0.6,		0.,		0.,		0.4,		0.,		0./)
!	product_lines(3)%proc_par(1,:)	= (/0.,		 0.,		0.6,	0.,		0.4,		0.,		0./)
!	product_lines(4)%proc_par(1,:)	= (/0.,		 0.,		0.,		0.6,	0.4,		0.,		0./)
!	product_lines(5)%proc_par(1,:)	= (/0.,		 0.,		0.,		0.,		1.,			0.,		0./)
!	product_lines(6)%proc_par(1,:)	= (/0.,		 0.,		0.,		0.,		0.,			1.,		0./)
!	product_lines(7)%proc_par(1,:)	= (/0.,		 0.,		0.,		0.,		0.,			0.,		1./)

	! distribution of timber: industrial lines to product lines
!	product_lines(1)%proc_par(2,:)	= (/0.6,	 0.,	0.12,		0.,		0.14,		0.,		0.12/)
!	product_lines(2)%proc_par(2,:)	= (/0.6,	 0.,	0.12,		0.,		0.14,		0.,		0.12/)
!	product_lines(3)%proc_par(2,:)	= (/0.,		 0.,	0.61,		0.,		0.16,		0.,		0.23/)
!	product_lines(4)%proc_par(2,:)	= (/0.,		 0.,	0.61,		0.,		0.16,		0.,		0.23/)
!	product_lines(5)%proc_par(2,:)	= (/0.,		 0.,	0.,			0.,		0.70,		0.,		0.30/)
!	product_lines(6)%proc_par(2,:)	= (/0.,		 0.,	0.,			0.,		0.71,		0.,		0.30/)
!	product_lines(7)%proc_par(2,:)	= (/0.,		 0.,	0.,			0.,		0.,			0.,		1.00/)

	!	40% of 1st,2nd,3rd,4th timber grades must go to the 5th timber grade (industrial wood)
	product_lines(1)%proc_par(1,:)	= (/0.6,	 0.,		0.,		0.,		0.4,		0.,		0./)
	product_lines(2)%proc_par(1,:)	= (/0.,		 0.6,		0.,		0.,		0.4,		0.,		0./)
	product_lines(3)%proc_par(1,:)	= (/0.,		 0.,		0.6,	0.,		0.4,		0.,		0./)
	product_lines(4)%proc_par(1,:)	= (/0.,		 0.,		0.,		0.6,	0.4,		0.,		0./)
	product_lines(5)%proc_par(1,:)	= (/0.,		 0.,		0.,		0.,		1.,			0.,		0./)
	product_lines(6)%proc_par(1,:)	= (/0.,		 0.,		0.,		0.,		0.,			1.,		0./)
	product_lines(7)%proc_par(1,:)	= (/0.,		 0.,		0.,		0.,		0.,			0.,		1./)

	! distribution of timber grades to industrial lines - Brandenburg
	product_lines(1)%proc_par(2,:)	= (/0.97,	 0.,		0.03,		0.,		0.,			0.,		0./)
	product_lines(2)%proc_par(2,:)	= (/0.,		 0.83,		0.17,		0.,		0.	,		0.,		0./)
	product_lines(3)%proc_par(2,:)	= (/0.86,	 0.,		0.01,		0.,		0.13,		0.,		0./)
	product_lines(4)%proc_par(2,:)	= (/0.,		 0.53,		0.10,		0.,		0.37,		0.,		0./)
	product_lines(5)%proc_par(2,:)	= (/0.,		 0.,		0.,			0.66,	0.34,		0.,		0./)
	product_lines(6)%proc_par(2,:)	= (/0.,		 0.,		0.,			0.,		0.,			0.,		0./)
	product_lines(7)%proc_par(2,:)	= (/0.,		 0.,		0.,			0.,		0.,			0.,		1./)

	!distribution of timber into industrial lines - Germany
!	product_lines(1)%proc_par(2,:)	= (/0.97,	 0.,		0.03,		0.,		0.,		0.,		0./)
!	product_lines(2)%proc_par(2,:)	= (/0.,		 0.83,		0.17,		0.,		0.	,	0.,		0./)
!	product_lines(3)%proc_par(2,:)	= (/0.86,	 0.,		0.01,		0.,		0.07,		0.06,		0./)
!	product_lines(4)%proc_par(2,:)	= (/0.,		 0.53,		0.10,		0.,		0.20,		0.17,		0./)
!	product_lines(5)%proc_par(2,:)	= (/0.,		 0.,		0.,		0.66,		0.18,		0.16,		0./)
!	product_lines(6)%proc_par(2,:)	= (/0.,		 0.,		0.,		0.,		0.,		0.,		0./)
!	product_lines(7)%proc_par(2,:)	= (/0.,		 0.,		0.,		0.,		0.,		0.,		1./)

! distribution of timber: industrial lines to product lines
select case (flag_wpm)
	
! central europe
case (1:10)
			
	product_lines(1)%proc_par(3,:)	= (/0.610,	 0.,		0.,			0.152,	0.141,		0.,		0.097/)
	product_lines(2)%proc_par(3,:)	= (/0.,		 0.670,		0.,			0.152,	0.119,		0.,		0.082/)
	product_lines(3)%proc_par(3,:)	= (/0.,		 0.,		0.530,		0.095,	0.,			0.,		0.375/)
	product_lines(4)%proc_par(3,:)	= (/0.,		 0.,		0.,			0.690,	0.080,		0.,		0.230/)
	product_lines(5)%proc_par(3,:)	= (/0.,		 0.,		0.,			0.,		0.472,		0.,		0.528/)
	product_lines(6)%proc_par(3,:)	= (/0.,		 0.,		0.,			0.,		0.,			0.928,	0.072/)
	product_lines(7)%proc_par(3,:)	= (/0.,		 0.,		0.,			0.,		0.,			0.,		1.000/)

! nothern europe
case (11:20)
	
	product_lines(1)%proc_par(3,:)	= (/0.435,	 0.,		0.,			0.270,	0.435,		0.,		0.130/)
	product_lines(2)%proc_par(3,:)	= (/0.,		 0.435,		0.,			0.270,	0.435,		0.,		0.130/)
	product_lines(3)%proc_par(3,:)	= (/0.,		 0.,		0.384,		0.,		0.339,		0.,		0.277/)
	product_lines(4)%proc_par(3,:)	= (/0.,		 0.,		0.,			0.690,	0.080,		0.,		0.230/)
	product_lines(5)%proc_par(3,:)	= (/0.,		 0.,		0.,			0.,		0.472,		0.,		0.528/)
	product_lines(6)%proc_par(3,:)	= (/0.,		 0.,		0.,			0.,		0.,			0.928,	0.072/)
	product_lines(7)%proc_par(3,:)	= (/0.,		 0.,		0.,			0.,		0.,			0.,		1.000/)

! southern europe
case (21:30)
	
	product_lines(1)%proc_par(3,:)	= (/0.610,	 0.,		0.152,		0.,		0.141,		0.,		0.097/)
	product_lines(2)%proc_par(3,:)	= (/0.,		 0.670,		0.129,		0.,		0.119,		0.,		0.082/)
	product_lines(3)%proc_par(3,:)	= (/0.,		 0.,		0.530,		0.,		0.,			0.,		0.375/)
	product_lines(4)%proc_par(3,:)	= (/0.,		 0.,		0.,			0.095,	0.080,		0.,		0.230/)
	product_lines(5)%proc_par(3,:)	= (/0.,		 0.,		0.,			0.690,	0.472,		0.,		0.528/)
	product_lines(6)%proc_par(3,:)	= (/0.,		 0.,		0.,			0.,		0.,			0.928,	0.072/)
	product_lines(7)%proc_par(3,:)	= (/0.,		 0.,		0.,			0.,		0.,			0.,		1.000/)

case (31:40)
	!central europe			
	product_lines(1)%proc_par(3,:)	= (/0.610,	 0.,		0.,			0.152,	0.141,		0.,		0.097/)
	product_lines(2)%proc_par(3,:)	= (/0.,		 0.670,		0.,			0.152,	0.119,		0.,		0.082/)
	product_lines(3)%proc_par(3,:)	= (/0.,		 0.,		0.530,		0.095,	0.,			0.,		0.375/)
	product_lines(4)%proc_par(3,:)	= (/0.,		 0.,		0.,			0.690,	0.080,		0.,		0.230/)
	product_lines(5)%proc_par(3,:)	= (/0.,		 0.,		0.,			0.,		0.472,		0.,		0.528/)
	product_lines(6)%proc_par(3,:)	= (/0.,		 0.,		0.,			0.,		0.,			0.928,	0.072/)
	product_lines(7)%proc_par(3,:)	= (/0.,		 0.,		0.,			0.,		0.,			0.,		1.000/)

	!distribution of timber into industrial lines - Germany
!	product_lines(1)%proc_par(2,:)	= (/0.97,	 0.,		0.03,		0.,		0.,		0.,		0./)
!	product_lines(2)%proc_par(2,:)	= (/0.,		 0.83,		0.17,		0.,		0.	,	0.,		0./)
!	product_lines(3)%proc_par(2,:)	= (/0.86,	 0.,		0.01,		0.,		0.07,		0.06,		0./)
!	product_lines(4)%proc_par(2,:)	= (/0.,		 0.53,		0.10,		0.,		0.20,		0.17,		0./)
!	product_lines(5)%proc_par(2,:)	= (/0.,		 0.,		0.,		0.66,		0.18,		0.16,		0./)
!	product_lines(6)%proc_par(2,:)	= (/0.,		 0.,		0.,		0.,		0.,		0.,		0./)
!	product_lines(7)%proc_par(2,:)	= (/0.,		 0.,		0.,		0.,		0.,		0.,		1./)

end select

	!**************************************************************************************************************
	! parameters for the product lines => "use categories"
	product_lines(1)%use_par	= (/0.35,	 0.30,		0.10,		0.15,		0.10,		0.,		0./)
	product_lines(2)%use_par	= (/0.35,	 0.30,		0.10,		0.15,		0.10,		0.,		0./)
	product_lines(3)%use_par	= (/0.05,	 0.05,		0.30,		0.30,		0.30,		0.,		0./)
	product_lines(4)%use_par	= (/0.20,	 0.30,		0.10,		0.20,		0.20,		0., 	0./)
	product_lines(5)%use_par	= (/0.,		 0.,		0.,			0.,			0.33,		0.33,	0.34/)
	product_lines(6)%use_par	= (/0.,		 0.,		0.,			0.,			0.34,		0.33, 	0.33/)
	product_lines(7)%use_par	= (/0.,		 0.,		0.,			0.,			0.,			0., 	0./)
	
	!******************* ini lifespan **************************
	short_lifespan%hl	= 1.	
	short_lifespan%a	= 120.	
	short_lifespan%b	= 5.
	short_lifespan%c	= 3.
	short_lifespan%d	= 120.

	medium_short_lifespan%hl	= 4.	
	medium_short_lifespan%a		= 120.	
	medium_short_lifespan%b		= 5.
	medium_short_lifespan%c		= 0.5
	medium_short_lifespan%d		= 120.

	medium_long_lifespan%hl		= 16.	
	medium_long_lifespan%a		= 120.	
	medium_long_lifespan%b		= 5.
	medium_long_lifespan%c		= 0.12
	medium_long_lifespan%d		= 120.

	long_lifespan%hl	= 50.	
	long_lifespan%a		= 120.	
	long_lifespan%b		= 5.
	long_lifespan%c		= 0.04
	long_lifespan%d		= 120.

	!************** ini use categories **************************
	do i=1,nr_use_cat
		use_categories(i)%value(:)	= 0.
	end do

	use_categories(1)%lifespan_function = long_lifespan
	use_categories(2)%lifespan_function = medium_long_lifespan
	use_categories(3)%lifespan_function = short_lifespan
	use_categories(4)%lifespan_function = medium_long_lifespan
	use_categories(5)%lifespan_function = short_lifespan
	use_categories(6)%lifespan_function = medium_short_lifespan
	use_categories(7)%lifespan_function = short_lifespan
	
	! recycling, landfill, burning
	use_categories(1)%rec_par = (/0.30,		0.35,	0.35/)	
	use_categories(2)%rec_par = (/0.25,		0.50,	0.25/)
	use_categories(3)%rec_par = (/0.15,		0.45,	0.40/)
	use_categories(4)%rec_par = (/0.25,		0.50,	0.25/)
	use_categories(5)%rec_par = (/0.72,		0.14,	0.14/)
	use_categories(6)%rec_par = (/0.72,		0.14,	0.14/)
	use_categories(7)%rec_par = (/0.72,		0.14,	0.14/)

	! recycling parameters
	! test parameters like in the wien model
!	use_categories(1)%rec_use_par	= (/1.00,	0.,		0.,		0.,		0.,		0.,		0./)
!	use_categories(2)%rec_use_par	= (/0.,		1.00,	0.,		0.,		0.,		0.,		0./)
!	use_categories(3)%rec_use_par	= (/0.,		0.,		1.00,	0.,		0.,		0.,		0./)
!	use_categories(4)%rec_use_par	= (/0.,		0.,		0.,		1.00,	0.,		0., 	0./)
!	use_categories(5)%rec_use_par	= (/0.,		0.,		0.,		0.,		1.00,	0.,		0./)
!	use_categories(6)%rec_use_par	= (/0.,		0.,		0.,		0.,		0.,		1.00, 	0./)
!	use_categories(7)%rec_use_par	= (/0.,		0.,		0.,		0.,		0.,		0., 	1.00/)

	! real parameters
	use_categories(1)%rec_use_par	= (/0.33,	0.34,		0.33,		0.,		0.,		0.,		0./)
	use_categories(2)%rec_use_par	= (/0.,		0.50,		0.50,		0.,		0.,		0.,		0./)
	use_categories(3)%rec_use_par	= (/0.,		0.,			1.00,		0.,		0.,		0.,		0./)
	use_categories(4)%rec_use_par	= (/0.,		0.,			0.5,		0.5,	0.,		0., 	0./)
	use_categories(5)%rec_use_par	= (/0.,		0.,			0.,			0.,		0.5,	0.,		0.5/)
	use_categories(6)%rec_use_par	= (/0.,		0.,			0.,			0.,		0.5,	0., 	0.5/)
	use_categories(7)%rec_use_par	= (/0.,		0.,			0.,			0.,		0.5,	0., 	0.5/)

	! test parameters
!	use_categories(1)%rec_use_par	= (/1.00,	0.,		0.,		0.,		0.,		0.,		0./)
!	use_categories(2)%rec_use_par	= (/0.,		1.00,	0.,		0.,		0.,		0.,		0./)
!	use_categories(3)%rec_use_par	= (/0.,		0.,		1.00,	0.,		0.,		0.,		0./)
!	use_categories(4)%rec_use_par	= (/0.,		0.,		0.,		1.00,	0.,		0., 	0./)
!	use_categories(5)%rec_use_par	= (/0.,		0.,		0.,		0.,		0.5,	0.5,	0./)
!	use_categories(6)%rec_use_par	= (/0.,		0.,		0.,		0.,		0.,		1.00, 	0./)
!	use_categories(7)%rec_use_par	= (/0.,		0.,		0.,		0.,		0.,		0., 	1.00/)

	! wood_pieces containes the wood_pieces of different age
	do i=1, nr_use_cat
		max_age(i) = use_categories(i)%lifespan_function%hl * 3
	end do
	
	! allocation of spinup values
	do i=1, nr_use_cat
		allocate(use_categories(i)%spinup(max_age(i)))
	end do
	
	do i=1, nr_use_cat
		use_categories(i)%spinup(:) = 0.
	end do

	burning(:)		= 0.
	atmo_cum(:)		= 0.
	atmo_year(:)	= 0.
	landfill(:)		= 0.
	sum_use_cat(:)	= 0.
	sum_input(:)	= 0.
	pl(:,:,:)		= 0.
	use_cat(:,:)	= 0.

!******************* ini substitution **************************

       emission_har = 0.
       sub_energy = 0.
       sub_material = 0.
       sub_sum = 0.
       sub_par (1) = -0.013  ! Emission from harvest
       sub_par (2) =  0.601
       sub_par (3) =  0.2651

end subroutine

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    
    
subroutine ini_input_sea	
use data_simul
use data_wpm

implicit none
	
	!************************ ini sea ***********************
	mansort_tg(:,:,:)		= 0.
	standsort_tg(:,:,:)		= 0.
	chainsaw_prices(:,:)	= 0.
	harvester_prices(:,:)	= 0.
	fence(:,:)				= 0.
	planting_prices(:)		= 0.
	planting_sub(:,:)		= 0.
	net_prices(:,:)			= 0.
	ms_costs(:,:)			= 0.
	st_costs(:,:)			= 0.
	ms_assets(:,:)			= 0.
	st_assets(:,:)			= 0.
	sum_costs(:,:)			= 0.
	subsidy(:,:)			= 0.
	npv(:,:)				= 0.
	brushing(:)				= 0.
	fix(:)					= 0.
	tending_prices(:)		= 0.
	dec_per					= 0.

end subroutine

!********************** FLAGS **********************************
! set flags for the run
subroutine setFlags

use data_wpm
implicit none

	! calculate product lines with or without bark
!	wob				= .FALSE.
	wob				= .TRUE.

	! spin up flag: true - read and add the spin up values
	spinup_on		= .FALSE.
!	spinup_on		= .TRUE.

	! debug and spinup outputs
	debug			= .FALSE.
!	debug			= .TRUE.
	output_spinup	= .FALSE.
!	output_spinup	= .TRUE.

end subroutine

!***************************************************************
subroutine allocate_in_output

use data_simul	
use data_wpm

implicit none

! integer mansort_lines, nr_years, nr_management_years
integer i

	! set some informations for wpm / sea
	nr_years		= year

	! allocate output
      if(flag_wpm.eq.5 .or.flag_wpm.eq.4 .or. flag_wpm.eq.6) then
	       nr_years = nr_management_years
	  end if

	if (.not. allocated(years)) allocate(years(nr_years))
	if (.not. allocated(management_years)) allocate(management_years(nr_management_years))

	! only wpm
	if (flag_wpm == 1 .or. flag_wpm == 3 .or. flag_wpm == 21 .or. flag_wpm == 11.or. flag_wpm == 5 .or. flag_wpm == 4 .or. flag_wpm == 6) then
		nr_pr_ln		= 7
		nr_use_cat		= 7

		! allocate wood processing
		if (.not. allocated(product_lines)) then
			allocate(product_lines(nr_pr_ln))
			do i=1,nr_pr_ln
				allocate(product_lines(i)%value(nr_management_years))
			end do
		end if

		! allocate pl: save results of the product lines sorting
		if (.not. allocated(pl))allocate(pl(3, nr_pr_ln, nr_years))

		! 6 use categories per simulation year
		if (.not. allocated(use_categories)) then
			allocate(use_categories(nr_use_cat))
			do i=1,nr_use_cat
				allocate(use_categories(i)%value(nr_years))
			end do
		end if
		if (.not. allocated(max_age))allocate(max_age(nr_use_cat))
		if (.not. allocated(burning))allocate(burning(nr_years))
		if (.not. allocated(landfill))allocate(landfill(nr_years))
		if (.not. allocated(atmo_cum))allocate(atmo_cum(nr_years))
		if (.not. allocated(atmo_year))allocate(atmo_year(nr_years))
		if (.not. allocated(sum_use_cat))allocate(sum_use_cat(nr_years))
		if (.not. allocated(sum_input))allocate(sum_input(nr_years))
		if (.not. allocated(use_cat))allocate(use_cat(nr_use_cat, nr_years))

! Substitution 
		if (.not. allocated(emission_har))allocate(emission_har(nr_years))
		if (.not. allocated(sub_energy))allocate(sub_energy(nr_years))
		if (.not. allocated(sub_material))allocate(sub_material(nr_years))
		if (.not. allocated(sub_sum))allocate(sub_sum(nr_years))

	end if	

	! only sea
	if (flag_wpm == 2 .or. flag_wpm == 3.or.flag_wpm.eq.5 .or. flag_wpm.eq.6) 	then
		nr_spec = 5
		nr_timb_grades	= 10
		if (.not. allocated(mansort_tg)) allocate(mansort_tg(nr_spec, nr_timb_grades, nr_years))
		if (.not. allocated(standsort_tg)) allocate(standsort_tg(nr_spec, nr_timb_grades, nr_years))

		if (.not. allocated(chainsaw_prices)) allocate(chainsaw_prices(nr_spec, nr_timb_grades))
		if (.not. allocated(harvester_prices)) allocate(harvester_prices(nr_spec, nr_timb_grades))
		if (.not. allocated(planting_prices)) allocate(planting_prices(nr_spec))
		if (.not. allocated(fence)) allocate(fence(2,nr_spec))
		if (.not. allocated(planting_sub)) allocate(planting_sub(2,nr_spec))
		if (.not. allocated(net_prices)) allocate(net_prices(nr_spec, nr_timb_grades))
		if (.not. allocated(ms_costs)) allocate(ms_costs(nr_spec, nr_years))
		if (.not. allocated(st_costs)) allocate(st_costs(nr_spec, nr_years))
		if (.not. allocated(sum_costs)) allocate(sum_costs(5, nr_years))
		if (.not. allocated(subsidy)) allocate(subsidy(2, nr_years))
		if (.not. allocated(npv)) allocate(npv(12, nr_years))
		
		if (.not. allocated(ms_assets)) allocate(ms_assets(nr_spec, nr_years))
		if (.not. allocated(st_assets)) allocate(st_assets(nr_spec, nr_years))
		
	end if

    end subroutine

!***************************************************************

subroutine deallocate_wpm

use data_wpm
use data_simul	

implicit none
integer i

	! deallocate mansort and manrec lists	
	if ( associated(first_manrec)) then
		act_manrec => first_manrec
		do while ( associated(act_manrec))
			first_manrec => act_manrec%next
			deallocate(act_manrec)	
			act_manrec => first_manrec
		end do
	endif

	if ( associated(first_mansort)) then
		act_mansort => first_mansort
		do while ( associated(act_mansort))
			first_mansort => act_mansort%next
			deallocate(act_mansort)	
			act_mansort => first_mansort
		end do
	endif

	! deallocate wood processing
	if (allocated(management_years)) deallocate(management_years)
	do i=1,nr_pr_ln
		if (associated(product_lines(i)%value)) deallocate(product_lines(i)%value)
	end do
	if (allocated(product_lines)) deallocate(product_lines)
	if (allocated(pl)) deallocate(pl)

	! deallocate output
	if (allocated(years)) deallocate(years)
	do i=1,nr_use_cat
		if (associated(use_categories(i)%value)) deallocate(use_categories(i)%value)
		if (associated(use_categories(i)%spinup)) deallocate(use_categories(i)%spinup)
	end do
	if (allocated(use_categories)) deallocate(use_categories)
	if (allocated(max_age)) deallocate(max_age)
	if (allocated(burning)) deallocate(burning)
	if (allocated(landfill)) deallocate(landfill)
	if (allocated(atmo_cum)) deallocate(atmo_cum)
	if (allocated(atmo_year)) deallocate(atmo_year)
	if (allocated(sum_use_cat)) deallocate(sum_use_cat)
	if (allocated(sum_input)) deallocate(sum_input)
	if (allocated(use_cat)) deallocate(use_cat)
! Sustitution
	if (allocated(emission_har)) deallocate(emission_har)
	if (allocated(sub_energy)) deallocate(sub_energy)
	if (allocated(sub_material)) deallocate(sub_material)
	if (allocated(sub_sum)) deallocate(sub_sum)

!sea
if (flag_wpm == 2 .or. flag_wpm == 3) then

	if ( associated(first_standsort)) then
		act_standsort => first_standsort
		do while ( associated(act_standsort))
			first_standsort => act_standsort%next
			deallocate(act_standsort)	
			act_standsort => first_standsort
		end do
	endif
	
	if (allocated(mansort_tg)) deallocate(mansort_tg)
	if (allocated(standsort_tg)) deallocate(standsort_tg)
	if (allocated(chainsaw_prices)) deallocate(chainsaw_prices)
	if (allocated(harvester_prices)) deallocate(harvester_prices)
	if (allocated(planting_prices)) deallocate(planting_prices)
	if (allocated(fence)) deallocate(fence)
	if (allocated(planting_sub)) deallocate(planting_sub)
	if (allocated(net_prices)) deallocate(net_prices)
	if (allocated(ms_costs)) deallocate(ms_costs)
	if (allocated(st_costs)) deallocate(st_costs)
	if (allocated(ms_assets)) deallocate(ms_assets)
	if (allocated(st_assets)) deallocate(st_assets)
	if (allocated(sum_costs)) deallocate(sum_costs)
	if (allocated(subsidy)) deallocate(subsidy)
	if (allocated(npv)) deallocate(npv)
	
end if
end subroutine
