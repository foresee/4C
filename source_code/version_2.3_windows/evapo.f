!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                                                               *!
!*                    Subroutines for:                           *!
!*                Soil and Water - Programs                      *!
!*                                                               *!
!*   contains:                                                   *!
!*   EVAPO     calculation of potential evapotranspiration       *!
!*   EVAPO_INI initialisation of potential evapotranspiration    *!
!*   turc_ivanov                                                 *!
!*   sunshine                                                    *! 
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

SUBROUTINE evapo

! Potential evapotranspiration PET

use data_climate
use data_evapo
use data_inter
use data_par
use data_simul
use data_site
use data_stand
use data_soil
use data_species

implicit none

integer i
real atemp25, cf, hxx, redcof
real pet0,     & ! PET Turc/Ivanov
     pet1,     & ! PET Priestley-Taylor
     pet2,     & ! PET Priestley-Taylor for each cohort
     pet3,     & ! PET Penman/Monteith
     pet4,     & ! PET Penman/Monteith for each cohort
     pet5,     & ! PET Haude
     pev0_s,   & ! soil evaporation from Turc/Ivanov
     pev1_s,   & ! soil evaporation from Priestley-Taylor
     pev2_s,   & ! soil evaporation from Priestley-Taylor for each cohort
     h_klim,   & ! height of reference station
     gamma,    & ! scheinbare Psychrometer-Konstante
     svp,      & ! saturated vapour pressure
     vpd,      & ! vapour pressure deficit
     vpress,   & ! vapour pressure
     delta,    & ! slope of vapour pressure curve against temperature
     dens_air, & ! density of dry air (kg/m3) (like MONTEITH (1973))
     alpha,    & ! Priestley-Taylor coefficient
     Rnet,     & ! net radiation W/m2 of whole canopy
     Rnet_s,   & ! absorbed global radiation W/m2 of soil
     Rnet_alb, & ! net radiation from radiation balance with intermediate calculation in J/m2
     Rnet_alb1,& ! net radiation from radiation balance without reflected radiation in J/cm2
     Rnet_tem, & ! net radiation from temperature and airpressure
     Rnet_fed, & ! net radiation according to Federer (1968) and Feddes (1971) 
     Rrefl,    & ! reflected long wave radiation
     Srel,     & ! relative sunshine duration
     albedo,   & 
     sigma,    & ! Boltzmannsche constant
     lamb,     & ! latent heat of vaporization of water (W / (m2 mm) day value)
     rc,       & ! empir. plant base resistance (s/m)
     v_conc,   & ! concentration water vapour
     hf, hln, hz, z0, tutrf,    &
     atmp_1
real Rnet1, Rnet2_sum, Rnet3, Rnet4_sum
real Rnet_mw,  & ! net radiation (J/cm2) measured value
     G_flux      ! soil heat flux (J/cm2) measured value 
character (10) text
real transd0, transd1, hx 
! for PET according to Haude
real svp_13, vp_13, vpd_13, relhum_13, dptemph, hh
real dpta, dptb, dptc    ! coefficients for calculation of dew point temperature
real, dimension(12)         :: ft_haude=(/0.22,0.22,0.22,0.29,0.29,0.28,0.26,0.25,0.23,0.22,0.22,0.22/)

! read flux data
if (flag_eva .gt.10) read (unit_eva,*) text, Rnet_mw, G_flux

alpha  = alpha_PT
atmp_1 = 1./(airtemp + 273.3)
svp    = 6.1078 * exp(17.2694 * airtemp * atmp_1) ! saturated vapour pressure (MURRAY, 1967)
vpress = 0.01 * hum * svp
vpd    = svp*(1. - hum*0.01)        ! vapour pressure deficit

! dew point temperature (DVWK 1996, P. 83)
if(airtemp .lt. 0.) then
  dpta = 272.2
  dptb = 24.27
else
  dpta = 243.12
  dptb = 19.43
endif
dptc = 1.81
dptemp = dpta * (log(vpress)-dptc) / (dptb-log(vpress))

! relative Sonnenscheindauer
call sunshine(Srel, iday, lat, dlength, rad)

!! net radiation from radiation balance ( Rijtema, 1965)
!   albedo   = 0.35   ! adjustment to Rnet for spruce (Tharandt), beech (Hesse), pine (Loobos)
!   albedo = 0.1      ! for pine from Lit.

!net radiation according to Federer (1968) and Feddes (1971)
    Rnet_fed = 0.649 * (rad/8.64) - 23    ! rad: J/cm2 ==> W/m2 
    Rnet_fed = 8.64 * Rnet_fed            ! W/m2 ==> J/cm2
    Rnet_tot = Rnet_fed
    Rnet   = (Rnet_tot/8.64)              ! J/cm2 ==> W/m2

if (((snow .gt. 0.) .or. lint_snow) .and. (airtemp .lt. 0.)) then
! snow or frost evaporation (DVWK S.73, 1996; Rachner, 1987)
    albedo = 0.85
    pev_sn = 0.41 * vpd - 0.22
    if (pev_sn .lt. 0.) then
        dew_rime = -pev_sn
        pev_s  = 0.1
    else
        pev_s  = pev_sn
    endif

    if (Rnet_fed .lt. 0.) then
       sigma    = 5.67 * 10.**(-8)      ! W / m2
       Rrefl    = sigma * (airtemp+273)**4 * (0.56 - 0.079*Sqrt(vpress))*(0.1 + 0.9*Srel)  ! J/m2
       Rnet_alb = (rad*10000.0 * (1.-albedo) - Rrefl)   ! J/m2
       Rnet_alb = Rnet_alb * 0.0001
       Rnet_tot = Rnet_alb         ! J/cm2
       Rnet   = (Rnet_tot/8.64)         ! J/cm2 ==> W/m2
    endif
    pet    = 0.
    zeig => pt%first
    do while (associated(zeig))
       zeig%coh%demand = 0.

       zeig => zeig%next
    enddo  ! zeig (cohorts)

else  

    if (Rnet_fed .lt. 0.) then
       albedo = 0.2
       sigma    = 5.67 * 10.**(-8)      ! W / m2
       Rrefl    = sigma * (airtemp+273)**4 * (0.56 - 0.079*Sqrt(vpress))*(0.1 + 0.9*Srel)  ! J/m2  
       Rnet_alb = (rad*10000.0 * (1.-albedo) - Rrefl)   ! J/m2
       Rnet_alb = Rnet_alb * 0.0001
       Rnet_tot = Rnet_alb              ! J/cm2
       Rnet   = (Rnet_tot/8.64)         ! J/cm2 ==> W/m2
    endif

    select case (flag_eva)
    case (0,6,7)
       call turc_ivanov

    case (1,2,3,4,16,17,36,37)

    ! preparation Priestley/Taylor and Penman/Monteith calculation
        gamma  = psycro * press
        delta  = 239. * 17.4 * svp * atmp_1*atmp_1        ! slope of vapour pressure curve
        lamb   = (2.498 - 0.00242*airtemp) * 1E06         ! W s /(m2 mm) == J/mm / m2
        lamb   = lamb/86400.                              ! W / (m2 mm) Tageswert

        if (anz_coh .le. 0) then
           pet   = alpha * Rnet * delta/((delta+gamma)*lamb)   ! potential evapotranspiration of canopy
           pev_s = 0.
   
        else
           if (all_leaves_on .eq. 0) then
              pet   = alpha * Rnet * delta/((delta+gamma)*lamb)   ! potential evapotranspiration of canopy
  
             ! potential transpiration demand of each cohort
              if (gp_can .gt. 1.E-6) then
                 hx = pet / gp_can
              else
                 hx = 0.
              endif

              zeig => pt%first
              do while (associated(zeig))
                zeig%coh%demand = zeig%coh%gp * zeig%coh%ntreea * hx 
                if (zeig%coh%species.eq.nspec_tree+2) then          !save demand of mistletoe calculated cohort-specific for later use in upt_wat (soil.f)
                    demand_mistletoe_cohort=zeig%coh%gp * zeig%coh%ntreea * hx
                end if

                zeig => zeig%next
              enddo  ! zeig (cohorts)

           !  soil evaporation
              redcof = 0.4   
              Rnet_s = (Rnet_tot/8.64) * redcof                  ! J/cm2 ==> W/m2

           else
               Rnet   = (Rnet_tot/8.64) * totFPARsum             ! J/cm2 ==> W/m2
               Rnet_s = (Rnet_tot/8.64) * (1.-totFPARsum)        ! J/cm2 ==> W/m2
               select case (flag_eva)
               case (1)    ! Priestley / Taylor
                  pet   = alpha * Rnet * delta/((delta+gamma)*lamb)   ! potential evapotranspiration of canopy

               case (2)    ! Priestley / Taylor for each cohort
                  pet2 = 0.

                  Rnet2_sum = 0
                  zeig => pt%first
                  do while (associated(zeig))
                    if (zeig%coh%gp .gt. 0.) then
                      Rnet   = (Rnet_tot/8.64) * zeig%coh%totFPAR * zeig%coh%nTreeA      ! J/cm2 ==> W/m2
                      Rnet2_sum = Rnet2_sum + Rnet    
                      zeig%coh%demand = alpha * Rnet * delta/((delta+gamma)*lamb)   ! potential evapotranspiration of cohort
                      if (zeig%coh%species.eq.nspec_tree+2) then         !save demand of mistletoe calculated cohort-specific for later use in upt_wat (soil.f)
                          demand_mistletoe_cohort=alpha * Rnet * delta/((delta+gamma)*lamb)
                      end if
                    else
                      zeig%coh%demand = 0.
                    endif

                    pet2 = pet2 + zeig%coh%demand

                    zeig => zeig%next
                  enddo  ! zeig (cohorts)

                  pet = pet2
               case(3,36,37)    ! Penman/Monteith

                  h_klim   = 200.               ! Hoehe Messstation     (cm)
                  dens_air = 1.2917 - 0.00434*airtemp   ! density of dry air (kg/m3) (like MONTEITH (1973))
                  dens_air = dens_air*0.001             ! kg/m3 --> g/cm3
                  hf       = dens_air * c_karman*c_karman * wind
                  if (hdom .ge. 0.5) then
	                hz = hdom
                  else
	                hz = 0.5
	              endif
	              z0       = 10.**(0.997*alog10(hz)-0.883)
                  hln      = alog(h_klim/z0)
                  tutrf    = hf*rmolw / (hln*hln*press)
                  !   canopy conductance verwenden:
                  v_conc   = (press*100.) / (R_gas * (273.15 + airtemp))    ! pressure in hPa --> Pa
                  if (gp_can .gt. 1E-8) then
                    rc    = gp_can / (8980.0 * v_conc)   ! gp_can  mol/m2*d --> m/s
                    rc    = 1. / rc
                    Rnet  = (Rnet_tot/8.64) * totFPARsum        ! J/cm2/d ==> W/m2
                    Rnet3 = Rnet
                    pet3  = (delta*Rnet + vpd*hf*c_air/(hln*hln)) /  &
                            ((delta+gamma*(1+rc*tutrf))*lamb)
                    pet = pet3
                  else
                    call turc_ivanov
                  endif    ! gp_can

               case(4)    ! Penman/Monteith for each cohort

                  pet4     = 0.
                  Rnet4_sum = 0  
                  h_klim   = 200.                       ! hight of measurement station (cm)
                  dens_air = 1.2917 - 0.00434*airtemp   ! density of dry air (kg/m3) (like MONTEITH (1973))
                  dens_air = dens_air*0.001             ! kg/m3 --> g/cm3
                  hf       = dens_air * c_karman*c_karman * wind
                  v_conc   = (press*100.) / (R_gas * (273.15 + airtemp))    ! pressure hPa --> Pa

                  zeig => pt%first
                  do while (associated(zeig))
                      if (zeig%coh%gp .gt. 0.) then
                        if (zeig%coh%height .ge. 0.5) then
	                       hz = zeig%coh%height
                        else
	                       hz = 0.5
	                    endif
                        z0     = 10.**(0.997*alog10(hz)-0.883)
                        hln    = alog(h_klim/z0)

                        if( hln.ne.0) then
			                tutrf  = hf*rmolw / (hln*hln*press)
                            !  canopy conductance verwenden:
                            rc     = zeig%coh%gp / (8980.0 * v_conc)   ! gp_can  mol/m2*d --> m/s
                            rc     = 1. / rc
                            Rnet   = (Rnet_tot/8.64) * zeig%coh%totFPAR * zeig%coh%nTreeA      ! J/cm2 ==> W/m2
                            Rnet4_sum = Rnet4_sum + Rnet     ! zum Test  
                            zeig%coh%demand = (delta*Rnet + vpd*hf*c_air/(hln*hln)) /  &  ! potential evapotranspiration of cohort
                                           ((delta+gamma*(1+rc*tutrf))*lamb)
                            !save demand of mistletoe calculated cohort-specific for later use in upt_wat (soil.f)
                            if (zeig%coh%species.eq.nspec_tree+2) then
                               if (zeig%coh%demand.lt.0) zeig%coh%demand=0                ! avoid further calculations with neg. demands
                               demand_mistletoe_cohort=zeig%coh%demand
                            endif
                        endif
                      else
                        zeig%coh%demand = 0.
                      endif    ! ...coh%gp

                      pet4 = pet4 + zeig%coh%demand
                      zeig => zeig%next
                  enddo  ! zeig (cohorts)
                  pet = pet4

               end select   ! flag_eva (inner cycle)

           endif   ! all_leaves_on

!  soil evaporation
           pev_s = alpha * Rnet_s * delta/((delta+gamma)*lamb)   ! potential soil evaporation

        endif   ! anz_coh

    case (5) ! PET Haude

       if(airtemp_min .gt. -90.) then
           dptemph    = airtemp_min - 4.      ! dew point temperature
           vp_13     = 6.1078 * exp(17.62 * dptemph / (243.12+dptemp)) ! estimated actual vapour pressure at 13.00 (DVWK)
           svp_13    = 6.1078 * exp(17.62 * airtemp_max / (243.12+airtemp_max)) ! saturated vapour pressure at 13.00 (DVWK)
           vpd_13    = svp_13 - vp_13      ! vapour pressure deficit at 13.00
           relhum_13 = 100. * vp_13 / svp_13
           hh =  ft_haude(monat)
           pet5 = hh* vpd_13
         ! without limit, because otherwise class5 wont be reached (maxwert = -35!)
         ! limit according to DVWK annotation (Merkblatt) is 7 mm
            pev_s = pet5 * exp(-0.6*LAI)   ! nach Belmans, Dekker & Bouma, 1982
            pet   = pet5 - pev_s
        else
            print *, ' >>>foresee message: Program aborted'
            print *, ' >>>                 Minimum air temperature required but not available'
            Stop
        endif
    end select   ! flag_eva (aeusserer Zyklus)
endif   ! snow

! Gesamt-PET als Summe PET-Bestand und Boden-Evaporation
pet = pet + pev_s
hx = alfm * (1. - exp(-gp_can/gpmax))
! climatic water balance of the last five days
do i= 1,4
   clim_waterb(i) = clim_waterb(i+1)
enddo
clim_waterb(5) = prec - pet
pet_cum        = pet_cum + pet
Rnet_cum =  rnet_cum + rnet_tot

END  subroutine evapo

!******************************************************************************

SUBROUTINE turc_ivanov

use data_climate
use data_evapo
use data_stand
implicit none

real atemp25, cf, hxx, pet0

!  calculation after DYCK/PESCHKE, 1995, S.200
if (airtemp .gt. 5.) then
  if (hum .lt. 50.) then
     cf = 1. + (50. - hum) / 70.
  else
     cf = 1.
  endif    ! hum
  pet0 = 0.0031 * cf * (rad+209.) * airtemp/(airtemp+15.)    ! from TURC
else
  atemp25 = (airtemp + 25.)
  pet0 = 3.6 * 10.**(-5) * (100 - hum) * atemp25 * atemp25   ! from IVANOV (daily)
endif       ! airtemp
pev_s = pet0 * exp(-0.6*LAI)   ! Belmans, Dekker & Bouma, 1982
pet   = pet0 - pev_s

END  subroutine turc_ivanov

!******************************************************************************

SUBROUTINE sunshine (sdrel, iday, xxlat, dayl, rad)
! Estimation of sunshine duration from global radiation
! (calculation after Angstrom)

!use data_site
implicit none

! input:
integer :: iday        ! actual day
real    :: dayl        ! daylength
real    :: rad         ! global radiation  (J/cm2)
real    :: xxlat       ! latitude

! output:
real    :: sdrel   !, sdrel1  ! sunshine duration (h)

! internal variables
real  :: rad_ex  , &   ! extraterrestrical radiation (MJ/m2)
		 dec     , &   ! declination of sun angle
		 sinld, cosld, tanld, dsinb, dsinbe,  &
         sc, radi, seas
real  :: pi = 3.141592654
real  :: solc = 1367.  ! solar constant (J/(m2*s) 
                       ! according to P. Hupfer: "Klimasystem der Erde", 1991                            

if (rad .lt. 1.E-6) then
  sdrel=0
  return
end if

!   change of units from degree to radians
radi = pi/180. 
 
!   term of seasonality (10 days in front of calendar)
seas = (iday+10.)/365. 
 
!   declination of sun angle
!   (Spitters et al. 1986, equations transformed for use or radians)
dec = -asin(sin(23.45*radi)*cos(2.*pi*seas)) 
 
!   some intermediate values
sinld = sin(xxlat*radi)*sin(dec) 
cosld = cos(xxlat*radi)*cos(dec) 
tanld = amax1(-1., amin1(1., sinld/cosld)) 
 
!   integral of sun elevation
dsinb = 3600.*(dayl*sinld+24.*cosld*sqrt(1.-tanld*tanld)/pi) 

!   corrected integral of sun elevation
dsinbe = 3600.*(dayl*(sinld+0.4*(sinld*sinld+cosld*cosld*0.5))  &
         +12.*cosld*(2.+3.*0.4*sinld)*sqrt(1.-tanld*tanld)/pi) 
 
!   intensity of radiation outside the atmosphere
      sc    = solc/(1.-0.016729*cos((360./365.)*(iday-4.)*radi))**2.
      rad_ex = sc*(1.+0.033*cos(2.*pi*iday/365.))*dsinbe
      
!   unit conversion in MJ/m2: rad_ex = rad_ex/1000000. 
!   unit conversion in J/cm2
    rad_ex = rad_ex * 0.0001
    if(rad_ex.eq.0) then
	    sdrel=0.
		return
	end if 
      
    sdrel  = (rad - rad_ex*0.19) / (0.55*rad_ex)     ! DVWK
if (sdrel .lt. 0.) sdrel = 0.
           
END	SUBROUTINE sunshine

!****************************************************************************

SUBROUTINE evapo_ini

! Initialisierung Potential evapotranspiration PET
use data_evapo
use data_simul

implicit none

character text
character (150) file_eva

   write (*,*)
   write (*,'(A)', advance='no') 'Read flux data for evaporation, name of input file: '
   read (*,'(A)') file_eva
   unit_eva = getunit()
   open (unit_eva, file=trim(file_eva), status='unknown')
   read (unit_eva,'(A)') text


END  subroutine evapo_ini

!******************************************************************************

