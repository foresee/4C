	PROGRAM foresee
!  main program for 4C
!  Unix Version

!  14.10.04 Su aktuelles Directory (actDir) hier bestimmen 
!  16.08.04 Su Schleife ueber run_nr in UP sim_control (File simul.f)
!  24.03.03 pl Aufruf finish_simul f�r flag_end>0
!  02.12.02 MF Aufruf SimEnv
!  30.07.02 MF Aufruf SIMENV
!  29.05.02 Su ip-Schleife fuer SIMENV (flag_multi=5) ueberspringen
!           call simenv mit Uebergabe von ip
!  26.04.02 Su call out_var_file
!  21.11.01 Su flag_end=2 testen
!  14.05.01 FB call fixclimscen added
!  18.12.97 BE new structure/name
!  11.12.97 BE include same changes for multi_runs
!  27.8.97 BE insert SIM_INI
!  26.8.97 BE insert DO-LOOP for whole program, until choice=end program
!  20.3.97 BE Erweiterung/Umstrukturierung Protokollfile


!	USE data_out
	USE data_simul
!	USE data_stand
!	USE data_species
!	IMPLICIT NONE
!	INTEGER run_nr, ipp

	actDir = ''

	CALL prepare_global

        CALL sim_control

	END PROGRAM foresee

