!*****************************************************************!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                                                               *!
!*        contains:                                              *!
!*            SR man_liocourt_ini                                *!
!*            SR liocourt_manag                                  *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

Subroutine man_liocourt_ini

 USE data_manag
 USE data_simul
 USE data_plant
 USE data_species

implicit none

integer :: manag_unit,i
character(len=150) :: filename
logical :: ex
character    :: text


manag_unit=getunit()
filename = manfile(ip)
call testfile(filename,ex)
open(manag_unit,file=trim(filename))

  allocate(thin_flag1(nspec_tree))

  thin_flag1=-1

 ! read head of data-file
 do
    read(manag_unit,*) text
    if(text .ne. '!')then

       backspace(manag_unit);exit
    endif
 enddo

read(manag_unit,*) thin_int
read(manag_unit,*) dbh_max
read(manag_unit,*) lic_a
read(manag_unit,*) lic_b
read(manag_unit,*) spec_lic
read(manag_unit,*) thin_proc

if(flag_reg.ne.0) then
   read(manag_unit,*) m_numclass
   do i = 1, m_numclass
      read(manag_unit,*) m_numplant(spec_lic,i), m_specpl(spec_lic,i), m_plant_height(spec_lic,i), m_plant_hmin(spec_lic,i), m_pl_age(spec_lic,i), m_hsdev(spec_lic,i)
   end do
end if


close(manag_unit)

end Subroutine man_liocourt_ini
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Subroutine liocourt_manag

USE data_manag
USE data_stand
USE data_species
USE data_simul
USE data_par

implicit none

integer  :: i, ih, nspech
real     :: diamh,  help, stembiom, stembiom_us, stembiom_all, stembiom_re, target_help, target_biom


target_biom=0.
if(Modulo(time,thin_int).eq.0) then


! calculation of mean diameter (correspondung to med_diam) and basal area of stand
    zeig => pt%first
    DO
        IF (.NOT. ASSOCIATED(zeig)) EXIT
    
! Modification for V Kint: no test for diameter
             IF((zeig%coh%ntreea>0).and.zeig%coh%species.eq.spec_lic.and.zeig%coh%underst.eq.0) THEN
             ! forester definition
! overstorey
				  stembiom = stembiom + (zeig%coh%x_sap + zeig%coh%x_hrt)*zeig%coh%ntreea

               ! Trees with DBH = 0 for population and per species
             ELSE IF( (zeig%coh%ntreea>0).and.zeig%coh%species.eq.spec_lic.and.zeig%coh%underst.eq.1) THEN
! seedings/regeneration
			         stembiom_re = stembiom_re + (zeig%coh%x_sap + zeig%coh%x_hrt)*zeig%coh%ntreea
             ELSE if((zeig%coh%ntreea>0).and.zeig%coh%species.eq.spec_lic.and.zeig%coh%underst.eq.2) THEN
! understorey
			        stembiom_us = stembiom_us + (zeig%coh%x_sap + zeig%coh%x_hrt)*zeig%coh%ntreea

             ENDIF
        zeig => zeig%next
    ENDDO

! mean diamteer for over and understorey
stembiom_all = stembiom + stembiom_us
target_help = stembiom_all*(thin_proc)
ntree_lic(1,spec_lic)=int(lic_a*exp(lic_b*2.5))

 Do i=1,21
      help=(dclass_w*i + dclass_w*(i+1))/2.
 	  ntree_lic(i+1,spec_lic)= int(lic_a*exp(lic_b*help))*kpatchsize/10000.
 end do

 zeig=>pt%first
     do while (target_biom.lt. target_help)
       if(.not.associated(zeig)) exit
	   if(zeig%coh%diam.gt. dbh_max) then
           zeig%coh%ntreem = zeig%coh%ntreea
		   zeig%coh%ntreea = 0
		   zeig%coh%nta = 0
		   diam_class(i,spec_lic) = diam_class(i,spec_lic) - 1
           target_biom = target_biom + zeig%coh%ntreem*(zeig%coh%x_sap + zeig%coh%x_hrt)
  		end if
	   zeig => zeig%next
	   
     end do

 do i = 1, num_class
    
 zeig=>pt%first
     do
       if(.not.associated(zeig)) exit
	   if(target_help.le.target_biom) exit

	   nspech = zeig%coh%species
       diamh = zeig%coh%diam
	   ih= i-1
	   if(diamh.le. dbh_max .and.nspech.eq.spec_lic) then

	     if(diamh.gt.dclass_w*ih .and. diamh.le. dclass_w*(ih+1) .and. zeig%coh%ntreea.ne.0) then
           if((diam_class(i,1)-zeig%coh%ntreea).ge. ntree_lic(i,1)) then 
		     zeig%coh%ntreem = zeig%coh%ntreea
		      zeig%coh%ntreea = 0
		      zeig%coh%nta = 0
			  diam_class(i,spec_lic) = diam_class(i,spec_lic) - zeig%coh%ntreem
			  target_biom = target_biom + zeig%coh%ntreem*(zeig%coh%x_sap + zeig%coh%x_hrt)

		   else if(diam_class(i,1).gt. ntree_lic(i,1)) then

               zeig%coh%ntreem= diam_class(i,spec_lic) - ntree_lic(i,spec_lic)
               zeig%coh%ntreea = zeig%coh%ntreea - zeig%coh%ntreem
               zeig%coh%nta = zeig%coh%nta - zeig%coh%ntreem
			   diam_class(i,spec_lic) = diam_class(i,spec_lic) - zeig%coh%ntreem
			   target_biom = target_biom + zeig%coh%ntreem*(zeig%coh%x_sap + zeig%coh%x_hrt)

		   end if

  		 end if
	   end if
	   zeig => zeig%next
	   if (target_biom.ge.target_help) exit

	 end do  ! cohort loop

  end do ! loop i for diamter classes 

! litter pools
 zeig=>pt%first

 do
   if(.not.associated(zeig)) exit
   if(zeig%coh%ntreem>0.and.zeig%coh%species.eq.spec_lic) then
! all parts  of trees are input for litter excepting stems
         zeig%coh%litC_fol = zeig%coh%litC_fol + zeig%coh%ntreem*(1.-spar(spec_lic)%psf)*zeig%coh%x_fol*cpart
         zeig%coh%litN_fol = zeig%coh%litN_fol + zeig%coh%ntreem*((1.-spar(spec_lic)%psf)*zeig%coh%x_fol*cpart)/spar(spec_lic)%cnr_fol
         zeig%coh%litC_frt = zeig%coh%litC_frt + zeig%coh%ntreem*zeig%coh%x_frt*cpart
         zeig%coh%litN_frt = zeig%coh%litN_frt + zeig%coh%ntreem*zeig%coh%x_frt*cpart/spar(spec_lic)%cnr_frt
         zeig%coh%litC_tb = zeig%coh%litC_tb + zeig%coh%ntreem*zeig%coh%x_tb*cpart
         zeig%coh%litN_tb = zeig%coh%litN_tb + zeig%coh%ntreem*zeig%coh%x_tb*cpart/spar(spec_lic)%cnr_tbc
         zeig%coh%litC_crt = zeig%coh%litC_crt + zeig%coh%ntreem*zeig%coh%x_crt*cpart
         zeig%coh%litN_crt = zeig%coh%litN_crt + zeig%coh%ntreem*zeig%coh%x_crt*cpart/spar(spec_lic)%cnr_crt
   endif
   zeig=>zeig%next

 enddo

! calculation of total dry mass of all harvested trees
   sumvsab = 0.
   sumvsab_m3 = 0.
   svar%sumvsab = 0.
   
  zeig=>pt%first
  do
  if(.not.associated(zeig)) exit
  	nspech = zeig%coh%species
	if(nspech.eq.spec_lic) then
       sumvsab          = sumvsab + zeig%coh%ntreem*(zeig%coh%x_sap + zeig%coh%x_hrt)
       sumvsab_m3       = sumvsab_m3 +  zeig%coh%ntreem*(zeig%coh%x_sap + zeig%coh%x_hrt)/(spar(nspech)%prhos*1000000)
       svar(nspech)%sumvsab = svar(nspech)%sumvsab + zeig%coh%ntreem*(zeig%coh%x_sap + zeig%coh%x_hrt)
    end if
   zeig=>zeig%next
  end do
  sumvsab = sumvsab *  10000./kpatchsize           ! kg/ha
  sumvsab_m3 = sumvsab_m3 *  10000./kpatchsize           ! kg/ha
  svar(spec_lic)%sumvsab = svar(spec_lic)%sumvsab  *  10000./kpatchsize           ! kg/ha
  cumsumvsab = cumsumvsab + sumvsab

end if ! loop management time

end Subroutine liocourt_manag
