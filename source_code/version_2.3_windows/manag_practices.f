!*****************************************************************!
!*                                                               *!
!*               4C (FORESEE) Simulation Model                   *!
!*                                                               *!
!*                                                               *!
!*         contains:                                             *!
!*            SR tending                                         *!
!*            SR direct_fel                                      *!
!*            SR thinning                                        *!
!*            SR felling                                         *!
!*            SR shelterwood_man                                 *!
!*            SR min_dbh                                         *!
!*            SR max_dbh                                         *!
!*            SR max_diam                                        *!
!*            SR min_dbh_overs                                   *!
!*            SR min_dbh_tar                                     *!
!*            SR target_thinning                                 *!
!*            SR calc_usp                                        *!
!*            SR calc_gfbg                                       *!
!*            SR stump                                           *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                                   !
!     tending plantations           !
!                                   !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
SUBROUTINE tending(actspec, i)
 use data_stand
 use data_manag
 use data_species
 use data_par
 use data_simul
 implicit none
 integer    :: tendnr,       &     ! number of trees to be removed
               anz,          &
               actspec

 real       :: pequal
 integer    :: help_tree,min_ident,h1,max_ident, h2 ,cohanz
 integer    :: taxnr, j, i, thinflag, num_coh, nhelp,anz_actspec
 integer, dimension(0:anz_coh)  ::cohl
 allocate (height_rank(anz_coh))
 cohanz = 0
anz_actspec = 0
 min_ident=1000
 max_ident = 0
 cohl=0.
 anz=0
 
! number of trees to removed from the top of the stand
     zeig=>pt%first
     do
       if(.not.associated(zeig)) exit
       cohanz = cohanz +1
       if(zeig%coh%species.eq.actspec.and. zeig%coh%shelter.ne.1) anz_actspec = anz_actspec + zeig%coh%ntreea
       if(zeig%coh%shelter.ne.1) then
         if(zeig%coh%ntreea.ne.0.and. zeig%coh%species.eq.actspec) then
            h1 = zeig%coh%ident
            if( h1.lt. min_ident) min_ident = h1
            h2 = zeig%coh%ident
            if(h2.gt.max_ident) max_ident = h2
         end if
       end if
     zeig=>zeig%next
     end do
 if(thr7.ne.2.and.anz_actspec.eq.0) then
 deallocate(height_rank)
 return
 end if
!calculation of relative proportion of stems thinned from tending only of trees which are not shelter trees
  tendnr = anz_actspec * tend(actspec)/2
 help_tree =  tendnr
! determination of heighest tree cohort
! sorting by height of cohorts into the field height_rank containing cohort identifier
 call dimsort(anz_coh, 'height',height_rank)

! removing of trees
 do j= anz_coh, 1, -1
   zeig=>pt%first
   do
     if(.not.associated(zeig)) exit
     if(zeig%coh%shelter.ne.1. .and. zeig%coh%species.eq.specnr(i)) then
     if(zeig%coh%ident.eq.height_rank(j)) then
       if(zeig%coh%ntreea.ge.tendnr) then
           zeig%coh%ntreea = zeig%coh%ntreea - help_tree
           zeig%coh%ntreet =  help_tree
           help_tree = 0.
       else
! number of trees to be left
            help_tree = help_tree-zeig%coh%ntreea

! number of trees removed
            zeig%coh%ntreet =  zeig%coh%ntreea
            zeig%coh%ntreea =  0
       end if
     end if
    end if
    zeig=> zeig%next
    end do

   if(help_tree.le.0 ) exit
 end do

! second part of felling, equal distributed from all cohorts
! equal distribution from all cohorts with trees
       nhelp = tendnr
 zeig=>pt%first
 do
    if(.not.associated(zeig)) exit
    if(zeig%coh%species.eq.actspec) then
    end if
    zeig=>zeig%next
 end do
       do
          j=0
          thinflag = 0
          call random_number(pequal)
           num_coh = min_ident + (max_ident - min_ident) * pequal
          zeig=>pt%first
          do

              if(.not.associated(zeig)) exit
              if(zeig%coh%shelter.ne.1.and. zeig%coh%species.eq.actspec) then
              j = j+1
              if (zeig%coh%ident.eq.num_coh) then
! check the value ntreea before
                  if(zeig%coh%ntreea.ge.1) then
                     zeig%coh%ntreea =  zeig%coh%ntreea  - 1
                     zeig%coh%nta = zeig%coh%ntreea

                     zeig%coh%ntreet =  zeig%coh%ntreet  + 1
                     nhelp = nhelp -1

                     thinflag = 1
                   else
                      exit
                   endif
              end if
              if(thinflag.eq.1) exit
              end if
              zeig => zeig%next
          end do
          if(nhelp.eq.0) exit
       end do

!   all biomasses are added to litter pools

 zeig=>pt%first
 do
   if(.not.associated(zeig)) exit
   taxnr=zeig%coh%species
   if(zeig%coh%ntreet>0.and.taxnr.eq.specnr(i))then
! all parts of trees are input for litter
         zeig%coh%litC_fol = zeig%coh%litC_fol + zeig%coh%ntreet*(1.-spar(taxnr)%psf)*zeig%coh%x_fol*cpart
         zeig%coh%litN_fol = zeig%coh%litN_fol + zeig%coh%ntreet*((1.-spar(taxnr)%psf)*zeig%coh%x_fol*cpart)/spar(taxnr)%cnr_fol
         zeig%coh%litC_frt = zeig%coh%litC_frt + zeig%coh%ntreet*zeig%coh%x_frt*cpart
         zeig%coh%litN_frt = zeig%coh%litN_frt + zeig%coh%ntreet*zeig%coh%x_frt*cpart/spar(taxnr)%cnr_frt
         zeig%coh%litC_tb = zeig%coh%litC_tb + zeig%coh%ntreet*zeig%coh%x_tb*cpart
         zeig%coh%litN_tb = zeig%coh%litN_tb + zeig%coh%ntreet*zeig%coh%x_tb*cpart/spar(taxnr)%cnr_tbc
         zeig%coh%litC_crt = zeig%coh%litC_crt + zeig%coh%ntreet*zeig%coh%x_crt*cpart
         zeig%coh%litN_crt = zeig%coh%litN_crt + zeig%coh%ntreet*zeig%coh%x_crt*cpart/spar(taxnr)%cnr_crt

         zeig%coh%litC_stem = zeig%coh%litC_stem + zeig%coh%ntreet*(zeig%coh%x_sap+zeig%coh%x_hrt)*cpart
         zeig%coh%litN_stem = zeig%coh%litC_stem/spar(taxnr)%cnr_stem
         zeig%coh%ntreet = 0
   endif
 zeig=>zeig%next
 enddo
 thinyear(actspec)=time
 deallocate(height_rank)
END SUBROUTINE tending

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!    Rueckegasse directional felling
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

SUBROUTINE direct_fel(hox)

 use data_manag
 use data_stand
 use data_simul
 use data_par
 use data_species
 implicit none
  integer    :: num_felt=0,      &
                num_coh=0,       &
                i,             &
                thinflag,      &
                taxnr,         &
                nhelp
  real       :: pequal,        &
                hox

 thinflag = 0

 if(thr5.eq.1) then
    if (thr6.eq.hox) then
! felling of direcfel*anz_tree trees equal distributed from all cohorts

       num_felt = direcfel*anz_tree
       nhelp = num_felt
       do
          i=0
          thinflag = 0
          call random_number(pequal)
          num_coh = nint(pequal * anz_coh)+1
          zeig=>pt%first
          do
              if(.not.associated(zeig)) exit
              i = i+1
              if (i.eq.num_coh) then
! check the value ntreea before
                  if(zeig%coh%ntreea.ge.1) then
                     zeig%coh%ntreea =  zeig%coh%ntreea  - 1
                     zeig%coh%ntreem =  zeig%coh%ntreem  + 1
                     nhelp = nhelp -1

                     thinflag = 1
                   else
                      exit
                   endif
              end if
              if(thinflag.eq.1) exit
              zeig => zeig%next
          end do
          if(nhelp.eq.0) exit
       end do
       flag_direct=1
    end if
 end if


! adding biomasses to litter pools depending on stage of stand
 zeig=>pt%first

 do
   if(.not.associated(zeig)) exit
   taxnr=zeig%coh%species

   if(zeig%coh%ntreem>0)then
! all parts without stems of trees are input for litter
         zeig%coh%litC_fol = zeig%coh%litC_fol + zeig%coh%ntreem*(1.-spar(taxnr)%psf)*zeig%coh%x_fol*cpart
         zeig%coh%litN_fol = zeig%coh%litN_fol + zeig%coh%ntreem*((1.-spar(taxnr)%psf)*zeig%coh%x_fol*cpart)/spar(taxnr)%cnr_fol
         zeig%coh%litC_frt = zeig%coh%litC_frt + zeig%coh%ntreem*zeig%coh%x_frt*cpart
         zeig%coh%litN_frt = zeig%coh%litN_frt + zeig%coh%ntreem*zeig%coh%x_frt*cpart/spar(taxnr)%cnr_frt
         zeig%coh%litC_tb = zeig%coh%litC_tb + zeig%coh%ntreet*zeig%coh%x_tb*cpart
         zeig%coh%litN_tb = zeig%coh%litN_tb + zeig%coh%ntreet*zeig%coh%x_tb*cpart/spar(taxnr)%cnr_tbc
         zeig%coh%litC_crt = zeig%coh%litC_crt + zeig%coh%ntreet*zeig%coh%x_crt*cpart
         zeig%coh%litN_crt = zeig%coh%litN_crt + zeig%coh%ntreet*zeig%coh%x_crt*cpart/spar(taxnr)%cnr_crt

   endif
 zeig=>zeig%next
 enddo
END SUBROUTINE direct_fel

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  different thinning regimes (1-4) for trees with dominant height above ho2
!         thinning regime 1 - moderate low-thinning / m�ssige Niederdurchforstung
!         thinning regime 2 - strong/heavy low-thinning / starke Niederdurchforstung
!         thinning regime 3 - high-thinning / Hochdurchforstung
!         thinning regime 4 - selective thinning (from upper or middle thirg of thickest trees
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

SUBROUTINE thinning(c1,c2,actspec, inum)

use data_stand
use data_manag
use data_simul
use data_species
use data_par
implicit none

real     ::  dbhmin=0,     &
             wpa=0,        &      ! Weibull parameter
             wpb=0,        &      !    -"-
             wpc=0,         &     !    -"-
             d63=0,         &

             pequal,      &
             tdbh=0,        &

             bas_help=0.,   &
             dbh_h =0,      &
             db_l = 0.,     &
             db_u = 0.,     &
             c1,            &
             d_est=0.,         &
             w_kb=0.,        &
             c_usp

real     ::  help_cra,      &      ! actual crown area
             density,       &      ! ratio of crown area to patch size

             bas_target,    &      ! relative value for basal area thinning
             bas_area,    &
             help

real     ::  hg,          &        ! hight of base area mean stem
             bg,          &        ! degree of tillering
             dfbg,        &        ! opt. base area
             stage,       &        ! actual age
             basha,       &
             stump_v,     &        ! volume and dry weight of stump
             stump_dw

integer  ::  nrmin,      &

             flagth,     &
             c2,         &
             taxnr,      &
             nhelp1,     &
             counth,     &
             nhelp2,     &
             zbnr_pa,    &
             callnum,    &
             actspec, inum                    ! number of species for thinning

integer    :: lowtree, agedm

! auxilarity for thinning routine 4: selective thinning
integer  :: nrmax,anz,anz1,count,flagexit, flagc, num_thin,j,  &
            nhelp,idum ,numtr, third,anztree_ha,i
integer,dimension(0:anz_coh) :: cohl
real     :: meanzb, stand,xhelp, sumdh, sumd, hh ,rel_bas
real,external   :: gasdev
real,dimension(nspecies) :: cr_rel    ! relative part of species specific crown area of total crown area

! target calculation for basal area  reduction

 bas_target = ((time-thinyear(actspec))/5)*0.05
 bas_area = 0.

 bas_help = 0.
 help_cra = 0.
 cr_rel = 1.
  callnum = 0
  count = 0
  cohl = -1
 flagth = 0
 help=0.
 lowtree=0
 anztree_ha = nint(anz_tree_dbh*10000./kpatchsize)
 third = nint(anz_tree_dbh*0.333333)

 sumdh = 0.; sumd = 0.

! calculation of mean diameter (corresponding to med_diam) and basal area of stand
! calculation hg ( hight of base area mean stem)
i = inum
    zeig => pt%first
    DO
        IF (.NOT. ASSOCIATED(zeig)) EXIT
            if(zeig%coh%species.eq.actspec) then
               stage =  zeig%coh%x_age
               help_cra = help_cra +  zeig%coh%ntreea* zeig%coh%crown_area
              IF((zeig%coh%ntreea>0).and.(zeig%coh%diam>0)) THEN
               ! foresters defenition
               sumd = sumd + zeig%coh%diam*zeig%coh%diam
               sumdh = sumdh + zeig%coh%diam*zeig%coh%diam* zeig%coh%height
               help = help + zeig%coh%ntreea*(zeig%coh%diam**2)
               bas_area = bas_area +  zeig%coh%ntreea*(zeig%coh%diam**2)*pi/4.
              ELSE
               ! trees with DBH = 0 for population and species
               lowtree = lowtree + zeig%coh%ntreea

              ENDIF
            end if
            zeig => zeig%next
    ENDDO  ! cohorts

 hg = (sumdh/sumd)/100.

! basal area /ha
 basha = bas_area/kpatchsize     ! cm�/patch ---> m�/ha

 rel_bas = bas_area/basarea_tot
 if(thin_ob.eq.1) then
! calculation of optimal basal area (Brandenburg) per patchsize

     call calc_gfbg(dfbg,specnr(i), stage, hg)
! correction
     dfbg = dfbg* kpatchsize        !   m�/ha ---> cm�/patchsize

     if(anz_spec.eq.1) then
         if(dfbg.lt.0.5*bas_area) dfbg = 0.5*bas_area
! calculation of BG (Bestockungsgrad)
     else
! calculation of relative part of crown area
         cr_rel(actspec) = svar(actspec)%crown_area / crown_area

     end if
      bg = rel_bas*bas_area/dfbg

! calculation of basale area target depending on target optb  'Bestockungsgrad'
     bas_target = rel_bas*optb*dfbg
 else
! calculation of density dependent target for thinning
     density = help_cra/kpatchsize

     call calc_usp (actspec,age_spec(i),density,c_usp)

! Modification of 'Nutzungsprozent'  to avoid large number for c_usp
     c_usp = c_usp*np_mod(actspec)

     if(thinyear(actspec).eq.0) then
         hh =  c_usp*(time)/10.
         if(hh.lt.0.7) then
               c_usp = hh
         else
             c_usp = 0.5
         end if
        bas_target = bas_area - bas_area*c_usp
     else
! Modification
         if(c_usp.gt.0.4) then
                c_usp =c_usp * (time -thinyear(actspec))/20.
         end if
         bas_target = bas_area - bas_area*c_usp
     end if
 end if
 select case(c2)


     case(1:3)
! different thinnings from below and above

     select case(c2)
          case(1)
!  moderate low-thinning 
                d_est = 1.02
! change of w_kb to exclude small diameter classes
                 w_kb = 2.5
          case(2)
! high low-thinning
                d_est = 1.03
                w_kb = 1.5
          case(3)
! high-thinning
                d_est = 1.04
                w_kb = 1.2
      end select

! calculation of Weibull-Parameter
 if(bas_area.gt.bas_target) then
  call min_dbh(nrmin,dbhmin,agedm,actspec)
    bas_help = bas_area
    wpa = dbhmin
    d63 = svar(actspec)%med_diam * d_est
    wpb = (d63 - wpa)/ w_kb
    wpc = 2

! selection of trees for thinning
 do
    call random_number(pequal)
    tdbh = wpa + wpb*(-log(1.-pequal))**(1./wpc)
    callnum = callnum +1
    flagth = 0
    zeig => pt%first

    DO
        IF (.NOT. ASSOCIATED(zeig)) EXIT
       if(zeig%coh%species.eq.actspec) then

          if(zeig%coh%diam.gt.0.) then
             dbh_h = zeig%coh%diam
              db_l =  dbh_h - 0.1*dbh_h
              db_u =  dbh_h + 0.1*dbh_h
              if (tdbh.ge.db_l.and.tdbh.le.db_u.and. zeig%coh%ntreea.ne. 0) then
                 zeig%coh%ntreea = zeig%coh%ntreea -1
                 zeig%coh%nta = zeig%coh%ntreea
                 zeig%coh%ntreem = zeig%coh%ntreem +1
                 bas_help = bas_help - (zeig%coh%diam**2)*pi/4.
                 flagth = 1
             end if
             if(flagth.eq.1) exit
          end if
        end if
        zeig=> zeig%next
     END DO  ! cohorts

     if(bas_help .le. bas_target) exit
 end do   ! selection of trees
 end if

 case(4)

! selective thinning

! normal(or equal) distributed thinning from one third of the trees (upper or middle): n*anz_ziel or
! depending an basal area
! ho2: n=2; ho3,ho4: n=1.5 ho>ho4: n=1
! determination of the third of trees with the thickest diameter  (sorting of cohorts concerning diameter
! necessary: normal distribution with 2 parameters:  mean diameter of the third and standard deviation

DO i=1,anz_spec
!Calculation of number of thinning trees
   IF ( c1.eq.ho2) THEN
      num_thin = NINT(2* zbnr(specnr(i))*kpatchsize/10000.)
   ELSE IF( c1.eq.ho3.or.c1.eq.ho4) THEN
! change of num_thin because of errors during thinning
      num_thin = NINT(zbnr(specnr(i))*kpatchsize/10000.)
   ELSE
      num_thin = NINT(zbnr(specnr(i))*kpatchsize/10000.)
   END IF
  if(anztree_ha.lt.(zbnr(specnr(i))+ zbnr(specnr(i))*0.2)) return

! determine cohorts which fulfill the upper third  --> selected for thinning
    anz = 0
    flagexit = 0
    flagc = 0
    if(anz_tree_dbh>1) then
      do
        call max_diam(nrmax,anz,cohl, specnr(i))
        zeig=>pt%first
        do
          if(.not.associated(zeig)) exit
          if(zeig%coh%diam.gt.0) then
            if(zeig%coh%ident.eq.nrmax) then
               count = count + zeig%coh%ntreea
               if(count.ge. third) flagexit = 1
               flagc = 1
             end if
             if (flagc.eq. 1) exit
           end if
           zeig=>zeig%next
        end do
        if(flagexit.eq.1) exit
        flagc = 0
      end do
    end if

  IF(c1.eq.0) THEN
      
! determine cohorts which fulfill the middle third of thickness
!     if the number of one third is not definded by an even number of cohorts
!     the middle third starts in the last cohort of the upper third
! some refinements are possible: the number of trees are marked in each cohort which
! are available for thinning (may be in the last cohort of the thirg only x%)

     if(count.eq.third) then

         anz1 = anz+1
     else
         anz1 = anz
         anz = anz-1
     end if

     count = 0
     flagexit = 0
     flagc = 0
     if(anz_tree>1) THEN
         do
            call max_diam(nrmax,anz,cohl, specnr(i))
            zeig=>pt%first
            do
            if(.not.associated(zeig)) exit
            if(zeig%coh%ident.eq.nrmax) then
                 count = count + zeig%coh%ntreea
                 if(count.ge. third) flagexit = 1
                 flagc = 1
            end if
            if (flagc.eq. 1) exit
            zeig=>zeig%next
         end do
         if(flagexit.eq.1) exit
         flagc = 0
        end do
      end if

  ENDIF

! calculation on mean and standard deviation of cohorts selected for thinning
   stand = 0.
   if(c1.ne.0) anz1 =1
   meanzb = 0.
   counth = 0
  do j = anz1,anz
     zeig=>pt%first
     do
       if(.not.associated(zeig)) exit
       nrmax = cohl(j-1)
       if (zeig%coh%ident.eq.nrmax) then
           meanzb = meanzb + zeig%coh%ntreea*zeig%coh%diam
           counth = counth + zeig%coh%ntreea
       end if
       zeig=>zeig%next

     end do
  end do
! mean value
   meanzb = meanzb/count
! standard deviation
    do j = anz1,anz
      zeig=>pt%first
     do
       if(.not.associated(zeig)) exit
       nrmax = cohl(j-1)
       if (zeig%coh%ident.eq.nrmax) then
         stand = stand+ zeig%coh%ntreea*(zeig%coh%diam - meanzb)*(zeig%coh%diam - meanzb)
       end if
       zeig=>zeig%next
     end do
   end do
   stand = sqrt(stand/count)

! thinning of num_thin trees from the upper third
! using normal distribution
! if ho>ho4 the selection of trees from the middle third is controlled by basal area
! a reduction of basal area by 10% 
   
  idum = -1
  nhelp = num_thin
  numtr = 0
  bas_help=bas_area

  do j=anz1,anz
       zeig=>pt%first
       DO
          IF (.NOT. ASSOCIATED(zeig)) EXIT
          if(zeig%coh%ident.eq.cohl(j-1)) numtr = numtr+zeig%coh%ntreea
          zeig=>zeig%next
      end do
  end do
  nhelp1 = anz_tree
  nhelp2 = count
  if(nhelp.gt.numtr) nhelp = numtr
    DO
         xhelp= meanzb+stand*gasdev(idum)
         flagth = 0
         
         DO j = anz1, anz
             zeig => pt%first

             DO
               IF (.NOT. ASSOCIATED(zeig)) EXIT
               if(zeig%coh%ident.eq.cohl(j-1)) then
                    dbh_h = zeig%coh%diam
                    db_l =  dbh_h - 0.1*dbh_h
                    db_u =  dbh_h + 0.1*dbh_h
                    if (xhelp.ge.db_l.and.xhelp.le.db_u.and. zeig%coh%ntreea.ne. 0) then
                       zeig%coh%ntreea = zeig%coh%ntreea -1
                       zeig%coh%nta = zeig%coh%ntreea
                       zeig%coh%ntreem = zeig%coh%ntreem +1
                       if(c1.eq.0) then
                         bas_help = bas_help - (zeig%coh%diam**2)*pi*0.25
                         nhelp1 = nhelp1 -1
                         nhelp2 = nhelp2 -1
                       else
                         nhelp= nhelp -1
                       endif
                       flagth = 1

                    end if
                end if
                if(flagth.eq.1) exit
                zeig=> zeig%next
             ENDDO
           if(flagth.eq.1) exit
         END DO

! criteria of finishing thinning

        zbnr_pa =  nint(zbnr(specnr(i))*kpatchsize/10000.)
        if(c1.eq.0 .and.( bas_help.le.(bas_area - bas_area*bas_target).or.nhelp1.eq.zbnr_pa) )  exit
        if(c1.eq.0 .and.( nhelp1.eq.0  .or. nhelp2.eq.0)) exit
        if(c1.ne.0 .and. nhelp.eq.0) exit

    ENDDO
END DO   ! speices loop
 end select

! adding biomasses to litter pools depending on stage of stand
stump_sum = 0
 zeig=>pt%first

 do
   if(.not.associated(zeig)) exit
   taxnr=zeig%coh%species

   if(zeig%coh%ntreem>0)then
! all parts without stems of trees are input for litter
         zeig%coh%litC_fol = zeig%coh%litC_fol + zeig%coh%ntreem*(1.-spar(taxnr)%psf)*zeig%coh%x_fol*cpart
         zeig%coh%litN_fol = zeig%coh%litN_fol + zeig%coh%ntreem*((1.-spar(taxnr)%psf)*zeig%coh%x_fol*cpart)/spar(taxnr)%cnr_fol
         zeig%coh%litC_frt = zeig%coh%litC_frt + zeig%coh%ntreem*zeig%coh%x_frt*cpart
         zeig%coh%litN_frt = zeig%coh%litN_frt + zeig%coh%ntreem*zeig%coh%x_frt*cpart/spar(taxnr)%cnr_frt
         zeig%coh%litC_tb = zeig%coh%litC_tb + zeig%coh%ntreem*zeig%coh%x_tb*cpart
         zeig%coh%litN_tb = zeig%coh%litN_tb + zeig%coh%ntreem*zeig%coh%x_tb*cpart/spar(taxnr)%cnr_tbc
         zeig%coh%litC_crt = zeig%coh%litC_crt + zeig%coh%ntreem*zeig%coh%x_crt*cpart
         zeig%coh%litN_crt = zeig%coh%litN_crt + zeig%coh%ntreem*zeig%coh%x_crt*cpart/spar(taxnr)%cnr_crt

! stumps into stem litter
          call stump( zeig%coh%x_ahb, zeig%coh%asapw,zeig%coh%dcrb,zeig%coh%x_hbole,  &
                      zeig%coh%height, taxnr,stump_v, stump_dw)
          zeig%coh%litC_stem = zeig%coh%litC_stem +  zeig%coh%ntreem*stump_dw*cpart
          zeig%coh%litN_stem = zeig%coh%litC_stem/spar(taxnr)%cnr_stem
          stump_sum = stump_sum + zeig%coh%ntreem*stump_dw

         if(maninf.eq.'brushing'.and.flag_brush.ne.0) then
             zeig%coh%litC_stem =zeig%coh%litC_stem + zeig%coh%ntreem*(zeig%coh%x_sap+zeig%coh%x_hrt)*cpart
             zeig%coh%litN_stem = zeig%coh%litC_stem/spar(taxnr)%cnr_stem
         end if
   endif
 zeig=>zeig%next
 enddo
END SUBROUTINE thinning
    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!   SR for clear cut
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

SUBROUTINE felling(nr,i)

 use data_stand
 use data_manag
 use data_simul
 use data_species
 use data_par
 use data_soil_cn
 
 implicit none
 
 integer    :: taxnr, i, nr
 real       :: stump_v, stump_dw, help

     zeig=>pt%first
     do
       if(.not.associated(zeig)) exit
       taxnr = zeig%coh%species
	   if(taxnr.le.nspec_tree) then
         
        if(thr7.eq.2.and. taxnr.eq.nr) then
             zeig%coh%ntreem = zeig%coh%ntreea
             zeig%coh%ntreea = 0
             zeig%coh%nta = 0.
         else if(thr7.ne.2.and. taxnr.eq.nr.and. zeig%coh%x_age.eq.age_spec(i).and. zeig%coh%shelter.eq.1) then
            zeig%coh%ntreem = zeig%coh%ntreea
             zeig%coh%ntreea = 0
             zeig%coh%nta = 0.

         end if
        else
! reduction of soil vegetation after felling

		 taxnr = zeig%coh%species
         help = zeig%coh%x_fol
		 zeig%coh%x_fol = 0.005*help
         zeig%coh%litC_fol = zeig%coh%litC_fol + 0.995*zeig%coh%ntreem*(1.-spar(taxnr)%psf)*help*cpart
         zeig%coh%litN_fol = zeig%coh%litN_fol + 0.995*zeig%coh%ntreem*((1.-spar(taxnr)%psf)*help*cpart)/spar(taxnr)%cnr_fol
         help = zeig%coh%x_frt
         zeig%coh%x_frt = 0.005*help
         zeig%coh%litC_frt = zeig%coh%litC_frt + 0.995*zeig%coh%ntreem*help*cpart
         zeig%coh%litN_frt = zeig%coh%litN_frt + 0.995*zeig%coh%ntreem*help*cpart/spar(taxnr)%cnr_frt
         help = zeig%coh%x_sap 
		 zeig%coh%x_sap = 0.005*help
         zeig%coh%litC_fol = zeig%coh%litC_fol + 0.995*zeig%coh%ntreem*help*cpart
         zeig%coh%litN_fol = zeig%coh%litN_fol + 0.995*zeig%coh%ntreem*((1.-spar(taxnr)%psf)*help*cpart)/spar(taxnr)%cnr_fol
         zeig%coh%Fmax    = zeig%coh%x_fol
         zeig%coh%t_leaf  = zeig%coh%med_sla* zeig%coh%x_fol      ! [m2]
         zeig%coh%nta     = zeig%coh%nTreeA	 

		end if
       zeig=>zeig%next
     end do
 zeig=>pt%first

 do
   if(.not.associated(zeig)) exit
   taxnr=zeig%coh%species

   if(zeig%coh%ntreem>0.and. taxnr.eq.nr)then
! all parts without stems of trees are input for litter
         zeig%coh%litC_fol = zeig%coh%litC_fol + zeig%coh%ntreem*(1.-spar(taxnr)%psf)*zeig%coh%x_fol*cpart
         zeig%coh%litN_fol = zeig%coh%litN_fol + zeig%coh%ntreem*((1.-spar(taxnr)%psf)*zeig%coh%x_fol*cpart)/spar(taxnr)%cnr_fol
         zeig%coh%litC_frt = zeig%coh%litC_frt + zeig%coh%ntreem*zeig%coh%x_frt*cpart
         zeig%coh%litN_frt = zeig%coh%litN_frt + zeig%coh%ntreem*zeig%coh%x_frt*cpart/spar(taxnr)%cnr_frt
         zeig%coh%litC_tb = zeig%coh%litC_tb + zeig%coh%ntreem*zeig%coh%x_tb*cpart
         zeig%coh%litN_tb = zeig%coh%litN_tb + zeig%coh%ntreem*zeig%coh%x_tb*cpart/spar(taxnr)%cnr_tbc
         zeig%coh%litC_crt = zeig%coh%litC_crt + zeig%coh%ntreem*zeig%coh%x_crt*cpart
         zeig%coh%litN_crt = zeig%coh%litN_crt + zeig%coh%ntreem*zeig%coh%x_crt*cpart/spar(taxnr)%cnr_crt

! stumps into stem litter
          call stump( zeig%coh%x_ahb, zeig%coh%asapw,zeig%coh%dcrb,zeig%coh%x_hbole,  &
                      zeig%coh%height, taxnr,stump_v, stump_dw)
          zeig%coh%litC_stem = zeig%coh%litC_stem +  zeig%coh%ntreem*stump_dw*cpart
          zeig%coh%litN_stem = zeig%coh%litC_stem/spar(taxnr)%cnr_stem
          stump_sum = stump_sum + zeig%coh%ntreem*stump_dw


   endif
 zeig=>zeig%next
 enddo
END SUBROUTINE felling

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!   subroutine for shelterwood management
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
SUBROUTINE shelterwood_man(nrsh,inum,domage)
 use data_stand
 use data_manag
 use data_simul
 use data_par
 use data_species
 implicit none

 real     :: bared,      &      ! reduction of basal area
             bas_help,   &
             bas_area,   &
             pequal,     &
             domage,     &
             help,       &
             stump_v,    &
             stump_dw
 integer  :: taxnr,      &

             flagc,      &
             flagexit,   &
             num_coh,    &
             thinflag, j, &
             count, third,&
             counth,      &
             anz_treesh=0, &
             anz_2th,      &
             nrsh,         &
             minident,     &
             inum, help_shnum

 integer, dimension(1:anz_coh) :: coh_2th
 allocate (dbh_rank(anz_coh))
 minident = 100000
 bas_area = 0.
 anz_treesh = 0
 help_shnum = 0
! tending of trees, planted at first shelterwood treatment
 help = time - shelteryear
 IF(help.eq.15..and.flag_shelter.eq.1 .and.shelteryear.ne.0) THEN
       call tending(nrsh,inum)
 END IF

! labelling of trees for shelterwood at first shelterwood treatment
if (shelteryear.eq.0.or.shelteryear.eq.time) then
    zeig=>pt%first

     do
        if(.not.associated(zeig)) exit
        write(5432,*) zeig%coh%ntreea
        if(zeig%coh%species.eq.nrsh.and.zeig%coh%x_age.gt.10) zeig%coh%shelter = 1.
        zeig=> zeig%next
     end do
 end if
! calculation of number of shelter trees
   zeig=>pt%first
     do
       if(.not.associated(zeig)) exit
       if(zeig%coh%shelter.eq.1.and. zeig%coh%species.eq.nrsh) anz_treesh = anz_treesh +zeig%coh%ntreea

       zeig=>zeig%next

     end do
write(5432,*) time, 'anz_treesh', anz_treesh
 count = 0
 IF((time-shelteryear).eq.15 .or. shelteryear .eq. 0..or.shelteryear.eq.time) THEN
 call dimsort(anz_coh, 'dbh',dbh_rank)
 flag_manreal = 1
 if (shelteryear.eq.0) then
   maninf = 'shelterwood system1'
 else
   maninf = 'shelterwood system2'
 end if
 meas = 0
 third = nint(anz_treesh*0.3333333)
 taxnr = nrsh

! calculation of  basal area of shelterwood
    zeig => pt%first
    DO
        IF (.NOT. ASSOCIATED(zeig)) EXIT

            if(zeig%coh%shelter.eq.1.and. zeig%coh%species.eq.taxnr) then
              IF((zeig%coh%ntreea>0).and.(zeig%coh%diam>0)) THEN

                 bas_area = bas_area +  zeig%coh%ntreea*(zeig%coh%diam**2)*pi/4.
              End if
            end if
        zeig => zeig%next
    ENDDO

! declaration of reduction coefficient of basal area
 if(domage.eq.regage(domspec)) then
       bared = 0.3
 else
       bared = 0.4
 end if

!  lower two thirds sorted by diameter in coh_2th
 counth = 0
 flagexit = 0
 flagc = 0
 anz_2th = 0
 coh_2th = -1
 if(anz_tree>1) then
  do j = 1,anz_coh
       zeig => pt%first
       do
         if(.not.associated(zeig)) exit
         if(zeig%coh%ident.eq.dbh_rank(j).and.zeig%coh%shelter.eq.1.and. zeig%coh%species.eq.nrsh) then
            counth = counth + zeig%coh%ntreea
            anz_2th = anz_2th +1
            if(counth.ge.2*third) flagexit =1
            coh_2th(anz_2th) = zeig%coh%ident
            if(zeig%coh%ident.lt.minident) minident =zeig%coh%ident
            flagc = 1
         end if
         if(flagc.eq.1) exit
         zeig=>zeig%next
      end do
      if (flagexit.eq.1) exit
      flagc = 0
   end do
 end if

! thinning with equal distribution from cohorts listed in coh_2th
 bas_help = bas_area

 DO
  flagexit = 0
  thinflag = 0
  call random_number(pequal)
  num_coh = nint(pequal*anz_2th + 0.5)

     zeig=> pt%first
     do
        if(.not.associated(zeig)) exit
        if(zeig%coh%ident.eq.coh_2th(num_coh).and.zeig%coh%shelter.eq.1.and. zeig%coh%species.eq.nrsh) then
           if(zeig%coh%ntreea.ge.1) then
                    zeig%coh%ntreea =   zeig%coh%ntreea - 1
                    help_shnum = help_shnum +1
                    zeig%coh%nta = zeig%coh%nta -1.
                    zeig%coh%ntreem = zeig%coh%ntreem + 1
                    bas_help = bas_help - (zeig%coh%diam**2)*pi/4
                    thinflag = 1
            end if
        end if
        if(thinflag.eq.1) exit
        zeig=>zeig%next
     end do
     if(bas_help.le.(bas_area -bas_area*bared)) exit
     if(help_shnum.eq. counth) exit
 END DO

! adding biomasses to litter pools depending on stage of stand
if(anz_treesh>0) then
 zeig=>pt%first

 do
   if(.not.associated(zeig)) exit
   taxnr=zeig%coh%species

   if(zeig%coh%ntreem>0..and. zeig%coh%species.eq.nrsh)then
! all parts without stems of trees are input for litter
         zeig%coh%litC_fol = zeig%coh%litC_fol + zeig%coh%ntreem*(1.-spar(taxnr)%psf)*zeig%coh%x_fol*cpart
         zeig%coh%litN_fol = zeig%coh%litN_fol + zeig%coh%ntreem*((1.-spar(taxnr)%psf)*zeig%coh%x_fol*cpart)/spar(taxnr)%cnr_fol
         zeig%coh%litC_frt = zeig%coh%litC_frt + zeig%coh%ntreem*zeig%coh%x_frt*cpart
         zeig%coh%litN_frt = zeig%coh%litN_frt + zeig%coh%ntreem*zeig%coh%x_frt*cpart/spar(taxnr)%cnr_frt
         zeig%coh%litC_tb = zeig%coh%litC_tb + zeig%coh%ntreem*zeig%coh%x_tb*cpart
         zeig%coh%litN_tb = zeig%coh%litN_tb + zeig%coh%ntreem*zeig%coh%x_tb*cpart/spar(taxnr)%cnr_tbc
         zeig%coh%litC_crt = zeig%coh%litC_crt + zeig%coh%ntreem*zeig%coh%x_crt*cpart
         zeig%coh%litN_crt = zeig%coh%litN_crt + zeig%coh%ntreem*zeig%coh%x_crt*cpart/spar(taxnr)%cnr_crt

! stumps into stem litter
          call stump( zeig%coh%x_ahb, zeig%coh%asapw,zeig%coh%dcrb,zeig%coh%x_hbole,  &
                      zeig%coh%height,taxnr, stump_v, stump_dw)
          zeig%coh%litC_stem = zeig%coh%litC_stem +  zeig%coh%ntreem*stump_dw*cpart
          zeig%coh%litN_stem = zeig%coh%litC_stem/spar(taxnr)%cnr_stem
          stump_sum = stump_sum + zeig%coh%ntreem*stump_dw
  
! stump biomass is added to stem litter litC_stem, litN_stem
   endif
 zeig=>zeig%next
 enddo
 END IF
 end if  ! anz_treesh
 deallocate(dbh_rank)
END SUBROUTINE  shelterwood_man

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    

SUBROUTINE min_dbh(nrmin,help_h1,agedm, spnr)
 use data_stand
implicit none
 integer  :: nrmin,spnr, agedm, agedmh
 integer  :: nrmin_h
 integer  :: testflag
 real     :: help_h1, help_h2

 testflag=0
 agedm = -1
 agedmh = -1
 nrmin = -1
 nrmin_h = -1
 help_h2=0.
 help_h1=1000.
 zeig=>pt%first
 do
    if(.not.associated(zeig)) exit
     if(zeig%coh%species.eq.spnr) then
      if(zeig%coh%diam.gt.0.) then

          help_h2= zeig%coh%diam
          nrmin_h = zeig%coh%ident
		  agedmh = zeig%coh%x_age
          if(help_h2.lt. help_h1) then
            help_h1 = help_h2
            nrmin = nrmin_h
			agedm = agedmh
          end if
       end if
      end if
    zeig=>zeig%next

 end do

END SUBROUTINE min_dbh

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
SUBROUTINE min_dbh_tar(nrmin,help_h1,spnr,tar)
 use data_stand
implicit none
 integer  :: nrmin,spnr
 integer  :: nrmin_h
 integer  :: testflag
 real     :: help_h1, help_h2
 real     :: tar

 testflag=0
 nrmin = -1
 nrmin_h = -1
 help_h2=0.
 help_h1=1000.
 zeig=>pt%first
 do
    if(.not.associated(zeig)) exit
     if(zeig%coh%species.eq.spnr) then
      if(zeig%coh%diam.gt.0..and. zeig%coh%height.gt.tar) then

          help_h2= zeig%coh%diam
          nrmin_h = zeig%coh%ident
          if(help_h2.lt. help_h1) then
            help_h1 = help_h2
            nrmin = nrmin_h
          end if
       end if
      end if
    zeig=>zeig%next

 end do

END SUBROUTINE min_dbh_tar
    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

SUBROUTINE min_dbh_overs(nrmin,help_h1,spnr)
 use data_stand
implicit none
 integer  :: nrmin,spnr
 integer  :: nrmin_h
 integer  :: testflag
 real     :: help_h1, help_h2

 testflag=0
 nrmin = -1
 nrmin_h = -1
 help_h2=0.
 help_h1=1000.
 zeig=>pt%first
 do
    if(.not.associated(zeig)) exit
     if(zeig%coh%species.eq.spnr) then
      if(zeig%coh%diam.gt.0..and. zeig%coh%underst.eq.0) then

          help_h2= zeig%coh%diam
          nrmin_h = zeig%coh%ident
          if(help_h2.lt. help_h1) then
            help_h1 = help_h2
            nrmin = nrmin_h
          end if
       end if
      end if
    zeig=>zeig%next

 end do

END SUBROUTINE min_dbh_overs
    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
SUBROUTINE min_dbh_unders(nrmin,help_h1,spnr)
 use data_stand
implicit none
 integer  :: nrmin,spnr
 integer  :: nrmin_h
 integer  ::  testflag
 real     :: help_h1, help_h2

 testflag=0
 nrmin = -1
 nrmin_h = -1
 help_h2=0.
 help_h1=1000.
 zeig=>pt%first
 do
    if(.not.associated(zeig)) exit
     if(zeig%coh%species.eq.spnr) then
      if(zeig%coh%diam.gt.0..and. zeig%coh%underst.eq.2) then

          help_h2= zeig%coh%diam
          nrmin_h = zeig%coh%ident
          if(help_h2.lt. help_h1) then
            help_h1 = help_h2
            nrmin = nrmin_h
          end if
       end if
      end if
    zeig=>zeig%next

 end do

END SUBROUTINE min_dbh_unders

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
SUBROUTINE max_dbh(nrmax,help_h1,agedm,spnr)
 use data_stand
implicit none
 integer  :: nrmax,spnr, agedm, agedmh
 integer  :: nrmax_h
 integer  :: testflag
 real     :: help_h1, help_h2

 testflag=0
 agedm =-1
 agedmh = -1
 nrmax = -1
 nrmax_h = -1
 help_h2=0.
 help_h1=0.
 zeig=>pt%first
 do
    if(.not.associated(zeig)) exit
     if(zeig%coh%species.eq.spnr) then
      if(zeig%coh%diam.gt.0.) then

          help_h2= zeig%coh%diam
          nrmax_h = zeig%coh%ident
		  agedmh = zeig%coh%x_age
          if(help_h2.gt. help_h1) then
            help_h1 = help_h2
            nrmax = nrmax_h
			agedm = agedmh
          end if
       end if
      end if
    zeig=>zeig%next

 end do

END SUBROUTINE max_dbh
    
!
! calculation of cohort number with maximal diameter
!

SUBROUTINE max_diam(nrmax,anz,cohl, specnum)
 use data_stand
implicit none

 integer  :: nrmax,i
 integer  :: nrmax_h, specnum
 integer  :: anz, testflag
 real     :: help_h1, help_h2
 integer,dimension(0:anz_coh) :: cohl

 testflag=0
 nrmax = -1
 nrmax_h = -1
 help_h2=0.
 help_h1=0.
 zeig=>pt%first
 do
    if(.not.associated(zeig)) exit
    do  i=0,anz-1
      if(cohl(i).eq.zeig%coh%ident.and. zeig%coh%species.eq.specnum) then
          testflag=1
      endif
    end do
        if (testflag.eq.0) then
          help_h2= zeig%coh%diam
          nrmax_h = zeig%coh%ident
          if(help_h2.gt. help_h1) then
            help_h1 = help_h2
            nrmax = nrmax_h
          end if
       end if

    zeig=>zeig%next
    testflag = 0
 end do
    anz = anz +1
    cohl(anz-1) = nrmax
END SUBROUTINE max_diam

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!     SR calc_usp
!     calculaiton of percent of using (NUtzungsprozent)
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine calc_usp (taxnr,ages,density,c_usp)

use data_species
use data_manag

real      ::density, c_usp
real,dimension(20) :: spec_den=(/0.,0.8,0.9,1.,0.8,0.9,1.,1.1,0.8,0.9,1.,1.1,0.7,0.8,0.9,1.,0.7,0.8,0.9,1./)
integer, dimension(13)       :: age_den=(/15,20,25,30,35,40,45,50,60,70,80,100,120/)
integer   :: j, i,help1, taxnr,ages
c_usp =0.

 do i=1,3
    help1=(taxnr-1)*4+i

    if(density.gt.spec_den(help1).and. density.le.spec_den(help1+1)) then
          do  j= 1,12
                   if(ages.ge.age_den(j).and.ages.lt.age_den(j+1))then
                        c_usp = usp(help1,j)
                    end if
          end do
    end if
 end do
 help1=(taxnr-1)*4+4
 if(c_usp.eq.0..and. density.gt.spec_den(help1)) then

           do  j= 1,12
                 if(ages.ge.age_den(j).and.ages.lt.age_den(j+1))then
                      c_usp = usp(help1,j)
                  end if
            end do
 else if (c_usp.eq.0..and.density .le. spec_den( help1-3)) then
           do  j= 1,12
                 if(ages.ge.age_den(j).and.ages.lt.age_den(j+1))then
                      c_usp = usp(help1-3,j)
                  end if
            end do
 end if
end subroutine calc_usp
    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!           4C-
!      Subroutine calc_gfbg
!      calculation of optimal basal area
!      coresponding to functions from
!      A. Degenhardt: Algorithmen und Programme zur
!                     waldwachstumskundlichen Auswertung von
!                     Versuchs- und probefl�chen
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

SUBROUTINE calc_gfbg(gfbg, ntax, stage, hg)
 use data_par
 use data_stand
 implicit none
real, dimension(12)   ::    p=(/5.3774914,4.3364045,1.7138966,                    &
                                0.1791894,0.6499329,0.581721,                     &
                                0.64149,1.39876,0.38106,3.48086,4.55256,1.10352/)     ! parameter pinus
real, dimension(14)   ::    s=(/52.021649311,17.01260031,1.817338508,             &
                                3.97091538,0.165219412,0.017015893,               &
                                17.17273582,77.00271993,180.95845108,69.85082406, &
                                0.284339648,6.211490243,8.057235477,2.600807284/)     ! parameter spruce
real, dimension(11)   ::    b=(/5.1961292,5.8518918,2.048007,                     &
                                0.1517038,0.8873933,0.9555725,                    &
                                0.845794,29.76635,9.89798,0.2033,0.092586/)            ! parameter beech
real, dimension(16)   ::    o=(/10.937989911, 30.98059032,36.683338986,4.8203797, &
                                0.217782149,0.559666286,1.253027352,2.447035652,  &
                                3.172437267,26.001075916,15.01095715,2.411330088, &
                                0.286619845,0.126747922,0.121360347,0.05650846/)
real, dimension(9)    ::    bi=(/2.304633491,5.7831992,0.057831992,               &
                                 99.89719563,4983.109428, 387539.3699,            &
                                 192.06078091,0.070580839, 0.624018136/)               ! birch (Sandbirke)
real, dimension(16)   ::    pa=(/12.114711547,13.90837359,11.746497917, 2.963065353,  &
                                 0.298215006,0.325115413,0.46694307,0.043088114,      &
								 5.314568374, 9.635476988, 23.20634163,9.473964111,   &
								 0.845408671,0.187292811,0.025416101,0.050721202/)
real                  ::  abon,  &
                          rbon,  &
                          h1,h2,h3,h4,alt10, alt100, nvb, dgvb,gfbg,stage,hg
integer               :: ntax
 alt10= 10/stage
 alt100= stage/100
 h1 = 0.;h2=0.;h3=0.;h4=0.
 select case(ntax)
      case(1)    !  beech
        h1 = b(1) + b(2)*alt100 - b(3)*alt100*alt100
        h2 = -b(4) - b(5)*alt10 - b(6)*alt10*alt10
        rbon = h1+h2*hg
        abon = 36.- 4.*rbon
        gfbg = b(7) + b(8)*alt100 -b(9)*alt100*alt100 +abon*(b(10) + b(11)*alt100)

      case(2)    !  spruce
        h1 =  (alog(hg)-s(4))/(-s(5)+alog(1.-exp(-s(6)*stage)))
        abon = s(1)-s(2)*h1 +s(3)*h1*h1
        rbon = (38.-abon)/4.
        h2 = - s(7)-s(8)*alt100+s(9)*alt100*alt100-s(10)*alt100*alt100*alt100
        h3 = s(11) + s(12)*alt100 -s(13)*alt100*alt100 + s(14)* alt100*alt100*alt100
        gfbg = h2 + h3*abon

      case(3)    !  pine
        h1 = p(1) + p(2)*alt100 - p(3)*alt100*alt100
        h2 = -p(4) - p(5)*alt10 -p(6)*alt10*alt10
        rbon = h1 + h2*hg
        abon = 32.- 4.*rbon
        h3 = p(7)+p(8)*alog10(stage)-p(9)*alog10(stage)*alog10(stage)
        h4 = -p(10) + p(11)*alog10(stage) - p(12)*alog10(stage)*alog10(stage)
        gfbg = 0.01*abon*10**h3 + 10**h4
        
      case(4)    !  oak
        h1 = o(1) - o(2)*alt10 + o(3)*alt10*alt10 - o(4)*alt10*alt10*alt10
        h2 =- o(5) - o(6)* alt10 + o(7)*alt10*alt10 - o(8)* alt10*alt10*alt10
        rbon = h1 + h2*hg
        abon = 31.3 - 3.9*rbon
        h3 = o(9) + o(10)* alt100 -o(11)*alt100*alt100 + o(12)*alt100*alt100*alt100
        h4 = o(13) + o(14)*alt100 - o(15)*alt10*alt100 + o(16)*alt100*alt100*alt100
        gfbg = h3 + h4*abon
        
      case(5)    ! birch
        rbon = 9. - 0.25*(hdom/100.)*exp(-bi(1)*(exp(-bi(2))-exp(-bi(3)*stage)))
        abon = 36. - 4.*rbon
        nvb = -bi(4) - bi(5)*(1./(hdom/100.)) +bi(6)*(1./(hdom/100.))*(1./(hdom/100.))
        dgvb = bi(7)*(1. + bi(8)*nvb)**(-bi(9))
        gfbg = pi*dgvb*dgvb*nvb/(4*10000)
        
      case(8)   ! aspen
        h1= pa(1) - pa(2)*alt10+pa(3)*alt10*alt10-pa(4)*alt10*alt10*alt10
		h2 = -pa(5)+pa(6)*alt10-pa(7)*alt10*alt10+pa(8)*alt10*alt10*alt10
		rbon=h1+h2*hdom
		abon=36.-4*rbon
		h3 = -pa(9)+pa(10)*alt10-pa(11)*alt10*alt10+pa(12)*alt10*alt10*alt10
		h4 = pa(13)-pa(14)*alt10 + pa(15)*alt10*alt10 -pa(16)*alt10*alt10*alt10
		gfbg = h3 + h4*abon
 end select

END SUBROUTINE calc_gfbg
    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 SUBROUTINE stump(x1, x2, xdcrb, xhbo, xh, i, stump_v, stump_dw)

 use data_tsort
 use data_par
 use data_species

 implicit none

 real    :: x1, x2, xdcrb, xhbo, xh, diam_base, dbsto, v1, stump_v, stump_dw
 integer :: i

 diam_base= sqrt((x1+x2)*4/pi)

  if(xhbo.ne.0) then
             dbsto = xdcrb + (xhbo-stoh(i))*(diam_base-xdcrb)/xhbo

  else if (xhbo.eq.0)then

              dbsto = diam_base*(xh+stoh(i))/xh
  end if

! volume of stump
         v1 = pi* stoh(i)*(diam_base*diam_base + diam_base*dbsto + dbsto*dbsto)/3.  ! frustum
         stump_v = v1/1000000.               !  m�
         stump_dw = v1*spar(i)%prhos   !  kg DW

 END SUBROUTINE stump
