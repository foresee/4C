!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                                                               *!
!*                    Subroutines for:                           *!
!*    - output routines - initialization and writing in files    *!
!*                                                               *!
!*   contains                                                    *!
!*   PREP_OUT       initialization of output files               *!
!*   PREP_OUTYEAR   prepare yearly output files                  *!
!*   PREP_COH       prepare output of cohorts                    *!
!*   PREP_OUT_COMP  prepare compressed output                    *!
!*   OUTYEAR        yearly output in files                       *!
!*   OUTDAY         daily output in files                        *!
!*   COH_OUT_D      daily cohort output                          *!
!*   COH_OUT_Y      yearly cohort output                         *!
!*   OUT_COMP       compressed output (multi run)                *!
!*   OUT_WPM	    ouput for WPM after the simulation is ended  *!
!*   OUT_SCEN       climate scenario control file (multi run)    *!
!*   ERROR_MESS     print error message in error file "error.log"*!
!*   STOP_MESS      print message on program abortion            *!
!*   OPEN_FILE      open special output file                     *!
!*   WR_HEADER_FILE write header of special output file          *!
!*   OUTVEG         output of species values (files veg_species) *!
!*   OUTSTORE       store of output variables (multi run 4)      *!
!*   OUT_VAR_FILE   store of output variables (multi run 4)      *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

SUBROUTINE prep_out

! Open output files
USE data_simul
USE data_species
USE data_stand
USE data_out

IMPLICIT NONE

CHARACTER(50) ::filename
INTEGER i,help_ip
INTEGER unit_n       ! output unit

IF(site_nr==1) THEN
    help_ip=site_nr
ELSE
    help_ip=ip
END IF

! 1. yearly output
! open all selected files
if (time_out .gt. 0) then
   call prep_outyear (help_ip)
endif

call old_out !behelfs, privatoutput

! 2. daily output
! open all selected files
if (flag_dayout .ge. 1) then
   do i = 1,outd_n
     if (outd(i)%out_flag .ne. 0) then
       select CASE (outd(i)%kind_name)
       CASE ('Cbcd')
        if (flag_bc .gt. 0) then
             call open_file (outd(i), help_ip)
             call wr_header_file (outd(i))
        endif

       CASE default
         call open_file (outd(i), help_ip)
         call wr_header_file (outd(i))

       end select
     endif
   END DO   !i
END IF

! 3.Cohort output
if(flag_cohout==1.or.flag_cohout==2) call prep_coh

! 4. end output
! open all selected files
if (flag_wpm .gt. 0) then
   do i = 1,oute_n
     if (oute(i)%out_flag .ne. 0) then
       select CASE (oute(i)%kind_name)

       CASE default
         call open_file (oute(i), help_ip)
         call wr_header_file (oute(i))

       end select
     endif
   END DO   !i
END IF

! 5.summation output
if(flag_sum>0)then
  unit_sum=getunit()
  filename = trim(site_name(help_ip))//'_sum.out'//trim(anh)
  open(unit_sum,file=trim(dirout)//filename,status='replace')
  WRITE(unit_sum,'(A)') '# Photsum   = Sum of gross photosynthesis gC/m2'
  WRITE(unit_sum,'(A)') '# NPPpotsum = Sum of potential NPP gC/m2'
  WRITE(unit_sum,'(A)') '# NPPsum    = Sum of NPP gC/m2'
  WRITE(unit_sum,'(A)') '# respsoil  = Sum of soil respiration gC/m2'
  WRITE(unit_sum,'(A)') '# lightsum  = Sum of global radiation MJ/m2'
  WRITE(unit_sum,'(A)') '# NEE       = Sum of respsoil - daily NPP gC/m2'
  WRITE(unit_sum,'(A)') '# ALS       = Sum of absorbed global radiation MJ/m2'
  WRITE(unit_sum,'(A)') '# Psum      = Sum of precipitation (mm)'
  WRITE(unit_sum,'(A)') '# Tmean     = mean temperature (�C)'
  WRITE(unit_sum,'(A)') '# GPP       = GPP  gC/m2'
  WRITE(unit_sum,'(A)') '# TER       = Total ecosystem respiration  gC/m2'
  WRITE(unit_sum,'(A)') '# respaut   = Autotrophe respiration gC/m2'

  select CASE(flag_sum)
    CASE(1)
      WRITE(unit_sum,'(A11)') '# Daily sum'
      WRITE(unit_sum,'(2A5,13A10)') '# Day','Year','Photsum','NPPpotsum','NPPsum', &
                                            'respsoil','lightsum','NEE', 'ALS', 'Psum',&
                                            'Tmean','cor_res', 'GPP','TER','respaut'
    CASE(2)
      WRITE(unit_sum,'(A50)') '# AET       = Sum of actual evapotranspiration (mm)'
      WRITE(unit_sum,'(A50)') '# PET       = Sum of potential evapotranspiration (mm)'
      WRITE(unit_sum,'(A50)') '# Percol.   = Sum  of percolation water from last layer (mm)'
      WRITE(unit_sum,'(A12)') '# Weekly sum'
      WRITE(unit_sum,'(2A6,17A10)') '# Week','Year','timedec','Photsum','NPPpotsum','NPPsum', &
                                             'respsoil','lightsum','NEE','ALS', 'Psum','Tmean', &
                                             'cor_res', 'AET', 'PET', 'Percol.', 'GPP','TER','respaut'
    CASE(3)
      WRITE(unit_sum,'(A50)') '# AET       = Sum of actual evapotranspiration (mm)'
      WRITE(unit_sum,'(A50)') '# PET       = Sum of potential evapotranspiration (mm)'
      WRITE(unit_sum,'(A50)') '# Ind_cout  = monthly climate index according Coutange'
      WRITE(unit_sum,'(A50)') '# Ind_wiss  = monthly climate index according v. Wissmann'
      WRITE(unit_sum,'(A50)') '# Ind_arid  = monthly aridity index according UNEP'
	  WRITE(unit_sum,'(A50)') '# CWB       = monthly climate water balance (P-PET)'
      WRITE(unit_sum,'(A50)') '# Percol.   = Sum  of percolation water from last layer (mm)'
      WRITE(unit_sum,'(A13)') '# Monthly sum'
      WRITE(unit_sum,'(A7,A5,20A10)') '# Month','Year','timedec','Photsum','NPPpotsum','NPPsum', &
                                             'respsoil','lightsum','NEE','ALS', 'Psum', 'Tmean', 'AET', 'PET', 'Ind_cout', &
											 'Ind_wiss', 'Ind_arid', 'CWB', 'Percol.', 'GPP','TER','respaut'
    CASE(4)
      WRITE(unit_sum,'(12A)') '# Yearly sum'
      WRITE(unit_sum,'(A6,A10,11A11)') '# Year','Photsum','NPPpotsum','NPPsum', &
                                            'respsoil','lightsum','NEE','ALS', 'Psum', 'Tmean', 'GPP','TER','respaut'
  end select
END IF

END subroutine prep_out

!**************************************************************

SUBROUTINE prep_outyear (help_ip)

! Open yearly output files
USE data_simul
USE data_stand
USE data_out
USE data_species

IMPLICIT NONE

CHARACTER(10)  :: helpunit
CHARACTER(2) :: helpvar
INTEGER i,j,help_ip,k
INTEGER unit_n       ! output unit

do i = 1,outy_n
   if (outy(i)%out_flag .ge. 1) then
      select CASE (outy(i)%kind_name)

      CASE ('AET_mon')
         if (ip .eq. 1) then
            nvar = nvar + 1
            outvar(nvar) = "AET_mon" 
         endif  
         call open_file (outy(i), help_ip)
         call wr_header_file (outy(i))

      CASE ('Cbc', 'Nbc')
        if (flag_bc .gt. 0) then
             call open_file (outy(i), help_ip)
             call wr_header_file (outy(i))
        endif
      
      CASE ('classd', 'classt')        !open classification file
         call open_file (outy(i), help_ip)
         unit_n = outy(i)%unit_nr
         WRITE(unit_n ,'(A)') trim(outy(i)%s_line)
         WRITE(unit_n ,'(A)',advance='no') trim(outy(i)%header)
         do k=1,nspecies
          do j=1,num_class
            WRITE(unit_n,'(A8,I2)',advance='no')'Class',j
          END DO  !j
         end do !k
         WRITE(unit_n,*) ' '

      CASE ('classage')        !open classification file
         call open_file (outy(i), help_ip)
         unit_n = outy(i)%unit_nr
         WRITE(unit_n ,'(A)') trim(outy(i)%s_line)
         WRITE(unit_n ,'(A)',advance='no') trim(outy(i)%header)
         do k=1,nspecies
          do j=1,num_class
            WRITE(unit_n,'(A8,I2)',advance='no')'Class',j
          END DO  !j
         end do !k
         WRITE(unit_n,*) ' '

      CASE ('classmvol')        !open classification file
         call open_file (outy(i), help_ip)
         unit_n = outy(i)%unit_nr
         WRITE(unit_n ,'(A)') trim(outy(i)%s_line)
         WRITE(unit_n ,'(A)',advance='no') trim(outy(i)%header)
         do k=1,nspecies
          do j=1,num_class
            WRITE(unit_n,'(A8,I2)',advance='no')'Class',j
          END DO  !j
         end do !k
         WRITE(unit_n,*) ' '

      CASE ('classd_h')        !open classification file
         call open_file (outy(i), help_ip)
         unit_n = outy(i)%unit_nr
         WRITE(unit_n ,'(A)') trim(outy(i)%s_line)
         WRITE(unit_n ,'(A)',advance='no') trim(outy(i)%header)
         do k=1,nspecies
          do j=1,num_class
            WRITE(unit_n,'(A8,I2)',advance='no')'Class',j
          END DO  !j
         end do
         WRITE(unit_n,*) ' '

      CASE ('classdm')        !open classification file
         call open_file (outy(i), help_ip)
         unit_n = outy(i)%unit_nr
         WRITE(unit_n ,'(A)') trim(outy(i)%s_line)
         WRITE(unit_n ,'(A)',advance='no') trim(outy(i)%header)
         do k=1,nspecies
          do j=1,num_class
            WRITE(unit_n,'(A8,I2)',advance='no')'Class',j
          END DO  !j
         end do
         WRITE(unit_n,*) ' '

     CASE ('classdm_h')        ! open classification file
         call open_file (outy(i), help_ip)
         unit_n = outy(i)%unit_nr
         WRITE(unit_n ,'(A)') trim(outy(i)%s_line)
         WRITE(unit_n ,'(A)',advance='no') trim(outy(i)%header)
         do k=1,nspecies
          do j=1,num_class
            WRITE(unit_n,'(A8,I2)',advance='no')'Class',j
          END DO  !j
         end do
         WRITE(unit_n,*) ' '

      CASE ('classh')       !open classification file
         call open_file (outy(i), help_ip)
         unit_n = outy(i)%unit_nr
         WRITE(unit_n ,'(A)') trim(outy(i)%s_line)
         WRITE(unit_n ,'(A)',advance='no')  trim(outy(i)%header)
         do j=1,num_class
            WRITE(unit_n,'(A8,I2)',advance='no')'Class',j
         END DO   !j
         WRITE(unit_n,*) ' '

      CASE ('GPP_mon')
         if (ip .eq. 1) then
            nvar = nvar + 1
            outvar(nvar) = "GPP_mon" 
         endif  
         call open_file (outy(i), help_ip)
         call wr_header_file (outy(i))

      CASE ('NEE_mon')
         if (ip .eq. 1) then
            nvar = nvar + 1
            outvar(nvar) = "NEE_mon" 
         endif  
         call open_file (outy(i), help_ip)
         call wr_header_file (outy(i))

      CASE ('NPP_mon')
         if (ip .eq. 1) then
            nvar = nvar + 1
            outvar(nvar) = "NPP_mon" 
         endif  
         call open_file (outy(i), help_ip)
         call wr_header_file (outy(i))
               
      CASE ('spec')       !open species file
         call open_file (outy(i), help_ip)
         unit_n = outy(i)%unit_nr

    !    header
         WRITE(unit_n ,'(A)',advance='no')  trim(outy(i)%header)
         do j=1,nspecies
            zeig=>pt%first
            do while (associated(zeig))
               if(zeig%coh%species.eq.j)then
                  WRITE(helpunit,'(I2)') zeig%coh%species
                  read(helpunit,*) helpvar
                  WRITE(unit_n,'(A10)',advance='no') 'Diam_S'//helpvar
                  WRITE(unit_n,'(A10)',advance='no') 'Heig_S'//helpvar
                  WRITE(unit_n,'(2A10)',advance='no') 'Tree_S'//helpvar,'Biom_S'//helpvar
                  exit
               END IF
               zeig=>zeig%next
            END DO
         END DO
         WRITE(unit_n,*) ' '

      CASE ('TER_mon')
         if (ip .eq. 1) then
            nvar = nvar + 1
            outvar(nvar) = "TER_mon" 
         endif  
         call open_file (outy(i), help_ip)
         call wr_header_file (outy(i))

      CASE default
         call open_file (outy(i), help_ip)
         call wr_header_file (outy(i))

      end select
   END IF
END DO   !i

if (nvar .gt. 0) then
    if (.not. allocated(output_unit_mon)) then
        allocate(output_unit_mon(nvar))
        if (.not. allocated(output_var)) allocate(output_var(nvar,1,0:0))
        if (.not. allocated(output_varm)) allocate(output_varm(nvar,site_nr,year,12))
        do i=1,nvar
             output_var(i,1,0) = i
        enddo     
        nvar = nvar + 1
    endif
endif

END subroutine prep_outyear

!**************************************************************

SUBROUTINE prep_coh

!prepare cohort output
USE data_simul
USE data_stand
USE data_out

IMPLICIT NONE

INTEGER help_ip
INTEGER i
INTEGER unit_n       ! output unit

IF(site_nr==1) THEN
    help_ip=site_nr
ELSE
    help_ip=ip
END IF

  ! output of all selected daily cohort files
   do i = 1,outcd_n
      if (outcd(i)%out_flag .ne. 0) then
         unit_n = outcd(i)%unit_nr

         select CASE (outcd(i)%kind_name)

         CASE default
           call open_file (outcd(i), help_ip)
           call wr_header_file (outcd(i))

         end select

      END IF
   END DO   !i

!prepare yearly cohort output
! output of all selected yearly files
do i = 1,outcy_n
  if (outcy(i)%out_flag .ne. 0) then
   unit_n = outcy(i)%unit_nr

      select CASE (outcy(i)%kind_name)

      CASE default
         call open_file (outcy(i), help_ip)
         call wr_header_file (outcy(i))

      end select
  END IF
END DO   !i
END subroutine prep_coh

!**************************************************************

SUBROUTINE prep_out_comp

! preparation: compressed output of final results for each run
USE data_simul
USE data_soil
USE data_stand
USE data_out

IMPLICIT NONE

character(70) filename

      filename   = trim(site_name(1))//'_B'//'.cmp'
      unit_comp1 = getunit()
      open(unit_comp1, file=trim(dirout)//filename, status='replace')
      write (unit_comp1, '(A)') '#   Compressed output of start values for each run'
      write (unit_comp1, 1000)
      write (unit_comp1, 2000)

      filename   = trim(site_name(1))//'_E'//'.cmp'
      unit_comp2 = getunit()
      open(unit_comp2, file=trim(dirout)//filename, status='replace')
      write (unit_comp2, '(A)') '#   Compressed output of final results for each run'
      write (unit_comp2, '(A, I5)') '#   Simulation time (years)', year
      write (unit_comp2, 500)
      write (unit_comp2, 1000)
      write (unit_comp2, 2000)

500  FORMAT ('#                          ||--------------------------------------------    final state    -------------------------------------------||---   mean annual values   ---||---  cumulative quantities  ---||-------------------  final state ',&
             '-------------------||-----------------------------------------------------------------------------     mean annual values      ---------------------------------------------------------------------------------------------------------------', &
             '-------------------------------------------------------------------------------------------------------------------------------|')
1000 FORMAT ('#                            m2_m2    /ha      t DW/ha  t DW/ha       cm       cm  t DW/ha  t DW/ha  t DW/ha  t DW/ha  t DW/ha  t DW/ha     t C/ha    kg C/ha    kg C/ha   kg DW/ha   kg DW/ha    kg DW/ha  t C/ha  t C/ha  t C/ha    t C/ha',&
             '    t C/ha    t C/ha   kg C/ha   kg C/ha   kg N/ha  kg N/ha  kg N/ha   kg C/ha   kg C/ha     mm     mm     mm     mm     mm     �C     mm   kg N/ha', 189X,'   J_cm2            mm  kg N/ha')
2000 FORMAT ('# ipnr site_id                LAI  nTree typ  Biomass  Biom._sv  Meddiam   Domhei   totfol    tottb   totsap   tothrt   totfrt   totcrt   mean_NPP   mean_NEP   mean_GPP c_Stem_inc   cumVs_ab cumVs_dead   C_sum C_d_stm   C_tot C_hum_tot',&
             '  C_tot_40  C_hum_40    C_accu  C_litter  N_litter    N_min   Nleach  Soil_Res  Tot_Resp    PET    AET percol interc transp   temp   prec   N_depo  drIndAl      GDD    cwb_an fire_inde fire_indb     I_arid   I_lang    I_cout    ', &
			 'I_wiss    I_mart    I_weck   I_reich     I_emb    CI_gor    CI_cur    CI_con   NTindex I_Nesterov I_Budyko     Rad    RedN dew/rime   Nupt  I_frost I_frost_sp  Ind_SHC' )

END subroutine prep_out_comp

!**************************************************************

SUBROUTINE outyear (flagout)

!yearly output
  USE data_biodiv
  USE data_climate
  USE data_depo
  USE data_evapo
  USE data_inter
  USE data_out
  USE data_par
  USE data_simul
  USE data_soil
  USE data_soil_cn
  USE data_species
  USE data_stand
  USE data_manag
  USE data_tsort
  USE data_site
  USE data_frost

  IMPLICIT NONE

  integer flagout    ! control of output
                     ! 1 - output with outyear,
                     ! 2 - output after management and mortality
  integer i,j,k,ihelp
  integer unit_n     ! output unit
  real hconv         ! conversion factor from patchsize into ha
 ! output variables of yearly C-balance in kg C/ha
  real y_GPP,   &    ! yearly gross productioin
       y_NPP,   &    ! yearly net primary productioin
       y_NEP,   &    ! yearly net ecosystem productioin
       y_autresp, &  ! yearly total resp of all cohorts and species
       y_sumbio,  &  ! total biomass of all cohorts and all species
       y_C_d_st,  &  ! C in stems of dead trees
       y_sumvsab, &  ! C in total sum of volume of removed stems by management
       y_C_tot, &    ! total soil C stock (OPM, humus and litter; whithout stems)
       y_C_tot_es, & ! total C of ecosystem (soil, dead stems and biomass)
       y_resps, &    ! yearly soil respiration
       y_resptot     ! yearly total respiration
 ! output variables of yearly C-balance in mol C/m2
  real ym_GPP,   &   ! yearly gross productioin
       ym_NPP,   &   ! yearly net primary productioin
       ym_NEP,   &   ! yearly net ecosystem productioin
       ym_autresp, & ! yearly total resp of all cohorts and species
       ym_sumbio,  & ! total biomass of all cohorts and all species
       ym_C_d_st,  & ! C in stems of dead trees
       ym_sumvsab, & ! C in total sum of volume of removed stems by management
       ym_C_tot, &   ! total soil C stock (OPM, humus and litter; whithout stems)
       ym_C_tot_es,& ! total C of ecosystem (soil, dead stems and biomass)
       ym_resps, &   ! yearly soil respiration
       ym_resptot, & ! yearly total respiration
       y_lai         ! LAI of stand without soil vegetation
 ! output variables of litter file: share in total biomasses
  real y_fol, y_tb, y_crt, y_frt, y_stem, y_totlit, y_C_lit, y_N_lit
 ! output variables harvested trees
  real      se_c_ha,  & ! sortiment element in C kg/ha
            se_m3_ha    ! volume of sortiment element in m�/ha
  real Cbc_ap  ! output variable of biochar application
  real help, h1, h2, h3, h4, q1, q2, q3, q4
  real hdnlf, hdnlf_sp, xhelp, xhelp1
  integer hdate_lf, hdate_lftot, hanzdlf
  real  hsumtlf

y_lai = 0.

if ((flagout .eq. 1) .and. (.not.allocated(sout))) allocate (sout(nspecies))
if (time.eq.0) then
   hdnlf = 0.
   hdnlf_sp = 0.
   hdate_lf = 0.
   hdate_lftot = 0.
   hanzdlf = 0.
   hsumtlf = 0.
else
   hdnlf = dnlf(time)
   hdnlf_sp = dnlf_sp(time)
   hdate_lf = date_lf(time)
   hdate_lftot = date_lftot(time)
   hanzdlf = anzdlf(time)
   hsumtlf = sumtlf(time)
end if

! output of all selected files
do i = 1,outy_n
   if (outy(i)%out_flag .eq. flagout) then
   unit_n = outy(i)%unit_nr

      select CASE (outy(i)%kind_name)

      CASE ('AET_mon','aet_mon')
         q1 = aet_mon(1) + aet_mon(2) + aet_mon(3)
         q2 = aet_mon(4) + aet_mon(5) + aet_mon(6)
         q3 = aet_mon(7) + aet_mon(8) + aet_mon(9)
         q4 = aet_mon(10) + aet_mon(11) + aet_mon(12)
         if (time .gt.1) then
            h1 = aet_dec + aet_mon(1) + aet_mon(2)
         else
            h1 = aet_mon(1) + aet_mon(2)
         endif
         h2 = aet_mon(3) + aet_mon(4) + aet_mon(5)
         h3 = aet_mon(6) + aet_mon(7) + aet_mon(8)
         h4 = aet_mon(9) + aet_mon(10) + aet_mon(11)
         WRITE(unit_n,'(I6)',advance='no') time_cur
         WRITE(unit_n,'(20F10.2)') aet_mon, q1, q2, q3, q4, h1, h2, h3, h4

      CASE ('c_bal')
         hconv     = 10000./kpatchsize
         y_NPP     = sumNPP * cpart                     ! kg DW --> kg C
         y_NPP     = y_NPP * hconv                      ! kg C/patch --> kg C/ha
         y_autresp = autresp * cpart * hconv            ! kg DW pro patch --> kg C/ha
         y_resps   = resps_c * gm2_in_kgha              ! g/m2 --> kg/ha
         y_resptot = y_resps + y_autresp
         y_GPP     = y_NPP + y_autresp
         y_NEP     = y_NPP - y_resps
         y_C_d_st  = C_opm_stem * gm2_in_kgha
         y_sumvsab = sumvsab * cpart                    ! kg DW /ha --> kg C
         y_sumbio  = (sumbio+sumbio_out) * cpart                     ! kg DW /ha --> kg C/ha
         y_C_tot   = C_tot * gm2_in_kgha * 0.001        ! g/m2 --> t/ha
         y_C_tot_es= y_C_tot + y_C_d_st + y_sumbio
         ym_NPP    = sumNPP * cpart                     ! kg DW --> kg C
         ym_NPP    = ym_NPP * 1./kpatchsize             ! kg C/patch --> kg C/m2
         ym_NPP    = ym_NPP * 1000. / Cmass             ! kg C --> mol C
         ym_autresp= autresp * cpart * kgha_in_gm2 * hconv / Cmass    ! kg DW pro patch --> mol/m2
         ym_resps  = resps_c /Cmass                     ! g/m2 --> mol/m2
         ym_resptot= ym_resps + ym_autresp
         ym_GPP    = ym_NPP + ym_autresp
         ym_NEP    = ym_NPP - ym_resps
         ym_C_d_st = C_opm_stem  /Cmass                       ! g/m2 --> mol/m2
         ym_sumvsab= sumvsab * cpart * kgha_in_gm2 / Cmass  ! kg DW /ha --> mol/m2
         ym_sumbio = sumbio * cpart * kgha_in_gm2 / Cmass   ! kg DW /ha --> mol/m2
         ym_C_tot  = C_tot /Cmass                           ! g/m2 --> mol/m2
         ym_C_tot_es= ym_C_tot + ym_C_d_st + ym_sumbio

        gppsum = gppsum * gm2_in_kgha

         WRITE(unit_n,'(I6)',advance='no') time_cur
         WRITE(unit_n,'(10F10.1,9F10.2,11F10.1,F10.1)') y_GPP, y_NPP, y_NEP, y_autresp, y_resps, y_resptot, &
               y_C_d_st, y_sumvsab, y_sumbio, y_C_tot_es, y_C_tot,  &
               C_tot_1, C_hum_1, C_tot_40, C_hum_40, C_tot_80, C_hum_80, C_tot_100, C_hum_100, &                    
               ym_GPP, ym_NPP, ym_NEP, ym_autresp, ym_resps, ym_resptot, &
               ym_C_d_st, ym_sumvsab, ym_sumbio, ym_C_tot_es, ym_C_tot, gppsum

      CASE ('Cbc')
         if (flag_bc .gt. 0) then
             WRITE(unit_n,'(I6)',advance='no') time_cur
             do j=1,nlay
               WRITE(unit_n,'(F10.3)',advance='no') C_bc(j)
             END DO   !j
             WRITE(unit_n,'(A)') ''
         endif

      CASE ('Chum')
         WRITE(unit_n,'(I6)',advance='no') time_cur
         do j=1,nlay
           WRITE(unit_n,'(F10.3)',advance='no') C_hum(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''

      CASE ('Copm')
         WRITE(unit_n,'(I6)',advance='no') time_cur
         do j=1,nlay
           WRITE(unit_n,'(F10.3)',advance='no') C_opm(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''

      CASE ('Copmfract')
         WRITE(unit_n,'(I6)',advance='no') time_cur
         do k=1,anrspec
           j = nrspec(k)
           xhelp = SUM(slit(j)%C_opm_frt)
           xhelp1 = SUM(slit(j)%C_opm_crt)
           WRITE(unit_n,'(I8,5F10.3)',advance='no') j, slit(j)%C_opm_fol, slit(j)%C_opm_tb, &
                          xhelp, xhelp1, slit(j)%C_opm_stem
         END DO   ! j
         WRITE(unit_n,'(A)') ''

      CASE ('classd')
         WRITE(unit_n,'(I6)',advance='no') time_cur
            do k=1,nspecies
             do j=1,num_class      
		       WRITE(unit_n,'(I10)',advance='no') diam_class(j,k)
              END DO
            end do
         WRITE(unit_n,'(A)') ''
      CASE ('classage')
         WRITE(unit_n,'(I6)',advance='no') time_cur
            do k=1,nspecies
             do j=1,num_class      
		       WRITE(unit_n,'(I10)',advance='no') diam_class_age(j,k)
              END DO
            end do
         WRITE(unit_n,'(A)') ''

      CASE ('classmvol')
         WRITE(unit_n,'(I6)',advance='no') time_cur
            do k=1,nspecies
             do j=1,num_class      
		       WRITE(unit_n,'(f10.3)',advance='no') diam_class_mvol(j,k)
              END DO
            end do
         WRITE(unit_n,'(A)') ''

      CASE ('classd_h')
         WRITE(unit_n,'(I6)',advance='no') time_cur
         do k=1,nspecies
          do j=1,num_class
           WRITE(unit_n,'(f10.3)',advance='no') diam_class_h(j,k)
          END DO
         end do
         WRITE(unit_n,'(A)') ''

      CASE ('classdm')
         WRITE(unit_n,'(I6)',advance='no') time_cur
         do k=1,nspecies
          do j=1,num_class
           WRITE(unit_n,'(I10)',advance='no') diam_classm(j,k)
          END DO
         end do
         WRITE(unit_n,'(A)') ''

      CASE ('classdm_h')
         WRITE(unit_n,'(I6)',advance='no') time_cur
         do k=1,nspecies
          do j=1,num_class
           WRITE(unit_n,'(f10.3)',advance='no') diam_classm_h(j,k)
          END DO
         end do
         WRITE(unit_n,'(A)') ''

      CASE ('classh')
         WRITE(unit_n,'(I6)',advance='no') time_cur
         do j=1,num_class
           WRITE(unit_n,'(I10)',advance='no') height_class(j)
         END DO
         WRITE(unit_n,'(A)') ''

      CASE ('classt')
         WRITE(unit_n,'(I6)',advance='no') time_cur
         do k=1,nspecies
          do j=1,num_class
 		   WRITE(unit_n,'(I10)',advance='no') diam_class_t(j,k)
          END DO
         end do
         WRITE(unit_n,'(A)') ''

      CASE ('clim')
         help = co2 * 1000000.

         WRITE(unit_n,'(2I5)',advance='no') time_cur
         WRITE(unit_n,'(6F10.2, 6I10, 7F10.2, E12.4, F8.2, 6F10.2, 2F8.2, 3I8, F10.2, I8, F10.2)') med_air,sum_prec,med_rad, med_wind, help, gdday, &
                                   days_summer, days_hot, days_ice, days_dry, days_hrain, days_snow, ind_arid_an, cwb_an, ind_lang_an, &
								   ind_cout_an, ind_wiss_an, ind_mart_an, ind_mart_vp, ind_emb, ind_weck, ind_reich,   &
								   con_gor, con_cur, con_con, ntindex, ind_bud, hdnlf, hdnlf_sp, hdate_lf, hdate_lftot, hanzdlf, hsumtlf, iday_vegper, ind_shc

      CASE ('clim_temp')
         q1 = (temp_mon(1) + temp_mon(2) + temp_mon(3)) / 3.
         q2 = (temp_mon(4) + temp_mon(5) + temp_mon(6)) / 3.
         q3 = (temp_mon(7) + temp_mon(8) + temp_mon(9)) / 3.
         q4 = (temp_mon(10) + temp_mon(11) + temp_mon(12)) / 3.
         if (time .gt.1) then
            h1 = (temp_dec + temp_mon(1) + temp_mon(2)) / 3.
         else
            h1 = (temp_mon(1) + temp_mon(2)) / 2.
         endif
         h2 = (temp_mon(3) + temp_mon(4) + temp_mon(5)) / 3.
         h3 = (temp_mon(6) + temp_mon(7) + temp_mon(8)) / 3.
         h4 = (temp_mon(9) + temp_mon(10) + temp_mon(11)) / 3.
         WRITE(unit_n,'(I6)',advance='no') time_cur
         WRITE(unit_n,'(20F10.2)') temp_mon, q1, q2, q3, q4, h1, h2, h3, h4

      CASE ('clim_prec')
         q1 = prec_mon(1) + prec_mon(2) + prec_mon(3)
         q2 = prec_mon(4) + prec_mon(5) + prec_mon(6)
         q3 = prec_mon(7) + prec_mon(8) + prec_mon(9)
         q4 = prec_mon(10) + prec_mon(11) + prec_mon(12)
         if (time .gt.1) then
            h1 = prec_dec + prec_mon(1) + prec_mon(2)
         else
            h1 = prec_mon(1) + prec_mon(2)
         endif
         h2 = prec_mon(3) + prec_mon(4) + prec_mon(5)
         h3 = prec_mon(6) + prec_mon(7) + prec_mon(8)
         h4 = prec_mon(9) + prec_mon(10) + prec_mon(11)
         WRITE(unit_n,'(I6)',advance='no') time_cur
         WRITE(unit_n,'(20F10.2)') prec_mon, q1, q2, q3, q4, h1, h2, h3, h4

      CASE ('clim_rad')
         q1 = (rad_mon(1) + rad_mon(2) + rad_mon(3)) / 3.
         q2 = (rad_mon(4) + rad_mon(5) + rad_mon(6)) / 3.
         q3 = (rad_mon(7) + rad_mon(8) + rad_mon(9)) / 3.
         q4 = (rad_mon(10) + rad_mon(11) + rad_mon(12)) / 3.
         if (time .gt.1) then
            h1 = (rad_dec + rad_mon(1) + rad_mon(2)) / 3.
         else
            h1 = (rad_mon(1) + rad_mon(2)) / 2.
         endif
         h2 = (rad_mon(3) + rad_mon(4) + rad_mon(5)) / 3.
         h3 = (rad_mon(6) + rad_mon(7) + rad_mon(8)) / 3.
         h4 = (rad_mon(9) + rad_mon(10) + rad_mon(11)) / 3.
         WRITE(unit_n,'(I6)',advance='no') time_cur
         WRITE(unit_n,'(20F10.2)') rad_mon, q1, q2, q3, q4, h1, h2, h3, h4

      CASE ('clim_hum')
         q1 = (hum_mon(1) + hum_mon(2) + hum_mon(3)) / 3.
         q2 = (hum_mon(4) + hum_mon(5) + hum_mon(6)) / 3.
         q3 = (hum_mon(7) + hum_mon(8) + hum_mon(9)) / 3.
         q4 = (hum_mon(10) + hum_mon(11) + hum_mon(12)) / 3.
         if (time .gt.1) then
            h1 = (hum_dec + hum_mon(1) + hum_mon(2)) / 3.
         else
            h1 = (hum_mon(1) + hum_mon(2)) / 2.
         endif
         h2 = (hum_mon(3) + hum_mon(4) + hum_mon(5)) / 3.
         h3 = (hum_mon(6) + hum_mon(7) + hum_mon(8)) / 3.
         h4 = (hum_mon(9) + hum_mon(10) + hum_mon(11)) / 3.
         WRITE(unit_n,'(I6)',advance='no') time_cur
         WRITE(unit_n,'(20F10.2)') hum_mon, q1, q2, q3, q4, h1, h2, h3, h4
         
      CASE ('indi')
         WRITE(unit_n,'(2I5)',advance='no') time_cur
         WRITE(unit_n,'(F10.2, 2(F8.2, 5I8), F10.1, I10, F8.2, 4I8 )') fire_indb, fire(1)%mean, fire(1)%frequ, &
                                    fire(2)%mean, fire(2)%frequ, fire_indi_max, fire_indi_day, fire(3)%mean, (fire(3)%frequ(j), j=1,4)

      CASE ('litter')
         if (totfol .gt. 1E-6) then
            y_fol = totfol_lit*100. / totfol
         else
            y_fol = -99.
         endif
         if (totfrt .gt. 1E-6) then
            y_frt = totfrt_lit*100. / totfrt
         else
            y_frt = -99.
         endif
         if (tottb .gt. 1E-6) then
            y_tb = tottb_lit*100. / tottb
         else
            y_tb = -99.
         endif
         if (totcrt .gt. 1E-6) then
            y_crt = totcrt_lit*100. / totcrt
         else
            y_crt = -99.
         endif
         hconv = totsap + tothrt
         if (hconv .gt. 1E-6) then
            y_stem= totstem_lit*100. / hconv
         else
            y_stem = -99.
         endif
         y_totlit = totfol_lit + totfrt_lit + totcrt_lit + tottb_lit + totstem_lit
         y_C_lit = (C_lit + C_lit_stem) * gm2_in_kgha
         y_N_lit = (N_lit + N_lit_stem) * gm2_in_kgha
         WRITE(unit_n,'(I6)',advance='no') time_cur
         WRITE(unit_n,'(8E12.4,2(6E12.4),5F12.2)') totfol_lit,totfol_lit_tree,totfrt_lit,totfrt_lit_tree,totcrt_lit,tottb_lit,totstem_lit, y_totlit, &
                                            C_lit_fol*gm2_in_kgha, C_lit_frt*gm2_in_kgha, C_lit_crt*gm2_in_kgha, &
                                            C_lit_tb*gm2_in_kgha, C_lit_stem*gm2_in_kgha, y_C_lit, & 
                                            N_lit_fol*gm2_in_kgha, N_lit_frt*gm2_in_kgha, N_lit_crt*gm2_in_kgha, &
                                            N_lit_tb*gm2_in_kgha, N_lit_stem*gm2_in_kgha, y_N_lit 

      CASE ('fcap_av')
         WRITE(unit_n,'(I6)',advance='no') time_cur
         do j=1,nlay
           WRITE(unit_n,'(F10.3)',advance='no') field_cap(j) - wilt_p(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''

      CASE ('fcapv_av')
         WRITE(unit_n,'(I6)',advance='no') time_cur
         do j=1,nlay
           WRITE(unit_n,'(F10.3)',advance='no') f_cap_v(j) - wilt_p_v(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''

      CASE ('GPP_mon')

         q1 = GPP_mon(1) + GPP_mon(2) + GPP_mon(3)
         q2 = GPP_mon(4) + GPP_mon(5) + GPP_mon(6)
         q3 = GPP_mon(7) + GPP_mon(8) + GPP_mon(9)
         q4 = GPP_mon(10) + GPP_mon(11) + GPP_mon(12)
         if (time .gt.1) then
            h1 = GPP_dec + GPP_mon(1) + GPP_mon(2)
         else
            h1 = GPP_mon(1) + GPP_mon(2)
         endif
         h2 = GPP_mon(3) + GPP_mon(4) + GPP_mon(5)
         h3 = GPP_mon(6) + GPP_mon(7) + GPP_mon(8)
         h4 = GPP_mon(9) + GPP_mon(10) + GPP_mon(11)
         WRITE(unit_n,'(I6)',advance='no') time_cur
         WRITE(unit_n,'(20F10.2)') GPP_mon, q1, q2, q3, q4, h1, h2, h3, h4

      CASE ('humusv')
         WRITE(unit_n,'(I6)',advance='no') time_cur
         do j=1,nlay
           WRITE(unit_n,'(F10.3)',advance='no') humusv(j)*100.
         END DO   !j
         WRITE(unit_n,'(A)') ''

      CASE ('NEE_mon')
         q1 = NEE_mon(1) + NEE_mon(2) + NEE_mon(3)
         q2 = NEE_mon(4) + NEE_mon(5) + NEE_mon(6)
         q3 = NEE_mon(7) + NEE_mon(8) + NEE_mon(9)
         q4 = NEE_mon(10) + NEE_mon(11) + NEE_mon(12)
         if (time .gt.1) then
            h1 = NEE_dec + NEE_mon(1) + NEE_mon(2)
         else
            h1 = NEE_mon(1) + NEE_mon(2)
         endif
         h2 = NEE_mon(3) + NEE_mon(4) + NEE_mon(5)
         h3 = NEE_mon(6) + NEE_mon(7) + NEE_mon(8)
         h4 = NEE_mon(9) + NEE_mon(10) + NEE_mon(11)
         WRITE(unit_n,'(I6)',advance='no') time_cur
         WRITE(unit_n,'(20F10.2)') NEE_mon, q1, q2, q3, q4, h1, h2, h3, h4

      CASE ('NPP_mon')
         q1 = NPP_mon(1) + NPP_mon(2) + NPP_mon(3)
         q2 = NPP_mon(4) + NPP_mon(5) + NPP_mon(6)
         q3 = NPP_mon(7) + NPP_mon(8) + NPP_mon(9)
         q4 = NPP_mon(10) + NPP_mon(11) + NPP_mon(12)
         if (time .gt.1) then
            h1 = NPP_dec + NPP_mon(1) + NPP_mon(2)
         else
            h1 = NPP_mon(1) + NPP_mon(2)
         endif
         h2 = NPP_mon(3) + NPP_mon(4) + NPP_mon(5)
         h3 = NPP_mon(6) + NPP_mon(7) + NPP_mon(8)
         h4 = NPP_mon(9) + NPP_mon(10) + NPP_mon(11)
         WRITE(unit_n,'(I6)',advance='no') time_cur
         WRITE(unit_n,'(20F10.2)') NPP_mon, q1, q2, q3, q4, h1, h2, h3, h4

      CASE ('Nbc')
         if (flag_bc .gt. 0) then
             WRITE(unit_n,'(I6)',advance='no') time_cur
             do j=1,nlay
               WRITE(unit_n,'(F10.3)',advance='no') N_bc(j)
             END DO   !j
             WRITE(unit_n,'(A)') ''
         endif

     CASE ('Nhum')
         WRITE(unit_n,'(I6)',advance='no') time_cur
         do j=1,nlay
           WRITE(unit_n,'(F10.3)',advance='no') N_hum(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''

      CASE ('Nopm')
         WRITE(unit_n,'(I6)',advance='no') time_cur
         do j=1,nlay
           WRITE(unit_n,'(F10.3)',advance='no') N_opm(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''

      CASE ('manrec')
         if (flag_manreal.eq.1) then
              WRITE(unit_n,'(I6)',advance='no') time_cur-1
              WRITE(unit_n,'(10x,A30,I6)')  maninf, meas
         end if

      CASE ('mansort')

         if ((flag_manreal.eq.1.or.flag_deadsort.eq.1).and.maninf.ne.'tending'.and.maninf.ne.'brushing') then
              ztim=>st%first
              do
                 IF (.not.ASSOCIATED(ztim)) exit
                 if(time.eq.ztim%tim%year.and. (ztim%tim%stype.eq.'ab'.or.ztim%tim%stype.eq.'tb')) then

                    se_m3_ha = (ztim%tim%vol/kpatchsize)*10000.   ! m�/patchsize ---> m3/ha
                    se_c_ha =  se_m3_ha*spar(ztim%tim%specnr)%prhos*1000000.*cpart   ! m�/patchsize ---> kg C/ha
                    write(unit_n,'(3I6,1x,A5,1x,F8.3,1x,f7.3,1x,f7.3,1x,f7.3,1x,f7.3,1x,f9.4,1x,f14.3,1x,i8,x,a4)') ztim%tim%year,&
                    ztim%tim%count,ztim%tim%specnr,ztim%tim%ttype,ztim%tim%length,ztim%tim%dia,ztim%tim%diaor, ztim%tim%zapfd,&
                    ztim%tim%zapfdor,se_m3_ha, se_c_ha,int(ztim%tim%tnum), ztim%tim%stype
                 end if
                 ztim=>ztim%next
              end do
              flag_manreal=0
              flag_deadsort=0
         else if (maninf.eq.'tending'.or.maninf.eq.'brushing') then
              flag_manreal=0
              maninf='       '
         end if

      CASE ('root')
         WRITE(unit_n,'(I6)',advance='no') time_cur
         do j=1,nlay
           WRITE(unit_n,'(F10.3)',advance='no') root_fr(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''

	  CASE ('fr_loss')
         WRITE(unit_n,'(I6)',advance='no') time_cur
         do j=1,nlay
           WRITE(unit_n,'(F10.3)',advance='no') fr_loss(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''

	  CASE ('redis')
         WRITE(unit_n,'(I6)',advance='no') time_cur
         do j=1,nlay
           WRITE(unit_n,'(F10.3)',advance='no') redis(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''
	  
	  CASE ('sdrought')
         WRITE(unit_n,'(I6)',advance='no') time_cur
         WRITE(unit_n,'(20I8)') s_drought

      CASE ('soil')
         help = -99.0
         Cbc_ap = 0.
         if (time .gt. 0) help = rnet_cum / recs(time)
         if (flag_bc .gt. 0) then    
            ihelp = y_bc_n - 1
            if (y_bc_n .eq. 1) ihelp = y_bc_n
            if (y_bc(ihelp) .eq. time) then
                Cbc_ap = Cbc_ap + C_bc_appl(ihelp)
            endif
         endif
         WRITE(unit_n,'(I6)',advance='no') time_cur
         WRITE(unit_n,'(13F10.3,5F10.2,17F10.3,4F10.2)') med_air, sum_prec, int_cum_can, &
               perc_cum, wupt_cum, wupt_r_c, tra_tr_cum, tra_sv_cum, wupt_e_c, aet_cum, wat_tot, gp_can_mean, &
               N_min, N_tot, C_tot, N_an_tot, N_hum_tot, C_hum_tot, N_hum(1), C_hum(1), &
               N_lit, C_lit, C_opm_fol, C_opm_frt, C_opm_crt, C_opm_tb, C_opm_stem, Nupt_c, &
               Nleach_c, Ndep_cum, resps_c, pet_cum, int_cum_sveg, thick(1), dew_cum, help, N_bc_tot, C_bc_tot, Cbc_ap

      CASE ('spec')
         WRITE(unit_n,'(I6)',advance='no') time_cur
         do j=1,nspecies
            zeig=>pt%first
            do while (associated(zeig))
               if(zeig%coh%species.eq.j)then
                  WRITE(unit_n,'(2F10.2,I10,F10.2)',advance='no') svar(j)%med_diam, &
                    svar(j)%dom_height, svar(j)%sum_ntreea, svar(j)%sum_bio
                  exit
               END IF
               zeig=>zeig%next
            END DO
         END DO
         WRITE(unit_n,*) ' '

      CASE('standsort')
          if (outy(i)%out_flag .eq. 1)  then
             outy(i)%out_flag = 2
          else if (outy(i)%out_flag .eq. 2) then
            ztim=>st%first
            do
              IF (.not.ASSOCIATED(ztim)) exit
               if(ztim%tim%year.eq.time.and. ztim%tim%stype.eq.'vb') then
                    se_m3_ha = (ztim%tim%vol/kpatchsize)*10000.   ! m�/patchsize ---> m3/ha
                    se_c_ha =  se_m3_ha*spar(ztim%tim%specnr)%prhos*1000000.*cpart   ! m�/patchsize ---> kg C/ha
                    write(unit_n,'(3I6,1x,A5,1x,F8.3,1x,f7.3,1x,f7.3,1x,f7.3,1x,f7.3,1x,f9.4,1x,f14.3,1x,i8)') ztim%tim%year,&
                    ztim%tim%count,ztim%tim%specnr,ztim%tim%ttype,ztim%tim%length,ztim%tim%dia,ztim%tim%diaor, ztim%tim%zapfd,&
                    ztim%tim%zapfdor,se_m3_ha, se_c_ha,int(ztim%tim%tnum)
               end if
               ztim=>ztim%next
            end do
         end if

      CASE ('TER_mon')
         q1 = TER_mon(1) + TER_mon(2) + TER_mon(3)
         q2 = TER_mon(4) + TER_mon(5) + TER_mon(6)
         q3 = TER_mon(7) + TER_mon(8) + TER_mon(9)
         q4 = TER_mon(10) + TER_mon(11) + TER_mon(12)
         if (time .gt.1) then
            h1 = TER_dec + TER_mon(1) + TER_mon(2)
         else
            h1 = TER_mon(1) + TER_mon(2)
         endif
         h2 = TER_mon(3) + TER_mon(4) + TER_mon(5)
         h3 = TER_mon(6) + TER_mon(7) + TER_mon(8)
         h4 = TER_mon(9) + TER_mon(10) + TER_mon(11)
         WRITE(unit_n,'(I6)',advance='no') time_cur
         WRITE(unit_n,'(20F10.2)') TER_mon, q1, q2, q3, q4, h1, h2, h3, h4

      CASE ('veg')
         if (outy(i)%out_flag .eq. 1) then

            vout%help_veg1(1) = anz_spec
            vout%help_veg1(2) = anz_coh_act
            vout%help_veg1(3) = anz_tree_ha

            do k = 1, nspec_tree
			  y_lai = y_lai + svar(k)%sum_lai
			end do
            vout%help_veg2(1) = y_lai
            vout%help_veg2(2) = sumbio
            vout%help_veg2(3) = sumnpp
            vout%help_veg2(4) = med_diam
            vout%help_veg2(5) = hdom
            vout%help_veg2(6) = totfol
            vout%help_veg2(7) = totsap
            vout%help_veg2(8) = totfrt
            vout%help_veg2(9) = tothrt
            vout%help_veg2(10) = totsteminc
            vout%help_veg2(11) = totstem_m3
            vout%help_veg3    = crown_area/kpatchsize
            outy(i)%out_flag = 2
         else if (outy(i)%out_flag .eq. 2) then
            WRITE(unit_n,'(I6)',advance='no') time_cur
            WRITE(unit_n,'(3I10)',advance='no') vout%help_veg1
            WRITE(unit_n,'(F10.3,2E12.3,2F12.3,14E12.3, 5F12.3)') vout%help_veg2, sumvsab, sumvsdead, &
                                        vout%help_veg3, drIndAl, Ndem, gp_can_mean, gp_can_min, gp_can_max, mean_diam, mean_height, basal_area, sumvsdead_m3, totsteminc_m3
            outy(i)%out_flag = 1
         endif

      CASE ('veg_in')
         WRITE(unit_n,'(2I5)',advance='no') time_cur
         WRITE(unit_n,'(3I10)',advance='no') anz_spec_in, anz_coh_in, anz_tree_in
         WRITE(unit_n,'(F10.3,E12.3,2F12.3,E12.3)') LAI_in, sumbio_in, med_diam_in, hmean_in, totfol_in

      CASE ('veg_out')
         WRITE(unit_n,'(2I5)',advance='no') time_cur
         WRITE(unit_n,'(3I10)',advance='no') anz_spec_out, anz_coh_out, anz_tree_out
         WRITE(unit_n,'(F10.3,E12.3,2F12.3,E12.3)') LAI_out, sumbio_out, med_diam_out, hmean_out, totfol_out

      CASE ('veg_be')
          ! beech - veg file
            call outveg (1, outy(i)%out_flag, unit_n)

      CASE ('veg_bi')
          ! birch - veg file
            call outveg (5, outy(i)%out_flag, unit_n)

      CASE ('veg_pi')
          ! pine - veg file
            call outveg (3, outy(i)%out_flag, unit_n)

      CASE ('veg_pc')
          ! pinus contorta - veg file
            if (nspec_tree .gt. 5) call outveg (6, outy(i)%out_flag, unit_n)

      CASE ('veg_pp')
          ! pinus ponderosa - veg file
            if (nspec_tree .gt. 6) call outveg (7, outy(i)%out_flag, unit_n)

      CASE ('veg_pt')
          ! populus tremula - veg file
            if (nspec_tree .gt. 7) call outveg (8, outy(i)%out_flag, unit_n)

      CASE ('veg_oa')
          ! oak - veg file
            call outveg (4, outy(i)%out_flag, unit_n)

      CASE ('veg_sp')
          ! spruce - veg file
            call outveg (2, outy(i)%out_flag, unit_n)

      CASE ('veg_ph')
          ! aleppo pine - veg file
            if (nspec_tree .gt. 8) call outveg (9, outy(i)%out_flag, unit_n)

      CASE ('veg_dg')
          ! douglas fir - veg file
            if (nspec_tree .gt. 9) call outveg (10, outy(i)%out_flag, unit_n)

      CASE ('veg_rb')
          ! robinia - veg file
            if (nspec_tree .gt. 10) call outveg (11, outy(i)%out_flag, unit_n)

      CASE ('veg_egl')
          ! Eucalyptus globulus - veg file
            if (nspec_tree .gt. 11) call outveg (12, outy(i)%out_flag, unit_n)
      
      CASE ('veg_egr')
          ! Ecalyptus grandis - veg file
            if (nspec_tree .gt. 12) call outveg (13, outy(i)%out_flag, unit_n)

      CASE ('veg_sveg')
          ! ground vegetation - veg file
            if (flag_sveg .gt. 0) call outveg (14, outy(i)%out_flag, unit_n)

      CASE ('veg_mist')
          ! Mistletoe (Viscum a.) - veg file
            if (flag_dis .gt. 0) call outveg (15, outy(i)%out_flag, unit_n)

      END SELECT
   END IF
END DO   !i

if(flag_cohout==1 .or. flag_cohout==2) call coh_out_y (flagout)
if (flagout .eq. 2) deallocate (sout)

END subroutine outyear

!**************************************************************

SUBROUTINE outday (flagout)
!daily output

  USE data_biodiv
  USE data_climate
  USE data_depo
  USE data_inter
  USE data_evapo
  USE data_inter
  USE data_simul
  USE data_stand
  USE data_species
  USE data_soil
  USE data_soil_cn
  USE data_soil_t
  USE data_out

  IMPLICIT NONE

  integer flagout    ! control of output
                     ! 1 - output with
                     ! 2 - output
  INTEGER i,j,jj,k
  integer tt, month
  INTEGER unit_n       ! output unit
  REAL    xhelp, xhelp1

! output of all selected files
do i = 1,outd_n
   if (outd(i)%out_flag .eq. flagout) then
      unit_n = outd(i)%unit_nr

      select CASE (outd(i)%kind_name)

      CASE ('Cday')
        j=iday
                   WRITE(unit_n,'(2I6)',advance='no') j,time_cur
                   WRITE(unit_n,'(13E12.4, F7.1)') phot_C, dailygrass_C, dailynetass_C, dailypotNPP_C, dailyNPP_C, NPP_day, GPP_day, Cout%NEE(j), &
                                                   TER_day, dailyautresp_C, Cout%Resp_aut(j), respsoil, dailyrespfol_C, 100.*totFPARsum

      CASE ('Chumd')
         WRITE(unit_n,'(2I6)',advance='no') iday,time_cur
         do j=1,nlay
           WRITE(unit_n,'(F10.3)',advance='no') C_hum(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''

      CASE ('Copmd')
         WRITE(unit_n,'(2I6)',advance='no') iday,time_cur
         do j=1,nlay
           WRITE(unit_n,'(F10.3)',advance='no') C_opm(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''

      CASE ('Copmfractd')
         WRITE(unit_n,'(2I6)',advance='no') iday,time_cur
         do k=1,anrspec
           j = nrspec(k)
           xhelp = SUM(slit(j)%C_opm_frt)
           xhelp1 = SUM(slit(j)%C_opm_crt)
           WRITE(unit_n,'(I8,5F10.3)',advance='no') j, slit(j)%C_opm_fol, slit(j)%C_opm_tb, &
                          xhelp, xhelp1, slit(j)%C_opm_stem
         END DO   ! j
         WRITE(unit_n,'(A)') ''

      CASE ('Cbcd')
         if (flag_bc .gt. 0) then
             WRITE(unit_n,'(2I6)',advance='no') iday,time_cur
             do j=1,nlay
               WRITE(unit_n,'(F10.3)',advance='no') C_bc(j)
             END DO   !j
             WRITE(unit_n,'(A)') ''
         endif

      CASE ('day')
         WRITE(unit_n,'(2I6)',advance='no') iday,time_cur
         xhelp = (NO_dep + NH_dep)*1000.        ! g/m� ==> mg/m�
         if (N_min > 100) then
         continue
         endif
         WRITE(unit_n,'(21F10.3, F10.1, 3I7, I8, F8.3, 4F10.2, 4F10.3)',advance='no') airtemp,rad,prec,interc_can,snow,pet,aet, &
               trans_dem,trans_tree,trans_sveg,gp_can,respsoil,Nleach,Nupt_d,N_min,N_an_tot,  &
               xhelp,cover,LAI, Irelpool(0), totFPARcan, fire_indi, fire(2)%index, fire(1)%index, fire(3)%index, snow_day, &
               drIndd, bucks_root, bucks_100, prec-pet, dptemp, dew_rime, Rnet_tot, rad_max
         WRITE(unit_n,'(A)') ''

      CASE ('day_short')
         call tzinda(tt,month,time_cur,iday)
         WRITE(unit_n,'(2(I2,1X), I4, 2X)',advance='no') tt,month,time_cur
         WRITE(unit_n,'(I8, F10.2)',advance='no') fire(2)%index, prec-pet
         WRITE(unit_n,'(A)') ''

      CASE ('NH4')
         WRITE(unit_n,'(I6,I5,1X)',advance='no') iday,time_cur
         do j=1,nlay
           WRITE(unit_n,'(E10.3)',advance='no') NH4(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''

      CASE ('NH4c')
         WRITE(unit_n,'(2I6)',advance='no') iday,time_cur
         do j=1,nlay
          ! convert gN/m2 into mgN/l
		   xhelp = pNH4f * NH4(j) * 1000. / wats(j)
           WRITE(unit_n,'(F10.4)',advance='no') xhelp
         END DO   !j
         WRITE(unit_n,'(A)') ''

      CASE ('NO3')
         WRITE(unit_n,'(I6,I5,1X)',advance='no') iday,time_cur
         do j=1,nlay
           WRITE(unit_n,'(E10.3)',advance='no') NO3(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''

      CASE ('NO3c')
         WRITE(unit_n,'(2I6)',advance='no') iday,time_cur
         do j=1,nlay
          ! convert gN/m2 into mgN/l
		   xhelp = pNO3f * NO3(j) * 1000. / wats(j)
           WRITE(unit_n,'(F10.4)',advance='no') xhelp
         END DO   !j
         WRITE(unit_n,'(A)') ''

      CASE ('Nhumd')
         WRITE(unit_n,'(2I6)',advance='no') iday,time_cur
         do j=1,nlay
           WRITE(unit_n,'(F10.3)',advance='no') N_hum(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''

      CASE ('Nopmd')
         WRITE(unit_n,'(2I6)',advance='no') iday,time_cur
         do j=1,nlay
           WRITE(unit_n,'(F10.3)',advance='no') N_opm(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''

      CASE ('NOPMfract')
         WRITE(unit_n,'(2I6)',advance='no') iday,time_cur
         do k=1,anrspec
           j = nrspec(k)
           WRITE(unit_n,'(5F10.3)',advance='no') slit(j)%N_opm_fol, slit(j)%N_opm_tb, &
                          slit(j)%N_opm_frt(1),  slit(j)%N_opm_crt(1),  slit(j)%N_opm_stem
         END DO   ! j
         WRITE(unit_n,'(A)') ''

      CASE ('Nuptd')
         WRITE(unit_n,'(2I6)',advance='no') iday,time_cur
         do j=1,nlay
           WRITE(unit_n,'(E10.2)',advance='no') Nupt(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''

      CASE ('Nmind')
         WRITE(unit_n,'(2I6)',advance='no') iday,time_cur
         do j=1,nlay
           WRITE(unit_n,'(E10.2)',advance='no') Nmin(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''

      CASE ('perc')
         WRITE(unit_n,'(2I6)',advance='no') iday,time_cur
         do j=1,nlay
           WRITE(unit_n,'(F10.3)',advance='no') perc(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''

      CASE ('specd')
         WRITE(unit_n,'(2I6)',advance='no') iday,time_cur
         k = 0
  do jj=1,anrspec
       j = nrspec(jj)
               if (k .gt. 0) WRITE(unit_n,'(A12)',advance='no') ''
               WRITE(unit_n,'(A16,I8)',advance='no') spar(j)%species_short_name, j
               WRITE(unit_n,'(4E12.3, F10.3)',advance='no') svar(j)%Ndem, svar(j)%Nupt, svar(j)%Ndemp, svar(j)%Nuptp, svar(j)%RedN
               WRITE(unit_n,'(A)') ''
               k = k+1
         END DO   !j 

      CASE ('temp')
         WRITE(unit_n,'(2I6,F10.3)',advance='no') iday,time_cur, temps_surf
         do j=1,nlay
           WRITE(unit_n,'(F10.3)',advance='no') temps(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''
	  
	  CASE ('water')
         WRITE(unit_n,'(2I6)',advance='no') iday,time_cur
         do j=1,nlay
           WRITE(unit_n,'(F10.3)',advance='no') wats(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''

      CASE ('watvol')
         WRITE(unit_n,'(2I6)',advance='no') iday,time_cur
         do j=1,nlay
           WRITE(unit_n,'(F10.3)',advance='no') watvol(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''

      CASE ('wat_res')
             WRITE(unit_n,'(2I6)',advance='no') iday,time_cur
             do j=1,nlay
               WRITE(unit_n,'(F10.4)',advance='no') wat_res(j)
             END DO   !j
             WRITE(unit_n,'(A)') ''

      CASE ('wupt')
         WRITE(unit_n,'(2I6)',advance='no') iday,time_cur
         do j=1,nlay
           WRITE(unit_n,'(F10.3)',advance='no') wupt_r(j)
         END DO   !j
         WRITE(unit_n,'(A)') ''

      end select
  END IF
END DO   !i

if(flag_cohout .gt. 0) call coh_out_d (flagout)
END subroutine outday

!**************************************************************

SUBROUTINE coh_out_d (flagout)
! daily cohort output

USE data_simul
USE data_stand
USE data_out
USE data_par

IMPLICIT NONE

integer flagout    ! control of output
                   ! 1 - output with
                   ! 2 - output
INTEGER i,j
INTEGER unit_n       ! output unit
logical lflag
real help

  ! output of all selected files
  do i = 1,outcd_n
     if (outcd(i)%out_flag .eq. flagout) then
        unit_n = outcd(i)%unit_nr
        WRITE(unit_n ,'(2I5)',advance='no') iday,time_cur

        do j= 1,max_coh
           zeig => pt%first
           lflag = .FALSE.

           do while (associated(zeig))
              if (zeig%coh%ident .eq. j) then

                 select CASE (outcd(i)%kind_name)
                 CASE ('ass')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%assi

                 CASE ('aevi')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%aev_i

                  CASE ('ddi')
                    WRITE(unit_n,'(F12.3)',advance='no') zeig%coh%drindd

                 CASE ('dem')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%demand

                 CASE ('dips')
                    WRITE(unit_n,'(F12.3)',advance='no') zeig%coh%drindps

                 CASE ('gp')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%gp

                 CASE ('gsdps')
                    WRITE(unit_n,'(F12.0)',advance='no') zeig%coh%ndaysps

                 CASE ('intcap')
                    help = SUM(zeig%coh%intcap)
                    WRITE(unit_n,'(E12.3)',advance='no') help

                 CASE ('interc')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%interc_st

                 CASE ('Ndemc_d')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%Ndemc_d

                 CASE ('Nuptc_d')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%Nuptc_d

                 CASE ('N_fol')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%N_fol

                 CASE ('N_pool')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%N_pool

                 CASE ('RedNc')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%RedNc

                 CASE ('resp')
                    help = zeig%coh%resp * kg_in_g * cpart   ! kg DW per tree ==> g C per tree
                    WRITE(unit_n,'(E12.3)',advance='no') help

                 CASE ('respaut')
!                    help = zeig%coh%respaut * kg_in_g * cpart   ! kg DW per tree ==> g C per tree
                     help = zeig%coh%maintres * kg_in_g * cpart 
                    WRITE(unit_n,'(E12.3)',advance='no') help

                 CASE ('respbr')
                    help = zeig%coh%respbr * kg_in_g * cpart   ! kg DW per tree ==> g C per tree
                    WRITE(unit_n,'(E12.3)',advance='no') help

                 CASE ('respfol')
                    help = zeig%coh%respfol * kg_in_g * cpart   ! kg DW per tree ==> g C per tree
                    WRITE(unit_n,'(E12.3)',advance='no') help

                 CASE ('resphet')
                    help = zeig%coh%resphet * kg_in_g * cpart   ! kg DW per tree ==> g C per tree
                    WRITE(unit_n,'(E12.3)',advance='no') help

                 CASE ('respsap')
                    help = zeig%coh%respsap * kg_in_g * cpart   ! kg DW per tree ==> g C per tree
                    WRITE(unit_n,'(E12.3)',advance='no') help

                 CASE ('respfrt')
                    help = zeig%coh%respfrt * kg_in_g * cpart   ! kg DW per tree ==> g C per tree
                    WRITE(unit_n,'(E12.3)',advance='no') help

                 CASE ('sup')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%supply

                 CASE ('totfpar')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%totfpar

                 end select
                 lflag = .TRUE.
                 exit
              ELSE
                 zeig => zeig%next
              END IF

           END DO

           if (.not. lflag) WRITE(unit_n,'(F12.3)',advance='no') -99.9

        END DO  !j

        WRITE(unit_n,'(A)') ''

     END IF  ! out_flag
  END DO  !i
END subroutine coh_out_d

!**************************************************************

SUBROUTINE coh_out_y (flagout)

!yearly cohort output
use data_simul
use data_soil
use data_stand
use data_out
use data_par

implicit none

integer flagout    ! control of cohort output
                   ! 1 - output with outyear,
                   ! 2 - output after management and mortality
integer i,j,k
integer unit_n       ! output unit
logical lflag
real help

  ! output of all selected files
  do i = 1,outcy_n
     if (outcy(i)%out_flag .eq. flagout) then
        unit_n = outcy(i)%unit_nr
        WRITE(unit_n ,'(I5)',advance='no') time_cur

        do j= 1,max_coh
           zeig => pt%first
           lflag = .FALSE.

           do while (associated(zeig))
              if (zeig%coh%ident .eq. j) then

                 select CASE (outcy(i)%kind_name)
                 CASE ('age')
                    WRITE(unit_n,'(I12)',advance='no') zeig%coh%x_age

                 CASE ('ahb')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%x_ahb

                 CASE ('ahbasrel')
                    if (zeig%coh%Asapw .gt. zero) then
                        help = zeig%coh%x_ahb / zeig%coh%Asapw
                    else
                        help = 0.
                    endif
                    WRITE(unit_n,'(E12.3)',advance='no') help

                 CASE ('ahc')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%ahc

                 CASE ('ahcasrel')
                    if (zeig%coh%Asapw .gt. zero) then
                        help = zeig%coh%ahc / zeig%coh%Asapw
                    else
                        help = 0.
                    endif
                    WRITE(unit_n,'(E12.3)',advance='no') help

                 CASE ('asapw')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%Asapw

                 CASE ('atr')
                    WRITE(unit_n,'(I12)',advance='no') int(zeig%coh%ntreea)

                 CASE ('bioi')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%bio_inc

                 CASE ('botlayer')
                    WRITE(unit_n,'(I12)',advance='no') zeig%coh%botLayer

                 CASE ('cpa')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%crown_area*int(zeig%coh%ntreea)

                 CASE ('crt')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%x_crt

                 CASE ('daybb')
                    WRITE(unit_n,'(I12)',advance='no') int(zeig%coh%day_bb)

                 CASE ('dcrb')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%dcrb

                 CASE ('diac')
                    if( zeig%coh%ndaysgr.ne.0) then
                        WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%drindal/zeig%coh%ndaysgr
                    else
                        WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%drindal
                    end if

                 CASE ('diam')
                    WRITE(unit_n,'(f12.5)',advance='no') zeig%coh%diam
          

                 CASE ('dtr')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%ntreed

                  CASE ('dwd')
                       help =    zeig%coh%ntreed*(zeig%coh%x_sap + zeig%coh%x_hrt)
                    WRITE(unit_n,'(E12.3)',advance='no') help

                 CASE ('fol')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%x_fol

                 CASE ('foli')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%fol_inc

                 CASE ('frt')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%x_frt
				 
				 CASE ('frti')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%frt_inc

                 CASE ('frtrel')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%frtrel(1)

                 CASE ('geff')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%geff

                 CASE ('gfol')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%gfol

                 CASE ('gfrt')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%gfrt

                 CASE ('grossass')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%grossass

                 CASE ('gsap')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%gsap

                 CASE ('gsd')
                    WRITE(unit_n,'(I12)',advance='no') zeig%coh%ndaysgr

                 CASE ('hbo')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%x_hbole

                 CASE ('hea')
                    WRITE(unit_n,'(I12)',advance='no') zeig%coh%x_health

                 CASE ('hei')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%height

                 CASE ('hrt')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%x_hrt

                 CASE ('leaf')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%t_leaf

                 CASE ('maintres')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%maintres

                 CASE ('nas')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%netass

                 CASE ('npp')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%npp

                 CASE ('Ndemc_c')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%Ndemc_c

                 CASE ('Nuptc_c')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%Nuptc_c

                 CASE ('Nfol')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%N_fol

                 CASE ('Npool')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%N_pool

                 CASE ('Nstr')
                    if(zeig%coh%Ndemc_c.ne.0) then
                        help = zeig%coh%Nuptc_c / zeig%coh%Ndemc_c
                    else
                        help = zeig%coh%Nuptc_c
!                        help = 1
                    end if
                    WRITE(unit_n,'(E12.3)',advance='no') help

                 CASE ('rdpt')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%x_rdpt

                 CASE ('rooteff')
                    WRITE(unit_n,'(F12.4)',advance='no') zeig%coh%rooteff(1)
				 
				 CASE ('sap')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%x_sap

                 CASE ('sfol')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%sfol

                 CASE ('sfrt')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%sfrt

                 CASE ('spn')
                    WRITE(unit_n,'(I12)',advance='no') zeig%coh%species

                 CASE ('ssap')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%ssap

                 CASE ('stem')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%stem_inc

                 CASE ('str')
                    WRITE(unit_n,'(I12)',advance='no') zeig%coh%x_stress

                 CASE ('tdb')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%dbio

                 CASE ('trman')
                    WRITE(unit_n,'(I12)',advance='no') int(zeig%coh%ntreem)

                 CASE ('toplayer')
                    WRITE(unit_n,'(I12)',advance='no') zeig%coh%topLayer

                 CASE ('ttb')
                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%totbio

                 CASE ('watleft')
                    WRITE(unit_n,'(F12.4)',advance='no') zeig%coh%watleft
                    
                 CASE ('yrw')
                    WRITE(unit_n,'(F12.4)',advance='no') zeig%coh%jrb

                 end select

                 lflag = .TRUE.
                 exit

              ELSE
                 zeig => zeig%next
              END IF

           END DO

           if (.not. lflag) WRITE(unit_n,'(F12.3)',advance='no') -99.9

        END DO  !j

        WRITE(unit_n,'(A)') ''

                 select CASE (outcy(i)%kind_name)
                  CASE ('frtrel')
                    do k=2,nroot_max
                       WRITE(unit_n ,'(I2,3X)',advance='no') k
                       do j= 1,max_coh
                          zeig => pt%first
                          lflag = .FALSE.
                          do while (associated(zeig))
                             if (zeig%coh%ident .eq. j) then
                                WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%frtrel(k)
                                lflag = .TRUE.
                                exit
                             ELSE
                                zeig => zeig%next
                             END IF
                          END DO  ! zeig
                          if (.not. lflag) WRITE(unit_n,'(F12.3)',advance='no') -99.9
                        END DO  ! j
                        WRITE(unit_n,'(A)') ''
                    END DO   ! k
                    WRITE(unit_n,'(A)') ''

                  CASE ('frtrelc')
                    do k=2,nroot_max
                       WRITE(unit_n ,'(I2,3X)',advance='no') k
                       do j= 1,max_coh
                          zeig => pt%first
                          lflag = .FALSE.
                          do while (associated(zeig))
                             if (zeig%coh%ident .eq. j) then
                                WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%frtrelc(k)
                                lflag = .TRUE.
                                exit
                             ELSE
                                zeig => zeig%next
                             END IF
                          END DO  ! zeig
                          if (.not. lflag) WRITE(unit_n,'(F12.3)',advance='no') -99.9
                        END DO  ! j
                        WRITE(unit_n,'(A)') ''
                    END DO   ! k
                    WRITE(unit_n,'(A)') ''

					CASE ('rld')
                    if (flag_wred .eq. 9) then
                        do k=2,nroot_max
                           WRITE(unit_n ,'(I2,3X)',advance='no') k
                           do j= 1,max_coh
                              zeig => pt%first
                              lflag = .FALSE.
                              do while (associated(zeig))
                                 if (zeig%coh%ident .eq. j) then
                                    WRITE(unit_n,'(E12.3)',advance='no') zeig%coh%rld(k)
                                    lflag = .TRUE.
                                    exit
                                 ELSE
                                    zeig => zeig%next
                                 END IF
                              END DO  ! zeig
                              if (.not. lflag) WRITE(unit_n,'(F12.3)',advance='no') -99.9
                            END DO  ! j
                            WRITE(unit_n,'(A)') ''
                        END DO   ! k
                    endif
                    WRITE(unit_n,'(A)') ''
                 end select

     endif  ! out_flag
  enddo  !i
END subroutine coh_out_y

!**************************************************************

SUBROUTINE out_wpm (flagout)

use data_out
use data_simul
use data_wpm

implicit none

  integer flagout    ! control of output
                     ! 0 - no output
                     ! 1 - output at end of simulation
  integer i,j,k
  integer unit_n     ! output unit

	integer dummy
    dummy = 0.

! output of all selected files
do j = 1,oute_n
   if (oute(j)%out_flag .eq. flagout) then
   unit_n = oute(j)%unit_nr

      select CASE (oute(j)%kind_name)

	  CASE ('sea')
	    	do i=1,size(years)
		    write(unit_n,	'(I6, 30F10.2)') &
						years(i),			&
						sum_costs(1,i),		&
						sum_costs(2,i),		&
						sum_costs(3,i),		&
						sum_costs(4,i),		&
						fix(2)-fix(1),		&
						sum_costs(5,i),		&
						st_costs(1,i),		&
						st_costs(2,i),		&
						st_costs(3,i),		&
						st_costs(4,i),		&
						st_costs(5,i),		&
						st_assets(1,i),		&
						st_assets(2,i),		&
						st_assets(3,i),		&
						st_assets(4,i),		&
						st_assets(5,i),		&
						ms_costs(1,i),		&
						ms_costs(2,i),		&
						ms_costs(3,i),		&
						ms_costs(4,i),		&
						ms_costs(5,i),		&
						ms_assets(1,i),		&
						ms_assets(2,i),		&
						ms_assets(3,i),		&
						ms_assets(4,i),		&
						ms_assets(5,i),		&
						fix(1),				&
						subsidy(1,i),		&
						subsidy(1,i),		&
						fix(2)										
			end do
	  case ('sea_npv')
	  	do i=1,size(years)
		  write(unit_n,	'(I6, 12F10.2)') &
						years(i),		&
						npv(1,i),		&
						npv(2,i),		&
						npv(3,i),		&
						npv(4,i),		&
						npv(5,i),		&
						npv(6,i),		&
						npv(7,i),		&
						npv(8,i),		&
						npv(9,i),		&
						npv(10,i),		&
						npv(11,i),		&
						npv(12,i)
		end do
      CASE ('sea_ms')
		    	do i=1,size(years)
		    write(unit_n,	'(I6,43E10.3)')	&
						years(i),			&
						mansort_tg(1,1,i),	&
						mansort_tg(1,2,i),	&
						mansort_tg(1,3,i),	&
						mansort_tg(1,6,i),	&
						mansort_tg(1,7,i),	&
						mansort_tg(1,8,i),	&
						mansort_tg(1,9,i),	&
						mansort_tg(1,10,i), &
						mansort_tg(2,1,i),	&
						mansort_tg(2,2,i),	&
						mansort_tg(2,4,i),	&
						mansort_tg(2,5,i),	&
						mansort_tg(2,6,i),	&
						mansort_tg(2,7,i),	&
						mansort_tg(2,8,i),	&
						mansort_tg(2,9,i),	&
						mansort_tg(2,10,i), &
						mansort_tg(3,1,i),	&
						mansort_tg(3,2,i),	&
						mansort_tg(3,3,i),	&
						mansort_tg(3,4,i),	&
						mansort_tg(3,5,i),	&
						mansort_tg(3,6,i),	&
						mansort_tg(3,7,i),	&
						mansort_tg(3,8,i),	&
						mansort_tg(3,9,i),	&
						mansort_tg(3,10,i), &
						mansort_tg(4,1,i),	&
						mansort_tg(4,2,i),	&
						mansort_tg(4,5,i),	&
						mansort_tg(4,6,i),	&
						mansort_tg(4,7,i),	&
						mansort_tg(4,8,i),	&
						mansort_tg(4,9,i),	&
						mansort_tg(4,10,i), &
						mansort_tg(5,1,i),	&
						mansort_tg(5,2,i),	&
						mansort_tg(5,5,i),	&
						mansort_tg(5,6,i),	&
						mansort_tg(5,7,i),	&
						mansort_tg(5,8,i),	&
						mansort_tg(5,9,i),	&
						mansort_tg(5,10,i)
			end do

      CASE ('sea_st')
		    	do i=1,size(years)
		    write(unit_n,	'(I6,43E10.3)')	&
						years(i),			&
						standsort_tg(1,1,i),	&
						standsort_tg(1,2,i),	&
						standsort_tg(1,5,i),	&
						standsort_tg(1,6,i),	&
						standsort_tg(1,7,i),	&
						standsort_tg(1,8,i),	&
						standsort_tg(1,9,i),	&
						standsort_tg(1,10,i),	&
						standsort_tg(2,1,i),	&
						standsort_tg(2,2,i),	&
						standsort_tg(2,4,i),	&
						standsort_tg(2,5,i),	&
						standsort_tg(2,6,i),	&
						standsort_tg(2,7,i),	&
						standsort_tg(2,8,i),	&
						standsort_tg(2,9,i),	&
						standsort_tg(2,10,i),	&
						standsort_tg(3,1,i),	&
						standsort_tg(3,2,i),	&
						standsort_tg(3,3,i),	&
						standsort_tg(3,4,i),	&
						standsort_tg(3,5,i),	&
						standsort_tg(3,6,i),	&
						standsort_tg(3,7,i),	&
						standsort_tg(3,8,i),	&
						standsort_tg(3,9,i),	&
						standsort_tg(3,10,i),	&
						standsort_tg(4,1,i),	&
						standsort_tg(4,2,i),	&
						standsort_tg(4,5,i),	&
						standsort_tg(4,6,i),	&
						standsort_tg(4,7,i),	&
						standsort_tg(4,8,i),	&
						standsort_tg(4,9,i),	&
						standsort_tg(4,10,i),   &
						standsort_tg(5,1,i),	&
						standsort_tg(5,2,i),	&
						standsort_tg(5,5,i),	&
						standsort_tg(5,6,i),	&
						standsort_tg(5,7,i),	&
						standsort_tg(5,8,i),	&
						standsort_tg(5,9,i),	&
						standsort_tg(5,10,i)
	    end do

      CASE ('wpm')
    	do i=1,size(years)
		    write(unit_n,	'(I6,13E10.3, 1E11.3, 3E10.3)')	&
						years(i),					&
						sum_input(i),				&
						use_categories(1)%value(i),	&
						use_categories(2)%value(i),	&
						use_categories(3)%value(i),	&
						use_categories(4)%value(i),	&
						use_categories(5)%value(i),	&
						use_categories(6)%value(i),	&
						use_categories(7)%value(i),	&
						sum_use_cat(i),				&
						burning(i),					&
						landfill(i),				&
						atmo_year(i),				&
						atmo_cum(i),                    &
						emission_har(i),		&
						sub_energy(i),			&
						sub_material(i),		&
						sub_sum(i)
	    end do

      CASE ('wpm_inter')
    	do i=1,size(years)
		    write(unit_n,	'(I6,27E10.3)')	&
						years(i),		&
						pl(1,1,i),		&
						pl(1,2,i),		&
						pl(1,3,i),		&
						pl(1,4,i),		&								
						pl(1,5,i),		&							
						pl(1,7,i),		&							
						pl(2,1,i),		&
						pl(2,2,i),		&
						pl(2,3,i),		&
						pl(2,4,i),		&							
						pl(2,5,i),		&							
						pl(2,6,i),		&							
						pl(2,7,i),		&
						pl(3,1,i),		&							
						pl(3,2,i),		&
						pl(3,3,i),		&
						pl(3,4,i),		&								
						pl(3,5,i),		&								
						pl(3,6,i),		&							
						pl(3,7,i),		&														
						use_cat(1,i),	&
						use_cat(2,i),	&
						use_cat(3,i),	&
						use_cat(4,i),	&
						use_cat(5,i),	&
						use_cat(6,i),	&
						use_cat(7,i)
	    end do

      end select
   endif
enddo

end subroutine out_wpm

!**************************************************************

SUBROUTINE out_scen
USE data_simul
USE data_out
IMPLICIT NONE

WRITE (unit_ctr,*) ip,'         ',deltaT,deltaPrec

END subroutine out_scen

!**************************************************************

SUBROUTINE out_comp(unit_comp)

! final result output for each run

USE data_biodiv
USE data_climate
USE data_depo
USE data_evapo
USE data_inter
USE data_manag
USE data_out
USE data_par
USE data_simul
USE data_site
USE data_soil
USE data_soil_cn
USE data_species
USE data_stand
USE data_climate
USE data_frost

IMPLICIT NONE

integer unit_comp
integer help1, i
real, dimension(31) ::  help2
real    hconv        ! conversion factor from patchsize into ha
! output variables of final results in kg/ha
real   y_NPP,   &    ! mean net primary productioin
       y_GPP,   &    ! mean yearly gross productioin
       y_NEP,   &    ! mean yearly net ecosystem productioin
       y_sumbio,  &  ! total biomass of all cohorts and all tree-species
       y_sumbio_sv,& ! total biomass of all cohorts and all ground-vegetation-species
       y_autresp, &  ! mean yearly total autotroph resp
       y_resps, &    ! mean yearly soil respiration
       y_resptot, &  ! mean yearly total respiration
       y_C_accu,  &  ! mean yearly C accumualtion
       y_RedN,    &  ! mean RedN of all species
       y_lai         ! LAI of stand without soil vegetation
real   C_sum         ! total C storage of the stand (biomass and soil)
real   help_gdd
character(20) idtext, datei
character(150) htext
character(1) aa

call wclas(waldtyp)
hconv        = 10000./kpatchsize
y_NPP        = cum_sumNPP * hconv * cpart/year   ! kg DW/patch --> kg C/ha
y_sumbio     = sumbio / 1000.                    ! kg DW / ha --> t DW/ha
y_sumbio_sv  = sumbio_sv / 1000.                 ! kg DW / ha --> t DW/ha
totfol       = totfol / 1000.                    ! kg / ha --> t/ha
totsap       = totsap / 1000.                    ! kg / ha --> t/ha
totfrt       = totfrt / 1000.                    ! kg / ha --> t/ha
tothrt       = tothrt / 1000.                    ! kg / ha --> t/ha
totcrt       = totcrt / 1000.                    ! kg / ha --> t/ha
tottb        = tottb / 1000.                     ! kg / ha --> t/ha
y_C_accu     = (C_tot - C_accu) * gm2_in_kgha / year  ! g C/m2 --> kg C/ha, mean
C_lit_m      = C_lit_m * gm2_in_kgha / year      ! g/m2 --> kg/ha, mean
N_lit_m      = N_lit_m * gm2_in_kgha / year      ! g/m2 --> kg/ha, mean
N_min_m      = N_min_m * gm2_in_kgha / year      ! g/m2 --> kg/ha, mean
Nupt_m       = Nupt_m * gm2_in_kgha / year       ! g/m2 --> kg/ha, mean
Nleach_m     = Nleach_m * gm2_in_kgha / year     ! g/m2 --> kg/ha, mean
y_resps      = resps_c_m * gm2_in_kgha / year    ! g C/m2 --> kg C/ha, mean
y_autresp    = autresp_m * cpart * hconv / year
y_resptot    = y_resps + y_autresp
y_GPP        = y_NPP + y_autresp
y_NEP        = y_NPP - y_resps                   ! kg C/ha
y_NPP        = y_NPP / 1000.                     ! kg C /ha --> t C/ha
dew_m        = dew_m / year
AET_m        = AET_m / year
pet_m        = pet_m / year
interc_m_can = interc_m_can / year
perc_m       = perc_m / year
wupt_r_m     = wupt_r_m / year
C_opm_stem   = C_opm_stem * gm2_in_kgha / 1000.  ! g C/m2 --> t C/ha
if (.not.lcomp1) C_tot = SUM(C_opm) + SUM(C_hum) ! calculated again (litter at the end)
C_tot        = C_tot * gm2_in_kgha / 1000.       ! g C/m2 --> t C/ha
C_hum_tot    = C_hum_tot * gm2_in_kgha / 1000.   ! g C/m2 --> t C/ha
med_air_all  = med_air_all / year

med_rad_all  = med_rad_all / year
mean_drIndAl = mean_drIndAl / year
help_gdd    = gdday_all / year
sum_prec_all = sum_prec_all / year
Ndep_cum_all = Ndep_cum_all * gm2_in_kgha / year   ! g/m2 --> kg/ha, mean
C_sum        = C_tot + (sumbio + cumsumvsab + cumsumvsdead) * cpart / 1000.    ! corrected due to C_opm_stem already in cumsumvsdead
if(fire_indb_m.gt.0) then
   fire_indb_m  = fire_indb_m / year       ! fire index Bruschek
end if
fire(2)%mean_m = fire(2)%mean_m / year     ! fire index east (Kaese M68)
fire(3)%mean_m = fire(3)%mean_m / year 
cwb_an_m     = cwb_an_m / year

ind_arid_an_m = ind_arid_an_m / year
ind_lang_an_m = ind_lang_an_m / year
ind_cout_an_m = ind_cout_an_m / year
ind_wiss_an_m = ind_wiss_an_m / year
ind_mart_an_m = ind_mart_an_m / year
ind_weck_m = ind_weck_m / year
ind_reich_m = ind_reich_m / year
ind_emb_m = ind_emb_m / year
con_gor_m = con_gor_m / year
con_cur_m = con_cur_m / year
con_con_m = con_con_m / year

ind_bud_m = ind_bud_m / year
ind_shc_m = ind_shc_m / year

if(time.gt.1) call frost_index_total

ntindex =0.
if(time.gt.1) then
    tempmean_mo = tempmean_mo/year
    call t_indices(tempmean_mo)
end if

y_lai  = 0.
y_RedN = 0.
do i = 1, nspec_tree
  y_lai  = y_lai + svar(i)%sum_lai
end do
if (anz_RedN .gt. 0) y_RedN = RedN_mean / anz_RedN

select case (flag_multi)

case (4,5,8)
   write (datei, '(A10)') adjustl(sitenum(ip))   ! standip can occur variable times, this ensures clear indetification
   read (datei, '(A)') idtext

case default
   htext  = adjustr(site_name(ip))
   idtext = adjustl(htext (131:150))   ! only write last 20 signs

end select

if(thin_dead .ne. 0) then
    cumsumvsab = cumsumvsdead
    cumsumvsdead = 0.
end if

if (time .le. 1) then
   aa = 'B'
else
   aa = 'E'
endif

if(flag_end .eq.0) then
        write (unit_comp, '(A, I5,1X, A20,F6.2,I7,I4,F9.2,E10.3, 8F9.2, F11.3, E11.3, 4E11.4, 3F8.2,4F10.2, F9.1, F9.3, 4F10.1, 7F7.1, 2F9.3, F9.1, 3F10.2, &
                            7(1X,F9.2), E12.4, F8.2, 5F10.2, F8.2, 3F8.3,3X, 3f8.2)') &
           aa, ip, idtext, y_lai, anz_tree_ha, waldtyp, y_sumbio, y_sumbio_sv, med_diam, hdom, totfol,tottb,totsap,tothrt,totfrt,totcrt, &
           y_NPP, y_NEP, y_GPP, cumsteminc, cumsumvsab, cumsumvsdead, C_sum, C_opm_stem, C_tot, C_hum_tot,C_tot_40,C_hum_40, &
           y_C_accu, C_lit_m, N_lit_m, N_min_m, Nleach_m, y_resps, y_resptot, pet_m, AET_m, perc_m, interc_m_can, wupt_r_m, med_air_all, &
           sum_prec_all, Ndep_cum_all, mean_drIndAl, help_gdd, cwb_an_m, fire(2)%mean_m, fire_indb_m, ind_arid_an_m, ind_lang_an_m, ind_cout_an_m,  &
		   ind_wiss_an_m, ind_mart_an_m, ind_weck_m, ind_reich_m, ind_emb_m, con_gor_m, con_cur_m, con_con_m, ntindex, fire(3)%mean_m, ind_bud_m, med_rad_all, y_RedN, dew_m, Nupt_m, mlfind, mlfind_sp, ind_shc_m
else
    help1 = 0
    help2 = 0.0
    write (unit_comp, '(A, I5,1X, A15,F6.2,I7,I4, 8F9.2, 6E11.4, 3F8.2, 3F10.2, F9.1, F9.3, 2F10.1, 6F7.1, F9.3)') &
           aa, ip, idtext, help2(1), help1, help1, (help2(i), i=1,31)
end if

END subroutine out_comp

!**************************************************************

SUBROUTINE error_mess(ti,mess,val)

USE data_out
USE data_simul
USE data_site

IMPLICIT NONE

INTEGER,intent(in) :: ti
CHARACTER(LEN=*),intent(in) :: mess
real,intent(in) :: val

if (flag_multi .ne. 5) then
   write (unit_err, *)
   write (unit_err, '(A8,I5,1X, A20, A10,I5)') 'ip/site ', ip, stand_id, '     Year ',ti
   write(unit_err,'(A)',advance='no') trim(mess)
   write(unit_err,*) val
endif

END subroutine error_mess

!**************************************************************

SUBROUTINE stop_mess(ti,mess)

USE data_out

IMPLICIT NONE

INTEGER,intent(in) :: ti
CHARACTER(LEN=*),intent(in) :: mess

WRITE(*,*) 'Program aborted in simulation year ',ti
WRITE(*,*) trim(mess)
WRITE(*,*) 'see error.log for reason'

END subroutine stop_mess

!**************************************************************

SUBROUTINE open_file (varout, help_ip)

! Open special output file

USE data_simul
USE data_out

IMPLICIT NONE

TYPE (out_struct)  :: varout
INTEGER         help_ip

CHARACTER(150) ::filename                ! complete name of output file

filename = trim(site_name(help_ip))//'_'//trim(varout%kind_name)//'.out'//trim(anh)

varout%unit_nr   = getunit()

open(varout%unit_nr,file=trim(dirout)//filename,status='replace')

END subroutine open_file

!**************************************************************

SUBROUTINE wr_header_file (varout)

! Write header of special output file

USE data_simul
USE data_out

IMPLICIT NONE

TYPE (out_struct)  :: varout

INTEGER         unit_n       ! output unit

unit_n = varout%unit_nr
WRITE(unit_n ,'(A)') trim(varout%f_line)
WRITE(unit_n ,'(A)') trim(varout%s_line)
WRITE(unit_n ,'(A)') trim(varout%header)

END subroutine wr_header_file

!**************************************************************

SUBROUTINE outveg (nsp, out_flag, unit_n)

! output of species values (files veg_species)

  USE data_climate
  USE data_simul
  USE data_species
  USE data_stand
  USE data_out

  IMPLICIT NONE

  integer:: nsp        ! species number
  integer:: out_flag   ! output flag
  integer:: unit_n     ! output unit
  real   :: dumvar=0.

         if (out_flag .eq. 1) then

            sout(nsp)%help_veg1(1) = nsp
            sout(nsp)%help_veg1(2) = svar(nsp)%anz_coh
            sout(nsp)%help_veg1(3) = svar(nsp)%sum_nTreeA

            sout(nsp)%help_veg2(1) = svar(nsp)%sum_lai
            sout(nsp)%help_veg2(2) = svar(nsp)%sum_bio
            sout(nsp)%help_veg2(3) = svar(nsp)%sumNPP
            sout(nsp)%help_veg2(4) = svar(nsp)%med_diam
            sout(nsp)%help_veg2(5) = svar(nsp)%dom_height
            sout(nsp)%help_veg2(6) = svar(nsp)%fol
            sout(nsp)%help_veg2(7) = svar(nsp)%sap
            sout(nsp)%help_veg2(8) = svar(nsp)%frt
            sout(nsp)%help_veg2(9) = svar(nsp)%hrt
            sout(nsp)%help_veg2(10)= svar(nsp)%totsteminc
            sout(nsp)%help_veg2(11)= svar(nsp)%totstem_m3
            sout(nsp)%help_veg3    = svar(nsp)%crown_area/kpatchsize
			sout(nsp)%help_veg4    = svar(nsp)%sumvsdead*10000/kpatchsize
			sout(nsp)%help_veg5    = svar(nsp)%sumvsdead_m3*10000/kpatchsize
            sout(nsp)%help_veg6    = svar(nsp)%totsteminc_m3

            out_flag = 2
         else if (out_flag .eq. 2) then
            WRITE(unit_n,'(I6)',advance='no') time_cur
            WRITE(unit_n,'(3I10)',advance='no') sout(nsp)%help_veg1
            WRITE(unit_n,'(F10.3,2E12.3,2F12.3,9E12.3, 4F12.3, I6, F6.0,3F12.3, 3F12.4)') sout(nsp)%help_veg2, svar(nsp)%sumvsab, sout(nsp)%help_veg4, &
                                                         sout(nsp)%help_veg3, svar(nsp)%drIndAl, svar(nsp)%Ndem, svar(nsp)%Nupt, svar(nsp)%RedNm, &
                                                         svar(nsp)%daybb, spar(nsp)%end_bb, svar(nsp)%mean_diam, svar(nsp)%mean_height, svar(nsp)%basal_area, sout(nsp)%help_veg5,sout(nsp)%help_veg6, svar(nsp)%mean_jrb
            out_flag = 1
         endif

END SUBROUTINE outveg

!**************************************************************

SUBROUTINE outstore

! store of output variables (multi run 4 and 8)
USE data_climate
USE data_depo
USE data_evapo
USE data_inter
USE data_manag
USE data_out
USE data_par
USE data_simul
USE data_soil
USE data_soil_cn
USE data_stand
USE data_biodiv
USE data_frost

IMPLICIT NONE

real   C_sum, &      ! total C storage of the stand (biomass and soil)
       hconv, help
integer i, j, k, ipp

if (flag_trace) write (unit_trace, '(I4,I10,A)') iday, time_cur, ' outstore '

  if (flag_mult910) then
    ipp = 1
  else
    ipp = ip
  endif

  hconv        = 10000./kpatchsize
  do i = 1, nvar-1

     select case (trim(outvar(i)))

     case('above_biom')
	     output_var(i,ipp,time)=(sumbio-totfrt-totcrt)/1000.	

     case ('AET','aet')
        output_var(i,ipp,time) = AET_cum

     case ('AET_year','AETyear','aetyear','aet_year')  !  AET
        outvar(i) = 'AET_year'
        output_var(i,ipp,time) = AET_cum

     case ('AET_mon','AETmon','aetmon','aet_mon')  ! monthly AET
        outvar(i) = 'AET_mon'
        k = output_var(i,1,0)
        do j = 1, 12
            output_varm(k,ipp,time,j) = AET_mon(j) 
        enddo

     case ('AET_week','AETweek','aetweek','aet_week')  ! weekly AET
        outvar(i) = 'AET_week'
        k = output_var(i,1,0)
        do j = 1, 52
            output_varw(k,ipp,time,j) = AET_week(j) 
        enddo
 
     case ( 'anzdlf') ! number of days with forst  April - June
         output_var(i,ipp,time) = anzdlf(time)
     
     case ( 'BA') ! basal area
         output_var(i,ipp,time) = basal_area
         

     case ('C_accu','Caccu','c_accu')   ! C accumulation per year
        if (time .eq. 1) then
            help = C_tot - C_accu
        else
            help = C_tot - C_accu
            do j = 1, time-1
                help = help - output_var(i,ipp,j)*1000.*kgha_in_gm2
            end do
        endif
        output_var(i,ipp,time) = help * gm2_in_kgha / 1000.  ! g C/m2 --> t C/ha
 
     case ('C_d_stem','c_d_stem')
        output_var(i,ipp,time) = C_opm_stem * gm2_in_kgha / 1000. 
    
     case ('chumtot','Chumtot','C_hum_tot')   ! total C in humus
        output_var(i,ipp,time) = C_hum_tot * gm2_in_kgha / 1000.   ! g C/m2 --> t C/ha

	 case('con_gor')
     	     output_var(i,ipp,time)=con_gor
			  
	 case('con_cur')
     	     output_var(i,ipp,time)=con_cur

	 case('con_con')
     	     output_var(i,ipp,time)=con_con

     case ('ctot','Ctot','C_tot')   ! total soil C
        output_var(i,ipp,time) = C_tot * gm2_in_kgha / 1000.       ! g C/m2 --> t C/ha

     case ('csum','Csum','C_sum')   ! total C in ecosystem
        output_var(i,ipp,time) = C_tot*gm2_in_kgha/1000. + (sumbio + cumsumvsab + cumsumvsdead) * cpart / 1000.   ! t/ha

	 case('cwb')                    ! climatic water balance
   	     output_var(i,ipp,time)=cwb_an

     case ('cwbyear','cwb_year')  ! climatic water balance
        outvar(i) = 'cwb_year'
   	     output_var(i,ipp,time)=cwb_an

     case ('cwbmon','cwb_mon')  ! monthly climatic water balance
        outvar(i) = 'cwb_mon'
        k = output_var(i,1,0)
        do j = 1, 12
            output_varm(k,ipp,time,j) = prec_mon(j) - pet_mon(j) 
        enddo

     case ('cwbweek','cwb_week')  ! weekly climatic water balance
        outvar(i) = 'cwb_week'
        k = output_var(i,1,0)
        do j = 1, 52
            output_varw(k,ipp,time,j) = prec_week(j) - pet_week(j) 
        enddo
         
     case ( 'date_lf') ! number of the day with the last late frost
         output_var(i,ipp,time) = date_lf(time)
               
     case ( 'date_lft') ! number of the day with the last late frost
         output_var(i,ipp,time) = date_lftot(time)

     case('daybb_be')
         output_var(i,ipp,time)= svar(1)%daybb

     case('daybb_oa')
         output_var(i,ipp,time)= svar(4)%daybb

     case('daybb_bi')
         output_var(i,ipp,time)= svar(5)%daybb

      case ('dbh')
        output_var(i,ipp,time) = mean_diam
        
    case ('dens')                 ! stem density
       output_var(i,ipp,time) = anz_tree_ha

     case ('dnlf')    ! number of frost days after start of vegetation period
         output_var(i,ipp,time) = dnlf(time)
         
     case ('dnlf_sp') !  number of frost days after bud burst
         output_var(i,ipp,time) = dnlf_sp(time)
     
     case ('drindal', 'drIndAl', 'drIndal', 'DrIndAl')   ! drought index for allocation calculation (cum.) for the whole stand [-], weighted by NPP
        output_var(i,ipp,time) = drIndAl

     case ('fire_indb')
        output_var(i,ipp,time) = fire_indb

     case ('fire_ind1')
        output_var(i,ipp,time) = fire(1)%mean

     case ('fire_ind2')
        output_var(i,ipp,time) = fire(2)%mean

     case ('fire_ind3')
        output_var(i,ipp,time) = fire(3)%mean

     case ('fire_ind1_c1')
        output_var(i,ipp,time) = fire(1)%frequ(1)

     case ('fire_ind1_c2')
        output_var(i,ipp,time) = fire(1)%frequ(2)

     case ('fire_ind1_c3')
        output_var(i,ipp,time) = fire(1)%frequ(3)

     case ('fire_ind1_c4')
        output_var(i,ipp,time) = fire(1)%frequ(4)

     case ('fire_ind1_c5')
        output_var(i,ipp,time) = fire(1)%frequ(5)

     case ('fire_ind2_c1')
        output_var(i,ipp,time) = fire(2)%frequ(1)

     case ('fire_ind2_c2')
        output_var(i,ipp,time) = fire(2)%frequ(2)

     case ('fire_ind2_c3')
        output_var(i,ipp,time) = fire(2)%frequ(3)

     case ('fire_ind2_c4')
        output_var(i,ipp,time) = fire(2)%frequ(4)

     case ('fire_ind2_c5')
        output_var(i,ipp,time) = fire(2)%frequ(5)

     case ('fire_ind3_c1')
        output_var(i,ipp,time) = fire(3)%frequ(1)

     case ('fire_ind3_c2')
        output_var(i,ipp,time) = fire(3)%frequ(2)

     case ('fire_ind3_c3')
        output_var(i,ipp,time) = fire(3)%frequ(3)

     case ('fire_ind3_c4')
        output_var(i,ipp,time) = fire(3)%frequ(4)

     case ('fire_ind3_c5')
        output_var(i,ipp,time) = fire(3)%frequ(5)

     case('fortyp')                ! forest type classified
        call wclas(waldtyp)
        output_var(i,ipp,time) = waldtyp

     case ('gpp','GPP')             ! yearly GPP
        output_var(i,ipp,time) = sumGPP * hconv/100.   ! g C/patch --> t C/ha

     case ('GPP_year','GPPyear','gppyear','gpp_year')  ! GPP for each year
        outvar(i) = 'GPP_year'
        output_var(i,ipp,time) = sumGPP * hconv/100.   ! g C/patch --> t C/ha

     case ('GPP_mon','GPPmon','gppmon','gpp_mon')  ! monthly GPP
        outvar(i) = 'GPP_mon'
        k = output_var(i,1,0)
        do j = 1, 12
            output_varm(k,ipp,time,j) = GPP_mon(j) * hconv/100.   ! g C/patch --> t C/ha
        enddo

     case ('GPP_week','GPPweek','gppweek','gpp_week')  ! weekly GPP
        outvar(i) = 'GPP_week'
        k = output_var(i,1,0)
        do j = 1, 52
            output_varw(k,ipp,time,j) = GPP_week(j) * hconv/100.   ! g C/patch --> t C/ha
        enddo

     case ('height')
        output_var(i,ipp,time) = hdom

     case ('iday_vp')          ! yearly canopy interception
        output_var(i,ipp,time) = iday_vegper

	 case('ind_arid')
	     	     output_var(i,ipp,time)=ind_arid_an

	  case('ind_cout')
	     	     output_var(i,ipp,time)=ind_cout_an

	 case('ind_emb')
	     	     output_var(i,ipp,time)=ind_emb

	 case('ind_lang')
	     	     output_var(i,ipp,time)=ind_lang_an	

	 case('ind_mart')
	     	     output_var(i,ipp,time)=ind_mart_an
				 		  
	 case('ind_reich')
	     	     output_var(i,ipp,time)=ind_reich
	
	 case('ind_weck')
	     	     output_var(i,ipp,time)=ind_weck	 
 
	 case('ind_wiss')
	     	     output_var(i,ipp,time)=ind_wiss_an	
   
     case ('int','interc')          ! yearly canopy interception
        output_var(i,ipp,time) = int_cum_can

     case ('lai','LAI')   
            output_var(i,ipp,time) = LAImax

     case ('NEE_mon','NEEmon','neemon','nee_mon')  ! monthly NEP
        outvar(i) = 'NEE_mon'
        k = output_var(i,1,0)
        do j = 1, 12
            output_varm(k,ipp,time,j) = NEE_mon(j)                                  ! g C/m�
        enddo

     case ('NEP', 'nep')
        outvar(i) = 'NEP'
        output_var(i,ipp,time) = sumNPP * hconv * cpart/1000. -  resps_c * gm2_in_kgha/1000. ! kg DW/patch --> t C/ha
 
     case ('NEP_year','NEPyear','nepyear','nep_year')  ! NEP of each year
        outvar(i) = 'NEP_year'
        output_var(i,ipp,time) = sumNPP * hconv * cpart/1000. -  resps_c * gm2_in_kgha/1000. ! kg DW/patch --> t C/ha

     case ('NEP_mon','NEPmon','nepmon','nep_mon')  ! monthly NEP
        outvar(i) = 'NEP_mon'
        k = output_var(i,1,0)
        do j = 1, 12
            output_varm(k,ipp,time,j) = NPP_mon(j) * hconv/100. -  resps_mon(j) * gm2_in_kgha/1000. ! kg C/patch --> t C/ha
        enddo

     case ('NEP_week','NEPweek','nepweek','nep_week')  ! weekly NPP
        outvar(i) = 'NEP_week'
        k = output_var(i,1,0)
        do j = 1, 52
            output_varw(k,ipp,time,j) = NPP_week(j) * hconv/100. -  resps_week(j) * gm2_in_kgha/1000. ! g C/patch --> t C/ha
        enddo

     case ('ndep','Ndep','N_dep')   ! yearly N deposition
        output_var(i,ipp,time) = Ndep_cum        ! g N/m2

     case('nleach', 'Nleach', 'N_leach')   ! Annual N leaching  kg N/ha
        output_var(i,ipp,time) = N_min * gm2_in_kgha      ! g/m2 --> kg/ha, mean

     case ('nmin','Nmin','N_min')   ! yearly N mineralization
        output_var(i,ipp,time) = N_min * gm2_in_kgha      ! g/m2 --> kg/ha, mean

     case ('npp','NPP')             ! NPP
        output_var(i,ipp,time) = sumNPP * hconv * cpart/1000.   ! kg DW/patch --> t C/ha

     case ('NPP_year','NPPyear','nppyear','npp_year')  ! NPP of each year
        outvar(i) = 'NPP_year'
        output_var(i,ipp,time) = sumNPP * hconv * cpart/1000.   ! kg DW/patch --> t C/ha

     case ('NPP_mon','NPPmon','nppmon','npp_mon')  ! monthly NPP
        outvar(i) = 'NPP_mon'
        k = output_var(i,1,0)
        do j = 1, 12
            output_varm(k,ipp,time,j) = NPP_mon(j) * hconv/100.   ! g C/patch --> t C/ha
        enddo

     case ('NPP_week','NPPweek','nppweek','npp_week')  ! weekly NPP
        outvar(i) = 'NPP_week'
        k = output_var(i,1,0)
        do j = 1, 52
            output_varw(k,ipp,time,j) = NPP_week(j) * hconv/100.   ! g C/patch --> t C/ha
        enddo

     case ('NTI', 'nti','NTindex','ntindex')                  ! Nonnen-Temperatur-Index
        output_var(i,ipp,time) = ntindex

     case ('perc')                  ! yearly percolation
        output_var(i,ipp,time) = perc_cum

     case ('perc_year')                  ! yearly percolation
        outvar(i) = 'perc_year'
        output_var(i,ipp,time) = perc_cum

     case ('perc_mon', 'percmon')                      ! monthly percolation
        outvar(i) = 'perc_mon'
        k = output_var(i,1,0)
        do j = 1, 12
            output_varm(k,ipp,time,j) = perc_mon(j)
        enddo

     case ('perc_week', 'percweek')                    ! weekly percolation
        outvar(i) = 'perc_week'
        k = output_var(i,1,0)
        do j = 1, 52
            output_varw(k,ipp,time,j) = perc_week(j)
        enddo
     
     case ('PET','pet')                                ! potential evapotranspiration sum
        output_var(i,ipp,time) = PET_cum

     case ('PET_year','PETyear','pet_year','petyear')  ! potential evapotranspiration sum  of each year
        outvar(i) = 'PET_year'
        output_var(i,ipp,time) = PET_cum

     case ('PET_mon','PETmon','pet_mon','petmon')     ! monthly potential evapotranspiration sum
        outvar(i) = 'PET_mon'
        k = output_var(i,1,0)
        do j = 1, 12
            output_varm(k,ipp,time,j) = PET_mon(j)
        enddo

     case ('PET_week','PETweek','pet_week','petweek') ! weekly potential evapotranspiration sum
        outvar(i) = 'PET_week'
        k = output_var(i,1,0)
        do j = 1, 52
            output_varw(k,ipp,time,j) = PET_week(j)
        enddo

     case ('prec')                  ! yearly precipitation
        output_var(i,ipp,time) = sum_prec

     case ('prec_year', 'precyear')                  ! precipitation sum of each year 
        outvar(i) = 'prec_year'
        output_var(i,ipp,time) = sum_prec

     case ('prec_mon', 'precmon')                  ! monthly precipitation sum
        outvar(i) = 'prec_mon'
        k = output_var(i,1,0)
        do j = 1, 12
            output_varm(k,ipp,time,j) = prec_mon(j)
        enddo

     case ('prec_week', 'precweek')                  ! weekly precipitation sum
        outvar(i) = 'prec_week'
        k = output_var(i,1,0)
        do j = 1, 52
            output_varw(k,ipp,time,j) = prec_week(j)
        enddo

     case ('resps','respsoil')                           ! yearly soil respiration 
        outvar(i) = 'resps'
        output_var(i,ipp,time) = resps_c * gm2_in_kgha   ! g C/m2 --> kg C/ha, mean

     case ('resps_year', 'respsyear')                  ! soil respiration of each year 
        outvar(i) = 'resps_year'
        output_var(i,ipp,time) = resps_c * gm2_in_kgha   ! g C/m2 --> kg C/ha, mean

     case ('resps_mon', 'respsmon')                  ! monthly soil respiration
        outvar(i) = 'resps_mon'
        k = output_var(i,1,0)
        do j = 1, 12
            output_varm(k,ipp,time,j) = resps_mon(j) * gm2_in_kgha   ! g C/m2 --> kg C/ha
        enddo

     case ('resps_week', 'respsweek')                  ! weekly soil respiration
        outvar(i) = 'resps_week'
        k = output_var(i,1,0)
        do j = 1, 52
            output_varw(k,ipp,time,j) = resps_week(j) * gm2_in_kgha   ! g C/m2 --> kg C/ha
        enddo

     case('steminc')
        output_var(i,ipp,time)= totsteminc/1000.

     case ('sumbio')                ! Biomass
        output_var(i,ipp,time) = sumbio / 1000.      ! kg DW / ha --> t DW/ha

     case ('sumtlf') ! temperature sum of days with frost April - June
         output_var(i,ipp,time) = sumtlf(time)
           
     case ('temp')                  ! airtemp
        output_var(i,ipp,time) = med_air

     case ('temp_year', 'tempyear')                  ! mean yearly air temperature
        outvar(i) = 'temp_year'
        output_var(i,ipp,time) = med_air

     case ('temp_mon', 'tempmon')                  ! mean monthly air temperature
        outvar(i) = 'temp_mon'
        k = output_var(i,1,0)
        do j = 1, 12
            output_varm(k,ipp,time,j) = temp_mon(j) ! Mittelung erfolgt schon in daily  (/ monrec(j))
        enddo

     case ('temp_week', 'tempweek')                  ! mean weekly air temperature
        outvar(i) = 'temp_week'
        k = output_var(i,1,0)
        do j = 1, 52
            output_varw(k,ipp,time,j) = temp_week(j) / 7.
        enddo

     case ('TER','ter')             ! yearly TER
        outvar(i) = 'TER'
        output_var(i,ipp,time) = sumTER * hconv/100.   ! g C/patch --> t C/ha

     case ('TER_year','TERyear','teryear','ter_year')  ! yearly TER
        outvar(i) = 'TER_year'
        output_var(i,ipp,time) = sumTER * hconv/100.   ! g C/patch --> t C/ha

     case ('TER_mon','TERmon','termon','ter_mon')  ! monthly TER
        outvar(i) = 'TER_mon'
        k = output_var(i,1,0)
        do j = 1, 12
            output_varm(k,ipp,time,j) = TER_mon(j) * hconv/100.   ! g C/patch --> t C/ha
        enddo

     case ('TER_week','TERweek','terweek','ter_week')  ! weekly TER
        outvar(i) = 'TER_week'
        k = output_var(i,1,0)
        do j = 1, 52
            output_varw(k,ipp,time,j) = TER_week(j) * hconv/100.   ! g C/patch --> t C/ha
        enddo

     case('totstem')
        output_var(i,ipp,time)= totstem_m3

     case('vsab')
        output_var(i,ipp,time)= sumvsab_m3

     case('vsdead')
        output_var(i,ipp,time)= sumvsdead_m3
	
     end select
  enddo
END SUBROUTINE outstore

!**************************************************************

SUBROUTINE out_var_file

! writing of output variables (multi run 4 and 8)
   use data_biodiv
   use data_out
   use data_simul
   use data_site

  IMPLICIT NONE

  integer i, ii, j, k, unit_nr
  real varerr
  character(50) :: filename    ! complete name of output file
  character(15) idtext, datei
  real, dimension(12) :: helpf  
  real, dimension(52) :: helpw  
  character(30) :: helpvar

if (flag_trace) write (unit_trace, '(I4,I10,A)') iday, time_cur, ' out_var_file '

  do i = 1, nvar-1

    helpvar = outvar(i)
    call out_var_select(helpvar, varerr, unit_nr)

    if (varerr .ne. 0.) then
      select case (trim(outvar(i)))
      case ('AET_week','cwb_week','GPP_week','NEP_week','NPP_week','perc_week','PET_week','temp_week','TER_week','prec_week','resps_week')  
            write (unit_nr, '(A)') '#       Site          Week1       Week2       Week3       Week4       Week5       Week6       Week7       Week8       Week9      &
      Week10      Week11      Week12      Week13      Week14      Week15      Week16      Week17      Week18      Week19      &
      Week20      Week21      Week22      Week23      Week24      Week25      Week26      Week27      Week28      Week29      &
      Week30      Week31      Week32      Week33      Week34      Week35      Week13      Week37      Week38      Week39      &
      Week40      Week41      Week42      Week43      Week44      Week45      Week46      Week47      Week48      Week49      &
      Week50      Week51      Week52'
        do ip = 1, site_nr
           write (datei, '(A10)') adjustl(sitenum(ip))
           read (datei, '(A)') idtext
           write (unit_nr, '(A15)', advance = 'no') idtext
           ii = output_var(i,1,0)
           helpw = 0.
           do k = 1, 52
             do j = 1, year
                helpw(k) = helpw(k) + output_varw(ii,ip,j,k)
             enddo
             helpw(k) = helpw(k) / year
           enddo
              write (unit_nr, '(52(E12.4))', advance = 'no')  helpw
           write (unit_nr, '(A)') ''
        enddo
        
      case ('AET_mon','cwb_mon','GPP_mon','NEP_mon','NPP_mon','perc_mon','PET_mon','temp_mon','TER_mon','prec_mon','resps_mon')  
        write (unit_nr, '(A)') '#       Site          Mean1       Mean2       Mean3       Mean       4&
       Mean5       Mean6       Mean7       Mean8       Mean9      Mean10      Mean11      Mean12'
        do ip = 1, site_nr
           write (datei, '(A10)') adjustl(sitenum(ip))
           read (datei, '(A)') idtext
           write (unit_nr, '(A15)', advance = 'no') idtext
           ii = output_var(i,1,0)
           helpf = 0.
           do k = 1, 12
             do j = 1, year
                helpf(k) = helpf(k) + output_varm(ii,ip,j,k)
             enddo
             helpf(k) = helpf(k) / year
           enddo
              write (unit_nr, '(12(E12.4))', advance = 'no')  helpf
           write (unit_nr, '(A)') ''
        enddo
 
      case default
        write (unit_nr, '(A)') '#       Site         Year 1      Year 2      Year 3      Year 4      Year 5   ...'
        do ip = 1, site_nr
           write (datei, '(A10)') adjustl(sitenum(ip))
           read (datei, '(A)') idtext
           write (unit_nr, '(A15)', advance = 'no') idtext
           do j = 1, year
              write (unit_nr, '(E12.4)', advance = 'no')  output_var(i,ip,j)
           enddo
           write (unit_nr, '(A)') ''
        enddo
      end select
    else
        write (*,*)
        write (*,*) '***  4C-error - output of variables (out_var_file): ', trim(outvar(i)), ' not found'
        write (*,*)
        write (unit_err,*)
        write (unit_err,*) '***  4C-error - no such output variable (out_var_file): ', trim(outvar(i))
    endif
    close(unit_nr)
  enddo
END SUBROUTINE out_var_file

!**************************************************************

SUBROUTINE out_var_select(varout, varerr, unit_nr)

! selection of output variables and open files (multi run 4, 8, 9)
   use data_biodiv
   use data_out
   use data_simul
   use data_site

  IMPLICIT NONE

  integer unit_nr
  real varerr
  character(50) :: filename    ! complete name of output file
  character(30) :: varout
  character(15) idtext, datei

if (flag_trace) write (unit_trace, '(I4,I10,A,F6.0,I4)') iday, time_cur, ' out_var_select '//varout, varerr, unit_nr

     filename = trim(site_name1)//'_'//trim(varout)//'.out'    
     unit_nr   = getunit()
     open(unit_nr,file=trim(dirout)//filename,status='replace')
     write (unit_nr, '(A)') '#  Output of '//varout
     varerr = 0.

     select case (trim(varout))
      
     case('anzdlf')
       write(unit_nr, '(A)') '# number of days with frost April - June'
       varerr = 1
 
     case ('AET','aet')
       write (unit_nr, '(A)') '#  Yearly actual evapotranspiration sum / mm'
       varerr = 1.

     case ('AET_year')
       write (unit_nr, '(A)') '#  Annual actual evapotranspiration sum / mm'
       varerr = 1.

     case ('AET_mon','aet_mon','AETmon','aetmon')
       write (unit_nr, '(A)') '#  Monthly actual evapotranspiration sum / mm'
       varerr = 1.

     case ('AET_week','aet_week','AETweek','aetweek')
       write (unit_nr, '(A)') '#  Weekly actual evapotranspiration sum / mm'
       varerr = 1.

     case('above_biom')
       write(unit_nr,'(A)') '# Total aboveground biomass / t DW/ha'
       varerr = 1.	   

     case('BA')
       write(unit_nr,'(A)') '# Basal arera m�'
       varerr = 1.	
       
     case ('C_accu','Caccu','c_accu')   ! C accumulation per year
       write (unit_nr, '(A)') '#  Soil carbon accumulation per year / t C/ha'
       varerr = 1.
     
     case ('C_d_stem','c_d_stem')   ! C accumulation per year
       write (unit_nr, '(A)') '#  carbon in dead trees / t C/ha'
       varerr = 1.
     
     case ('C_hum_tot','C_humtot','chumtot','Chumtot')   ! total soil C
       write (unit_nr, '(A)') '#  Total carbon in humus / t C/ha'
       varerr = 1.

     case ('C_sum','csum','Csum')   ! total C in ecosystem
       write (unit_nr, '(A)') '#  Total carbon in ecosystem / t C/ha'
       varerr = 1.

     case ('C_tot','ctot','Ctot')   ! total soil C
       write (unit_nr, '(A)') '#  Total carbon in soil / t C/ha'
       varerr = 1.

	 case('con_gor')
       write(unit_nr,'(A)') '#  Continentality index Gorczynski'
       varerr = 1.	   

     case('con_cur')
       write(unit_nr,'(A)') '#  Continentality index Currey'
       varerr = 1.	 

     case('con_con')
       write(unit_nr,'(A)') '#  Continentality index Conrad'
       varerr = 1.	 

     case('cwb_year','cwb')
       write(unit_nr,'(A)') '#  Annual climate water balance'
       varerr = 1.	   

     case('cwb_mon')
       write(unit_nr,'(A)') '#  Monthly climate water balance'
       varerr = 1.	   

     case('cwb_week')
       write(unit_nr,'(A)') '#  Weekly climate water balance'
       varerr = 1.	   

     case('date_lf')
      write(unit_nr, '(A)') '# number of day of last late frost after start of vegetation period'
      varerr = 1
   
     case('date_lft')
       write(unit_nr, '(A)') '# number of day of last late frost'
       varerr = 1

     case('daybb_be')
       write(unit_nr,'(A)') '#  Day of bud burst beech'
       varerr = 1.

     case('daybb_bi')
       write(unit_nr,'(A)') '#  Day of bud burst betula'
       varerr = 1.

     case('daybb_oa')
       write(unit_nr,'(A)') '#  Day of bud burst oak'
       varerr = 1.
     
     case ('dbh')                   ! mean DBH 
       write (unit_nr, '(A)') '# DBH / cm'
       varerr = 1.

     case ('dens')                 ! stem density /ha
       write (unit_nr, '(A)') '#  Stem density per ha'
       varerr = 1.

     case('dnlf')
       write(unit_nr, '(A)') '# number of frost days since start of vegetation period'
       varerr = 1.
 
     case('dnlf_sp')
       write(unit_nr, '(A)') '# number of frost days since start of bud burst'
       varerr = 1.

     case ('drindal','drIndAl','drIndal','DrIndAl')   ! drought index for allocation calculation (cum.) for the whole stand [-], weighted by NPP
       write (unit_nr, '(A)') '#  Drought index for allocation calculation'
       varerr = 1.

     case ('fire_indb')
       write (unit_nr, '(A)') '# Fire index Bruschek'
       varerr = 1.
     
     case ('fire_ind1')
       write (unit_nr, '(A)') '# Fire index west'
       varerr = 1.
     
     case ('fire_ind2')
       write (unit_nr, '(A)') '# Fire index east'
       varerr = 1.
     
     case ('fire_ind3')
       write (unit_nr, '(A)') '# Fire index Nesterov'
       varerr = 1.
     
     case ('fire_ind1_c1')
       write (unit_nr, '(A)') '# Fire index west class 1'
       varerr = 1.
     
     case ('fire_ind1_c2')
       write (unit_nr, '(A)') '# Fire index west class 2'
       varerr = 1.
     
     case ('fire_ind1_c3')
       write (unit_nr, '(A)') '# Fire index west class 3'
       varerr = 1.
     
     case ('fire_ind1_c4')
       write (unit_nr, '(A)') '# Fire index west class 4'
       varerr = 1.
     
     case ('fire_ind1_c5')
       write (unit_nr, '(A)') '# Fire index west class 5'
       varerr = 1.
     
     case ('fire_ind2_c1')
       write (unit_nr, '(A)') '# Fire index east class 1'
       varerr = 1.
     
     case ('fire_ind2_c2')
       write (unit_nr, '(A)') '# Fire index east class 2'
       varerr = 1.
     
     case ('fire_ind2_c3')
       write (unit_nr, '(A)') '# Fire index east class 3'
       varerr = 1.
     
     case ('fire_ind2_c4')
       write (unit_nr, '(A)') '# Fire index east class 4'
       varerr = 1.
     
     case ('fire_ind2_c5')
       write (unit_nr, '(A)') '# Fire index east class 5'
       varerr = 1.

     case ('fire_ind3_c1')
       write (unit_nr, '(A)') '# Fire index Nesterov class 1'
       varerr = 1.
     
     case ('fire_ind3_c2')
       write (unit_nr, '(A)') '# Fire index Nesterov class 2'
       varerr = 1.
     
     case ('fire_ind3_c3')
       write (unit_nr, '(A)') '# Fire index Nesterov class 3'
       varerr = 1.
     
     case ('fire_ind3_c4')
       write (unit_nr, '(A)') '# Fire index Nesterov class 4'
       varerr = 1.
     
     case ('fire_ind3_c5')
       write (unit_nr, '(A)') '# Fire index Nesterov class 5'
       varerr = 1.

     case ('fortyp')
       write (unit_nr, '(A)') '# Forest classification'
       varerr = 1.

     case ('GPP')             ! GPP
       write (unit_nr, '(A)') '#  Yearly gross primary production / t C/ha'
       varerr = 1.

     case ('GPP_year')             ! GPP
       write (unit_nr, '(A)') '#  Annual gross primary production / t C/ha'
       varerr = 1.

     case ('GPP_mon')  ! monthly GPP
       write (unit_nr, '(A)') '#  Monthly gross primary production / t C/ha'
       varerr = 1.

     case ('GPP_week')  ! weekly GPP
       write (unit_nr, '(A)') '#  Weekly gross primary production / t C/ha'
       varerr = 1.

     case ('height')               ! height, in this case dominant height
       write (unit_nr, '(A)') '# Height / cm'
       varerr = 1.

     case ('iday_vp')               
       write (unit_nr, '(A)') '# start day of vegetation period'
       varerr = 1.
   
     case('ind_arid')
       write(unit_nr,'(A)') '# Aridity index (UNEP)'
       varerr = 1.	   

     case('ind_lang')
       write(unit_nr,'(A)') '# Climate index Lang'
       varerr = 1.	   

     case('ind_cout')
       write(unit_nr,'(A)') '# Climate index Coutange'
       varerr = 1.	   

     case('ind_emb')
       write(unit_nr,'(A)') '# Climate index Emberger'
       varerr = 1.	   

     case('ind_mart')
       write(unit_nr,'(A)') '# Climate index Martonne'
       varerr = 1.	   

     case('ind_reich')
       write(unit_nr,'(A)') '# Climate index Reichel'
       varerr = 1.	   

     case('ind_weck')
       write(unit_nr,'(A)') '# Climate index Weck'
       varerr = 1.	   

     case('ind_wiss')
       write(unit_nr,'(A)') '# Climate index v. Wissmann'
       varerr = 1.	   

     case ('int','interc')          ! yearly canopy interception
       write (unit_nr, '(A)') '#  Yearly canopy interception / mm'
       varerr = 1.

     case ('lai','LAI')          ! yearly canopy interception
       write (unit_nr, '(A)') '#  Maximum LAI '
       varerr = 1.
	 
	 case ('N_dep','ndep','Ndep')   ! yearly N deposition
       write (unit_nr, '(A)') '#  Yearly N deposition / g N/m2'
       varerr = 1.

     case('N_leach', 'nleach', 'Nleach')
       write(unit_nr,'(A)') '# Annual N leaching  kg N/ha'
       varerr = 1.

     case ('N_min','nmin','Nmin')   ! yearly N mineralization
       write (unit_nr, '(A)') '#  Yearly N mineralization / kg N/ha'
       varerr = 1.
     
     case ('nep','NEP')             ! NEP
       write (unit_nr, '(A)') '#  Yearly net ecosystem production / t C/ha'
       varerr = 1.
     
     case ('NEP_year')             ! NEP
       write (unit_nr, '(A)') '#  Annual net ecosystem production / t C/ha'
       varerr = 1.
     
     case ('NEP_mon')             ! monthly NEP
       write (unit_nr, '(A)') '#  Monthly net ecosystem production / t C/ha'
       varerr = 1.
     
     case ('NEP_week')             ! weekly NEP
       write (unit_nr, '(A)') '#  Weekly net ecosystem production / t C/ha'
       varerr = 1.

     case ('NPP','npp')             ! NPP
       write (unit_nr, '(A)') '#  Yearly net primary production / t C/ha'
       varerr = 1.

     case ('NPP_year')             ! NPP of each year 
       write (unit_nr, '(A)') '#  Annual net primary production / t C/ha'
       varerr = 1.

     case ('NPP_mon')  ! monthly NPP
       write (unit_nr, '(A)') '#  Monthly net primary production / t C/ha'
       varerr = 1.

     case ('NPP_week')  ! weekly NPP
       write (unit_nr, '(A)') '#  Weekly net primary production / t C/ha'
       varerr = 1.

     case ('NTI', 'nti','NTindex','ntindex')                  ! Nonnen-Temperatur-Index
       write (unit_nr, '(A)') '#  Nun temperature index'
       varerr = 1.

     case ('perc')                  ! yearly percolation
       write (unit_nr, '(A)') '#  Yearly percolation / mm'
       varerr = 1.

     case ('perc_year')                  ! yearly percolation
       write (unit_nr, '(A)') '#  Annual percolation / mm'
       varerr = 1.

     case ('perc_mon', 'percmon')                  ! monthly percolation
       write (unit_nr, '(A)') '#  Monthly percolation / mm'
       varerr = 1.

     case ('perc_week', 'percweek')                  ! weekly percolation
       write (unit_nr, '(A)') '#  Weekly percolation / mm'
       varerr = 1.

     case ('PET','pet')             ! PET
       write (unit_nr, '(A)') '#  Yearly potential evapotranspiration / mm'
       varerr = 1.

     case ('PET_year')             ! PET
       write (unit_nr, '(A)') '#  Annual potential evapotranspiration / mm'
       varerr = 1.

     case ('PET_mon')             ! PET
       write (unit_nr, '(A)') '#  Monthly potential evapotranspiration / mm'
       varerr = 1.

     case ('PET_week')             ! PET
       write (unit_nr, '(A)') '#  Weekly potential evapotranspiration / mm'
       varerr = 1.

     case ('prec')                  ! yearly precipitation
       write (unit_nr, '(A)') '#  Yearly precipitation sum / mm'
       varerr = 1.

     case ('prec_year')                  ! yearly precipitation
     write (unit_nr, '(A)') '#  Annual precipitation sum / mm'
     varerr = 1.

     case ('prec_mon', 'precmon')                  ! monthly precipitation sum
       write (unit_nr, '(A)') '#  Monthly precipitation sum / mm'
       varerr = 1.

     case ('prec_week', 'precweek')                  ! weekly precipitation sum
       write (unit_nr, '(A)') '#  Weekly precipitation sum / mm'
       varerr = 1.

     case ('resps', 'respsoil')                  ! yearly soil respiration
       write (unit_nr, '(A)') '#  Yearly soil respiration / kg C/ha'
       varerr = 1.

     case ('resps_year')                  ! yearly soil respiration
       write (unit_nr, '(A)') '#  Annual soil respiration / kg C/ha'
       varerr = 1.

     case ('resps_mon', 'respsmon')                  ! monthly soil respiration
       write (unit_nr, '(A)') '#  Monthly soil respiration / kg C/ha'
       varerr = 1.

     case ('resps_week', 'respsweek')                  ! Weekly soil respiration
       write (unit_nr, '(A)') '#  Weekly soil respiration / kg C/ha'
       varerr = 1.

     case('steminc')
       write(unit_nr,'(A)') '#  Total annual stem increment t/ha'
       varerr = 1.

     case ('sumbio')                ! Biomass
       write (unit_nr, '(A)') '#  Total Biomass / t DW/ha'
      varerr = 1.

     case('sumtlf')
       write(unit_nr, '(A)') '# temperature sum of minimum temperature < 0 April - June'
       varerr = 1

     case ('temp')                  ! airtemp
       write (unit_nr, '(A)') '#  Mean yearly air temperature / �C'
       varerr = 1.

     case ('temp_year')                  ! airtemp
       write (unit_nr, '(A)') '#  Mean annual air temperature / �C'
       varerr = 1.

     case ('temp_mon', 'tempmon')                  ! mean monthly air temperature
       write (unit_nr, '(A)') '#  Mean monthly air temperature / �C'
       varerr = 1.

     case ('temp_week', 'tempweek')                  ! mean weekly air temperature
       write (unit_nr, '(A)') '#  Mean weekly air temperature / �C'
       varerr = 1.

     case ('TER')             ! TER
       write (unit_nr, '(A)') '#  Yearly total ecosystem respiration / t C/ha'
       varerr = 1.

     case ('TER_year')             ! TER
       write (unit_nr, '(A)') '#  Annual total ecosystem respiration / t C/ha'
       varerr = 1.

     case ('TER_mon')  ! monthly TER
       write (unit_nr, '(A)') '#  Monthly total ecosystem respiration / t C/ha'
       varerr = 1.

     case ('TER_week')  ! weekly TER
       write (unit_nr, '(A)') '#  Weekly total ecosystem respiration / t C/ha'
       varerr = 1.

     case('totstem')
       write(unit_nr,'(A)') '#  Total annual stem volume m�/ha'
       varerr = 1.

     case('vsdead')
       write(unit_nr,'(A)') '#  Total annual dead stem volume m�/ha (not in the litter pool)'
       varerr = 1.

     case('vsab')
       write(unit_nr,'(A)') '#  Total annual harvested stem volume m�/ha'
       varerr = 1.
     end select
END SUBROUTINE out_var_select

!**************************************************************
