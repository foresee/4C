!*****************************************************************!
!*                                                               *!
!*                      4C (FORESEE)                             *!
!*                                                               *!
!*                                                               *!
!* returns a random number N drawn from a Poisson distribution   *!
!* with expected value U. I is a seed for                        *!
!* the random number generator                                   *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

SUBROUTINE PRAND(U,N)

      REAL UTOP
      PARAMETER(UTOP=188.)
      INTEGER N
      REAL P,R,Q,U

! function declarations

      REAL    RAND

      IF(U.GT.UTOP)STOP 'Failure in PRAND: expected value too high'
      P=EXP(-U)
      Q=P
      R=RAND()
      N=0
100   IF(Q.GE.R)RETURN
      N=N+1
      P=P*U/N
      Q=Q+P
      GOTO 100

END subroutine prand   
