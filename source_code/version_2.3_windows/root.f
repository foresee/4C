!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                                                               *!
!*                    Subroutines for:                           *!
!*                   Root distribution                           *!
!*                                                               *!
!* - ROOT_DISTR                                                  *!
!* - ROOT_EFF                                                    *!
!* - ROOT_DEPTH                                                  *!
!* - ROOT_INI                                                    *!
!* - DEALLOC_ROOT                                                *!
!* - ROOTC_NEW (nicht benutzt wegen Problemen bei Verkettung)    *!
!* - CR_DEPTH                                                    *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

SUBROUTINE root_distr

! Calculation of root distribution for each cohorte

use data_simul
use data_soil
use data_stand
use data_par
use data_species

implicit none

integer specn     ! species type (number)
integer i, j, nj, k, jlay
integer nr        ! aux. var. for nroot (rooting depth)
integer rkind     ! kind of calculation of root depth
real    frtrel_1, frtrel_j  ! rel fine root fraction of previous layer
real    frtrel_s  ! Sum of fine root fractions
real    radius    ! radius of cylyndric space created by roots of the root length density 

real beta         ! base of power
real help
real alpha, b	! Parameters for Arora function
real troot2				! theoretical root biomass of population (coarse and fine roots) only for Arora funktion spereated according to cohorts [kg/m�]
real :: part_coef=0.0	! Verteilungskoeffizient um Verh�ltnis zwischen fr_loss und redis zu bestimmen
real, dimension (1:nlay) :: fr_loss1, valspace, frtrelcoh	    !auxiliary vectors

rkind = rdepth_kind

if ((anz_tree + anz_sveg) .eq. 0) return
select case (flag_wurz)

case (0)
   root_fr = 0.
   zeig => pt%first
   do while (associated(zeig))

      call root_depth (rkind, zeig%coh%species, zeig%coh%x_age, zeig%coh%height, zeig%coh%x_frt, zeig%coh%x_crt, nr, troot2, zeig%coh%x_rdpt, zeig%coh%nroot)
      zeig%coh%nroot = nr
      zeig%coh%frtrel = thick/depth(nr)
      specn = zeig%coh%species

	  do j = 1, nr
         root_fr(j) = root_fr(j) + zeig%coh%frtrel(j) * zeig%coh%ntreeA
      enddo
      do j = nr+1, nlay
         zeig%coh%frtrel(j) = 0.
      enddo
      zeig%coh%rooteff = 0.   ! zero after use
      zeig => zeig%next
   enddo

case (1)    ! Funktion
   root_fr = 0.
   zeig => pt%first
   do while (associated(zeig))
      call root_depth (rkind, zeig%coh%species, zeig%coh%x_age, zeig%coh%height, zeig%coh%x_frt, zeig%coh%x_crt, nr, troot2, zeig%coh%x_rdpt, zeig%coh%nroot) !�nderung MG: �bergabe von Grob und Feinwurzelmasse an root_depth
      zeig%coh%nroot = nr
      specn = zeig%coh%species
      if (specn .eq. 2 .or. specn .eq. 3) then
         beta = 0.976
      else
         beta = 0.966
      endif
	  frtrel_1        = 1.
      zeig%coh%frtrel = 0.
      do j=1,nr
         frtrel_j           = beta ** depth(j)
         zeig%coh%frtrel(j) = frtrel_1 - frtrel_j
         frtrel_1           = frtrel_j
      enddo
      frtrel_s = SUM(zeig%coh%frtrel)
      frtrel_s = 1./frtrel_s
      do j=1,nr
 !     scaling of root distribution
         zeig%coh%frtrel(j) = zeig%coh%frtrel(j) * frtrel_s
         root_fr(j)         = root_fr(j) + zeig%coh%frtrel(j) * zeig%coh%ntreeA
      enddo

      zeig%coh%rooteff = 0.   ! zero after use
      zeig => zeig%next
   enddo

case (2)    ! read/use default distribution; not changed 

   root_fr = 0.
   zeig => pt%first
   do while (associated(zeig))
      if (zeig%coh%frtrel(1) .gt. 0.) then
          do j = 1,nroot_max
            root_fr(j) = root_fr(j) + zeig%coh%frtrel(j) * zeig%coh%ntreeA
          enddo
      else
           root_fr = 0.
              call root_depth (1, zeig%coh%species, zeig%coh%x_age, zeig%coh%height, zeig%coh%x_frt, zeig%coh%x_crt, nr, troot2, zeig%coh%x_rdpt, zeig%coh%nroot) 
              zeig%coh%nroot = nr
              specn = zeig%coh%species
              if (specn .eq. 2 .or. specn .eq. 3) then
                 beta = 0.98
              else
                 beta = 0.967
              endif

	          frtrel_1        = 1.
              zeig%coh%frtrel = 0.
              do j=1,nr
                 frtrel_j           = beta ** depth(j)
                 zeig%coh%frtrel(j) = frtrel_1 - frtrel_j
                 frtrel_1           = frtrel_j
              enddo
              frtrel_s = SUM(zeig%coh%frtrel)
              frtrel_s = 1./frtrel_s
              do j=1,nr
                 zeig%coh%frtrel(j) = zeig%coh%frtrel(j) * frtrel_s
                 root_fr(j)         = root_fr(j) + zeig%coh%frtrel(j) * zeig%coh%ntreeA
              enddo

      endif

      zeig%coh%rooteff = 0.   ! zero after use
      zeig => zeig%next
   enddo

case (3)
root_fr = 0.
rkind=5

zeig => pt%first
   do while (associated(zeig))
	  call root_depth (rkind, zeig%coh%species, zeig%coh%x_age, zeig%coh%height, zeig%coh%x_frt, zeig%coh%x_crt, nr, troot2, zeig%coh%x_rdpt, zeig%coh%nroot) !�nderung MG: �bergabe von Grob und Feinwurzelmasse an root_depth
      zeig%coh%nroot = nr
      specn = zeig%coh%species      
	  alpha=0.7
	  if (specn .eq. 2 .or. specn .eq. 3 .or. specn .eq. 6 .or. specn .eq. 7) then
         b = 7.95
      else
         b = 10.91
      endif
 
	  frtrel_1        = 1.
      zeig%coh%frtrel = 0.
      do j=1,nr
!        root distribution (Arora et al., 2003)
         frtrel_j           = exp((-b/troot2**alpha)*(depth(j)/100))
         zeig%coh%frtrel(j) = frtrel_1 - frtrel_j
         frtrel_1           = frtrel_j		 
	  enddo
      frtrel_s = SUM(zeig%coh%frtrel)
      frtrel_s = 1./frtrel_s
      do j=1,nr
 !     scaling of root distribution
         zeig%coh%frtrel(j) = zeig%coh%frtrel(j) * frtrel_s
         root_fr(j)         = root_fr(j) + zeig%coh%frtrel(j) * zeig%coh%ntreeA		 
	  enddo

      zeig%coh%rooteff = 0.   ! zero after use
      zeig => zeig%next
   enddo

case(4)		! TRAP-model Rasse et al. (2001)
root_fr = 0.
rkind   = 6
fr_loss1= 0
k       = 0

zeig => pt%first
   do while (associated(zeig))
	k=k+1
	zeig%coh%x_rdpt=gr_depth(k)  
    specn = zeig%coh%species
  if (specn .eq. 12) then
  continue
  endif
	call root_depth (rkind, specn, zeig%coh%x_age, zeig%coh%height, zeig%coh%x_frt, zeig%coh%x_crt, nr, troot2, zeig%coh%x_rdpt, zeig%coh%nroot)
	zeig%coh%nroot = nr
	
	frtrel_1        = 1.
    zeig%coh%frtrel = 0.
      do j=1,nr
         if (j .eq. 1) then
		  zeig%coh%frtrel(j) = (zeig%coh%x_rdpt**3-(zeig%coh%x_rdpt-depth(j))**3)/zeig%coh%x_rdpt**3
		   elseif (j .eq. nr) then
		    zeig%coh%frtrel(j)= frtrel_1 
		     else
			  zeig%coh%frtrel(j) = ((zeig%coh%x_rdpt-depth(j-1))**3-((zeig%coh%x_rdpt-depth(j))**3))/zeig%coh%x_rdpt**3
         endif
		 frtrel_1 = frtrel_1-zeig%coh%frtrel(j)		 
	  enddo
	frtrel_s = SUM(zeig%coh%frtrel)
    frtrel_s = 1./frtrel_s
	zeig%coh%frtrel = zeig%coh%frtrel * frtrel_s
	
	fr_loss1 = zeig%coh%frtrel
	fr_loss  = zeig%coh%frtrel*svar(specn)%Smean(1:nlay)
	fr_loss  = part_coef*(fr_loss1-fr_loss)
	redis    = zeig%coh%frtrel*svar(specn)%Smean(1:nlay)
	redis    = part_coef*(fr_loss1-redis)

	do j=1,nr
 !     scaling of root distribution
       if (sum(svar(specn)%Smean(1:nr)) .lt. 0.0001) then
       zeig%coh%frtrel(j) = 0.
       else
       zeig%coh%frtrel(j) = zeig%coh%frtrel(j)*svar(specn)%Smean(j)+(sum(redis)*svar(specn)%Smean(j)/sum(svar(specn)%Smean(1:nr)))
       endif
	enddo

	  frtrel_s = SUM(zeig%coh%frtrel)
      if (frtrel_s .lt. 1.E-6) then
          do j=1,nr
             zeig%coh%frtrel(j) = 0
	      enddo
      else
          frtrel_s = 1./frtrel_s
          do j=1,nr
   !     scaling of root distribution
             zeig%coh%frtrel(j) = zeig%coh%frtrel(j) * frtrel_s
             root_fr(j)         = root_fr(j) + zeig%coh%frtrel(j) * zeig%coh%ntreeA		 
	      enddo
      endif
			
	zeig%coh%rooteff = 0. 
	zeig => zeig%next
   enddo

case(5)
root_fr = 0.
rkind=5
   zeig => pt%first
   do while (associated(zeig))


      call root_depth (rkind, zeig%coh%species, zeig%coh%x_age, zeig%coh%height, zeig%coh%x_frt, zeig%coh%x_crt, nr, troot2, zeig%coh%x_rdpt, zeig%coh%nroot) !�nderung MG: �bergabe von Grob und Feinwurzelmasse an root_depth
      zeig%coh%nroot = nr
      specn = zeig%coh%species
      if (specn .eq. 2 .or. specn .eq. 3) then
         beta = 0.98
      else
         beta = 0.967
      endif

 
	  frtrel_1        = 1.
      zeig%coh%frtrel = 0.
      do j=1,nr
!       root distribution (Jackson et al., 1996): beta ** depth
         frtrel_j           = beta ** depth(j)
         zeig%coh%frtrel(j) = frtrel_1 - frtrel_j
         frtrel_1           = frtrel_j
      enddo
      frtrel_s = SUM(zeig%coh%frtrel)
      frtrel_s = 1./frtrel_s
      do j=1,nr
 !     scaling of root distribution
         zeig%coh%frtrel(j) = zeig%coh%frtrel(j) * frtrel_s
         root_fr(j)         = root_fr(j) + zeig%coh%frtrel(j) * zeig%coh%ntreeA
      enddo

      zeig%coh%rooteff = 0.   ! zero after use
      zeig => zeig%next
   enddo

case(6)

root_fr = 0.
rkind=7
   zeig => pt%first
   k=1
   do while (associated(zeig))
	  
	  zeig%coh%x_rdpt=gr_depth(k)
      call root_depth (rkind, zeig%coh%species, zeig%coh%x_age, zeig%coh%height, zeig%coh%x_frt, zeig%coh%x_crt, nr, troot2, zeig%coh%x_rdpt,zeig%coh%nroot) !�nderung MG: �bergabe von Grob und Feinwurzelmasse an root_depth
      
	  if (time .le. 1) then
	   root_lay(k)=nr
	  else
	   root_lay(k)=root_lay(k)+nr
	  endif
	  
	  if (root_lay(k) .gt. nroot_max) root_lay(k) = nroot_max
	  
	  zeig%coh%nroot=root_lay(k)
	  nr=root_lay(k)
	  
      specn = zeig%coh%species
      if (specn .eq. 2 .or. specn .eq. 3) then
         beta = 0.98
      else
         beta = 0.967
      endif

 
	  frtrel_1        = 1.
      zeig%coh%frtrel = 0.
      do j=1,nr
!       root distribution (Jackson et al., 1996): beta ** depth
         frtrel_j           = beta ** depth(j)
         zeig%coh%frtrel(j) = frtrel_1 - frtrel_j
         frtrel_1           = frtrel_j
      enddo
      frtrel_s = SUM(zeig%coh%frtrel)
      frtrel_s = 1./frtrel_s
      do j=1,nr
 !     scaling of root distribution
         zeig%coh%frtrel(j) = zeig%coh%frtrel(j) * frtrel_s
         root_fr(j)         = root_fr(j) + zeig%coh%frtrel(j) * zeig%coh%ntreeA
      enddo

      zeig%coh%rooteff = 0.   ! zero after use
	  k=k+1
      zeig => zeig%next
   enddo

case (7)    ! Funktion nach Jackson (1996) mit fester Tiefe
   root_fr = 0.
   nr = nroot_max
   zeig => pt%first
   do while (associated(zeig))

      zeig%coh%nroot = nroot_max
      specn = zeig%coh%species
      if (specn .eq. 2 .or. specn .eq. 3) then
         beta = 0.98
      else
         beta = 0.967
      endif

	  frtrel_1        = 1.
      zeig%coh%frtrel = 0.
      do j=1,nr
!       root distribution (Jackson et al., 1996): beta ** depth
         frtrel_j           = beta ** depth(j)
         zeig%coh%frtrel(j) = frtrel_1 - frtrel_j
         frtrel_1           = frtrel_j
      enddo
      frtrel_s = SUM(zeig%coh%frtrel)
      frtrel_s = 1./frtrel_s
      do j=1,nr
 !     scaling of root distribution
         zeig%coh%frtrel(j) = zeig%coh%frtrel(j) * frtrel_s
         root_fr(j)         = root_fr(j) + zeig%coh%frtrel(j) * zeig%coh%ntreeA
      enddo

      zeig%coh%rooteff = 0.   ! zero after use

      zeig => zeig%next
   enddo

end select

root_fr = root_fr / (anz_tree + anz_sveg)   ! normieren
zeig => pt%first
do while (associated(zeig))
    help = zeig%coh%x_frt * zeig%coh%ntreea
    do jlay = 1, nroot_max
        if (root_fr(jlay) .gt. zero) then 
            zeig%coh%frtrelc(jlay) = zeig%coh%frtrel(jlay) * help / (root_fr(jlay) * totfrt_p)  ! mass of root part of total cohort in a layer
        else
            zeig%coh%frtrelc(jlay) = 0.
        endif
    enddo
    zeig => zeig%next
enddo

if (flag_wred .eq. 9) then

   !Calculation of root length density
   zeig => pt%first
   do while (associated(zeig))
        if (specn .le. nspec_tree) then
            radius = (zeig%coh%diam/6.)*100.     ! formula bhd [cm]/6 yield radius in [m] so *100 (aus Wagner 2005)
            valspace = pi * radius**2 * thick
        else
            valspace = kpatchsize * 100*100 * thick
        endif                                                  !circular cylinder 
    
        frtrelcoh = zeig%coh%frtrel * zeig%coh%x_frt * zeig%coh%ntreea          
    
        if (zeig%coh%ntreea .gt. 0 .AND. minval(valspace(1:nr)) .gt. 0.) then
            zeig%coh%rld = (frtrelcoh*1000*spar(specn)%spec_rl*100)/(valspace* zeig%coh%ntreea)   !in cm root length /cm3 volume
	    else
            zeig%coh%rld = -99
        endif
       
      zeig => zeig%next
   enddo
endif

if (allocated(wat_root)) wat_root=0.

END	subroutine root_distr

!**************************************************************

SUBROUTINE root_eff

! Calculation of root efficiency in dependence of water and N uptake
use data_soil
use data_soil_cn
use data_stand

implicit none

integer i,j
integer nr    ! layer number of root depth
real hroot    ! root depth
real fdc      ! discounting function describing transport resistance
real gw, gN   ! accounting functions of water resp. N uptake
real glimit   ! limitation constant for use of rooting layer

glimit = 0.   ! min. assumption

   i = 1
   zeig => pt%first
   do while (associated(zeig))
      nr = zeig%coh%nroot
      do j = 1,nr
         fdc   = 50./depth(j)
         if (zeig%coh%supply .gt. 1e-06) then
            gw = xwatupt(i,j)/zeig%coh%supply
            gw = gw / thick(j)
         else
		    gw = 0.
		 endif

         gw = xwatupt(i,j)     
         zeig%coh%rooteff(j) = zeig%coh%rooteff(j) + gw
      enddo
      zeig%coh%watuptc = zeig%coh%watuptc + zeig%coh%supply
      i = i + 1
      zeig => zeig%next
   enddo

END	subroutine root_eff

!**************************************************************

SUBROUTINE root_depth(rkind, specn, agec, heightc, froot, croot, nr, troot2, crdepth, nrooth)

use data_simul
use data_soil
use data_soil_cn
use data_stand

implicit none

! input:
integer rkind ! kind of calculation of root depth
integer specn ! species number
integer agec  ! tree age
integer nrooth ! for case(7)

real heightc, froot, croot					! tree height of cohort, fine and coarse root mass[kg]/ tree 
real troot, troot1,troot2, troot_stand		! total root mass 1./tree 2./ha according to  cohorts 3. /m� according to cohorts Kohorten 4./ha of 4C
real :: wat_demand							! query whether one cohort was unable to cover water demand with the from root penetrated soil layer 
real rootingdepth, crdepth				    ! rooting depth nach Arora function in [m]
real alpha, b								! parameter for Arorafunction	
! output:
integer nr                                  ! last root layer

integer i,j
real hc, wtiefe
real, dimension(4,3):: rdepth  ! effective rooting depth depending on tree age and soil texture
! data from Raissi et al. (2001)
data rdepth /85, 130, 175, 95, 140, 185, 135, 180, 225, 90, 110, 135/

select case (rkind)

case (1)
 ! nroot depending on tree height and soil profile depth
   nr = 1
   do j=1,nlay
      if (heightc .ge. depth(j)) nr = j
   enddo
   if (nr .gt. nroot_max) nr = nroot_max
   crdepth = depth(nr)

case (2)
 ! fixed nroot for all adult cohorts
   if (agec .lt. 10) then
     nr = 1
     wtiefe=depth(nroot_max)/(1+exp(1.5-0.55*real(agec)))		! logicla function to determin root depth [cm] until age 10	
		do j=1,nlay
			if (wtiefe .ge. depth(j)) nr = j
		enddo
     if (nr .gt. nroot_max) nr = nroot_max
   else
     nr = nroot_max
   endif
    crdepth = depth(nr)

case (3)
 ! nroot depending on root efficiency
   nr = nlay  
   crdepth = depth(nr)

case (4)
 ! nroot depending on soil texture and age
   if (agec .lt. 15) then
      i = 1
   else if (agec .gt. 45) then
      i = 3
   else
      i = 2
   endif

   nr = 1
   if (heightc .gt. rdepth(s_typen,i)) then
      hc = rdepth(s_typen,i)
   else
      hc = heightc
   endif
   do j=1,nlay
      if (hc .ge. depth(j)) nr = j
   enddo
   if (nr .gt. nroot_max) nr = nroot_max

case (5)
	alpha=0.7
	if (specn .eq. 2 .or. specn .eq. 3 .or. specn .eq. 6 .or. specn .eq. 7) then
         b = 7.95
      else
         b = 10.91
      endif
	
	troot=froot+croot
	troot1=troot*anz_tree_ha        ! total root biomass per ha if population of a cohort is soley comprised of trees
	troot_stand=totfrt+totcrt		! total root biomass per ha calculated by 4C
	troot2=troot1/10000				! conversion to m�
	rootingdepth=(3*troot2**alpha)/b	!Arora function
		nr = 1
		do j=1,nlay
			if (rootingdepth*100 .ge. depth(j)) nr = j
		enddo
   if (nr .gt. nroot_max) nr = nroot_max
    crdepth = depth(nr)

case (6)	!Calculation in soil.f in cr_depth

     if (crdepth .eq.0) then
	    ! nroot depending on soil texture and age
       if (agec .lt. 15) then
          i = 1
       else if (agec .gt. 45) then
          i = 3
       else
          i = 2
       endif

       nr = 1
       if (heightc .gt. rdepth(s_typen,i)) then
          crdepth = rdepth(s_typen,i)
       else
          crdepth = heightc
       endif
   
     endif

     do j=1,nlay
	    if (depth(j) .le. crdepth) nr=j
     enddo
     if (nr .gt. nroot_max) nr = nroot_max

case (7)	!further growth only if next layer bears water
	wat_demand=maxval(wat_root)
	if (time .le. 1) then
	 crdepth=30.0
	 do j=1,nlay
	  if (depth(j) .le. 30.) nr=j
	 enddo
	else
      if (wat_demand .gt. 0) then
	   nr=1
	  else
	   nr=0
	  endif
	endif

   if (nr .gt. nroot_max) nr = nroot_max
    crdepth = depth(nr)
end select

if (crdepth < 0.) then
continue
endif

END	subroutine root_depth

!**************************************************************

SUBROUTINE root_ini

! Allocation and initialisation of root distribution

use data_simul
use data_soil
use data_species
use data_stand

implicit none

integer i, j, nj, rkind, hspec, ios
integer unit_root
integer nr        ! aux. var. for nroot (rooting depth)
real    frtrel_j, frtrel_1  ! rel fine root fraction of previous layer
real    frtrel_s  ! Sum of fine root fractions
real hfrt, help, troot2
real, allocatable, dimension(:,:):: hd,hr
integer, allocatable, dimension(:):: nlspec
character text
character (150) file_root

logical :: pruefer=.false.

root_fr = 0.
  if (wlam(3) .gt. 0.4) then
     s_typen = 1     ! sand
  else if (wlam(3) .le. 0.15) then
     s_typen = 4     ! clay
  else if (wlam(3) .gt. 0.25) then
     s_typen = 3     ! silt
  else
     s_typen = 2     ! loam
  endif

  if (nroot_max .lt. 0) then
     nroot_max = 1
     rkind = 4
  else
     rkind = 2
  endif
  rdepth_kind = rkind

select case (flag_wurz)

case (0,1,5)
   if (anz_tree .gt. 0 .or. (anz_tree.eq.0 .and. flag_sveg .eq.1)) call root_distr

case (3,4,6)
    !intercept the case that the ground vegetatuin is already initialised but no trees have been initialised so cohorts are not finalised
    if (anz_tree.eq.0 .and. flag_sveg .eq.1) then
	    if (.not. allocated(wat_root)) then
	     allocate(wat_root(anz_coh))
	     wat_root=0.
	     allocate(root_lay(anz_coh))
         root_lay=0
         allocate(gr_depth(anz_coh))
         gr_depth=0.
	     Pruefer=.true.
	    endif
    else
        if (Pruefer .OR. (.not. allocated(wat_root))) then
	     if (Pruefer) deallocate(wat_root)
	     allocate(wat_root(anz_coh))
	     wat_root=0.
	     if (Pruefer) deallocate(root_lay)
	     allocate(root_lay(anz_coh))
         root_lay=0
         if (Pruefer) deallocate(gr_depth)
	     allocate(gr_depth(anz_coh))
         gr_depth=0.
         Pruefer=.false.
	    endif
    endif
    if (anz_tree .gt. 0 .or. (anz_tree.eq.0 .and. flag_sveg .eq.1)) call root_distr

case (2)
! read root distribution once in the beginning alone
   write (*,*)
   write (*,'(A)', advance='no') 'Define root distribution, name of input file: '
   read (*,'(A)') file_root
   unit_root = getunit()
   open (unit_root, file=trim(file_root), status='unknown')
   allocate (hd(0:40, 1:nspecies))
   allocate (hr(0:40, 1:nspecies))
   allocate (nlspec(nspecies))

   do
       read (unit_root,'(A)') text
       if (text .ne. '!') then
            backspace(unit_root);exit
       endif
   enddo

   ios    = 0
   hd     = 0.
   hr     = 0.
   nlspec = 0
   do while (ios .ge. 0)
       j = 1
       read (unit_root, *, iostat=ios) hspec
       if (ios .lt. 0) exit
       read (unit_root, *, iostat=ios) hd(1,hspec), hr(1,hspec)
       do while (hd(j,hspec) .ge. 0.)
          nlspec(hspec) = j
          j = j+1
          read (unit_root, *, iostat=ios) hd(j,hspec), hr(j,hspec)
       enddo
       if (hd(j,hspec) .lt. depth(nlay)) hd(j,hspec) = depth(nlay)
   enddo
   close (unit_root)

   zeig => pt%first
   do while (associated(zeig))

      ns = zeig%coh%species
      zeig%coh%frtrel = 0.

  ! rel. root distribution of cohorts to species allocated
     if (nlspec(ns) .gt. 0) then

         frtrel_j = 0.
         hfrt = 0.
         j= 1
         do while (hd(j,ns) .lt. depth(1))
            hfrt = hfrt + hr(j,ns)
            j = j+1
         enddo
            frtrel_j           = hr(j,ns) / (hd(j,ns)-hd(j-1,ns))
            hfrt               = hfrt + frtrel_j * (depth(1)-hd(j-1,ns))
            zeig%coh%frtrel(1) = hfrt
            nj = j

         do i=2,nlay
             hfrt = 0.
             do j = nj,nlspec(ns)+1
                if (hd(j,ns) .lt. depth(i)) then
                    frtrel_j = hr(j,ns) / (hd(j,ns)-hd(j-1,ns))
                    hfrt     = hfrt + frtrel_j * (hd(j,ns)-depth(i-1))
                else
                    if (depth(i-1) .gt. hd(j-1,ns)) then
                        help = depth(i)-depth(i-1)
                    else
                        help = depth(i)-hd(j-1,ns)
                    endif
                    frtrel_j = hr(j,ns) / (hd(j,ns)-hd(j-1,ns))
                    hfrt     = hfrt + frtrel_j * help
                    nj = j
                    exit
                endif
             enddo

             zeig%coh%frtrel(i) = hfrt
          enddo
     else

            continue
     endif

      frtrel_s  = SUM(zeig%coh%frtrel)
      zeig%coh%rooteff   = 0.
      zeig => zeig%next
   enddo
   rdepth_kind = 2
end select

END	subroutine root_ini

!**************************************************************

SUBROUTINE dealloc_root

use data_simul
use data_stand

if (flag_wurz .eq. 1) then
   zeig => pt%first
   do while (associated(zeig))

      deallocate (zeig%coh%frtrel)
      deallocate (zeig%coh%rooteff)

      zeig => zeig%next
   enddo
endif

END	subroutine dealloc_root

!**************************************************************

SUBROUTINE rootc_new (zeig1)

! root initialisation of a new cohort

use data_stand
use data_soil

implicit none

type(coh_obj), pointer   :: zeig1     ! pointer variable for cohorts
real troot2
integer j, nr

        allocate (zeig1%coh%frtrel(nlay))
        allocate (zeig1%coh%rooteff(nlay))
        zeig1%coh%frtrel  = 0.      ! initialisation
        call root_depth (1, zeig1%coh%species, zeig1%coh%x_age, zeig1%coh%height, zeig1%coh%x_frt, zeig1%coh%x_crt, nr, troot2, zeig%coh%x_rdpt, zeig%coh%nroot)
        zeig1%coh%nroot = nr
        do j=1,nr
           zeig1%coh%rooteff = 1.   ! assumption for the first use
        enddo
        do j=nr+1, nlay
           zeig1%coh%rooteff = 0.   ! layers with no roots
        enddo

END	subroutine rootc_new

!**************************************************************

SUBROUTINE cr_depth 

! Calculation of the rooting depth after Rasse et al. 2001 

use data_soil
use data_stand
use data_simul
use data_climate
use data_species

implicit none
real :: vcr		! growth rate rootdepth [cm]
integer :: j,k

vcr=0.

select case (flag_wurz)

case(4,6)
zeig => pt%first
 k=1
 do while (associated(zeig))
  	do j=1,nlay
	 if (zeig%coh%x_rdpt .lt. depth(j)) then
	  if (zeig%coh%x_age .le. 100) then
		if (j .eq. 1) then
        vcr=spar(zeig%coh%species)%v_growth*((100-real(zeig%coh%x_age))/100)*svar(zeig%coh%species)%Rstress(j)
	    zeig%coh%x_rdpt=zeig%coh%x_rdpt+(vcr/recs(time))
		gr_depth(k)=zeig%coh%x_rdpt
		exit
	    else
	    vcr=spar(zeig%coh%species)%v_growth*((100-real(zeig%coh%x_age))/100)*svar(zeig%coh%species)%Rstress(j)
	    zeig%coh%x_rdpt=zeig%coh%x_rdpt+(vcr/recs(time))
	    gr_depth(k)=zeig%coh%x_rdpt
		exit
	    endif
	   endif
	  endif
	enddo
  
  if (zeig%coh%x_rdpt .gt. depth(nroot_max)) zeig%coh%x_rdpt = depth(nroot_max)
  k=k+1	  
  zeig => zeig%next
 enddo
end select

END subroutine cr_depth

!*******************************************************************************

