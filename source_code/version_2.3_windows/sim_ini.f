!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!
!*                                                               *!
!*                    Subroutines for:                           *!
!*    - Simulation initialisation (SIM_INI)                      *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

SUBROUTINE sim_ini

use data_biodiv
use data_climate
use data_depo
use data_evapo
use data_inter
use data_manag
use data_simul
use data_site
use data_stand
use data_soil
use data_soil_cn
use data_species
use data_par
use data_frost

implicit none

type(Coh_Obj), pointer :: p  ! pointer to cohort list

anz_sim  = anz_sim + 1
time     = 0
time_cur = time_b - 1    ! before Sim.-Start in year_ini time_cur=time_cur+1
iday     = 0

act_thin_year = 1
flag_cum  = 0
flag_lit  = 0
flag_sens = 0
flag_redn = .FALSE.

lai=0.
gp_can    = 0.
sumbio    = 0.
totfrt    = 0.
sumNPP    = 0.
nppsum    = 0.
gppsum    = 0.
cum_sumNPP= 0.
NEE_mon   = 0.
NPP_mon   = 0.
autresp   = 0.
autresp_m = 0.
anrspec   = 0
anz_coh   = 0
anz_spec  = 0
anz_tree  = 0
med_diam  = 0.
hdom      = 0.
mean_drIndAl = 0.

med_air    = 0.
med_rad    = 0.
med_air_cm = 0.
med_air_wm = 0.
med_air_ms = 0.
med_air_mj = 0.
med_wind   = 0.
temp_mon   = 0.
prec_mon   = 0.
sum_prec   = 0.
sum_prec_ms= 0.;
sum_prec_mj= 0.
gdday = 0.
days_summer = 0
days_hot    = 0
days_ice    = 0
days_dry    = 0
days_hrain  = 0
days_rain   = 0
days_rain_mj= 0
days_snow   = 0
days_wof = 0
gdday_all = 0.
med_air_all  = 0.
sum_prec_all = 0.
med_rad_all = 0.
int_cum_can  = 0.
int_cum_sveg = 0.
interc_m_can = 0.
interc_m_sveg= 0.
perc_cum    = 0.
perc_mon    = 0.
wupt_cum    = 0.
wupt_r_c    = 0.
wupt_e_c    = 0.
tra_tr_cum  = 0.
tra_sv_cum  = 0.
dew_m       = 0.
aet_cum     = 0.
pet_cum     = 0.
pet_m		= 0.
AET_m       = 0.
wupt_r_m    = 0.
perc_m      = 0.
wat_tot     = 0.
gp_can_mean = 0.
gp_can_max  = 0.
snow        = 0.
snow_day    = 0
totFPARcan  = 0.
Rnet_cum    = 0.

! fire index
fire(1)%mean_m = 0
fire(2)%mean_m = 0
fire(3)%mean_m = 0
fire_indb_m    = 0

ind_arid_an = 0.
ind_lang_an = 0.
ind_cout_an = 0.
ind_wiss_an = 0.
ind_mart_an = 0.
ind_mart_vp = 0.
ind_emb		= 0.
ind_weck	= 0.
ind_reich	= 0.
con_gor		= 0.
con_cur		= 0.
con_con		= 0.
cwb_an		= 0.
cwb_an_m    = 0.
ind_bud     = 0.
ind_shc     = 0.

ind_arid_an_m = 0.
ind_lang_an_m = 0.
ind_cout_an_m = 0.
ind_wiss_an_m = 0.
ind_mart_an_m = 0.
ind_mart_vp_m = 0.
ind_emb_m     = 0.
ind_weck_m    = 0.
ind_reich_m   = 0.
con_gor_m     = 0.
con_cur_m     = 0.
con_con_m     = 0.
ind_bud_m     = 0.
ind_shc_m     = 0.
ntindex       = 0.

tempmean_mo = 0

aet_dec  = 0.
temp_dec = 0.
prec_dec = 0.
rad_dec  = 0.
hum_dec  = 0.

! frost index
if(flag_climtyp .ge. 3) then
 ! calculation for airtemp_min > -90. 
    tminmay=0
    lfind=0
    dlfabs=0.
    tminmay_sp=0
    dlfabs_sp=0.
    flag_tveg=0
else
    tminmay=-99
    lfind=-99
    dlfabs=-99.
    tminmay_sp=-99
    dlfabs_sp=-99.
    flag_tveg=-99
endif


!! initialisation of root distribution
RedN_mean = 0.
anz_RedN  = 0
N_min     = 0.
N_min_m   = 0.
resps_c   = 0.
resps_c_m = 0.
resps_mon = 0.
N_tot     = 0.
N_an_tot  = 0.
N_hum_tot = 0.
C_tot     = 0.
C_hum_tot = 0.
N_lit     = 0.
C_lit     = 0.
Nupt_c    = 0.
Nupt_m    = 0.
C_accu      = 0.
Nleach_c    = 0.
Nleach_m    = 0.
N_lit_m     = 0.
C_lit_m     = 0.
totfol_lit  = 0.
totfol_lit_tree = 0.
totfrt_lit  = 0.
totfrt_lit_tree = 0.
tottb_lit   = 0.
totcrt_lit  = 0.
totstem_lit = 0.
C_opm_fol   = 0.
C_opm_frt   = 0.
C_opm_crt   = 0.
C_opm_tb    = 0.
C_opm_stem  = 0.
N_opm_stem  = 0.
N_opm_fol   = 0.
N_opm_frt   = 0.
N_opm_crt   = 0.
N_opm_tb    = 0.
Ndep_cum    = 0.
Ndep_cum_all= 0.
if (flag_multi .ne. 8) then
    if ((flag_multi .ne. 2) .or. (ip .le. 1)) then
        NOdep(ip) = 0.
        NHdep(ip) = 0.
    endif
endif

flag_bc = 0
totsteminc   = 0.
cumsteminc   = 0.
cumsumvsdead = 0.
cumsumvsab   = 0.
sumvsdead    = 0.
sumvsab      = 0.

p => pt%first
do while (associated(p))
  p%coh%N_pool  = 0.

  p => p%next
enddo  ! p (cohorts)

allocate(dayfract(ns_pro))

!  fields for frost index
allocate(dnlf(year))
allocate(tminmay_ann(year))
allocate(date_lf(year))
allocate(date_lftot(year))
allocate(dnlf_sp(year))
allocate (anzdlf(year))
allocate (sumtlf(year))

dnlf_sp=0
dnlf = 0
tminmay_ann = 0.
date_lf = 0
date_lftot = 0
anzdlf = 0.
sumtlf = 0.

end subroutine sim_ini