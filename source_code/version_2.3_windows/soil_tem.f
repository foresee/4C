!*****************************************************************!
!*                                                               *!
!*            4C (FORESEE) Simulation Model                      *!
!*                                                               *!
!*                                                               *!
!*                   Subroutines for:                            *!
!*                SOIL-Temperature - Programs                    *!
!*                                                               *!
!*                  Author: F. Suckow                            *!
!*                                                               *!
!*   contains:                                                   *!
!*   SOIL_TEMP main program for soil temperature                 *!
!*   S_T_INI   initialisation of soil temperature model          *!
!*   S_T_STRT  initialisation of geometry parameter for the      *!
!*            numerical solution of the heat conduction equation *!
!*   SURF_T    calculation of the soil surface temperature       *!
!*   COND      calculation of conductivity parameters            *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!
    
SUBROUTINE soil_temp

! soil temperature model

use data_simul
use data_climate
use data_soil
use data_soil_t
use data_out

implicit none

integer i

! Surface temperature
call surf_t

      if (flag_dayout .eq. 3) then
         write (3334,*) 
         write (3334,*)    iday 
      endif   

! Calculation of thermal conductivity and capacity
do i=1,nlay
   call cond(i,wats(i),dens(i),thick(i),pv_v(i),sandv(i),clayv(i),siltv(i),skelv(i),vol(i),spheat(i),t_cond(i),h_cap(i))
enddo   ! i (nlay)
call cond(nlay, wats(nlay),dens(nlay),sh(nlay1),pv_v(nlay),sandv(nlay),clayv(nlay),siltv(nlay),skelv(nlay),vol(nlay),spheat(nlay),t_cond(nlay1),h_cap(nlay1))
call cond(nlay, wats(nlay),dens(nlay),sh(nlay2),pv_v(nlay),sandv(nlay),clayv(nlay),siltv(nlay),skelv(nlay),vol(nlay),spheat(nlay),t_cond(nlay2),h_cap(nlay2))

! Calculation of thermal diffusivity
t_cb(1) = t_cond(1)
do i=2,nlay2
   t_cb(i) = (sh(i-1)*t_cond(i-1) + sh(i)*t_cond(i))/(sh(i)+sh(i-1))
enddo  

  if (flag_dayout .eq. 4) then
    do i=1,nlay
        write (3336,'(3I4, 5E11.4)') time,iday,i,watvol(i),dens(i),spheat(i),t_cond(i),h_cap(i)
    enddo    
    write (3336, *) 
  endif

! Numerical solution of the heat conduction equation
call num_t

lfirst = .FALSE.
! Restore of temperature
do i=1,nlay
   if (abs(sbt(i)) .lt. 1e-6) sbt(i)=0.
   temps(i) = sbt(i)
enddo

! soil heat flux at soil surface 
hflux_surf = 2. * t_cond(1) * (temps_surf - temps(1)) / thick(1)
 
1010  FORMAT (2I5, 20F8.1)
END  subroutine soil_temp
    
!******************************************************************************

SUBROUTINE s_t_ini

! Initialisation of soil temperature model

use data_simul
use data_soil
use data_soil_t

implicit none

integer i
real, external:: kw
real tc_cont    ! thermal conductivity of continuum 

! Preparation of subroutine cond
! Parameter initialisation
water%tc = 0.005945  ! thermal conductivity of water at 20�C  J/cm/s/K
quarz%tc = 0.0879228 ! thermal conductivity of quarz at 20�C
humus%tc = 0.00251   ! thermal conductivity of humus
clay%tc  = 0.0251208 ! thermal conductivity of clay minerals
silt%tc  = 0.02931   ! thermal conductivity of silt
air%tc   = 0.00026   ! thermal conductivity of air
ice%tc   = 0.021771  ! thermal conductivity of ice
stone%tc = 0.041868  ! thermal conductivity of stone
water%hc = 4.1868    ! heat capacity of water   J/cm3/K
quarz%hc = 2.01      ! heat capacity of quarz
humus%hc = 2.512     ! heat capacity of humus
clay%hc  = 2.01      ! heat capacity of clay minerals
silt%hc  = 2.01      ! heat capacity of silt
air%hc   = 0.0012    ! heat capacity of air
ice%hc   = 1.884     ! heat capacity of ice
stone%hc = 1.8       ! heat capacity of stone

! shape factors
quarz%ga = 0.144   ! de Vries, S. 224
clay%ga  = 0.144
silt%ga  = 0.144
stone%ga = 0.144
humus%ga = 0.333
air%ga   = 0.333
ice%ga   = 0.125

! weighting factors for dry soil (continuous medium air)
tc_cont   = air%tc
water%kwa = kw(water, tc_cont)    
quarz%kwa = kw(quarz, tc_cont)
clay%kwa  = kw(clay, tc_cont)
silt%kwa  = kw(silt, tc_cont)
humus%kwa = kw(humus, tc_cont)
ice%kwa   = kw(ice, tc_cont)
stone%kwa = kw(stone, tc_cont)
air%kwa   = 1

! weighting factors for wet soil (continuous medium water)
tc_cont   = water%tc
water%kww = 1    
quarz%kww = kw(quarz, tc_cont)
clay%kww  = kw(clay, tc_cont)
silt%kww  = kw(silt, tc_cont)
humus%kww = kw(humus, tc_cont)
ice%kww   = kw(ice, tc_cont)
stone%kww = kw(stone, tc_cont)
air%kww   = kw(air, tc_cont)

if (flag_dayout .eq. 3) then
    write (3335, '(A)') 'wet soil'
    write (3335,'(6E11.4)') water%kww, air%kww, humus%kww, quarz%kww, clay%kww,ice%kww
    write (3335, '(A)') 'dry soil'
    write (3335,'(6E11.4)') water%kwa, air%kwa, humus%kwa, quarz%kwa, clay%kwa,ice%kwa
endif

! Calculation of thermal diffusivity
do i=1,nlay
   call cond(i,wats(i),dens(i),thick(i),pv_v(i),sandv(i),clayv(i),siltv(i),skelv(i),vol(i),spheat(i),t_cond(i),h_cap(i))
enddo
call s_t_prof

call s_t_strt   

! Calculation of thermal diffusivity (additional layers)
call cond(nlay, wats(nlay),dens(nlay),sh(nlay1),pv_v(nlay),sandv(nlay),clayv(nlay),siltv(nlay),skelv(nlay),vol(nlay),spheat(nlay),t_cond(nlay1),h_cap(nlay1))
call cond(nlay, wats(nlay),dens(nlay),sh(nlay2),pv_v(nlay),sandv(nlay),clayv(nlay),siltv(nlay),skelv(nlay),vol(nlay),spheat(nlay),t_cond(nlay2),h_cap(nlay2))

t_cb(1) = t_cond(1)
do i=2,nlay2
   t_cb(i) = (sh(i-1)*t_cond(i-1) + sh(i)*t_cond(i))/(sh(i)+sh(i-1))
enddo   

END  subroutine s_t_ini
 
!******************************************************************************

SUBROUTINE s_t_strt

! Initialisation of geometry parameter for the
! numerical solution of the heat conduction equation

use data_soil
use data_soil_t

implicit none

integer i
real h_0, h_1
real :: ntau = 1.  ! potential time step 

lfirst = .TRUE.

nlay1 = nlay+1
nlay2 = nlay+2

   sh(1)  = thick(1)
   sb(1)  = 2. / sh(1)

sv(mfirst)  = sh(mfirst)
sbt(mfirst) = temps_surf

do i=mfirst+1,nlay
   sbt(i) = temps(i)
   sh(i)  = thick(i)
enddo

sbt(nlay1) = temps(nlay)
sbt(nlay2) = temps(nlay)
sh(nlay1)  = 2. * thick(nlay)
sh(nlay2)  = 100.

h_0        = sh(1)
do i= mfirst+1, nlay2
   h_1   = sh(i)
   sb(i) = 2. / (h_1 + h_0)
   sv(i) = h_1 * ntau
   h_0   = h_1
enddo
END  subroutine s_t_strt
 
!******************************************************************************

SUBROUTINE surf_t

! Calculation of soil surface temperature
use data_climate
use data_simul
use data_soil
use data_soil_t
use data_stand

implicit none

real day
real cof         ! daily correction cefficient
real dampcof     ! stand damping coefficient
real helplai     ! thermal conductivity of organic layer (global vereinbaren und vom Vortag merken!!!)
integer unit_tmp, helptyp
character(80) text

! read surface temperature; Oberflaechentemperatur einlesen 
if (flag_surf .eq. 2) then
   if (lfirst) then
      write (*,'(A)', advance='no') 'Reading of soil surface temperature, please type file name:'
      read (*,'(A)') text
      unit_tmp = getunit()
      open (unit_tmp, file=trim(text), status='unknown')
      read (unit_tmp,'(A)') text
      read (unit_tmp, *) day, temps_surf
      return
   else
      read (unit_tmp, *) day, temps_surf
      return
   endif 
endif

! snow
if (snow .lt. 0.05) then     ! calculation of temps_surf in subroutine snowpack

    dampcof = 1.0

    if (waldtyp .ge. 110 .and. (waldtyp .ne. 125)) then
        helptyp = 110
    else
        helptyp = waldtyp
    endif
    select case (helptyp)

    case (10,20,25,30,31,35,37,38,70,71,75,76,125)    ! Spruce; Fichte
        if (iday .lt. 90 .or. (iday .gt. 320)) then
              dampcof=0.8
          else if (iday .lt. 115) then
              dampcof=1.0
          else if (iday .gt. 240) then
              dampcof=1.0
          else
              dampcof=0.7
        endif

    case (40,50,51,52,54,55,56,60,61,62,64,65,66,90,100)    ! Pine; Kiefer
        if (iday .lt. 90 .or. (iday .gt. 320)) then
              dampcof=1.5
          else if (iday .lt. 115) then 
              dampcof=1.2
          else if (iday .gt. 285) then
              dampcof=1.3
          else
              dampcof=0.8
          endif

    case (110)   ! Beech and other decidous trees; Buche und andere Laubhoelzer
       if (LAI .gt. 1.) then
        if (iday .gt. 50) then
          if (iday .lt. 100 .or. (iday .gt. 300 .and. iday .lt. 345)) then
              dampcof=1.2
          else if (iday .gt. 130 .and. iday .le. 300) then     ! for beech; fuer Buche
                  dampcof=0.8   ! for beech; fuer Buche
          endif
        endif
       else
         dampcof=1.2   ! for beech; fuer Buche
       endif 

    end select
 
!     Daempfung berechnen nach Paul et al. (2004)
        day = iday
       cof = abs(-0.00003*day*day + 0.0118*day - 0.0703)
    if (flag_surf .eq. 0) then
        temps_surf = (c0*airtemp + c1*airtemp_1 + c2*airtemp_2) * cof * dampcof
        temps(1) = temps_surf
    else
        if (flag_surf .eq. 3)  then
            cof = 1
            dampcof = 1.0
        endif
        temps_surf = (c0*airtemp + c1*airtemp_1 + c2*airtemp_2) * cof * dampcof
    endif

endif  !  snow


 if (flag_dayout .eq. 3) then
    write (1222,'(A,I5,F10.4,3F8.2)') 'day, cof, dampcof', iday, cof, dampcof, temps_surf, airtemp
 endif

END  subroutine surf_t
 
!******************************************************************************

SUBROUTINE cond(ilay,watsi,densi,thicki,pvi,sandi,clayi,silti,skelvi,voli,spheati,tcondi,hcapi)

! Calculation of thermal conductivity and capacity
! de Vries-approach

use data_par
use data_soil
use data_soil_cn
use data_soil_t
use data_simul

implicit none

! input
integer  ilay   ! number of layer
real watsi      ! water content mm
real densi      ! soil density
real thicki     ! layer thickness
real spheati    ! specific heat capacity 
real dmi        ! dry mass  g/m2
real pvi        ! pore volume
real quarzi     ! quarz fraction in soild soil
real sandi      ! sand fraction in soild soil
real clayi      ! clay fraction in soild soil
real silti      ! silt fraction in soild soil
real skelvi     ! skeleton fraction in soil
real tc_cont    ! thermal conductivity of continuum 
real wcvol      ! water content (vol%)

! output
real tcondi, tcond0, tcond1, tcond2, tcond3 ! thermal conductivity
real hcapi, hcap0, hcap1, hcap2, hcap3      ! thermal capacity

real numera, denom  ! numerator, denominator of calculation of thermal conductivity 
real hum_dens, densi1, pvi1, hvf, hvf1
real aa, bb, cc, dd, vfm, vfs, massfr   ! Campbell-Ansatz
real wkw,akw,hkw,qkw,ckw,skw,ikw,tkw, skel, voli, restvol

! density   g/cm3
hum_dens   = 1.3     !Density of humus (compressed, without air)
quarzi = sandi  
  ! dry mass
   dmi  = voli * densi 
   voli = thicki * 10000.  
   hvf  = (C_opm(ilay) + C_hum(ilay)) / cpart    ! Masse (g)

  ! volume fractions
   skel     = 1. - skelvi
   pvi1     = skel * pvi/100.
   water%vf = skel * watsi/(10.*thicki)
   air%vf   = pvi1 - water%vf
   if (air%vf .lt. 0.) then
   continue
   endif
       hvf      = hvf / hum_dens        ! volume; Volumen 
       restvol  = voli - (skelvi + pvi1)*voli - hvf 
       humus%vf = hvf / voli
       quarz%vf = quarzi*restvol / voli
       clay%vf  = clayi*restvol / voli
       silt%vf  = silti*restvol / voli
       stone%vf = skelvi 
   ice%vf   = 0.

      if (flag_dayout .ge. 3) then
         write (3334,'(3I4,F8.3,8F10.4)') time,iday,ilay,pvi1, water%vf, air%vf,humus%vf,quarz%vf,clay%vf,silt%vf,stone%vf,ice%vf
         if (ilay .eq. nlay) write (3334, *) 
      endif   

select CASE (flag_cond)

CASE (1, 11, 21, 31, 41)    ! Neusypina 
   if (densi .lt. 0.6) then
      densi1 = 0.6
   else
      densi1 = densi
   endif
   wcvol   = watsi/(10.*thicki)
   tcondi = ((3.*densi1-1.7)*0.001)/(1.+(11.5-5.*densi1)    &
		     *EXP((-50.)*(wcvol/densi1)**1.5))*86400.   
   tcondi = tcondi * 4.1868     ! convertation cal/(cm s K) in J/(cm s K)

  ! heat capacity  J/(cm3 K)
   hcapi  = densi1*spheati + wcvol*4.1868
   hcap1  = hcapi
   tcond1 = tcondi

CASE (0, 10, 20, 30, 40)   ! de Vries

  ! Determination of continuous medium   

   if (watsi .gt. 0.95 * pv(ilay)) then
     ! wet soil
       wkw = water%kww     
       akw = air%kww
       hkw = humus%kww
       qkw = quarz%kww
       ckw = clay%kww
       skw = silt%kww
       tkw = stone%kww
       ikw = ice%kww
   else
     ! dry soil
       wkw = water%kwa     
       akw = air%kwa
       hkw = humus%kwa
       qkw = quarz%kwa
       ckw = clay%kwa
       skw = silt%kwa
       tkw = stone%kwa
       ikw = ice%kwa
   endif

     numera = wkw * water%vf * water%tc + qkw * quarz%vf * quarz%tc + ckw * clay%vf * clay%tc +  &
            skw * silt%vf * silt%tc + hkw * humus%vf * humus%tc + akw * air%vf * air%tc + &
            tkw * stone%vf * stone%tc + ikw * ice%vf * ice%tc
     denom  = wkw * water%vf + qkw * quarz%vf + ckw * clay%vf + skw * silt%vf +   &
            hkw * humus%vf + akw * air%vf + tkw * stone%vf + ikw * ice%vf

   tcond0 = numera/denom * 86400.   ! s --> day

CASE(3, 13, 23, 33, 43)    ! Campbell
    vfm = clay%vf + silt%vf + stone%vf
    vfs = vfm + quarz%vf + humus%vf
    if (watsi .gt. 0.95 * pv(ilay)) then
     ! wet soil
        aa = 0.57 + 1.73*quarz%vf + 0.93*vfm
        aa = aa / (1. - 0.74*quarz%vf - 0.49*vfm) - 2.8*vfs*(1.-vfs) 
        bb = 2.8 * vfs * water%vf
        tcond3 = (aa + bb * water%vf)  ! W/m/K
    else if (watsi .le. wilt_p(ilay)) then
     ! dry soil
        tcond3 = 0.03 + 0.7 * vfs * vfs  ! W/m/K
    else
        massfr = 2.65 * (vfm + quarz%vf) + 1.3 * humus%vf 
        massfr = 2.65 * clay%vf / massfr
        aa = 0.57 + 1.73*quarz%vf + 0.93*vfm
        aa = aa / (1. - 0.74*quarz%vf - 0.49*vfm) - 2.8*vfs*(1.-vfs) 
        bb = 2.8 * vfs * water%vf
        cc = 1. + 2.6 * sqrt(clay%vf)
        dd = 0.03 + 0.7 * vfs * vfs
        tcond3 = aa + bb*water%vf - (aa-dd) * exp(-(cc*water%vf)**4)  ! W/m/K       
    endif 
    tcond3 = tcond3  / 100.    ! W/m/K ==> J/(cm s K) 
    tcond3 = tcond3  * 86400.  ! s --> day
end select

  ! heat capacity  J/(cm3 K)
   hcap0  = water%vf * water%hc + quarz%vf * quarz%hc + clay%vf * clay%hc + silt%vf * silt%hc +    &
            humus%vf * humus%hc + air%vf * air%hc + stone%vf * stone%hc + ice%vf * ice%hc

  if (flag_dayout .eq. 4) then
    write (3337,'(3I4, 6E11.4)') time,iday,ilay,tcond0,tcond1,tcond2,tcond3,hcap0,hcap1
    if (ilay .eq. nlay) write (3337, *) 
  endif

select CASE (flag_cond)

CASE (0, 10, 20, 30, 40)    ! de Vries
    hcapi  = hcap0
    tcondi = tcond0 

CASE (1, 11, 21, 31, 41)    ! Neusypina
    hcapi  = hcap1
    tcondi = tcond1 

CASE (3, 13, 23, 33, 43)    ! Campbell
    hcapi  = hcap0
    tcondi = tcond3
end select

END  subroutine cond

!**************************************************************
real FUNCTION kw(part, tc_cont)

! Function for calculating weighting factor k
! in calculating thermal conductivity

use data_soil_t
implicit none

type (therm_par):: part   ! soil fraction (particles)
real tc_cont              ! thermal conductivity of continuum 
real term, ga

   term  = part%tc / tc_cont -1.
   ga    = part%ga
   kw    = (2./(1.+ term*ga) + 1./(1.+ term*(1.-2.*ga)))/3.

end  FUNCTION
 
!******************************************************************************

SUBROUTINE num_t

! Numerical solution of the heat conduction equation

use data_soil
use data_soil_t
use data_simul

implicit none

integer i
logical lcase  ! logical control of Cholesky procedure
real hflux     ! heat flux at surface (right side)

lcase = .TRUE.

! Determination of the volume matrix
svv = sv * h_cap
if (lfirst) svva = svv

! Determination (side diagonal; Nebendiagonale) !
do i=1,nlay2
   son(i) = -sb(i) * t_cb(i)
enddo 
son(nlay2+1) = 0.0

! Determination (main diagonal; Hauptdiagonale) !
do i=1,nlay2
   soh(i) = svv(i) - son(i) - son(i+1)
enddo 

hflux = temps_surf * sb(1) * t_cb(1)  ! Set heat flux at surface at right side

if (.not.lfirst) then
   ! Calculation of the right side
   do i=1,nlay2
      sxx(i) = (svva(i) + (svv(i)-svva(i))/sh(i)) * sbt(i)
   enddo
   sxx(1) = sxx(1) + hflux
   
   ! Iteration (Cholesky procedure)
  call chl3 (nlay2, son, soh, sxx, lcase)
  
   ! Results of iteration on temperature help array 
   sbt = sxx
endif   ! lfirst      

! Restore of geometry matrix
svva = svv
END  subroutine num_t
 
!******************************************************************************

SUBROUTINE chl3 (n, a, b, x, lcase)

! Solution of EX = Z (E - tridiagonal, symmetric matrix)
! with Cholesky procedure (E = LDL')

implicit none

! input
integer n   ! rang of matrix
logical lcase  ! logical control of Cholesky procedure
               ! .TRUE. for start of iteration 
real, dimension(n)  :: a, &  ! Nebendiagonale
					   b     ! main diagonal

! output
real, dimension(n)  :: x     ! solution vector

! local variables
integer i, j, j1
real, dimension(n)  :: d, ul
 
! Calculation of the left upper triangle matrix L
! and of the diagonal matrix D at start of iteration
if (lcase) then
   d(1) = b(1)
   do i=2,n
      ul(i) = a(i) / d(i-1)
      d(i)  = b(i) - ul(i)*a(i)
   enddo
   lcase = .FALSE.
endif

! Solution of LY = Z
do i=2,n
   x(i) = x(i) - ul(i)*x(i-1)
enddo 

! Solution of L'X = D(-1)Y
x(n) = x(n) / d(n)
do i=1,n-1
   j  = n-i
   j1 = j+1
   x(j) = x(j)/d(j) - ul(j1)*x(j1)
enddo
 
END  subroutine chl3
 
!******************************************************************************

