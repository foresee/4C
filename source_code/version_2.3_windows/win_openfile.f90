! ********************************************************************
! *                                                                  *
! *  Copyright 2000 Compaq Computer Corporation                      *
! *                                                                  *
! *  COMPAQ Registered in U.S. Patent and Trademark Office.          *
! *                                                                  *
! *  Confidential computer software. Valid license from Compaq or    *
! *  authorized sublicensor required for possession, use or copying. *
! *  Consistent with FAR 12.211 and 12.212, Commercial Computer      *
! *  Software, Computer Software Documentation, and Technical Data   *
! *  for Commercial Items are licensed to the U.S. Government under  *
! *  vendor's standard commercial license.                           *
! *                                                                  *
! ********************************************************************
!
!DEC$ IF .NOT. DEFINED (COMDLG32_ )
!DEC$ DEFINE COMDLG32_  
!
!
!
!************This version of comdlg32 contains new items****************** 
! Whether new interfaces for routines in COMDLG32.LIB extracted from
! VC++6 header files are included is controlled by a statement with the
! following format towards the end of this file.
! 
!IF .NOT. DEFINED(__DO_NOT_INCLUDE_VC6_ITEMS)
!
!Unless the inclusion is explicitly turned off by defining the
!above symbol, the items will be included. 
!
module comdlg32
use dfwinty
!DEC$OBJCOMMENT LIB:"COMDLG32.LIB"
!
!                       *****COMMDLG******
interface !lib=comdlg32.lib
logical(4) function  GetOpenFileName (dummy ) 
!DEC$ ATTRIBUTES DEFAULT :: GetOpenFileName
!DEC$ IF DEFINED(_X86_)
!DEC$ ATTRIBUTES STDCALL, ALIAS:'_GetOpenFileNameA@4' :: GetOpenFileName
!DEC$ ELSE
!DEC$ ATTRIBUTES STDCALL, ALIAS: 'GetOpenFileNameA'   :: GetOpenFileName
!DEC$ ENDIF
!DEC$ ATTRIBUTES REFERENCE :: dummy
use dfwinty
type(T_OPENFILENAME)  dummy
end function GetOpenFileName
end interface
interface !lib=comdlg32.lib
logical(4) function  GetSaveFileName (dummy ) 
!DEC$ ATTRIBUTES DEFAULT :: GetSaveFileName
!DEC$ IF DEFINED(_X86_)
!DEC$ ATTRIBUTES STDCALL, ALIAS:'_GetSaveFileNameA@4' :: GetSaveFileName
!DEC$ ELSE
!DEC$ ATTRIBUTES STDCALL, ALIAS: 'GetSaveFileNameA'   :: GetSaveFileName
!DEC$ ENDIF
!DEC$ ATTRIBUTES REFERENCE :: dummy
use dfwinty
type(T_OPENFILENAME)    dummy
end function GetSaveFileName
end interface
interface !lib=comdlg32.lib
integer*2 function  GetFileTitle (dummya ,dummyb ,dummyc ) 
!DEC$ ATTRIBUTES DEFAULT :: GetFileTitle
!DEC$ IF DEFINED(_X86_)
!DEC$ ATTRIBUTES STDCALL, ALIAS:'_GetFileTitleA@12' :: GetFileTitle
!DEC$ ELSE
!DEC$ ATTRIBUTES STDCALL, ALIAS: 'GetFileTitleA'    :: GetFileTitle
!DEC$ ENDIF
!DEC$ ATTRIBUTES REFERENCE :: dummya
!DEC$ ATTRIBUTES REFERENCE :: dummyb
character*(*)   dummya
character*(*)   dummyb
integer*2       dummyc
end function GetFileTitle
end interface
interface !lib=comdlg32.lib
logical(4) function  ChooseColor (dummy ) 
!DEC$ ATTRIBUTES DEFAULT :: ChooseColor
!DEC$ IF DEFINED(_X86_)
!DEC$ ATTRIBUTES STDCALL, ALIAS:'_ChooseColorA@4' :: ChooseColor
!DEC$ ELSE
!DEC$ ATTRIBUTES STDCALL, ALIAS: 'ChooseColorA'   :: ChooseColor
!DEC$ ENDIF
!DEC$ ATTRIBUTES REFERENCE :: dummy
use dfwinty
type(T_CHOOSECOLOR) dummy
end function ChooseColor
end interface
interface !lib=comdlg32.lib
integer*4 function  FindText (dummy ) 
!DEC$ ATTRIBUTES DEFAULT :: FindText
!DEC$ IF DEFINED(_X86_)
!DEC$ ATTRIBUTES STDCALL, ALIAS:'_FindTextA@4' :: FindText
!DEC$ ELSE
!DEC$ ATTRIBUTES STDCALL, ALIAS: 'FindTextA'   :: FindText
!DEC$ ENDIF
!DEC$ ATTRIBUTES REFERENCE :: dummy
use dfwinty
type(T_FINDREPLACE) dummy
end function FindText
end interface
interface !lib=comdlg32.lib
integer*4 function  ReplaceText (dummy ) 
!DEC$ ATTRIBUTES DEFAULT :: ReplaceText
!DEC$ IF DEFINED(_X86_)
!DEC$ ATTRIBUTES STDCALL, ALIAS:'_ReplaceTextA@4' :: ReplaceText
!DEC$ ELSE
!DEC$ ATTRIBUTES STDCALL, ALIAS: 'ReplaceTextA'   :: ReplaceText
!DEC$ ENDIF
!DEC$ ATTRIBUTES REFERENCE :: dummy
use dfwinty
type(T_FINDREPLACE) dummy
end function ReplaceText
end interface
interface !lib=comdlg32.lib
logical(4) function  ChooseFont (dummy ) 
!DEC$ ATTRIBUTES DEFAULT :: ChooseFont
!DEC$ IF DEFINED(_X86_)
!DEC$ ATTRIBUTES STDCALL, ALIAS:'_ChooseFontA@4' :: ChooseFont
!DEC$ ELSE
!DEC$ ATTRIBUTES STDCALL, ALIAS: 'ChooseFontA'   :: ChooseFont
!DEC$ ENDIF
!DEC$ ATTRIBUTES REFERENCE :: dummy
use dfwinty
type(T_CHOOSEFONT) dummy
end function ChooseFont
end interface
interface !lib=comdlg32.lib
logical(4) function  PrintDlg (dummy) 
!DEC$ ATTRIBUTES DEFAULT :: PrintDlg
!DEC$ IF DEFINED(_X86_)
!DEC$ ATTRIBUTES STDCALL, ALIAS:'_PrintDlgA@4' :: PrintDlg
!DEC$ ELSE
!DEC$ ATTRIBUTES STDCALL, ALIAS: 'PrintDlgA'   :: PrintDlg
!DEC$ ENDIF
!DEC$ ATTRIBUTES REFERENCE :: dummy
use dfwinty
type(T_PRINTDLG)    dummy
end function PrintDlg
end interface
interface !lib=comdlg32.lib
integer*4 function  CommDlgExtendedError () 
!DEC$ ATTRIBUTES DEFAULT :: CommDlgExtendedError
!DEC$ IF DEFINED(_X86_)
!DEC$ ATTRIBUTES STDCALL, ALIAS:'_CommDlgExtendedError@0' :: CommDlgExtendedError
!DEC$ ELSE
!DEC$ ATTRIBUTES STDCALL, ALIAS: 'CommDlgExtendedError'   :: CommDlgExtendedError
!DEC$ ENDIF
end function CommDlgExtendedError
end interface
!
!DEC$ IF .NOT. DEFINED(__DO_NOT_INCLUDE_VC6_ITEMS)
!
INTERFACE 
FUNCTION PageSetupDlg( &
        arg1)
USE DFWINTY
  integer(BOOL) :: PageSetupDlg ! BOOL
    !DEC$ ATTRIBUTES DEFAULT :: PageSetupDlg
    !DEC$IF DEFINED(_X86_)
    !DEC$ ATTRIBUTES STDCALL, ALIAS:'_PageSetupDlgA@4' :: PageSetupDlg
    !DEC$ ELSE
    !DEC$ ATTRIBUTES STDCALL, ALIAS:'PageSetupDlgA' :: PageSetupDlg
    !DEC$ ENDIF
!DEC$ ATTRIBUTES REFERENCE :: arg1
  TYPE (T_PAGESETUPDLGA) arg1 ! LPPAGESETUPDLGA arg1
 END FUNCTION
END INTERFACE
!
!DEC$ ENDIF ! /* __DO_NOT_INCLUDE_VC6_ITEMS */
!
end module comdlg32
!
!DEC$ ENDIF ! /* COMDLG32_ */
