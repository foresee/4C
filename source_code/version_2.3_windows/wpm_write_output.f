!*****************************************************************!
!*                                                               *!
!*              4C (FORESEE) Simulation Model                    *!
!*                                                               *!  
!*                                                               *!
!*              Post Processing: output files WPM                *!
!*                                                               *!
!*    Subroutines for:                                           *!
!*    - write_wpm_output                                         *!
!*    - write_product_lines                                      *!
!*    - write_spinup                                 		     *!
!*                                                               *!
!*                  Copyright (C) 1996-2018                      *!
!*     Potsdam Institute for Climate Impact Reserach (PIK)       *!
!*          Authors and contributors see AUTHOR file             *!
!*  This file is part of 4C and is licensed under BSD-2-Clause   *!
!*                   See LICENSE file or under:                  *!
!*     http://www.https://opensource.org/licenses/BSD-2-Clause   *!
!*                           Contact:                            *!
!*       https://gitlab.pik-potsdam.de/foresee/4C                *!
!*                                                               *!
!*****************************************************************!

subroutine write_wpm_output()

use data_simul

use data_wpm

character(150) outputFile
integer i, unit, ios

	unit = getunit()

	! Headers to output files
	outputFile = trim(dirout) // 'wpm_output.out' // trim(anh)

	open(unit,	FILE=trim(outputFile),	STATUS='unknown')
	! Headers to output files
	! prodyht.dat
	open(unit,	FILE=trim(outputFile),	STATUS='unknown')
	write(unit,'(A50)') '# Carbon in different products, Gg C'
	write(unit,'(A50)') '# use categories'
	write(unit,'(11(A20))') ' ','build','other','struct','furni','pack','long','short','','land	'
	write(unit,'(11(A20))') 'year',' mat.','build','support','mat','mat','paper','paper','burn','fill','atmo'
	
	! how many years?
	! write
	do i=1,size(years)
		write(unit,	'(I9,10(F20.3) )',iostat=ios)	&
						years(i),					&
						use_categories(1)%value(i),	&
						use_categories(2)%value(i),	&
						use_categories(3)%value(i),	&
						use_categories(4)%value(i),	&
						use_categories(5)%value(i),	&
						use_categories(6)%value(i),	&
						use_categories(7)%value(i),	&
						burning(i),					&
						landfill(i),				&
						atmo_cum(i)
	end do

end subroutine write_wpm_output

!***************************************************************************
subroutine write_product_lines(outputFile)

use data_simul

use data_wpm

character(150) outputFile
integer i, unit, ios

	unit = getunit()

	! Headers to output files
	! prodyht.dat
	open(unit,	FILE=trim(outputFile),	STATUS='unknown')
	write(unit,'(A50)') '# Carbon in different products, Gg C'
	write(unit,'(A50)') '# product lines'
	write(unit,'(7(A15))') 'year',' 1','2','3','5','6','7'
	
	! how many years?
	! write
	do i=1,size(management_years)
		write(unit,	'(I9,7(F15.3) )',iostat=ios)	&
						management_years(i),		&
						product_lines(1)%value(i),	&
						product_lines(2)%value(i),	&
						product_lines(3)%value(i),	&
						product_lines(4)%value(i),	&
						product_lines(5)%value(i),	&
						product_lines(6)%value(i),	&
						product_lines(7)%value(i)
	end do

end subroutine write_product_lines

!*****************************************************************************
subroutine write_spinup(outputFile)

use data_simul

use data_wpm

character(150) outputFile
integer i, j, unit, ios
integer max
real, dimension(nr_use_cat + 1) :: spinny


	unit = getunit()

	! Headers to output files
	! prodyht.dat
	open(unit,	FILE=trim(outputFile),	STATUS='unknown')
	write(unit,'(A50)') '# Carbon in different products, Gg C'
	write(unit,'(A30)') '# use categories'
	write(unit,'(9(A15))') 'year','1','2','3','4','5','6','7','landfill'
	
	! how many years?
	! write
	max = max_age(1)
	do j = 1,nr_use_cat
		if ( max < max_age(j) ) max = max_age(j)
	end do

	! write for max age every value, fill not existing values with 0
	do i=1,max
		spinny(8) = 0.
		do j = 1,nr_use_cat
			if ( i <= max_age(j) ) then
				spinny(j) = use_categories(j)%spinup(i)
			else
				spinny(j) = 0.
			end if
		end do
		if (i == 1) then
			spinny(8) = landfill_spinup
		end if	
		write(unit,	'(I9,8(F15.3) )',iostat=ios)		&
						i,								&
						spinny(1),	&
						spinny(2),	&
						spinny(3),	&
						spinny(4),	&
						spinny(5),	&
						spinny(6),	&
						spinny(7),	&
						spinny(8)

	end do

end subroutine write_spinup


