!  4C Species Parameter File
! contains the following data:
! with * marked parameters are sensitive
!
! nspecies    number of all species
! nspec_tree  number of tree species
! name        species name
! Amax        maximum age (years)
! yrec        stress recovery time (years)
! stol        shade tolerance (1-5)
! pfext       extinction coefficient
! sigman*     root activity rate
! respcoeff   respiration coeff. = fraction of gross production respired by the plant
! prg*        growth respiration parameter
! prms*       maintenance respiration parameters: sapwood,
! prmr                                         fine roots;
! psf*        senescence parameters: foliage,
! pss*                               sapwood,
! psr*                               fine roots;
! pcnr*       N/C ratio of biomass       [kg N/ kg C]
! ncon_fol    N concentration of folmg / g TM nicht / g C!!?
! ncon_frt    N concentration of fine roots              [mg/g]
! ncon_crt    N concentration of coarse roots            [mg/g]
! ncon_tbc    N concentration of twigs and branches      [mg/g]
! ncon_stem   N concentration of stemwood                [mg/g]
! reallo_fol  reallocation parameter of foliage
! reallo_frt  reallocation parameter of fine root
! alphac      ratio of coarse wood (twigs, branches, roots) to sapwood
! cr_frac     fraction of tbc (twigs, branches, roots) that is coarse roots
! prhos*      sapwood density  [kg/m³}
! pnus        foliage mass to sapwood area ratio [kg/cm**2] (pipe model)
! pha         height growth parameter [cm/kg]
! pha_coeff1  height growth parameter coefficient 1
! pha_coeff2  height growth parameter coefficient 2
! pha_v1      height growth parameter 1 for non linear foliage height relationship
! pha_v2      height growth parameter 2 for non linear foliage height relationship
! pha_v3      height growth parameter 3 for non linear foliage height relationship
! crown_a     parameter for crown radius - DBH relation [m/cm]
! crown_b     parameter for crown radius - DBH relation [m]
! crown_c     parameter for crown radius - DBH relation [m]
! psla_min*   specific one-sided leaf area [m2/kg DW]
! psla_a
! phic*       efficiency parameter, different for everg/decid [-]
! pnc         N content (to be replaced by plant N model!)
! kco2_25     Michaelis constant of temperature (base 25 °C) [Pa]
! ko2_25      inhibition constant of O2 [kPa]  (eqn 20)
! pc_25*      CO2/O2 specificity value (base 25 °C)
! q10_kco2    Q10 values (acclimated to 25 °C ???)
! q10_ko2
! q10_pc*
! pb          Rd to Vm ratio
!PItmin   PIM: Inhibitor min temp.
!PItopt   PIM: Inhibitor opt temp.
!PItmax   PIM: Inhibitor max temp.
!PIa      PIM: Inhibitor scaling factor
!PPtmin   PIM: Promotor min temp.
!PPtopt   PIM: Promotor opt temp.
!PPtmax   PIM: Promotor max temp.
!PPa      PIM: Promotor scaling factor
!PPb      PIM: Promotor scaling factor
!CSTbC    CSM: chilling base temp.
!CSTbT    CSM: base temp.
!CSa      CSM: scaling factor
!CSb      CSM: scaling factor
!LTbT     TSM: base temp.
!LTcrit   TSM: critical temperature sum
!Lstart   TSM: start day after 1.11.
!Phmodel  used pheno model 0: no model, 1: PIM, 2: CSM, 3: TSM
! end_bb      leaf shedding/ end of vegetation period
! fpar_mod    modifying parameter in canopy_geometry
! ceppot_spec interception capacity parameter
! Nresp       slope of photosynthesis response to N-limitation
! regflag     flag for regeneration
! seedrate    max. seeddling rate /m2
! seedmass    mass of a single seed [g]
! seedsd      standard deviation of seed mass [g]
! seeda       parameter of allometric relation shoot biomass - foliage bm
! seedb        ---------"-----------
! pheight1    parameter of all. relation shoot biomass - height of seedling
! pheight2    ------------------" -----------
! pheight3
! pdiam1      parameter of all. relation shoot biomass - diameter at shoot basis
! pdiam2      ----------"-----------------
! pdiam3      ----------"--------------
! k_opm_fol
! k_syn_fol
! k_opm_stem
! k_syn_stem
! k_opm_tb
! k_syn_tb
! k_opm_frt
! k_syn_frt
! k_opm_crt
! k_syn_crt
!
nspecies          15
nspec_tree        13
!***************************************************************************************************************************************************************
beech     'Fagus    sylvatica'
spruce    'Picea    abies'
pine      'Pinus    sylvestris'
oak       'Quercus  robur'
birch     'Betula   pendula'
pinec     'Pinus    contorta'
pinep     'Pinus    ponderosa'
aspen     'Populus  tremula'
pineh     'Pinus    halepensis'
dougfir   'Pseudotsuga menziesii'
robinia   'Robinia pseudoacacia'
eglobulus 'Eucalyptus globulus'
egrandis  'Eucalyptus grandis'
grveg     'Ground vegetation'
mistletoe 'Viscum alba'
!***************************************************************************************************************************************************************
name      'beech'   'spruce'  'pine'    'oak'     'birch'   'pinec'   'pinep'   'aspen'   'pineh'   'dougfir' 'robinia' 'eglobulus' 'egrandis' 'grveg'  'mistletoe'
amax             430       930       760      1060       220       600       850       160       250       930       200        50        50         5            30
yrec               3         3         3         3         3         3         3         3         3         3         3         3         3         3             3
stol               5         4         1         2         1         1         1         2         1         4         2         1         1         4             1
pfext            0.4       0.6       0.6       0.5      0.54       0.6       0.5     0.872       0.6       0.3      0.46       0.5       0.5       0.5           0.5
sigman         0.032      0.05      0.03      0.03       1.1      0.03      0.03   0.07508      0.03     0.025  0.176172     0.025      0.75         1           -99
respcoeff        0.5      0.52      0.52       0.5       0.5      0.71      0.44       0.5      0.52       0.5       0.5      0.53      0.53       0.5           0.5
prg             0.25      0.25       0.2      0.25      0.25       0.2       0.2      0.25      0.32      0.25       -99      0.25      0.25      0.25          0.25
prms          0.0035   0.00043   0.00024    0.0035    0.0035  0.000032   0.00024    0.0014   0.00024       -99       -99       0.3       0.3    0.0035           -99
prmr          0.0133    0.0033     0.007      0.01      0.01   0.00338    0.0007    0.0034     0.007       -99    0.1938       -99       -99      0.01           -99
psf                1     0.181      0.31         1         1     0.167      0.24         1       0.5       0.2         1      0.33      0.33         1         0.166
pss            0.026     0.022      0.04      0.05      0.02     0.023      0.05      0.04      0.04      0.05      0.25      0.03      0.03       0.4           -99
psr             0.65       0.5       0.5       0.5       0.5       0.4       0.8       0.5     1.095      0.75      0.67         1         1      0.75           -99
pcnr           0.008    0.0052    0.0079     0.008      0.02    0.0079    0.0212     0.008    0.0079   0.00955    0.0502     0.005     0.005       0.2           -99
ncon_fol       26.01     13.36     13.46        25        25      13.4      10.6        25     13.46     15.22    33.623        12        12      13.4           -99
ncon_frt        7.15     10.77      7.44      8.94       8.9       7.4      7.44        16      7.44      3.67     23.55       9.6       9.6       7.4           -99
ncon_crt        3.03      4.14      1.77      3.71       3.7       1.7      1.77         4      1.77      1.62    17.168       9.6       9.6       1.7           -99
ncon_tbc        4.27      5.24      3.61      6.19       5.4       3.6      3.61         6      3.61      3.62     17.16       3.8       3.8       3.6           -99
ncon_stem       1.54      1.22      1.09       2.1       1.7       1.1      1.09       0.6      1.09     1.035    15.345         1         1       1.1           -99
reallo_fol       0.1       0.1       0.1       0.1       0.1       0.1       0.1       0.6       0.1       0.1       0.1       0.5       0.5       0.1           0.1
reallo_frt       0.1       0.1       0.1       0.1       0.1       0.1       0.1       0.1       0.1       0.1       0.1      0.55      0.55       0.1           -99
alphac          0.48       0.5      0.46      0.56       0.5      0.24      0.46      0.46       0.5      0.54      0.51      0.22      0.22       0.5           -99
cr_frac          0.5       0.6       0.6      0.55      0.42       0.5       0.6     0.328       0.1      0.54      0.86      0.52      0.52       0.5           -99
prhos        0.00065   0.00042  0.000403   0.00056 0.0005365   0.00042  0.000434  0.000402   0.00062  0.000405   0.00078    0.0005    0.0005       -99           -99
pnus            0.03     0.096      0.05      0.02     0.025     0.046     0.022      0.02     0.043     0.093      0.05      0.04      0.06      0.03           -99
pha              125        40       190       100       150       190       190       150       190        40       -99       -99       -99       -99           -99
pha_coeff1       0.5   0.66666   0.66666       0.5       0.5   0.66666   0.66666    1.0823   0.66666   0.66666       -99       -99       -99       -99           -99
pha_coeff2       0.5   0.33333   0.33333       0.5       0.5   0.33333   0.33333    0.3189   0.33333   0.33333       -99       -99       -99       -99           -99
pha_v1        1089.3     284.8       206     946.7       900      1100       206       900       210       750       500       840       854       -99           -99
pha_v2        0.1351   -0.0151   0.03177     0.299      0.38       0.3   0.03177      0.08      0.08    -0.015    0.8541  -0.02973      -0.2       -99           -99
pha_v3         0.504    0.5039     0.877     0.948       0.9    0.4184     0.877       0.6       0.6      0.35    0.4404  0.291323   0.29277       -99           -99
crown_a      0.09571   0.06383   0.05213     0.095    0.0896    0.1437   0.05213      0.06     0.074  0.081287    0.0776    0.1249    0.1249       -99           -99
crown_b      0.57732   0.33567   0.48139       0.5    0.5716    0.1081   0.48139       0.4     0.255  0.355485    0.7368    0.7879    0.7879       -99           -99
crown_c           15        12        10        15         6        10        10         6        10         5         4       5.9       5.9       -99           -99
psla_min          12      3.78         4        14        10       7.5       3.1       7.2      4.84      2.82      13.2        15      0.18        20          4.18
psla_a            12       2.4         1       4.7        20       1.2         1       4.4         1      4.87      19.5     13.52     13.52        20          4.18
phic               1       0.8       0.9         1         1       0.9       0.9         1       0.9       0.8         1         1         1         1           0.8
pnc               20        20        20        20      17.9        50        20        24        20        45    33.623       -99       -99        10           -99
kco2_25           30        30        30        30        30        30        30        30      40.4        30        30        30        30        30            30
ko2_25            30        30        60        30        30        60        60        60      24.8        30        30        30        30        30            30
pc_25           3400      2600      3400      3400      3400      3400      3400      3400      3400      2600      3400      3400      3400      3400          3400
Q10_kco2         2.1       2.1       2.1       2.1       2.1       2.1       2.1       2.1       2.1       2.1       2.1       2.1       2.1       2.1           2.1
Q10_ko2          1.2       1.2       1.2       1.2       1.2       1.2       1.2       1.2       1.2       1.2       1.2       1.2       1.2       1.2           1.2
Q10_pc          0.57      0.57      0.57      0.57      0.57      0.57      0.57      0.57      0.57      0.57      0.57      0.57      0.57      0.57          0.57
pb              0.01     0.015      0.01      0.01     0.035      0.01      0.01    0.0143    0.0241     0.015      0.01     0.035     0.035      0.01         0.015
PItmin        -10.34       -99       -99    -23.05    -24.96       -99       -99    -10.34       -99       -99       -99       -99       -99       -99           -99
PItopt         -0.89       -99       -99      -0.3       -10       -99       -99     -0.89       -99       -99       -99       -99       -99       -99           -99
PItmax         18.11       -99       -99     16.91     15.05       -99       -99     18.11       -99       -99       -99       -99       -99       -99           -99
PIa         0.058326       -99       -99  0.055149  0.030619       -99       -99  0.058326       -99       -99       -99       -99       -99       -99           -99
PPtmin        -10.03       -99       -99      3.46     -7.03       -99       -99    -10.03       -99       -99       -99       -99       -99       -99           -99
PPtopt         28.61       -99       -99     34.55      21.8       -99       -99     28.61       -99       -99       -99       -99       -99       -99           -99
PPtmax         44.49       -99       -99     34.55     25.35       -99       -99     44.49       -99       -99       -99       -99       -99       -99           -99
PPa         0.109494       -99       -99  0.331253  0.064803       -99       -99  0.109494       -99       -99       -99       -99       -99       -99           -99
PPb         0.039178       -99       -99  0.010379  0.045432       -99       -99  0.039178       -99       -99       -99       -99       -99       -99           -99
CSTbC          11.03       -99       -99     18.31     18.42       -99       -99     11.03       -99       -99       -99       -99       -99       -99           -99
CSTbT            6.5       -99       -99      5.58      4.19       -99       -99       6.5       -99       -99       -99       -99       -99       -99           -99
CSa          3039.76       -99       -99   3451.18   3075.99       -99       -99   3039.76       -99       -99       -99       -99       -99       -99           -99
CSb          -574.49       -99       -99    -631.5   -573.67       -99       -99   -574.49       -99       -99       -99       -99       -99       -99           -99
LTbT           -6.98       -99       -99      0.49     -6.07       -99       -99     -6.98       -99       -99      11.5       -99       -99       -99           -99
LTcrit        664.88       -99       -99    372.49     672.9       -99       -99    664.88       -99       -99     350.9       -99       -99       -99           -99
Lstart            70       -99       -99        70        47       -99       -99        70       -99       -99        70       -99       -99       -99           -99
Phmodel            1         0         0         1         1         0         0         1         0         0         3       -99       -99         0             0
end_bb           282       366       366       287       278       366       366       282       366       366       282       366       366       302           366
fpar_mod           0         0         0         0         0         0         0         0         0         0         0         0         0         0             0
ceppot_spe       0.6       0.8       0.9       0.5       0.5       0.4       0.4       0.3       0.9       0.8       0.5       0.3       0.3       0.5           0.6
Nresp         0.0068    0.0068    0.0062    0.0068    0.0068    0.0062    0.0062    0.0068    0.0062    0.0068    0.0068    0.0068    0.0068     0.006        0.0068
regflag            1         0         0         0         0         0         0         0         0         0         0         0         0         0             0
seedrate           1         1         1         1         1         1         1         1       1.5         1         1         1         1       -99           -99
seedmass       0.225     0.009     0.006       3.8    0.0002     0.005    0.0055   0.00016     0.019      0.01    0.0228       -99       -99       -99           -99
seedsd             0         0 0.0011268         0         0 0.0011268 0.0011268         0 0.0011268         0         0       -99       -99       -99           -99
seeda          0.398    0.8524    0.3927    0.2505     0.313    0.2609    0.3927     0.381    0.5044    1.3217     0.778       -99       -99       -99           -99
seedb          0.947     1.007    0.6786    0.7232       0.8     0.757    0.6786    0.8365    0.6486    1.1288    0.9645       -99       -99       -99           -99
pheight1       2.388     0.443     2.512    1.6947     4.651       1.5     2.512    0.2829    1.3145    5.7278  0.118272       -99       -99       -99           -99
pheight2       0.366      0.23     0.357    0.3896    0.3333    0.7236     0.357      0.57    0.3426    0.2443  0.757755       -99       -99       -99           -99
pheight3           0    0.0134         0         0         0       -99         0       -99         0       -99       -99       -99       -99       -99           -99
pdiam1        -1.637    -1.658         0         0         0       -99         0       -99         0       -99       -99       -99       -99      0.35           -99
pdiam2         0.433     0.386         0         0         0       -99         0       -99         0       -99       -99       -99       -99      0.35           -99
pdiam3       -0.0126         0         0         0         0       -99         0       -99         0       -99       -99       -99       -99       0.3           -99
k_opm_fol       0.02      0.06      0.05     0.015      0.01     0.005     0.015      0.04     0.002      0.08       0.3       0.5       0.5      0.03           0.01
k_syn_fol        0.3       0.2       0.1       0.4      0.78       0.6       0.8       0.4       0.5       0.2       0.2       0.01      0.01     0.2            0.5
k_opm_frt       0.02      0.05      0.05      0.01   0.00106     0.001     0.009      0.03    0.0004      0.05     0.008       0.001     0.001    0.001          -99
k_syn_frt        0.4       0.1       0.1       0.3       0.3       0.3       0.3       0.4       0.5       0.3       0.3       0.01      0.01     0.3            -99
k_opm_crt     0.0009    0.0009    0.0009    0.0009    0.0009    0.0009    0.0009    0.0009    0.0009    0.0009    0.0009    0.0009    0.0009         0           -99
k_syn_crt        0.1       0.1         0       0.1       0.1       0.1         0       0.1         0       0.1       0.1       0.1       0.1       0.1           -99
k_opm_tb       0.006     0.006     0.006     0.006    0.0009    0.0009    0.0009    0.0009     0.006     0.006    0.0009    0.0009    0.0009         0           -99
k_syn_tb         0.5       0.8       0.5       0.5       0.8       0.5       0.5       0.5       0.5       0.8       0.8       0.8       0.8         0           -99
k_opm_stem    0.0025    0.0005    0.0005    0.0005    0.0005    0.0001   0.00055    0.0001    0.0005    0.0005    0.0005    0.0005    0.0005         0           -99
k_syn_stem       0.1       0.1         0       0.1       0.1       0.1         0       0.1         0       0.1       0.1       0.1       0.1         0           -99
spec_rl           30        15        15        30        30        15        15        30        15        15        30        30        30        60           -99
tbase              5         5         5         5         5         5         5         5         5         5         5         5         5         5           -99
topt            19.4      16.5      16.5        22      16.5      16.5      16.5      16.5      16.5      16.5      16.5      16.5      16.5      16.5           -99
bdmax_coef      1.55      1.62      1.62      1.55      1.55      1.62      1.62      1.55      1.62      1.62      1.55      1.55      1.55      1.62           -99
porcrit_co       0.4       0.7       0.7       0.4       0.4       0.7       0.7       0.4       0.7       0.7       0.4       0.4       0.4       0.7           -99
ph_opt_max      8.25      5.75      5.75      8.25      8.25      5.75      5.75      8.25      5.75      5.75      8.25      8.25      8.25      8.25           -99
ph_opt_min      4.25      3.75      3.75      4.25      3.75      3.75      3.75      4.25      3.75      3.75      3.75      3.75      3.75       3.5           -99
ph_max          9.25      7.75      7.75      9.25      9.25      7.75      7.75      9.25      7.75      7.75      9.25      9.25      9.25      9.25           -99
ph_min          3.75      2.25      2.25      3.75      2.25      2.25      2.25      3.75      2.25      2.25      2.25      2.25      2.25         2           -99
v_growth           1         1         1         1         1         1         1         1         1         1         1         1         1         5           -99


